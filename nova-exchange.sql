-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6337
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table nova_exch.main_settings
CREATE TABLE IF NOT EXISTS `main_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_message` varchar(5000) DEFAULT NULL,
  `market_min_bet` int(11) DEFAULT 0,
  `market_max_bet` int(11) DEFAULT 0,
  `session_min_bet` int(11) DEFAULT 0,
  `session_max_bet` int(11) DEFAULT 0,
  `bookmaker_min_bet` int(11) DEFAULT 0,
  `bookmaker_max_bet` int(11) DEFAULT 0,
  `market_commission` int(11) DEFAULT 0,
  `session_commission` int(11) DEFAULT 0,
  `bookmaker_commission` int(11) DEFAULT 0,
  `maket_delay` int(11) DEFAULT 0,
  `session_delay` int(11) DEFAULT 0,
  `bookmaker_delay` int(11) DEFAULT 0,
  `logo` varchar(255) DEFAULT NULL,
  `match_stack` varchar(1000) DEFAULT NULL COMMENT 'store stack values in comma seperated exa.(500,1000,2000)',
  `is_maintenance` enum('1','0') DEFAULT '0',
  `is_show_change_password_popup` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table nova_exch.main_settings: 1 rows
/*!40000 ALTER TABLE `main_settings` DISABLE KEYS */;
INSERT INTO `main_settings` (`id`, `site_message`, `market_min_bet`, `market_max_bet`, `session_min_bet`, `session_max_bet`, `bookmaker_min_bet`, `bookmaker_max_bet`, `market_commission`, `session_commission`, `bookmaker_commission`, `maket_delay`, `session_delay`, `bookmaker_delay`, `logo`, `match_stack`, `is_maintenance`, `is_show_change_password_popup`) VALUES
	(1, 'welcome to betting', 10000, 0, 100, 10000, 0, 0, 3, 3, 2, 2, 2, 2, 'logo.png', '100,200,500,1000,1500,5000,10000,20000,50000,100000,2500000', '0', '0');
/*!40000 ALTER TABLE `main_settings` ENABLE KEYS */;

-- Dumping structure for table nova_exch.markets
CREATE TABLE IF NOT EXISTS `markets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sport_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `series_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `match_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `market_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `runner_json` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_result_declared` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  `is_abandoned` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  `is_active` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '1',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_market_match` (`match_id`,`market_id`),
  KEY `unique_index` (`market_id`),
  KEY `sport_id` (`sport_id`),
  KEY `series_id` (`series_id`),
  KEY `match_id` (`match_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table nova_exch.markets: ~0 rows (approximately)
/*!40000 ALTER TABLE `markets` DISABLE KEYS */;
/*!40000 ALTER TABLE `markets` ENABLE KEYS */;

-- Dumping structure for table nova_exch.matches
CREATE TABLE IF NOT EXISTS `matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sport_id` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `series_id` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `match_id` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `match_date` datetime DEFAULT NULL,
  `liability_type` enum('0','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '0',
  `is_resulted` enum('0','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '0',
  `is_active` enum('0','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `match_id` (`match_id`),
  KEY `sport_id` (`sport_id`),
  KEY `series_id` (`series_id`),
  KEY `match_date` (`match_date`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table nova_exch.matches: ~0 rows (approximately)
/*!40000 ALTER TABLE `matches` DISABLE KEYS */;
/*!40000 ALTER TABLE `matches` ENABLE KEYS */;

-- Dumping structure for table nova_exch.series
CREATE TABLE IF NOT EXISTS `series` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sport_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `series_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` enum('0','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `series_id` (`series_id`),
  KEY `sport_id` (`sport_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table nova_exch.series: ~0 rows (approximately)
/*!40000 ALTER TABLE `series` DISABLE KEYS */;
/*!40000 ALTER TABLE `series` ENABLE KEYS */;

-- Dumping structure for table nova_exch.sports
CREATE TABLE IF NOT EXISTS `sports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sport_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_live_sport` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  `is_active` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '1',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `sport_id` (`sport_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table nova_exch.sports: ~0 rows (approximately)
/*!40000 ALTER TABLE `sports` DISABLE KEYS */;
/*!40000 ALTER TABLE `sports` ENABLE KEYS */;

-- Dumping structure for table nova_exch.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) DEFAULT NULL COMMENT '1=Admin, 2=User',
  `parent_id` int(10) DEFAULT NULL,
  `parent_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_ids` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `exposure` decimal(16,2) NOT NULL DEFAULT 0.00,
  `profit_loss` decimal(16,2) NOT NULL DEFAULT 0.00,
  `available_chip` decimal(16,2) NOT NULL DEFAULT 0.00,
  `total_chip` decimal(16,2) NOT NULL DEFAULT 0.00,
  `credited_chip` decimal(16,2) NOT NULL DEFAULT 0.00,
  `partnership` int(3) NOT NULL DEFAULT 0,
  `market_delay` int(3) NOT NULL DEFAULT 0,
  `session_delay` int(3) NOT NULL DEFAULT 0,
  `bookmaker_delay` int(3) NOT NULL DEFAULT 0,
  `market_commission` int(3) NOT NULL DEFAULT 0,
  `session_commission` int(3) NOT NULL DEFAULT 0,
  `bookmaker_commission` int(3) NOT NULL DEFAULT 0,
  `password_changed` enum('0','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `parent_id` (`parent_id`),
  KEY `user_type_id` (`user_type_id`),
  KEY `name` (`name`),
  KEY `parent_ids` (`parent_ids`(768))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table nova_exch.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `user_type_id`, `parent_id`, `parent_name`, `parent_ids`, `user_name`, `name`, `password`, `city`, `mobile`, `exposure`, `profit_loss`, `available_chip`, `total_chip`, `credited_chip`, `partnership`, `market_delay`, `session_delay`, `bookmaker_delay`, `market_commission`, `session_commission`, `bookmaker_commission`, `password_changed`, `created_at`, `updated_at`) VALUES
	(1, 1, 0, NULL, NULL, 'admin', 'Admin', '$2a$08$PKRaYGKTejlZqeMT6W0bieZBIvC/qNrg9gvZ5K.0ru7PJqeah2V7e', 'Delhi', '987456123', 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, '0', '2022-02-06 10:18:58', '2022-02-06 10:19:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
