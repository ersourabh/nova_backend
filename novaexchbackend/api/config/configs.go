package config

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"github.com/go-redis/redis"

	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/yaml.v3"
)

// Client URLS
const (
	OnlineSportURL                   = "http://142.93.36.1/api/v1/fetch_data?Action=listEventTypes"
	OnlineSeriesURL                  = "http://142.93.36.1/api/v1/fetch_data?Action=listCompetitions&EventTypeID="
	OnlineMatchURL                   = "http://142.93.36.1/api/v1/fetch_data?Action=listEvents&EventTypeID=matchid&CompetitionID="
	OnlineMarketURL                  = "http://142.93.36.1/api/v1/fetch_data?Action=listMarketTypes&EventID="
	OnlineSessionURL                 = "http://142.93.36.1/api/v1/listMarketBookSession?match_id="
	OnlineBookmakerURL               = "http://46.101.9.108/api/v1/fetch_data?Action=listBookmakerMarket&EventID="
	OnlineMarketsRunnersURL          = "http://142.93.36.1/api/v1/fetch_data?Action=listMarketRunner&MarketID="
	OnlineBookmakerMarketsRunnersURL = "http://46.101.9.108/api/v1/fetch_data?Action=listBookmakerMarketRunner&MarketID="
	ONLINEMARKETODDSURL              = "http://142.93.36.1/api/v1/listMarketBookOdds?market_id="
	ONLINEBOOKMAKERMARKETODDSURL     = "http://46.101.9.108/api/v1/listBookmakerMarketOdds?market_id="
	OnlineScoreMatchesURL            = "http://167.99.198.2/api/matches/list"
	OnlineMatchScoreURL              = "http://167.99.198.2/api/matches/score/"
)

// Error Messages

var (
	AccessErrorMessage  = "Invalid Access"
	InvalidJsonMessage  = "JSON not supported"
	SuccessMessage      = "Success"
	RecordsEmptyMessage = "Records not found"
	RedisMarketKey      = "MARKET_"
	RedisSessionKey     = "SESSION_"
	// RedisBookmakerKey   = "BOOKMAKER_"
	PageLimit   = 50
	USER        = 9
	AGENT       = 8
	SUPERAGENT  = 7
	MASTER      = 5
	SUPERMASTER = 4
	SUBADMIN    = 3
	ADMIN       = 2
	SUPERADMIN  = 1
)

// Get Credentials from config file
var Config Credentials

type Credentials struct {
	SqlPort       string `yaml:"sqlPort"`
	SqlHost       string `yaml:"sqlHost"`
	SqlUsername   string `yaml:"sqlUsername"`
	SqlPassword   string `yaml:"sqlPassword"`
	SqlDBName     string `yaml:"sqlDBName"`
	RedisPort     string `yaml:"redisPort"`
	RedisHost     string `yaml:"redisHost"`
	RedisUsername string `yaml:"redisUsername"`
	RedisPassword string `yaml:"redisPassword"`
}

//MySQL :
var MySQL *sql.DB

//Redis
var Redis *redis.Client

// InitMySQLDB initialises the database pools with
func InitMySQLDB() (dB *sql.DB, err error) {
	var sqlDBHost, sqlDBPort, sqlDBUser, sqlDBPass, sqlDBName string

	sqlDBHost = Config.SqlHost     //"45.79.126.34"
	sqlDBPort = Config.SqlPort     //"3306"
	sqlDBUser = Config.SqlUsername //"dbuser"
	sqlDBPass = Config.SqlPassword //"p2BKmzDgsA"
	sqlDBName = Config.SqlDBName   //"brexchangedm"

	MySQL, err = sql.Open("mysql", sqlDBUser+":"+sqlDBPass+"@tcp("+sqlDBHost+":"+sqlDBPort+")/"+sqlDBName)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	if err = MySQL.Ping(); err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	fmt.Println("MySQL connected ...")
	return MySQL, nil
}

func InitRedisDB() {
	Redis = redis.NewClient(&redis.Options{
		Addr:     Config.RedisHost + ":" + Config.RedisPort, //"127.0.0.1:6379",
		Password: "",                                        // no password set
		DB:       0,                                         // use default DB
	})

	_, err := Redis.Ping().Result()
	if err != nil {
		log.Panic(err.Error())
	}
	fmt.Println("Redis connected ...")
}

func ReadCredentials(filename string) {
	creds, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Panic("Cannot read credentials from config file " + filename + " : " + err.Error())
	}

	err = yaml.Unmarshal(creds, &Config)
	if err != nil {
		log.Panic("Cannot read credentials from config file " + filename + " : " + err.Error())
	}
}

//Set on local server
func RedisSet(key string, value string) bool {
	err := Redis.Set(key, value, 0).Err()
	if err != nil {
		return false
	}
	return true
}

//Get from local server
func RedisGet(key string) string {
	val, err := Redis.Get(key).Result()
	if err != nil {
		return ""
	}
	// if err == redis.Nil {
	// 	return ""
	// }

	return val
}

//Set on local server
func RedisDelete(key string) bool {
	err := Redis.Del(key).Err()
	if err != nil {
		return false
	}
	return true
}

// RedisSetExp is a wrapper for the "SET" function for Redis cache, with an option to set expiry.
func RedisSetExp(key string, value interface{}, expiration time.Duration) bool {
	val, _ := json.Marshal(value)
	c := Redis.Set(key, val, expiration)
	i, err := c.Result()
	if err != nil {
		return false
	} else if i == "" || i != "OK" {
		return false
	}
	return true
}

// RedisKeyExist is a wrapper for the "EXISTS" function for Redis cache.
func RedisKeyExist(key string) bool {
	c := Redis.Exists(key)
	i, err := c.Result()
	if err != nil {
		return false
	} else if i == 0 {
		return false
	}
	return true
}

// hash
func SetValueInHashRedis(hashkey, key string, value interface{}) {
	Redis.HSet(hashkey, key, value)
}

func GetValuesFromHashRedis(hashkey string) map[string]string {
	val, _ := Redis.HGetAll(hashkey).Result()
	return val
}

func DeleteValueFromHashRedis(hashkey, key string) {
	Redis.HDel(hashkey, key)
}

// normal key val
func SetValuesInRedis(keyValues map[string]interface{}) {
	Redis.MSet(keyValues, 0)
}

func GetValuesFromRedis(keys []string) []interface{} {
	val, _ := Redis.MGet(keys...).Result()
	return val
}

func DeleteValuesFromRedis(key []string) {
	Redis.Del(key...)
}

func SetObjectInRedis(key string, value map[string]interface{}) {
	p, err := json.Marshal(value)
	if err == nil {
		Redis.Set(key, string(p), 0)
	}
}

func GetObjectsFromRedis(keys []string) []map[string]interface{} {
	dest := []map[string]interface{}{}
	values, err := Redis.MGet(keys...).Result()
	if err == nil {
		temp := map[string]interface{}{}
		for _, value := range values {
			if value != nil {
				temp = map[string]interface{}{}
				json.Unmarshal([]byte(value.(string)), &temp)
				dest = append(dest, temp)
			}
		}
	}
	return dest
}

func RedisGetAllLoginUser(key string) []string {
	val, err := Redis.Keys(key + "*").Result()
	if err == nil {
		userArr := make([]string, len(val))
		for i := 0; i < len(val); i++ {
			userArr[i], _ = Redis.Get(val[i]).Result()
		}
		return userArr
	} else {
		log.Println("RedisGetAllLoginUser", err.Error())
		userArr := make([]string, 0)
		return userArr
	}
}
