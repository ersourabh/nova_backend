package controllers

import (
	"encoding/json"
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"novaexchbackend/api/services"
	"novaexchbackend/api/util"

	"github.com/gin-gonic/gin"
)

/*
Method:POST
{
 user_id 			(number - Required)
 description 		(string - Optional)
 amount 			(number - Required)
 cr_de 				(number - Required - values: 1/2, 1=Credit, 2=Debit)
 password 			(string - Required)
 master_password 	(string - Required)
}
*/
var DepositWithdrawl = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		var account models.AccountStatement
		err := json.NewDecoder(c.Request.Body).Decode(&account)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		if account.UserId == 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "User Id Is Required")
			return
		}
		if account.Amount <= 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Amount Is Required")
			return
		}
		if account.CrDe != 1 && account.CrDe != 2 {
			util.ErrorResponse(c, http.StatusOK, nil, "cr_de Is Required")
			return
		}
		if tokenData.UserTypeId != 2 && account.UserId == tokenData.UserId {
			util.ErrorResponse(c, http.StatusOK, nil, "Self Credit/Debit Not Allowed")
			return
		}
		if account.MasterPassword == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Transaction Password Is Required")
			return
		}
		msg, status := services.GetTransactionPassword(tokenData.UserId, account.MasterPassword)
		if !status {
			util.ErrorResponse(c, http.StatusOK, nil, msg)
			return
		} else {

			userData, status := services.GetUserDetailsByUserId(account.UserId)
			if !status {
				account.StatementType = 1
				account.ParentId = userData.ParentId
				account.OperatorName = tokenData.UserName
				account.UserName = userData.UserName
				if account.Amount > userData.AvailableChip && account.CrDe == 2 {
					util.ErrorResponse(c, http.StatusOK, nil, "Insufficient Credit Limit")
					return
				} else {
					var parentData models.User
					if userData.UserTypeId != 1 {
						parentData, _ = services.GetUserBalance(account.ParentId)
						if parentData.UserTypeId != 1 {
							if account.Amount > parentData.AvailableChip && account.CrDe == 1 {
								util.ErrorResponse(c, http.StatusOK, nil, "Insufficient Credit Limit")
								return
							}
						}
					}
					message, status := services.DepositWithdrawl(account, userData, parentData)
					if status {
						util.SuccessResponse(c, http.StatusOK, nil, message)
						return
					} else {
						util.ErrorResponse(c, http.StatusOK, nil, message)
						return
					}
				}
			} else {
				util.ErrorResponse(c, http.StatusOK, nil, "Invalid User")
				return
			}
		}
	}
}

/*
Method:POST
{
 user_id 		(number - Required )
 page 			(number - Required )
 from_date 		(string - Optional  example: "2020-03-24")
 to_date 		(string - Optional  example: "2020-03-24")
 is_download 	(number - Optional  values = 0/1)
}
*/
var DepositWithdrawlHistory = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }
		type inputStruct struct {
			UserId     int64  `json:"user_id"`
			Page       int64  `json:"page"`
			FromDate   string `json:"from_date"`
			ToDate     string `json:"to_date"`
			IsDownload int64  `json:"is_download"`
		}

		var inputData inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		if inputData.UserId == 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "User Id Is Required")
			return
		}

		count, limit, returnData, status := services.DepositWithdrawlHistory(inputData.UserId, inputData.Page, inputData.FromDate, inputData.ToDate, inputData.IsDownload)
		data := map[string]interface{}{
			"total": count,
			"limit": limit,
			"data":  returnData,
		}
		if status {
			util.ErrorResponse(c, http.StatusOK, nil, "error")
			return
		} else {
			util.SuccessResponse(c, http.StatusOK, data, config.SuccessMessage)
			return
		}
	}
}

/*
METHOD: POST
{
account_type 		(Required string : ""/GR/BR)
search_client 		(Optional string, client name)
search 				(Optional string)
from_date 			(Required string example: "2020-03-24")
to_date 			(Required string example: "2020-03-24")
is_download 		(Required number values = 0/1)
page 				(Required number values = 0/1)
}
*/
var UserAccountStatementHistory = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		var inputData models.InputGetUserAccountStatementHistory
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		if inputData.AccountType != "" && inputData.AccountType != "GR" && inputData.AccountType != "BR" {
			util.ErrorResponse(c, http.StatusOK, nil, "Not A Valid Account Type")
			return
		}

		// if inputData.FromDate == "" || inputData.ToDate == "" {
		// 	util.ErrorResponse(c, http.StatusOK, nil, "From/To Date Cannnot Be Empty")
		// 	return
		// }
		count, limit, returnData, _ := services.GetUserAccountStatementHistory(inputData, tokenData.UserId)
		if len(returnData) == 0 {
			util.ErrorResponse(c, http.StatusOK, nil, config.RecordsEmptyMessage)
			return
		}
		// opningconstBalance := map[string]interface{}{
		// 	"description": "Opening",
		// 	"date":        strings.Split(inputData.FromDate, "T")[0],
		// 	"balance":     opening,
		// }

		data := map[string]interface{}{
			"total": count,
			"limit": limit,
			"data":  returnData,
			// "opening": opningconstBalance,
		}
		util.SuccessResponse(c, http.StatusOK, data, config.SuccessMessage)
		return
	}
}

/*
Header
"Authorization":"token"
	user_id (required, number),
	description (optional, string),
	amount (required, number),
	cr_dr (required, number, values: 1/2, 1=Credit, 2=Debit),
	password (required, string)
	master_password (required, string)
*/
// func ChipInOutForSuperAdmin(w http.ResponseWriter, r *http.Request) {

// 	var accountStatement models.AccountStatement
// 	decoder := json.NewDecoder(r.Body)
// 	err := decoder.Decode(&accountStatement)
// 	if err != nil {
// 		util.ErrorResponse(c, http.StatusBadRequest, "JSON not supported", err.Error())
// 		return
// 	}
// 	if accountStatement.UserId == 0 {
// 		util.ErrorResponse(c, http.StatusOK, nil, "User Id Is Required")
// 		return
// 	}
// 	if accountStatement.Amount == 0 {
// 		util.ErrorResponse(c, http.StatusOK, nil, "Amount Is Required")
// 		return
// 	}
// 	if accountStatement.CrDr == 0 {
// 		util.ErrorResponse(c, http.StatusOK, nil, "cr_dr Is Required")
// 		return
// 	}
// 	// if tokenData.UserTypeId != 2 && accountStatement.UserId == tokenData.UserId {
// 	// 	util.ErrorResponse(c, http.StatusOK, nil, "Self Credit/Debit Not Allowed")
// 	// 	return
// 	// }
// 	if accountStatement.MasterPassword == "" {
// 		util.ErrorResponse(c, http.StatusOK, nil, "Transaction Password Is Required")
// 		return
// 	}
// 	msg, status := services.GetTransactionPassword(accountStatement.UserId, accountStatement.MasterPassword)
// 	if !status {
// 		util.ErrorResponse(c, http.StatusOK, nil, msg)
// 		return
// 	} else {

// 		userData, err := services.GetUserDetailsByUserId(accountStatement.UserId)
// 		if err == false {
// 			// if accountStatement.CrDr == 1 {
// 			// 	if userData.CreditReferance < accountStatement.Amount {
// 			// 		util.ErrorResponse(c, http.StatusOK, nil, "Insufficient Credit Referance Limit")
// 			// 		return
// 			// 	}
// 			// }
// 			accountStatement.StatementType = 1
// 			accountStatement.ParentId = userData.ParentId
// 			accountStatement.OperatorName = accountStatement.UserName
// 			accountStatement.UserName = userData.UserName

// 			message, status := services.ChipInOut(accountStatement, userData, models.User{})
// 			if status {
// 				util.SuccessResponse(c, http.StatusOK, nil, message)
// 				return
// 			} else {
// 				util.ErrorResponse(c, http.StatusOK, nil, message)
// 				return
// 			}

// 			// if accountStatement.Amount > userData.FreeChip && accountStatement.CrDr == 2 {
// 			// 	util.ErrorResponse(c, http.StatusOK, nil, "Insufficient Credit Limit")
// 			// 	return
// 			// } else {
// 			// 	var parentData models.User
// 			// 	if userData.UserTypeId != 1 {
// 			// 		parentData, _ = services.GetUserBalance(accountStatement.ParentId)
// 			// 		if parentData.UserTypeId != 1 {
// 			// 			if accountStatement.Amount > parentData.FreeChip && accountStatement.CrDr == 1 {
// 			// 				util.ErrorResponse(c, http.StatusOK, nil, "Insufficient Credit Limit")
// 			// 				return
// 			// 			}
// 			// 		}
// 			// 	}
// 			// 	message, status := services.ChipInOut(accountStatement, userData, parentData)
// 			// 	if status {
// 			// 		util.SuccessResponse(c, http.StatusOK, nil, message)
// 			// 		return
// 			// 	} else {
// 			// 		util.ErrorResponse(c, http.StatusOK, nil, message)
// 			// 		return
// 			// 	}
// 			// }
// 		} else {
// 			util.ErrorResponse(c, http.StatusOK, nil, "Invalid User")
// 			return
// 		}
// 	}
// }

// /*
// Header
// "Authorization":"token"
// 	METHOD: POST
//  account_type (required string : All/GR/BR)
// 	game_name (required string : All/upper/lower/sport)
// 	search_client (optional string, client name)
// 	search (optional string)
// 	from_date (optional string example: "2020-03-24")
// 	to_date (optional string example: "2020-03-24")
// 	is_download (required number values = 0/1)
// 	page (required number)
// */
// func GetUserAccountStatementHistory(w http.ResponseWriter, r *http.Request) {
// 	tokenData, tokenMsg, tokenErr := global.VerifyUserToken(r.Header.Get("Authorization"))
// 	if tokenErr {
// 		util.ErrorResponse(c, http.StatusForbidden, tokenMsg)
// 		return
// 	}
// 	var inputData models.InputGetUserAccountStatementHistory
// 	decoder := json.NewDecoder(r.Body)
// 	err := decoder.Decode(&inputData)
// 	if err != nil {
// 		util.ErrorResponse(c, http.StatusBadRequest, "JSON not supported", err.Error())
// 		return
// 	}
// 	if inputData.AccountType != "All" && inputData.AccountType != "GR" && inputData.AccountType != "BR" {
// 		util.ErrorResponse(c, http.StatusOK, nil, "Not A Valid Account Type")
// 		return
// 	}
// 	if ((inputData.SportId != "All" && inputData.SportId != "upper" && inputData.SportId != "lower") || inputData.SportId == "0") && inputData.SportId != "" {
// 		util.ErrorResponse(c, http.StatusOK, nil, "Not A Valid Game Type")
// 		return
// 	}
// 	if inputData.FromDate == "" || inputData.ToDate == "" {
// 		util.ErrorResponse(c, http.StatusOK, nil, "From/To Date Cannnot Be Empty")
// 		return
// 	}
// 	count, limit, returnData, opening := services.GetUserAccountStatementHistory(inputData, tokenData.UserId)
// 	if len(returnData) == 0 {
// 		util.ErrorResponse(c, http.StatusOK, nil, config.RecordsEmptyMessage)
// 		return
// 	}
// 	opningconstBalance := map[string]interface{}{
// 		"description": "Opening",
// 		"date":        strings.Split(inputData.FromDate, "T")[0],
// 		"balance":     opening,
// 	}

// 	data := map[string]interface{}{
// 		"total":   count,
// 		"limit":   limit,
// 		"data":    returnData,
// 		"opening": opningconstBalance,
// 	}
// 	util.SuccessResponse(c, http.StatusOK, data, config.SuccessMessage)
// 	return
// }
