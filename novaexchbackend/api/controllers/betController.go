package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"novaexchbackend/api/services"
	"novaexchbackend/api/util"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

/*
Method := POST
{
 match_id			(string - Required)
 market_id			(string - Required)
 odds				(number - Required)
 stack				(number - Required)
 is_back 			(string - Required)
 selection_id 		(string - Required)
 selection_name 	(string - Required)
}
*/
// PlaceMarketBet :
var PlaceMarketBet = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		if int(tokenData.UserTypeId) != config.USER {
			util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
			return
		}

		var betInput = models.Bet{}
		err := json.NewDecoder(c.Request.Body).Decode(&betInput)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if betInput.MarketId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Market Id Required")
			return
		}
		if betInput.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Id Required")
			return
		}
		if betInput.Odds < 1 {
			util.ErrorResponse(c, http.StatusOK, nil, "Odds Required")
			return
		}
		if betInput.Stack <= 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Stack Required")
			return
		}
		if betInput.IsBack != "0" && betInput.IsBack != "1" {
			util.ErrorResponse(c, http.StatusOK, nil, "IsBack Required")
			return
		}
		if betInput.SelectionId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Selection Id Required")
			return
		}
		if betInput.SelectionName == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Selection Name Required")
			return
		}
		userDetails, _ := services.GetUserDetailsByUserId(tokenData.UserId)
		if userDetails.UserLock == "1" || userDetails.BetLock == "1" {
			util.ErrorResponse(c, http.StatusOK, nil, "User/Bet Locked")
			return
		}
		marketDetails, minStack, maxStack, _, _, _ := services.GetMarketById("1", betInput.MarketId, betInput.MatchId)
		if marketDetails.MarketId == "" || marketDetails.IsResultDeclared != "0" {
			util.ErrorResponse(c, http.StatusOK, nil, "Market Not Found/ Resulted")
			return
		}
		if minStack != 0 && betInput.Stack < int64(minStack) {
			util.ErrorResponse(c, http.StatusOK, nil, "Minimum Stack Is"+fmt.Sprintf("%.2f", minStack))
			return
		}
		if maxStack != 0 && betInput.Stack > int64(maxStack) {
			util.ErrorResponse(c, http.StatusOK, nil, "Maximum Stack Is"+fmt.Sprintf("%.2f", maxStack))
			return
		}
		time.Sleep(time.Duration(userDetails.MarketDelay))
		liveOdds, _ := services.GetSessionAndMarketOdds(betInput.MarketId, "1")
		var marketOdds models.MarketOdds
		if len(liveOdds) > 0 {
			marketOdds = liveOdds[0]
		}
		if marketOdds.Status != "" && strings.ToUpper(marketOdds.Status) != "OPEN" {
			util.ErrorResponse(c, http.StatusOK, nil, "Market Closed")
			return
		}
		var flagSel bool
		var currentRedisOdds float64
		for _, runner := range marketOdds.Runners {
			selId, _ := strconv.Atoi(betInput.SelectionId)
			if runner.SelectionID == selId {
				flagSel = true
				if runner.Status != "" && strings.ToUpper(runner.Status) != "ACTIVE" {
					util.ErrorResponse(c, http.StatusOK, nil, "Market Closed")
					return
				}
				if betInput.IsBack == "1" {
					if len(runner.Ex.AvailableToBack) > 0 {

						currentRedisOdds = runner.Ex.AvailableToBack[0].Price

					} else {
						util.ErrorResponse(c, http.StatusOK, nil, "Unmatched Bet not Allowed")
						return
					}
				} else {
					if len(runner.Ex.AvailableToLay) > 0 {
						currentRedisOdds = runner.Ex.AvailableToLay[0].Price
					} else {
						util.ErrorResponse(c, http.StatusOK, nil, "Unmatched Bet not Allowed")
						return
					}
				}
			}
		}
		if !flagSel {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Selection Id")
			return
		}
		var calculatedDataForMarketBet models.CalculatedDataForMarketBet
		calculatedDataForMarketBet.IsMatched = "0"
		calculatedDataForMarketBet.PL = 0
		calculatedDataForMarketBet.Liability = 0
		if currentRedisOdds > marketDetails.OddsLimit {
			util.ErrorResponse(c, http.StatusOK, nil, "Max odd limit not allowed greater than "+fmt.Sprintf("%.2f", marketDetails.OddsLimit))
			return
		}
		if betInput.IsBack == "1" {
			if betInput.Odds <= currentRedisOdds {
				calculatedDataForMarketBet.IsMatched = "1"
			} else {
				util.ErrorResponse(c, http.StatusOK, nil, "Unmatched Bet Not Placed")
				return
			}
			betInput.Odds = currentRedisOdds
			calculatedDataForMarketBet.PL = (betInput.Odds * float64(betInput.Stack)) - float64(betInput.Stack)
			calculatedDataForMarketBet.Liability = -float64(betInput.Stack)
		} else {
			if betInput.Odds >= currentRedisOdds {
				calculatedDataForMarketBet.IsMatched = "1"
			} else {
				util.ErrorResponse(c, http.StatusOK, nil, "Unmatched Bet Not Placed")
				return
			}
			betInput.Odds = currentRedisOdds
			calculatedDataForMarketBet.PL = float64(betInput.Stack)
			calculatedDataForMarketBet.Liability = -((betInput.Odds * float64(betInput.Stack)) - float64(betInput.Stack))
		}

		var singleRunner models.RunnerData
		for _, runner := range marketDetails.RunnerJson {
			if betInput.SelectionId == runner.SelectionId {
				singleRunner = runner
				break
			}
		}
		liability, validationMessage, status := services.ValidateMarketBet(userDetails, betInput, calculatedDataForMarketBet, singleRunner, "0", marketDetails)
		if status {
			util.ErrorResponse(c, http.StatusOK, nil, validationMessage)
			return
		}
		betInput.UserId = userDetails.Id
		betInput.UserName = userDetails.UserName
		betInput.ParentIds = userDetails.ParentIds
		betInput.SportId = marketDetails.SportId
		betInput.SportName = marketDetails.SportName
		betInput.SeriesId = marketDetails.SeriesId
		betInput.SeriesName = marketDetails.SeriesName
		betInput.MatchName = marketDetails.MatchName
		betInput.Profit = calculatedDataForMarketBet.PL
		betInput.Exposure = calculatedDataForMarketBet.Liability
		// betInput.IpAddress = ipAddress
		// betInput.DeviceInfo = r.Header.Get("User-Agent")
		message, isError := services.PlaceMarketBet(betInput, liability)
		if isError {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}

		betInput.MarketName = marketDetails.Name
		betInput.MarketType = "1"
		util.SuccessResponse(c, http.StatusOK, betInput, message)
		return
	}
}

/*
Method := POST
{
 match_id			(string - Required)
 market_id			(string - Required)
 size				(number - Required)
 odds				(number - Required)
 stack				(number - Required)
 is_back 			(string - Required)
}
*/
// PlaceMarketBet :
var PlaceSessionBet = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		if int(tokenData.UserTypeId) != config.USER {
			util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
			return
		}

		var betInput = models.Bet{}
		err := json.NewDecoder(c.Request.Body).Decode(&betInput)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if betInput.MarketId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Session Id Required")
			return
		}
		if betInput.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Id Required")
			return
		}
		if betInput.Odds < 1 {
			util.ErrorResponse(c, http.StatusOK, nil, "Odds Required")
			return
		}
		if betInput.Stack <= 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Stack Required")
			return
		}
		if betInput.Size <= 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Size Required")
			return
		}
		if betInput.IsBack != "0" && betInput.IsBack != "1" {
			util.ErrorResponse(c, http.StatusOK, nil, "IsBack Required")
			return
		}
		userDetails, _ := services.GetUserDetailsByUserId(tokenData.UserId)
		if userDetails.UserLock == "1" || userDetails.BetLock == "1" {
			util.ErrorResponse(c, http.StatusOK, nil, "User/Bet Locked")
			return
		}
		marketDetails, _, _, minStack, maxStack, _ := services.GetMarketById("1", betInput.MarketId, betInput.MatchId)
		if marketDetails.MarketId == "" || marketDetails.IsResultDeclared != "0" {
			util.ErrorResponse(c, http.StatusOK, nil, "Market Not Found/ Resulted")
			return
		}
		if minStack != 0 && betInput.Stack < int64(minStack) {
			util.ErrorResponse(c, http.StatusOK, nil, "Minimum Stack Is"+fmt.Sprintf("%.2f", minStack))
			return
		}
		if maxStack != 0 && betInput.Stack > int64(maxStack) {
			util.ErrorResponse(c, http.StatusOK, nil, "Maximum Stack Is"+fmt.Sprintf("%.2f", maxStack))
			return
		}
		time.Sleep(time.Duration(userDetails.SessionDelay))
		_, liveOdds := services.GetSessionAndMarketOdds(betInput.MarketId, "2")
		var marketOdds models.RemoteRedisFancy
		if len(liveOdds) > 0 {
			marketOdds = liveOdds[0]
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, "Session Closed")
			return
		}

		var pl float64
		var liability float64
		if betInput.IsBack == "1" {
			// backPrice1, _ := strconv.ParseFloat(fmt.Sprintf("%v", marketOdds.BackPrice1), 64)
			// if backPrice1 != betInput.Odds {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Run Changed")
			// 	return
			// }
			// backSize1, _ := strconv.ParseFloat(fmt.Sprintf("%v", marketOdds.BackSize1), 64)
			// if backSize1 != betInput.Size {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Size Changed")
			// 	return
			// }
			liability = -float64(betInput.Stack)
			pl = float64(betInput.Stack) * (betInput.Size / 100)
		} else {
			layPrice1, _ := strconv.ParseFloat(fmt.Sprintf("%v", marketOdds.LayPrice1), 64)
			if layPrice1 != betInput.Odds {
				util.ErrorResponse(c, http.StatusOK, nil, "Run Changed")
				return
			}
			laySize1, _ := strconv.ParseFloat(fmt.Sprintf("%v", marketOdds.LaySize1), 64)
			if laySize1 != betInput.Size {
				util.ErrorResponse(c, http.StatusOK, nil, "Size Changed")
				return
			}
			liability = -(float64(betInput.Stack) * (betInput.Size / 100))
			pl = float64(betInput.Stack)
		}
		outputData, message, status := services.ValidateFancyBet(userDetails, betInput, marketDetails)
		if status {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}
		betInput.UserId = userDetails.Id
		betInput.UserName = userDetails.UserName
		betInput.ParentIds = userDetails.ParentIds
		betInput.SportId = marketDetails.SportId
		betInput.SportName = marketDetails.SportName
		betInput.SeriesId = marketDetails.SeriesId
		betInput.SeriesName = marketDetails.SeriesName
		betInput.MatchName = marketDetails.MatchName
		betInput.MarketName = marketDetails.Name
		betInput.SelectionName = marketDetails.Name
		betInput.Profit = pl
		betInput.Exposure = liability
		// betInput.IpAddress = ipAddress
		// betInput.DeviceInfo = r.Header.Get("User-Agent")
		finalFancyPosition := []models.FancyScorePosition{}
		validateFancyBetUser := models.ValidateFancyBetOutput{}
		for _, singleUserPostion := range outputData {
			fancyScorePosition := models.FancyScorePosition{}
			if userDetails.Id == singleUserPostion.UserId {
				validateFancyBetUser = singleUserPostion
			}
			fancyScorePosition.UserId = singleUserPostion.UserId
			fancyScorePosition.MatchId = marketDetails.MatchId
			fancyScorePosition.FancyId = marketDetails.MarketId
			fancyScorePosition.Liability = singleUserPostion.LiabilityFancy
			fancyScorePosition.Profit = singleUserPostion.ProfitFancy
			fancyScorePosition.UserTypeId = singleUserPostion.UserTypeId
			fancyScorePosition.FancyScorePositionJson = singleUserPostion.FancyScorePositionJson
			finalFancyPosition = append(finalFancyPosition, fancyScorePosition)
		}
		message, isError := services.PlaceFancyBet(betInput, finalFancyPosition, validateFancyBetUser.LiabilityForBalance)
		if isError {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}
		betInput.MarketName = marketDetails.Name
		betInput.MarketType = "2"
		util.SuccessResponse(c, http.StatusOK, betInput, message)
		return
	}
}

/*
Method := POST
{
 market_id			(string - Required)
 bet_id				(number - Required)
 is_void 			(string - Required)
}
*/
// PlaceMarketBet :
var DeleteMarketBet = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		if int(tokenData.UserTypeId) != config.SUPERADMIN {
			util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
			return
		}
		type inputBet struct {
			BetId    int64  `json:"bet_id"`
			MarketId string `json:"market_id"`
			IsVoid   string `json:"is_void"`
		}
		var betInput = inputBet{}
		err := json.NewDecoder(c.Request.Body).Decode(&betInput)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if betInput.MarketId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Market Id Required")
			return
		}
		if betInput.BetId < 1 {
			util.ErrorResponse(c, http.StatusOK, nil, "Bet Id Is Required")
			return
		}
		if betInput.IsVoid != "0" && betInput.IsVoid != "1" {
			util.ErrorResponse(c, http.StatusOK, nil, "Is Void Required")
			return
		}
		message, status := services.DeleteBetOdds(betInput.BetId, tokenData.UserId, betInput.MarketId, betInput.IsVoid)
		if status {
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		}
		util.ErrorResponse(c, http.StatusOK, nil, message)
		return
	}
}

/*
	METHOD: POST
	page       		(number - Required)
	bet_type 		(string - Required values = "M"/"D"/"A")
	user_code 		(string - Optional)
	ip_address 		(string - Optional)
	from_amount  	(number - Optional)
	to_amount    	(number - Optional)
	type 			(string - Optional example: all/back/lay)
*/
var BetHistory = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		var inputData models.InputBetHistory
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		if inputData.BetType != "M" && inputData.BetType != "D" && inputData.BetType != "A" {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid bet type")
			return
		}

		inputData.UserId = tokenData.UserId
		count, limit, returnData := services.BetHistory(inputData)
		data := map[string]interface{}{
			"total":    count,
			"limit":    limit,
			"bet_data": returnData,
		}
		util.SuccessResponse(c, http.StatusOK, data, config.SuccessMessage)
		return
	}
}

/*
	METHOD: POST
	match_id string
*/
var GetCurrentBet = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		var inputData models.InputBetHistory
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if inputData.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match id is required")
			return
		}
		returnData := services.GetCurrentBet(tokenData.UserId, inputData.MatchId)
		util.SuccessResponse(c, http.StatusOK, returnData, config.SuccessMessage)
		return
	}
}
