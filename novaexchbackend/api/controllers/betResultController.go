package controllers

import (
	"encoding/json"
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"novaexchbackend/api/services"
	"novaexchbackend/api/util"

	"github.com/gin-gonic/gin"
)

/*
Method := POST
{
 match_id			(string - Required)
 market_id			(string - Required)
 selection_id 		(string - Required)
 selection_name 	(string - Required)
}
*/
// DeclareMarket :
var DeclareMarket = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		if int(tokenData.UserTypeId) != config.SUPERADMIN {
			util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
			return
		}

		var betInput = models.ResultInput{}
		err := json.NewDecoder(c.Request.Body).Decode(&betInput)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if betInput.MarketId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Market Id Required")
			return
		}
		if betInput.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Id Required")
			return
		}
		if betInput.SelectionId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Selection Id Required")
			return
		}
		if betInput.SelectionName == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Selection Name Required")
			return
		}
		message, status := services.DeclareMarket(betInput)
		if status {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}
		util.SuccessResponse(c, http.StatusOK, nil, message)
		return
	}
}

/*
Method := POST
{
 match_id			(string - Required)
 fancy_id			(string - Required)
 result 			(string - Required)
}
*/
// DeclareSession :
var DeclareSession = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		//ipAddress := global.GetIpAddress(c.Request)
		// tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if int(tokenData.UserTypeId) != config.SUPERADMIN {
		// 	util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
		// 	return
		// }

		var betInput = models.ResultInput{}
		err := json.NewDecoder(c.Request.Body).Decode(&betInput)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if betInput.FancyId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Session Id Required")
			return
		}
		if betInput.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Id Required")
			return
		}
		if betInput.Result == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Result Required")
			return
		}
		message, status := services.DeclareSession(betInput)
		if status {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}
		util.SuccessResponse(c, http.StatusOK, nil, message)
		return
	}
}

/*
METHOD := POST
{
 match_id			(string - Required )
 market_id			(string - Required )
 is_rollback		(number - Required ) (0/1) 0=Abandoned, 1=Rollback Abandoned Market
 sport_id			(string - Required ) if is_rollback is 0
 series_id			(string - Required ) if is_rollback is 0
 sport_name			(string - Required ) if is_rollback is 0
 series_name		(string - Required ) if is_rollback is 0
 match_name			(string - Required ) if is_rollback is 0
 market_name		(string - Required ) if is_rollback is 0
}
*/
var AbandonedMarketFancy = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		if userInfo.UserTypeId != 1 && userInfo.UserTypeId != 2 {
			util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
			return
		}
		var inputData models.InputAbandonedMarket
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if inputData.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match id is required")
			return
		}
		if inputData.MarketId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Market id is required")
			return
		}
		if inputData.IsRollback == 0 {
			if inputData.SportId == "" {
				util.ErrorResponse(c, http.StatusOK, nil, "Sport id is required")
				return
			}
			if inputData.SeriesId == "" {
				util.ErrorResponse(c, http.StatusOK, nil, "Series id is required")
				return
			}
			if inputData.SportName == "" {
				util.ErrorResponse(c, http.StatusOK, nil, "Sport name is required")
				return
			}
			if inputData.SeriesName == "" {
				util.ErrorResponse(c, http.StatusOK, nil, "Series name is required")
				return
			}
			if inputData.MatchName == "" {
				util.ErrorResponse(c, http.StatusOK, nil, "Match name is required")
				return
			}
			if inputData.MarketName == "" {
				util.ErrorResponse(c, http.StatusOK, nil, "Market name is required")
				return
			}
		}
		message, status := services.AbandonedMarketFancy(inputData)
		if status {
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}

	}
}

/*
Method: POST
{
 match_id 		(string - Required)
 market_id 		(string - Required)
 is_fancy 		(number - Required)
}
*/
var RollbackMarketFancy = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		if userInfo.UserTypeId != 1 && userInfo.UserTypeId != 2 {
			util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
			return
		}

		// settings, _ := services.GetSettingInfo()
		// if settings.RollbackAllowed == "0" {
		// 	util.ErrorResponse(c, http.StatusOK,nil, "Rollback Not Allowed")
		// 	return
		// }
		var inputData models.InputMarketResultRollback
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if inputData.MarketId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Market Id Is Required")
			return
		}
		if inputData.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Id Is Required")
			return
		}

		message, status := services.RollbackMarketFancyResult(inputData)
		if status {
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}
	}
}
