package controllers

import (
	"encoding/json"
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/models"
	"novaexchbackend/api/services"
	"novaexchbackend/api/util"

	"github.com/gin-gonic/gin"
)

/*
Method: POST
{
 sport_id 			(string - Required)
 series_id 			(string - Required)
 match_id 			(string - Required)
 selection_id 		(string - Required)
 name 				(string - Required)
 session_size_no 	(number - Required)
 session_size_yes 	(number - Required)
}
*/

var CreateFancy = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if userInfo.UserTypeId != 1 && userInfo.UserTypeId != 2 {
		// 	util.ErrorResponse(c, http.StatusOK, "", config.AccessErrorMessage)
		// 	return
		// }

		var fancy models.Fancy
		err := json.NewDecoder(c.Request.Body).Decode(&fancy)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		fancy.FancyId = fancy.MatchId + "_" + fancy.SelectionId
		// fancy.Active = "1"
		// fancy.Result = sql.NullFloat64{}
		status, message := services.CreateFancy(fancy)
		if status {
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}
	}
}

/*
Method: POST
 fancy_id 		(string - Required)
 status 		(string - Required) ("0", "1", "2")
*/
var UpdateFancyStatus = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if userInfo.UserTypeId != 1 && userInfo.UserTypeId != 2 {
		// 	util.ErrorResponse(c, http.StatusOK, "", config.AccessErrorMessage)
		// 	return
		// }

		type inputStruct struct {
			FancyId string `json:"fancy_id"`
			Status  string `json:"status"`
		}

		var inputData inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		if inputData.FancyId == "" {
			util.ErrorResponse(c, http.StatusBadRequest, nil, "Fancy Id Required")
			return
		}
		if inputData.Status == "4" || inputData.Status == "2" || inputData.Status == "1" || inputData.Status == "0" {
			message := services.UpdateFancyStatus(inputData.FancyId, inputData.Status)

			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Status")
			return
		}
	}
}

/*
Method: POST
 sport_id 		(string - Optional)
 series_id 		(string - Optional)
 match_id 		(string - Optional)
 status 		(string - Optional) ("0", "1")
 search 		(string - Optional)
*/
var GetFancies = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if userInfo.UserTypeId != 1 && userInfo.UserTypeId != 2 {
		// 	util.ErrorResponse(c, http.StatusOK, "", config.AccessErrorMessage)
		// 	return
		// }

		type inputStruct struct {
			SportId  string `json:"sport_id"`
			SeriesId string `json:"series_id"`
			MatchId  string `json:"match_id"`
			Status   string `json:"status"`
			Search   string `json:"search"`
		}

		var inputData inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		fancyData, err := services.GetFancies(inputData.SportId, inputData.SeriesId, inputData.MatchId, inputData.Status, inputData.Search)
		if err == nil && len(fancyData) > 0 {
			util.SuccessResponse(c, http.StatusOK, fancyData, config.SuccessMessage)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, config.RecordsEmptyMessage)
			return
		}
	}
}
