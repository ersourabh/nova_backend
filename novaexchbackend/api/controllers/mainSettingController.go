package controllers

import (
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/models"
	"novaexchbackend/api/services"
	"novaexchbackend/api/util"

	"github.com/gin-gonic/gin"
)

var GetSetting = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		returnData, err := services.GetSettingInfo()
		if err != nil {
			util.ErrorResponse(c, http.StatusOK, nil, "Something went wrong")
			return
		} else if returnData == (models.GlobalSetting{}) {
			util.ErrorResponse(c, http.StatusOK, nil, config.RecordsEmptyMessage)
			return
		}
		util.SuccessResponse(c, http.StatusOK, returnData, config.SuccessMessage)
		return
	}
}

// /*
// Header
// "Authorization":"token"
// Method:POST
// {
// "site_title"
// "site_message"
// "one_click_stack"
// "match_stack"
// "fancy_stack"
// "site_under_maintenance"
// "match_max_bet"
// "cricket_odds_commission"
// "cricket_fancy_commission"
// "cricket_bookmaker_commissio"
// "soccer_odds_commission"
// "soccer_fancy_commission"
// "soccer_bookmaker_commissio"
// "tennis_odds_commission"
// "tennis_fancy_commission"
// "tennis_bookmaker_commissio"
// "cricket_min_bet"
// "soccer_min_bet"
// "tennis_min_bet"
// "cricket_max_bet"
// "soccer_max_bet"
// "tennis_max_bet"
// "cricket_odds_delay"
// "soccer_odds_delay"
// "tennis_odds_delay"
// "cricket_fancy_delay"
// "soccer_fancy_delay"
// "tennis_fancy_delay"
// "sign_up"
// "payment_gateway"
// }
// */
// func UpdateBySuperAdmin(w http.ResponseWriter, r *http.Request) {
// 	ipAddress := global.GetIpAddress(r)
// 	userInfo, tokenMsg, tokenErr := global.VerifyUserToken(r.Header.Get("Authorization"), ipAddress)
// 	if tokenErr {
// 		// return c.RenderJSON(helper.ErrorResponse("", tokenMsg))
// 		util.ErrorResponse(c, http.StatusForbidden, "", tokenMsg)
// 		return
// 	}
// 	if userInfo.UserTypeId != 1 {
// 		util.ErrorResponse(c, http.StatusOK, "", global.AccessErrorMessage)
// 		return
// 	}
// 	var global models.GlobalSetting
// 	decoder := json.NewDecoder(r.Body)
// 	err := decoder.Decode(&global)
// 	if err != nil {
// 		util.ErrorResponse(c, http.StatusBadRequest, "", "JSON not supported", err.Error())
// 		return
// 	}
// 	defer r.Body.Close()
// 	if global.TransactionPassword == "" {
// 		util.ErrorResponse(c, http.StatusBadRequest, "", "Transaction Password Is Required")
// 		return
// 	}
// 	msg, status := services.GetTransactionPassword(userInfo.UserId, global.TransactionPassword)
// 	if !status {
// 		util.ErrorResponse(c, http.StatusOK, "", msg)
// 		return
// 	}
// 	returnData, _ := services.UpdateBySuperAdmin(global)
// 	util.SuccessResponse(c, http.StatusOK, returnData, "success")
// 	return
// }

// func LogoUpload(w http.ResponseWriter, r *http.Request) {
// 	ipAddress := global.GetIpAddress(r)
// 	userInfo, tokenMsg, tokenErr := global.VerifyUserToken(r.Header.Get("Authorization"), ipAddress)
// 	if tokenErr {
// 		util.ErrorResponse(c, http.StatusForbidden, "", tokenMsg)
// 		return
// 	}
// 	if userInfo.UserTypeId != 1 {
// 		util.ErrorResponse(c, http.StatusOK, "", global.AccessErrorMessage)
// 		return
// 	}

// 	file, handler, err := r.FormFile("fileupload") // Retrieve the file from form data

// 	if err != nil {
// 		util.ErrorResponse(c, http.StatusOK, "", "Unable Upload", err.Error())
// 		return
// 	}
// 	defer file.Close()
// 	f, err := os.OpenFile("./upload/image/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0777)

// 	if err != nil {
// 		util.ErrorResponse(c, http.StatusOK, "", "Unable Upload", err.Error())
// 		return
// 	}
// 	io.Copy(f, file)
// 	services.UploadFaviconAndLogo(handler.Filename, "0")
// 	util.SuccessResponse(c, http.StatusOK, "", "File Uploaded successfully")
// 	return
// }
// func LogoFavicon(w http.ResponseWriter, r *http.Request) {
// 	ipAddress := global.GetIpAddress(r)
// 	userInfo, tokenMsg, tokenErr := global.VerifyUserToken(r.Header.Get("Authorization"), ipAddress)
// 	if tokenErr {
// 		util.ErrorResponse(c, http.StatusForbidden, "", tokenMsg)
// 		return
// 	}
// 	if userInfo.UserTypeId != 1 {
// 		util.ErrorResponse(c, http.StatusOK, "", global.AccessErrorMessage)
// 		return
// 	}
// 	r.ParseMultipartForm(32 << 20)

// 	file, handler, err := r.FormFile("fileupload") // Retrieve the file from form data

// 	if err != nil {
// 		util.ErrorResponse(c, http.StatusOK, "", "Unable Upload", err.Error())
// 		return
// 	}
// 	defer file.Close() // Close the file when we finish
// 	// This is path which we want to store the file
// 	f, err := os.OpenFile("./upload/image/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0777)

// 	if err != nil {
// 		util.ErrorResponse(c, http.StatusOK, "", "Unable Upload", err.Error())
// 		return
// 	}
// 	io.Copy(f, file)
// 	services.UploadFaviconAndLogo(handler.Filename, "1")
// 	util.SuccessResponse(c, http.StatusOK, "", "File Uploaded successfully")
// 	return
// }

// func GetUserGlobalSetting(w http.ResponseWriter, r *http.Request) {
// 	ipAddress := global.GetIpAddress(r)
// 	_, tokenMsg, tokenErr := global.VerifyUserToken(r.Header.Get("Authorization"), ipAddress)
// 	if tokenErr {
// 		util.ErrorResponse(c, http.StatusForbidden, "", tokenMsg)
// 		return
// 	}
// 	type inputStruct struct {
// 		UserId int64 `json:"user_id"`
// 	}
// 	var inputData inputStruct
// 	decoder := json.NewDecoder(r.Body)
// 	err := decoder.Decode(&inputData)
// 	if err != nil {
// 		util.ErrorResponse(c, http.StatusBadRequest, "", "JSON not supported", err.Error())
// 		return
// 	}
// 	if inputData.UserId < 1 {
// 		util.ErrorResponse(c, http.StatusBadRequest, "", "User Id is Required")
// 		return
// 	}
// 	userDetails, _ := services.GetUserDetailsByUserId(inputData.UserId)
// 	returnData, err := services.GetUserGlobalSettingInfo(global.TrimCommaBothSide(userDetails.ParentIds + strconv.Itoa(int(inputData.UserId))))
// 	if err != nil {
// 		util.ErrorResponse(c, http.StatusOK, "", "Setting Not Found"+err.Error())
// 		return
// 	}
// 	util.SuccessResponse(c, http.StatusOK, returnData, "")
// 	return
// }
// func UpdateUserGlobalSetting(w http.ResponseWriter, r *http.Request) {
// 	ipAddress := global.GetIpAddress(r)
// 	userInfo, tokenMsg, tokenErr := global.VerifyUserToken(r.Header.Get("Authorization"), ipAddress)
// 	if tokenErr {
// 		// return c.RenderJSON(helper.ErrorResponse("", tokenMsg))
// 		util.ErrorResponse(c, http.StatusForbidden, "", tokenMsg)
// 		return
// 	}
// 	if userInfo.UserTypeId == 7 {
// 		util.ErrorResponse(c, http.StatusOK, "", global.AccessErrorMessage)
// 		return
// 	}
// 	var inputData models.GlobalUserSetting
// 	decoder := json.NewDecoder(r.Body)
// 	err := decoder.Decode(&inputData)
// 	if err != nil {
// 		util.ErrorResponse(c, http.StatusBadRequest, "", "JSON not supported", err.Error())
// 		return
// 	}
// 	defer r.Body.Close()
// 	if inputData.TransactionPassword == "" {
// 		util.ErrorResponse(c, http.StatusBadRequest, "", "Transaction Password Is Required")
// 		return
// 	}
// 	if inputData.UserId < 1 {
// 		util.ErrorResponse(c, http.StatusBadRequest, "", "User Id is Required")
// 		return
// 	}
// 	msg, status := services.GetTransactionPassword(userInfo.UserId, inputData.TransactionPassword)
// 	if !status {
// 		util.ErrorResponse(c, http.StatusOK, "", msg)
// 		return
// 	}
// 	userDetails, _ := services.GetUserDetailsByUserId(inputData.UserId)
// 	if userDetails.ParentId != userInfo.UserId {
// 		util.ErrorResponse(c, http.StatusOK, "", global.AccessErrorMessage)
// 		return
// 	}
// 	settingDetails, _ := services.GetUserGlobalSettingInfo(global.TrimCommaBothSide(userDetails.ParentIds))
// 	if inputData.CricketMinBet < settingDetails.CricketMinBet {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Cricket Min Bet Allowed Is : "+fmt.Sprintf("%v", settingDetails.CricketMinBet))
// 		return
// 	}
// 	if inputData.SoccerMinBet < settingDetails.SoccerMinBet {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Soccer Min Bet Allowed Is : "+fmt.Sprintf("%v", settingDetails.SoccerMinBet))
// 		return
// 	}
// 	if inputData.TennisMinBet < settingDetails.TennisMinBet {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Tennis Min Bet Allowed Is : "+fmt.Sprintf("%v", settingDetails.TennisMinBet))
// 		return
// 	}
// 	if inputData.CricketMaxBet > settingDetails.CricketMaxBet {
// 		util.ErrorResponse(c, http.StatusOK, "", "Maximum Cricket Max Bet Allowed Is : "+fmt.Sprintf("%v", settingDetails.CricketMaxBet))
// 		return
// 	}
// 	if inputData.SoccerMaxBet > settingDetails.SoccerMaxBet {
// 		util.ErrorResponse(c, http.StatusOK, "", "Maximum Soccer Max Bet Allowed Is : "+fmt.Sprintf("%v", settingDetails.SoccerMaxBet))
// 		return
// 	}
// 	if inputData.TennisMaxBet > settingDetails.TennisMaxBet {
// 		util.ErrorResponse(c, http.StatusOK, "", "Maximum Tennis Max Bet Allowed Is : "+fmt.Sprintf("%v", settingDetails.TennisMaxBet))
// 		return
// 	}
// 	if inputData.CricketOddsDelay < settingDetails.CricketOddsDelay {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Cricket Odds Delay Allowed Is : "+fmt.Sprintf("%v", settingDetails.CricketOddsDelay))
// 		return
// 	}
// 	if inputData.SoccerOddsDelay < settingDetails.SoccerOddsDelay {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Soccer Odds Delay Allowed Is : "+fmt.Sprintf("%v", settingDetails.SoccerOddsDelay))
// 		return
// 	}
// 	if inputData.TennisOddsDelay < settingDetails.TennisOddsDelay {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Tennis Odds Delay Allowed Is : "+fmt.Sprintf("%v", settingDetails.TennisOddsDelay))
// 		return
// 	}
// 	if inputData.CasinoDelay < settingDetails.CasinoDelay {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Casino Odds Delay Allowed Is : "+fmt.Sprintf("%v", settingDetails.CasinoDelay))
// 		return
// 	}
// 	if inputData.CricketFancyDelay < settingDetails.CricketFancyDelay {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Cricket Fancy Delay Allowed Is : "+fmt.Sprintf("%v", settingDetails.CricketFancyDelay))
// 		return
// 	}
// 	if inputData.SoccerFancyDelay < settingDetails.SoccerFancyDelay {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Soccer Fancy Delay Allowed Is : "+fmt.Sprintf("%v", settingDetails.SoccerFancyDelay))
// 		return
// 	}
// 	if inputData.TennisFancyDelay < settingDetails.TennisFancyDelay {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Tennis Fancy Delay Allowed Is : "+fmt.Sprintf("%v", settingDetails.TennisFancyDelay))
// 		return
// 	}
// 	if inputData.SoccerOddsCommission < settingDetails.SoccerOddsCommission {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Soccer Odds Commission Allowed Is : "+fmt.Sprintf("%v", settingDetails.SoccerOddsCommission))
// 		return
// 	}
// 	if inputData.CricketOddsCommission < settingDetails.CricketOddsCommission {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Cricket Odds Commission Allowed Is : "+fmt.Sprintf("%v", settingDetails.CricketOddsCommission))
// 		return
// 	}
// 	if inputData.TennisOddsCommission < settingDetails.TennisOddsCommission {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Tennis Odds Commission Allowed Is : "+fmt.Sprintf("%v", settingDetails.TennisOddsCommission))
// 		return
// 	}
// 	if inputData.SoccerFancyCommission > settingDetails.SoccerFancyCommission {
// 		util.ErrorResponse(c, http.StatusOK, "", "Maximum Soccer Fancy Commission Allowed Is : "+fmt.Sprintf("%v", settingDetails.SoccerFancyCommission))
// 		return
// 	}
// 	if inputData.CricketFancyCommission > settingDetails.CricketFancyCommission {
// 		util.ErrorResponse(c, http.StatusOK, "", "MaxiMum Cricket Fancy Commission Allowed Is : "+fmt.Sprintf("%v", settingDetails.CricketFancyCommission))
// 		return
// 	}
// 	if inputData.TennisFancyCommission > settingDetails.TennisFancyCommission {
// 		util.ErrorResponse(c, http.StatusOK, "", "Maximum Tennis Fancy Commission Allowed Is : "+fmt.Sprintf("%v", settingDetails.TennisFancyCommission))
// 		return
// 	}
// 	if inputData.SoccerBookmakerCommission < settingDetails.SoccerBookmakerCommission {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Soccer Bookmaker Commission Allowed Is : "+fmt.Sprintf("%v", settingDetails.SoccerBookmakerCommission))
// 		return
// 	}
// 	if inputData.CricketBookmakerCommission < settingDetails.CricketBookmakerCommission {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Cricket Bookmaker Commission Allowed Is : "+fmt.Sprintf("%v", settingDetails.CricketBookmakerCommission))
// 		return
// 	}
// 	if inputData.TennisBookmakerCommission < settingDetails.TennisBookmakerCommission {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Tennis Bookmaker Commission Allowed Is : "+fmt.Sprintf("%v", settingDetails.TennisBookmakerCommission))
// 		return
// 	}
// 	if inputData.SessionMinBet < settingDetails.SessionMinBet {
// 		util.ErrorResponse(c, http.StatusOK, "", "Minimum Session Min Bet Allowed Is : "+fmt.Sprintf("%v", settingDetails.SessionMinBet))
// 		return
// 	}
// 	if inputData.SessionMaxBet > settingDetails.SessionMaxBet {
// 		util.ErrorResponse(c, http.StatusOK, "", "Maximum Session Max Bet Allowed Is : "+fmt.Sprintf("%v", settingDetails.SessionMaxBet))
// 		return
// 	}
// 	if inputData.BookMakerMaxBet > settingDetails.BookMakerMaxBet {
// 		util.ErrorResponse(c, http.StatusOK, "", "Maximum BookMaker Max Bet Allowed Is : "+fmt.Sprintf("%v", settingDetails.BookMakerMaxBet))
// 		return
// 	}
// 	returnData, status := services.UpdateGlobalSetting(inputData, userDetails.ParentIds)
// 	if status {
// 		util.SuccessResponse(c, http.StatusOK, "", returnData)
// 		return
// 	} else {
// 		util.ErrorResponse(c, http.StatusOK, "", returnData)
// 		return
// 	}
// }
// func UpdateDeleteAndRollbackSetting(w http.ResponseWriter, r *http.Request) {
// 	type inputStruct struct {
// 		BetDeleteAllowed string `json:"bet_delete_allowed"`
// 		RollbackAllowed  string `json:"rollback_allowed"`
// 	}
// 	var inputData inputStruct
// 	decoder := json.NewDecoder(r.Body)
// 	err := decoder.Decode(&inputData)
// 	if err != nil {
// 		util.ErrorResponse(c, http.StatusBadRequest, "", "JSON not supported", err.Error())
// 		return
// 	}
// 	if inputData.BetDeleteAllowed != "0" && inputData.BetDeleteAllowed != "1" {
// 		util.ErrorResponse(c, http.StatusOK, "", "Invalid Bet delete Status")
// 		return
// 	}
// 	if inputData.RollbackAllowed != "0" && inputData.RollbackAllowed != "1" {
// 		util.ErrorResponse(c, http.StatusOK, "", "Invalid Rollback Status")
// 		return
// 	}
// 	services.UploadBetAndRollbackSetting(inputData.BetDeleteAllowed, inputData.RollbackAllowed)
// 	util.SuccessResponse(c, http.StatusOK, "", "Success")
// 	return
// }
