package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"novaexchbackend/api/services"
	"novaexchbackend/api/util"
	"strconv"

	"github.com/gin-gonic/gin"
)

/*
Method := POST
{
 match_id					(string - Required)
 market_id					(string - Required)
 name						(string - Required)
 market_start_time			(string - Required)
}
*/
// CreateMarket :
var CreateMarket = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if tokenData.UserTypeId != 1 {
		// 	util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
		// 	return
		// }

		var market models.Market
		err := json.NewDecoder(c.Request.Body).Decode(&market)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if market.MarketId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Market Id Required")
			return
		}
		if market.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Id Required")
			return
		}
		if market.Name == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Name Required")
			return
		}
		if market.Name == "Bookmaker" || market.Name == "bookmaker" {
			market.IsBookmaker = "1"
		} else {
			market.IsBookmaker = "0"
		}
		message, status := saveMarket(market)
		if status {
			if market.IsBookmaker == "0" {
				services.GetOnlineSelection(market.MarketId, market.MatchId)
			} else {
				services.GetOnlineSelectionBookmaker(market.MarketId, market.MatchId)
			}
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}
	}
}

//saveMarket :
func saveMarket(market models.Market) (message string, status bool) {
	currentTime := global.GetCurrentDateTime()

	var arr []models.OnlineMarketStructInt
	var arr1 []models.OnlineMarketStructStr

	content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineMarketsRunnersURL + market.MarketId)
	if market.IsBookmaker == "1" {
		content, statusCode, err = util.RequestAPIDataClientURL(config.OnlineBookmakerMarketsRunnersURL + market.MarketId)
	}

	if statusCode != 200 || err != nil {
		log.Println("CreateMarket >> Get market runner >> statuscode >> ", statusCode, ": error >> ", err.Error())
	}
	// fmt.Println(string(body))
	_ = json.Unmarshal(content, &arr)
	_ = json.Unmarshal(content, &arr1)
	if len(arr) > 0 {
		runners := arr[0].Runners
		layBackData := []models.LayBackData{{Size: "--", Price: "--"}, {Size: "--", Price: "--"}, {Size: "--", Price: "--"}}
		var runnerJson []models.RunnerData
		var marketSelection []models.MarketSelection

		if len(runners) > 0 {
			for i := 0; i < len(runners); i++ {
				runnerData := runners[i]
				runnerDataStr := arr1[0].Runners[i]
				selectionId := ""
				if runnerData.SelectionId != 0 {
					selectionId = strconv.Itoa(int(runnerData.SelectionId))
				} else {
					selectionId = runnerDataStr.SelectionId
				}
				marketSelection = append(marketSelection, models.MarketSelection{
					MatchId:       market.MatchId,
					MarketId:      market.MarketId,
					LiabilityType: "0",
					SortName:      "",
					SelectionId:   selectionId,
					Name:          runnerData.RunnerName,
					SortPriority:  runnerData.SortPriority,
				})

				runnerJson = append(runnerJson, models.RunnerData{
					SelectionId:   selectionId,
					Name:          runnerData.RunnerName,
					SortPriority:  float64(runnerData.SortPriority),
					LiabilityType: "0",
					SortName:      "",
					Status:        "",
					Lay:           layBackData,
					Back:          layBackData,
				})
			}

			market.RunnerJson = runnerJson
			// market.Name = arr[0].MarketName
			// market.MaxBetLiability = 0
			// market.MaxMarketLiability = 0
			// market.MaxMarketProfit = 0
			// market.IsActive = "1"
			// market.IsVisible = "1"
			// market.IsBookmakerMarket = "0"
			market.CreatedAt = currentTime
			// market.UpdatedAt = currentTime

			message, status = services.CreateMarket(market)
			// if status {
			// 	services.CreateMarketSelections(marketSelection)
			// }
		} else {
			message = "Failed To Create Market, Runners Not Available"
			status = false
		}
	} else {
		message = "Market Not Exists"
		status = false
	}
	return message, status
}

/*
Method := POST
 market_id 		(string - Required)
 is_active 		(string - Required)
*/
var UpdateMarketStatus = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		if userInfo.UserTypeId != 1 {
			util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
			return
		}
		var market models.Market
		err := json.NewDecoder(c.Request.Body).Decode(&market)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if market.MarketId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Market Id Required")
			return
		}

		if market.IsActive == "1" || market.IsActive == "0" {
			message := services.UpdateMarketStatus(market)
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Status")
			return
		}
	}
}

/*
Method := POST
 sport_id 		(string - Optional)
 series_id 		(string - Optional)
 is_active 		(string - Optional)
 search 		(string - Optional)
*/
var GetMarkets = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		type inputStruct struct {
			SportId    string `json:"sport_id"`
			SeriesId   string `json:"series_id"`
			IsActive   string `json:"is_active"`
			MatchId    string `json:"match_id"`
			IsResulted string `json:"is_resulted"`
		}

		var inputData inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.AccessErrorMessage)
			return
		}
		if inputData.IsResulted == "" {
			inputData.IsResulted = "0"
		}
		returnData, err := services.GetMarkets(inputData.SportId, inputData.SeriesId, inputData.IsActive, inputData.MatchId, inputData.IsResulted)
		if err != nil {
			util.ErrorResponse(c, http.StatusOK, nil, "Market Not Available")
			return
		}

		util.SuccessResponse(c, http.StatusOK, returnData, config.SuccessMessage)
		return
	}
}

/*
Method := POST
 ids 			(string - Required)
 call_type 		(string - Required)
*/
var GetSessionAndMarketOdds = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		type inputStruct struct {
			Ids      string `json:"ids"`       //"1,2,3,4"
			CallType string `json:"call_type"` //1 for market, 2 for session
		}

		var inputData inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.AccessErrorMessage)
			return
		}
		if inputData.Ids == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Id")
			return
		}
		if inputData.CallType != "1" && inputData.CallType != "2" {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Call Type")
			return
		}
		returnData, returnData2 := services.GetSessionAndMarketOdds(inputData.Ids, inputData.CallType)
		if len(returnData) > 0 && inputData.CallType == "1" {
			util.SuccessResponse(c, http.StatusOK, returnData, config.SuccessMessage)
			return
		} else if len(returnData2) > 0 && inputData.CallType == "2" {
			util.SuccessResponse(c, http.StatusOK, returnData2, config.SuccessMessage)
			return
		}
	}
}

/*
Method: POST
 match_id      (string - Required)
 market_id     (string - Required)
*/
var GetMarketSelections = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }
		type inputStruct struct {
			MatchId  string `json:"match_id"`
			MarketId string `json:"market_id"`
		}

		var inputData inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.AccessErrorMessage)
			return
		}

		if inputData.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Id Is Required")
			return
		}
		if inputData.MarketId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Market Id Is Required")
			return
		}

		// liveSportDetails := services.CheckIsLiveSportByMatchId(inputData.MatchId)
		// var marketData []models.MarketSelection
		// if liveSportDetails.IsLiveSport == "1" {
		// 	marketData = services.MarketSelectionListByMatchId(inputData.MatchId)
		// } else {
		// 	marketData = services.MarketSelectionList(inputData.MatchId, inputData.MarketId)
		// }
		marketData := services.MarketSelectionList(inputData.MatchId, inputData.MarketId)
		if len(marketData) == 0 {
			util.ErrorResponse(c, http.StatusOK, nil, config.RecordsEmptyMessage)
			return
		}
		util.SuccessResponse(c, http.StatusOK, marketData, config.SuccessMessage)
		return
	}
}
