package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"novaexchbackend/api/services"
	"novaexchbackend/api/util"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

/*
METHOD := POST
{
 sport_id 		(string - Required)
 series_id		(string - Required)
 match_id		(string - Required)
 name			(string - Required)
 match_date		(string - Required)
}
*/
var CreateMatch = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }
		// if userInfo.UserTypeId != 1 {
		// 	util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
		// 	return
		// }
		var match models.Match
		err := json.NewDecoder(c.Request.Body).Decode(&match)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if match.SportId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Sport Id Required")
			return
		}
		if match.SeriesId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Series Id Required")
			return
		}
		if match.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Id Required")
			return
		}
		if match.Name == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Name Required")
			return
		}
		if match.MatchDate == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Date Required")
			return
		}

		matchSettings, _ := services.GetMatchSettingInfo(match.SportId)
		if match.SportId == "1" {
			match.MinStack = float64(matchSettings.MarketMinBet)
			match.MaxStack = float64(matchSettings.MarketMaxBet)
		} else if match.SportId == "2" {
			match.MinStack = float64(matchSettings.MarketMinBet)
			match.MaxStack = float64(matchSettings.MarketMaxBet)
		} else {
			match.MinStack = float64(matchSettings.MarketMinBet)
			match.MaxStack = float64(matchSettings.MarketMaxBet)
			match.SessionMinStack = float64(matchSettings.SessionMinBet)
			match.SessionMaxStack = float64(matchSettings.SessionMaxBet)
		}
		match.StartDate = global.ConvertUTCtoIST(match.MatchDate)
		match.ScoreType = "1"
		match.ScoreKey = "0"
		match.IsActive = "1"
		match.LiabilityType = "0"
		status, message := services.CreateMatch(match)

		if status {
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}
	}
}

/*
Method := POST
 sport_id 		(string - Optional)
 series_id 		(string - Optional)
 is_active 		(string - Optional)
 search 		(string - Optional)
*/
var GetMatches = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		type inputStruct struct {
			SportId  string `json:"sport_id"`
			SeriesId string `json:"series_id"`
			IsActive string `json:"is_active"`
			Search   string `json:"search"`
			// IsHideDeclaredMatch string `json:"is_hide_declared_match"`
		}

		var inputData inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.AccessErrorMessage)
			return
		}
		returnData, err := services.GetMatches(inputData.SportId, inputData.SeriesId, inputData.IsActive, inputData.Search, userInfo.UserTypeId)
		if err != nil {
			log.Println("GetMatches >> Unable get GetMatches", err.Error())
			util.ErrorResponse(c, http.StatusOK, nil, "Matches Not Available")
			return
		}

		util.SuccessResponse(c, http.StatusOK, returnData, config.SuccessMessage)
		return
	}
}

/*
Method := POST
 match_id 		(string - Required)
 is_active 		(string - Required)
*/
var UpdateMatchStatus = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		if userInfo.UserTypeId != 1 {
			util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
			return
		}
		var match models.Match
		err := json.NewDecoder(c.Request.Body).Decode(&match)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if match.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Id Required")
			return
		}

		if match.IsActive == "1" || match.IsActive == "0" {
			message := services.UpdateMatchStatus(match)
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Status")
			return
		}
	}
}

/*
Method := GET
*/
var GetSportsListForMenu = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }
		search := c.Request.URL.RawQuery
		returnData, err := services.GetSportsListForMenu(search)
		if err != nil {
			util.ErrorResponse(c, http.StatusOK, nil, config.RecordsEmptyMessage)
			return
		}

		util.SuccessResponse(c, http.StatusOK, returnData, config.SuccessMessage)
		return
	}
}

// /*
// Method := GET
// */
// var GetSportsMatchCountForMenu = func() func(c *gin.Context) {
// 	return func(c *gin.Context) {
// 		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
// 		// if tokenErr {
// 		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
// 		// 	return
// 		// }

// 		returnData, err := services.GetSportsSeriesMatchesCount()
// 		if err != nil {
// 			util.ErrorResponse(c, http.StatusOK, nil, config.RecordsEmptyMessage)
// 			return
// 		}

// 		util.SuccessResponse(c, http.StatusOK, returnData, config.SuccessMessage)
// 		return
// 	}
// }

/*
METHOD: POST
 match_id 			 (string - Required)
 user_id 			 (number - Required)
 user_type_id		 (number - Required)
 is_show_100_percent (number - Optional int, 0/1, default 0)
 market_id 			 (string - Optional)
*/
var GetMatchDetails = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		type inputStruct struct {
			MatchId string `json:"match_id"`
			// UserId           int64  `json:"user_id"`
			// UserTypeId       int64  `json:"user_type_id"`
			// IsShow100Percent int64  `json:"is_show_100_percent"`
			// MarketId         string `json:"market_id"`
		}

		var inputData inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.AccessErrorMessage)
			return
		}
		if inputData.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Id Is Required")
			return
		}
		// returnData, status := services.GetMatchDetail(inputData.MatchId, inputData.UserId, inputData.UserTypeId, inputData.IsShow100Percent, inputData.MarketId)
		// returnData, status := services.GetMatchDetail(inputData.MatchId, userInfo.UserId, userInfo.UserTypeId)
		returnData, status := services.GetMatchDetail(inputData.MatchId, 3, 9)

		if status {
			// returnData.IsActiveParent = true
			// returnData.SelfActive = true
			// parentDeActiveCount := services.GetMatchStatusValidation(inputData.MatchId, userInfo.ParentIds)
			// if parentDeActiveCount > 0 {
			// 	returnData.IsActiveParent = false
			// }
			// selfCount := services.GetMatchStatusCountUserWise(inputData.MatchId, userInfo.UserId)
			// if selfCount > 0 {
			// 	returnData.SelfActive = false
			// }
			// maxStack := services.GetMaxStackUserWise(inputData.MatchId, userInfo.ParentIds+strconv.Itoa(int(userInfo.UserId))+",", 1)
			// if maxStack < returnData.MaxStack && maxStack > 0 {
			// 	returnData.MaxStack = maxStack
			// }
			util.SuccessResponse(c, http.StatusOK, returnData, config.SuccessMessage)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Not Available")
			return
		}
	}
}

/*
METHOD: POST
 sport_id  			(string - Optional)
 series_id  		(string - Optional)
 in_play_page  		(number - Optional)
 is_cup 			(string - Optional, values("0"/"1"))
*/
var GetHomeMatches = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		type inputStruct struct {
			SportId    string `json:"sport_id"`
			SeriesId   string `json:"series_id"`
			InPlayPage int64  `json:"in_play_page"`
			IsCup      string `json:"is_cup"`
		}

		var inputData inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.AccessErrorMessage)
			return
		}
		returnData := services.HomePageMatches(inputData.SportId, inputData.SeriesId, tokenData.UserId, tokenData.UserTypeId, inputData.InPlayPage, inputData.IsCup)

		if len(returnData) > 0 {
			util.SuccessResponse(c, http.StatusOK, returnData, config.SuccessMessage)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, config.RecordsEmptyMessage)
			return
		}
	}
}

/*
match_id 				(string - Required)
min_stack 				(number - Required)
max_stack 				(number - Required)
score_key 				(string - Required)
score_type 				(string - Required,"0"/"1")
tv_url 					(string - Optional)
session_min_stack 		(number - Required)
session_max_stack 		(number - Required)
bookmaker_min_stack 	(number - Required)
bookmaker_max_stack 	(number - Required)
*/
var UpdateMatchDetails = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		if userInfo.UserTypeId != 1 && userInfo.UserTypeId != 2 {
			util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
			return
		}

		var inputData models.InputMatchSetting
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.AccessErrorMessage)
			return
		}

		if inputData.MatchId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Id Is Required")
			return
		}
		if inputData.ScoreType == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Score Type Is Required")
			return
		}
		if inputData.ScoreKey == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Score Key Is Required")
			return
		}

		// if inputData.TvUrl == "" {
		// 	util.ErrorResponse(c, http.StatusOK,nil, "Tv Url Is Required")
		// 	return
		// }
		if inputData.MinStack < 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Min Limit Is Required")
			return
		}
		if inputData.MaxStack < 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Max Limit Is Required")
			return
		}

		if inputData.SessionMinStack < 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Session Min Limit Is Required")
			return
		}

		if inputData.SessionMaxStack < 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Session Max Limit Is Required")
			return
		}
		if inputData.BookmakerMinStack < 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Bookmaker Min Limit Is Required")
			return
		}
		if inputData.BookmakerMaxStack < 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Bookmaker Max Limit Is Required")
			return
		}
		if inputData.ScoreType == "1" || inputData.ScoreType == "0" {
			message, status := services.UpdateMatchDetails(inputData)
			if status {
				util.SuccessResponse(c, http.StatusOK, nil, message)
				return
			} else {
				util.ErrorResponse(c, http.StatusOK, nil, message)
				return
			}
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Score Type")
			return
		}
	}
}

/*
METHOD: GET
*/
var GetUpcomingFixtures = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		returnData := services.GetUpcomingFixtures()

		if len(returnData) > 0 {
			util.SuccessResponse(c, http.StatusOK, returnData, config.SuccessMessage)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, config.RecordsEmptyMessage)
			return
		}
	}
}

var GetScoreMatches = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineScoreMatchesURL)
		if statusCode != 200 || err != nil {
			log.Println("GetScoreMatches >> statuscode >> ", statusCode, ": error >> ", err.Error())
		}
		var curlData interface{}
		_ = json.Unmarshal(content, &curlData)
		if curlData != nil {
			util.SuccessResponse(c, http.StatusOK, curlData, config.SuccessMessage)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, "Matches Not Available")
			return
		}
	}
}

/*
Method := GET
*/
var MatchScore = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }
		matchID := c.Request.URL.RawQuery
		if matchID == "" {
			util.ErrorResponse(c, http.StatusBadRequest, nil, "Match id is required")
			return
		}

		content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineMatchScoreURL + matchID)
		if statusCode != 200 || err != nil {
			log.Println("MatchScore >> statuscode >> ", statusCode, ": error >> ", err.Error())
		}
		var curlData interface{}
		_ = json.Unmarshal(content, &curlData)
		if curlData != nil {
			util.SuccessResponse(c, http.StatusOK, curlData, config.SuccessMessage)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, "Match Score Not Available")
			return
		}
	}
}

/*
METHOD := POST
{
 sport_id 		(string - Required)
 series_id		(string - Required)
}
*/
var CreateMatchesAuto = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }
		// if userInfo.UserTypeId != 1 {
		// 	util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
		// 	return
		// }
		var matchInput models.Match
		var matchesData []models.Match
		err := json.NewDecoder(c.Request.Body).Decode(&matchInput)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if matchInput.SportId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Sport Id Required")
			return
		}
		if matchInput.SeriesId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Series Id Required")
			return
		}

		matches, err := services.GetSportSeriesMatches(matchInput.SportId, matchInput.SeriesId)
		if err != nil {
			util.ErrorResponse(c, http.StatusNotFound, nil, config.RecordsEmptyMessage)
			return
		}

		matchSettings, _ := services.GetMatchSettingInfo(matchInput.SportId)
		for _, match := range matches {
			var matchData models.Match
			if matchInput.SportId == "1" {
				matchData.MinStack = float64(matchSettings.MarketMinBet)
				matchData.MaxStack = float64(matchSettings.MarketMaxBet)
			} else if matchInput.SportId == "2" {
				matchData.MinStack = float64(matchSettings.MarketMinBet)
				matchData.MaxStack = float64(matchSettings.MarketMaxBet)
			} else {
				matchData.MinStack = float64(matchSettings.MarketMinBet)
				matchData.MaxStack = float64(matchSettings.MarketMaxBet)
				matchData.SessionMinStack = float64(matchSettings.SessionMinBet)
				matchData.SessionMaxStack = float64(matchSettings.SessionMaxBet)
			}
			matchData.StartDate = global.ConvertUTCtoIST(match.Event.OpenDate.Format("2006-01-02T15:04:05Z"))
			matchData.ScoreType = "1"
			matchData.ScoreKey = "0"
			matchData.IsActive = "1"
			matchData.LiabilityType = "0"
			matchData.MatchId = match.Event.ID
			matchData.Name = match.Event.Name
			matchData.MatchDate = match.Event.OpenDate.Format("2006-01-02T15:04:05Z")
			matchData.Added = match.Added

			matchesData = append(matchesData, matchData)
		}

		status, message := services.CreateMatchesAuto(matchInput.SeriesId, matchesData)
		if status {
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}
	}
}

var ScoreboardMatchesKeyUpdate = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineScoreMatchesURL)
		if statusCode != 200 || err != nil {
			log.Println("GetScoreMatches >> statuscode >> ", statusCode, ": error >> ", err.Error())
		}

		// var curlData interface{}
		var curlDataInput models.ScoreboardData
		_ = json.Unmarshal(content, &curlDataInput)
		for _, curlData := range curlDataInput.Data {
			dateArr := strings.Split(curlData.MatchDate, " at ")
			if len(dateArr) > 0 {
				timeArr := strings.Split(dateArr[1], "-")
				if len(timeArr) > 0 {
					t1, err := time.Parse("02-Jan-2006", dateArr[0])
					if err != nil {
						log.Println(err.Error())
					}
					tdate := t1.Format("2006-01-02")
					timeStr := ""
					timeStr = strings.Replace(timeArr[0], "PM", ":00PM", -1)
					timeStr = strings.Replace(timeStr, "AM", ":00AM", -1)
					t, err := time.Parse("15:04:05PM", timeStr)
					if err != nil {
						log.Println(err.Error())
					}
					curlData.MatchDate = tdate + " " + t.Format("15:04:05")
				}
				status := services.UpdateScoreboardMatchKey(curlData)
				if !status {
					util.ErrorResponse(c, http.StatusOK, nil, "Currently unable to update Score Key")
					return
				}
			}

		}
		util.SuccessResponse(c, http.StatusOK, nil, config.SuccessMessage)
		return
	}
}
