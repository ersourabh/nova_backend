package controllers

import (
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/services"
	"novaexchbackend/api/util"
	"strings"

	"github.com/gin-gonic/gin"
)

// GetSports:
var GetSports = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if userInfo.UserTypeId != 1 {
		// 	util.ErrorResponse(c, http.StatusOK, "", "Invalid Request, Please Try Again With Correct Information")
		// 	return
		// }

		sports, err := services.GetSports()
		if err != nil {
			util.ErrorResponse(c, http.StatusNotFound, nil, config.RecordsEmptyMessage)
			return
		}

		util.SuccessResponse(c, http.StatusOK, sports, config.SuccessMessage)
		return
	}
}

// GetSportSeries:
var GetSportSeries = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if userInfo.UserTypeId != 1 {
		// 	util.ErrorResponse(c, http.StatusOK, "", "Invalid Request, Please Try Again With Correct Information")
		// 	return
		// }

		type Competition struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		}

		type SportSeries struct {
			Competition       Competition `json:"competition"`
			MarketCount       int         `json:"marketCount"`
			CompetitionRegion string      `json:"competitionRegion"`
		}
		//var inputReq []SportSeries
		sportID := c.Request.URL.RawQuery //r.URL.Query().Get("sportID")
		if sportID == "" {
			util.ErrorResponse(c, http.StatusBadRequest, nil, "Sport id is required")
			return
		}

		series, err := services.GetSportSeries(sportID)
		if err != nil {
			util.ErrorResponse(c, http.StatusNotFound, nil, config.RecordsEmptyMessage)
			return
		}

		util.SuccessResponse(c, http.StatusOK, series, config.SuccessMessage)
		return
	}
}

// GetSportSeriesMatches:
var GetSportSeriesMatches = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if userInfo.UserTypeId != 1 {
		// 	util.ErrorResponse(c, http.StatusOK, "", "Invalid Request, Please Try Again With Correct Information")
		// 	return
		// }

		queryParams := strings.Split(c.Request.URL.RawQuery, "&")
		if len(queryParams) != 2 {
			util.ErrorResponse(c, http.StatusBadRequest, nil, "Invalid query params required match id and series id")
			return
		}
		sportID := queryParams[0]  //r.URL.Query().Get("matchID")
		seriesID := queryParams[1] //r.URL.Query().Get("seriesID")

		if sportID == "" {
			util.ErrorResponse(c, http.StatusBadRequest, nil, "Sport id is required")
			return
		}
		if seriesID == "" {
			util.ErrorResponse(c, http.StatusBadRequest, nil, "Series id is required")
			return
		}

		matches, err := services.GetSportSeriesMatches(sportID, seriesID)
		if err != nil {
			util.ErrorResponse(c, http.StatusNotFound, nil, config.RecordsEmptyMessage)
			return
		}

		util.SuccessResponse(c, http.StatusOK, matches, config.SuccessMessage)
		return
	}
}

// GetSportSeriesMatchMarkets :
var GetSportSeriesMatchMarkets = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if userInfo.UserTypeId != 1 {
		// 	util.ErrorResponse(c, http.StatusOK, "", "Invalid Request, Please Try Again With Correct Information")
		// 	return
		// }

		matchID := c.Request.URL.RawQuery
		if matchID == "" {
			util.ErrorResponse(c, http.StatusBadRequest, nil, "Match id is required")
			return
		}

		markets, err := services.GetSportSeriesMatchMarkets(matchID)
		if err != nil {
			util.ErrorResponse(c, http.StatusNotFound, nil, config.RecordsEmptyMessage)
			return
		}

		util.SuccessResponse(c, http.StatusOK, markets, config.SuccessMessage)
		return
	}
}

// GetSportSeriesMatchSessions :
var GetSportSeriesMatchSessions = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if userInfo.UserTypeId != 1 {
		// 	util.ErrorResponse(c, http.StatusOK, "", "Invalid Request, Please Try Again With Correct Information")
		// 	return
		// }

		matchID := c.Request.URL.RawQuery
		if matchID == "" {
			util.ErrorResponse(c, http.StatusBadRequest, nil, "Match id is required")
			return
		}

		markets, err := services.GetSportSeriesMatchSessions(matchID)
		if err != nil {
			util.ErrorResponse(c, http.StatusNotFound, nil, config.RecordsEmptyMessage)
			return
		}

		util.SuccessResponse(c, http.StatusOK, markets, config.SuccessMessage)
		return
	}
}

// GetSportSeriesMatchBookmakerMarkets :
var GetSportSeriesMatchBookmakerMarkets = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if userInfo.UserTypeId != 1 {
		// 	util.ErrorResponse(c, http.StatusOK, "", "Invalid Request, Please Try Again With Correct Information")
		// 	return
		// }

		matchID := c.Request.URL.RawQuery
		if matchID == "" {
			util.ErrorResponse(c, http.StatusBadRequest, nil, "Match id is required")
			return
		}

		markets, err := services.GetSportSeriesMatchBookmakerMarkets(matchID)
		if err != nil {
			util.ErrorResponse(c, http.StatusNotFound, nil, config.RecordsEmptyMessage)
			return
		}

		util.SuccessResponse(c, http.StatusOK, markets, config.SuccessMessage)
		return
	}
}

// func GetOdds() {
// 	for {
// 		services.GetOnlineMarketOdds()
// 		fmt.Println("cron run ")
// 		time.Sleep(3 * time.Second)
// 	}
// }

/*
// GetMarketsOdds:
var GetMarketsOdds = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if userInfo.UserTypeId != 1 {
		// 	util.ErrorResponse(c, http.StatusOK, "", "Invalid Request, Please Try Again With Correct Information")
		// 	return
		// }

		marketIDs := c.Request.URL.RawQuery //r.URL.Query().Get("market_id")
		if marketIDs == "" {
			util.ErrorResponse(c, http.StatusBadRequest, "", "Market id's required")
			return
		}

		// var markets []models.InplayMarketData
		markets := services.GetMarketsOddsRedis(marketIDs)
		if len(markets) == 0 {
			util.ErrorResponse(c, http.StatusNotFound, "", "Market not found")
			return
		}

		util.SuccessResponse(c, http.StatusOK, markets, config.SuccessMessage)
		return
	}
}
*/
