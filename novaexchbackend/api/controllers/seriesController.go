package controllers

import (
	"encoding/json"
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"novaexchbackend/api/services"
	"novaexchbackend/api/util"

	"github.com/gin-gonic/gin"
)

/*
Method := POST
{
 sport_id		(string - Required)
 series_id		(string - Required)
 name			(string - Required)
}
*/
// CreateSeries :
var CreateSeries = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		// if userInfo.UserTypeId != 1 && userInfo.UserTypeId != 2 {
		// 	util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
		// 	return
		// }

		var series = models.Series{}
		err := json.NewDecoder(c.Request.Body).Decode(&series)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if series.SportId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Sport Id Required")
			return
		}
		if series.SeriesId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Series Id Required")
			return
		}
		if series.Name == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Name Required")
			return
		}

		message, status := services.CreateSeries(series)
		if status {
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}
	}
}

/*
Method := POST
{
 sport_id		(string - Optional)
 is_active 		(string - Optional)
 search      	(string - Optional)
}
*/
// GetSeries :
var GetSeries = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		_, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		type inputStruct struct {
			SportId  string `json:"sport_id"`
			IsActive string `json:"is_active"`
			Search   string `json:"search"`
		}

		var inputData inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		returnSeries := services.GetSeries(inputData.SportId, inputData.IsActive, inputData.Search)
		var resultData []map[string]interface{}
		for i := range returnSeries {
			returnData := map[string]interface{}{
				"name":      returnSeries[i].Name,
				"sport_id":  returnSeries[i].SportId,
				"series_id": returnSeries[i].SeriesId,
				"is_active": returnSeries[i].IsActive,
			}
			resultData = append(resultData, returnData)
		}
		util.SuccessResponse(c, http.StatusOK, resultData, config.SuccessMessage)
		return
	}
}

/*
Method := POST
{
 series_id 		(string - Required)
 is_active 		(string - Required)0/1
}
*/
// UpdateSeriesStatus :
var UpdateSeriesStatus = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		_, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		// if userInfo.UserTypeId != 1 && userInfo.UserTypeId != 2 {
		// 	util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
		// 	return
		// }
		var series models.Series
		err := json.NewDecoder(c.Request.Body).Decode(&series)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if series.SeriesId == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Series Id Required")
			return
		}
		if series.IsActive == "1" || series.IsActive == "0" {
			message := services.UpdateSeriesStatus(series)
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Status")
			return
		}
	}
}
