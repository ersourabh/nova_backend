package controllers

import (
	"encoding/json"
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"novaexchbackend/api/services"
	"novaexchbackend/api/util"

	"github.com/gin-gonic/gin"
)

/*
Method := POST
{
 is_active 		(string - Optional)0/1
}
*/
// GetSportsDB :
var GetSportsDB = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }

		var sport models.Sport
		err := json.NewDecoder(c.Request.Body).Decode(&sport)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		returnSport := services.GetSportsList(sport.IsActive)
		util.SuccessResponse(c, http.StatusOK, returnSport, config.SuccessMessage)
		return
	}
}

/*
Method := POST
{
 sport_id	(string - Required)
 is_active	(string - Required)0/1
}
*/
// UpdateSportStatus :
var UpdateSportStatus = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		_, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		// if userInfo.UserTypeId != 1 && userInfo.UserTypeId != 2 {
		// 	util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
		// 	return
		// }

		var sport models.Sport
		err := json.NewDecoder(c.Request.Body).Decode(&sport)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		if sport.IsActive == "1" || sport.IsActive == "0" {
			message := services.UpdateSportStatus(sport)
			util.SuccessResponse(c, http.StatusOK, nil, message)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Status")
			return
		}
	}
}
