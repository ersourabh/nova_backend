package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"novaexchbackend/api/services"
	"novaexchbackend/api/util"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

/*
Method:POST
{
 user_name 		(string - Required)
 password  		(string - Required)
}
*/
// Login :
var Login = func() func(c *gin.Context) {
	return func(c *gin.Context) {

		ipAddress := global.GetIpAddress(c.Request)
		userAgent := c.GetHeader("User-Agent")
		type inputStruct struct {
			UserName string `json:"user_name"`
			Password string `json:"password"`
		}
		var user inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&user)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if user.UserName == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "User name required")
			return
		}
		if user.Password == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Password required")
			return
		}

		ReturnData, message, status := services.VerifyUserLogin(user.UserName, user.Password)
		if status {
			var tokenData models.TokenData
			tokenData.ParentId = ReturnData.ParentId
			tokenData.UserId = ReturnData.Id
			tokenData.UserTypeId = ReturnData.UserTypeId
			tokenData.Name = ReturnData.Name
			tokenData.UserName = ReturnData.UserName
			tokenData.LoginDateTime = global.GetCurrentDateTime()
			tokenData.IpAddress = ipAddress
			tokenData.Mobile = ReturnData.Mobile
			tokenString := services.EncodeToken(tokenData)

			var finalData models.LoginData
			finalData.Name = ReturnData.Name
			finalData.UserName = ReturnData.UserName
			finalData.Name = ReturnData.Name
			finalData.UserTypeId = ReturnData.UserTypeId
			finalData.UserId = ReturnData.Id
			finalData.Mobile = ReturnData.Mobile
			finalData.City = ReturnData.City
			finalData.Token = tokenString

			// Redis store for token
			cacheKey := "login:" + strconv.Itoa(int(ReturnData.Id)) + "_" + tokenString
			isRedisSet := config.RedisSet(cacheKey, tokenString)
			if !isRedisSet {
				util.ErrorResponse(c, http.StatusOK, nil, "Unable to Login")
				return
			}

			c.SetCookie("login_token", tokenString, 86400, "", "", true, true)
			c.Request.AddCookie(&http.Cookie{
				Name:    "login_token",
				Value:   tokenString,
				Expires: time.Now().Add(86400 * time.Second),
			})

			var logs models.LoginDataLog
			logs.UserId = tokenData.UserId
			logs.ParentIds = ReturnData.ParentIds
			logs.BrowserInfo = userAgent
			logs.IpAddress = ipAddress
			logs.UserName = tokenData.UserName
			logs.LoginTime = global.GetCurrentDateTime()
			logs.TimeStamp = global.GetCurrentTimeUnix()
			// log.Location = user.Location
			err = services.UserLoginLog(logs)
			if err != nil {
				log.Println("Unable to set login log with user_login_log")
				util.ErrorResponse(c, http.StatusOK, nil, "Unable to Login")
				return
			}
			util.SuccessResponse(c, http.StatusOK, finalData, config.SuccessMessage)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, message)
			return
		}
	}
}

/*
Method:POST
{
 user_id		(number - Required)
 token			(string - Required)
}
*/
// Logout :
var LogOut = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		_, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		var user models.LoginData
		err := json.NewDecoder(c.Request.Body).Decode(&user)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		UserData, err1 := services.GetUserDetailsByUserId(user.UserId)
		if err1 {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid User")
			return
		}
		if UserData == (models.User{}) {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid User")
			return
		}
		cacheKey := "login:" + strconv.Itoa(int(user.UserId)) + "_" + user.Token
		token := config.RedisGet(cacheKey)
		if token != user.Token && token != "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid User")
			return
		}
		if token != "" {
			_ = config.RedisDelete(cacheKey)
		}

		util.SuccessResponse(c, http.StatusOK, nil, "Logout Successfully")
		return
	}
}

/*
Method:POST
{
 user_name 		(string - Required)
 name 			(string - Required)
 password 		(string - Required)
 mobile 		(string - Required)
}
*/
// SignUp :
var SignUp = func() func(c *gin.Context) {
	return func(c *gin.Context) {

		var reqData models.User
		err := json.NewDecoder(c.Request.Body).Decode(&reqData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		userCount := services.ExistUser(reqData.UserName)
		if userCount > 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Username already exist")
			return
		}

		if reqData.UserName == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "User name Required")
			return
		}
		if reqData.Password == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Password Required")
			return
		}
		if reqData.Mobile == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Contact no Required")
			return
		}

		adminId := services.GetAdminId()
		if adminId == 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Unable to get Admin Id")
			return
		}
		ParentData, err1 := services.GetUserDetailsByUserId(adminId)
		if err1 {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Parent")
			return

		} else {
			reqData.ParentId = ParentData.Id
			reqData.ParentName = ParentData.Name
			reqData.UserTypeId = 9
			if ParentData.ParentIds == "" {
				reqData.ParentIds = "," + strconv.Itoa(int(ParentData.Id)) + ","
			} else {
				reqData.ParentIds = ParentData.ParentIds + strconv.Itoa(int(ParentData.Id)) + ","
			}

			message, status := services.CreateUser(reqData, models.User{})
			if status {
				util.SuccessResponse(c, http.StatusOK, nil, message)
				return
			} else {
				util.ErrorResponse(c, http.StatusOK, nil, message)
				return
			}
		}
	}
}

/*
Method:POST
{
 user_name 		(string - Required)
 name 			(string - Required)
 mobile 		(string - Required)
 city 			(string - Required)
 parent_id 		(number - Required)
 user_type_id 	(number - Required)
}
*/
// AddUser :
var AddUser = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		var reqData models.User
		err := json.NewDecoder(c.Request.Body).Decode(&reqData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		userCount := services.ExistUser(reqData.UserName)
		if userCount > 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Username already exist")
			return
		}

		if reqData.Name == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Name Required")
			return
		}
		if reqData.Mobile == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Contact no Required")
			return
		}
		if reqData.UserName == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "User name Required")
			return
		}
		if reqData.UserTypeId < 2 {
			util.ErrorResponse(c, http.StatusOK, nil, "User type id Required")
			return
		}
		if reqData.Password == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Password Required")
			return
		}
		if reqData.ConfirmPassword == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Confirm password Required")
			return
		}
		if reqData.ConfirmPassword != reqData.Password {
			util.ErrorResponse(c, http.StatusOK, nil, "Password and confirm password must be same")
			return
		}
		if reqData.AvailableChip < 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Credit Amount")
			return
		}
		if reqData.ParentId < 1 {
			util.ErrorResponse(c, http.StatusOK, nil, "Parent id Required")
			return
		}

		ParentData, err1 := services.GetUserDetailsByUserId(reqData.ParentId) //reqData.ParentId
		if err1 {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid Parent")
			return

		} else {
			if tokenData.UserId != reqData.ParentId && strings.Contains(ParentData.ParentIds, strconv.Itoa(int(reqData.ParentId))) {
				util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
				return
			}
			if ParentData.Partnership < reqData.Partnership || reqData.Partnership < 0 {
				util.ErrorResponse(c, http.StatusOK, nil, "Invalid partnership")
				return
			}
			reqData.ParentName = ParentData.Name
			reqData.ParentUserTypeId = ParentData.UserTypeId
			if ParentData.UserTypeId > reqData.UserTypeId {
				util.ErrorResponse(c, http.StatusOK, nil, "Invalid Request, Please Try Again With Correct Information")
				return
			}
			if ParentData.AvailableChip < reqData.AvailableChip {
				util.ErrorResponse(c, http.StatusOK, nil, "Insufficient Credit Limit")
				return
			}
			if ParentData.ParentIds == "" {
				reqData.ParentIds = "," + strconv.Itoa(int(ParentData.Id)) + ","
			} else {
				reqData.ParentIds = ParentData.ParentIds + strconv.Itoa(int(ParentData.Id)) + ","
			}
			if reqData.AvailableChip > 0 {
				reqData.CrDe = 1
			}

			message, status := services.CreateUser(reqData, ParentData)
			if status {
				util.SuccessResponse(c, http.StatusOK, nil, message)
				return
			} else {
				util.ErrorResponse(c, http.StatusOK, nil, message)
				return
			}
		}
	}
}

/*
Method:POST
{
 parent_id		(number - Required)
 page number	(number - Required)
}
*/
// GetChildDetailById :
var GetChildDetailById = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		// ipAddress := global.GetIpAddress(c.Request)
		// _, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		// if tokenErr {
		// 	util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
		// 	return
		// }
		var reqData models.User
		err := json.NewDecoder(c.Request.Body).Decode(&reqData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		if reqData.ParentId < 1 {
			util.ErrorResponse(c, http.StatusOK, nil, "Parent id Required")
			return
		}
		result, status := services.GetChildDetailById(reqData.ParentId)
		if !status {
			util.SuccessResponse(c, http.StatusOK, result, config.SuccessMessage)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, config.RecordsEmptyMessage)
			return
		}
	}
}

/*
Method:POST
{
 id 			(number - Required)
}
*/
// GetUserDetailById :
var GetUserDetailById = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		_, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		var reqData models.User
		err := json.NewDecoder(c.Request.Body).Decode(&reqData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		if reqData.Id < 1 {
			util.ErrorResponse(c, http.StatusOK, nil, "User id Required")
			return
		}

		result, status := services.GetUserDetailsByUserId(reqData.Id)
		if !status {
			util.SuccessResponse(c, http.StatusOK, result, config.SuccessMessage)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, config.RecordsEmptyMessage)
			return
		}
	}
}

/*
Method:GET
*/
var GetUserBalance = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		//settingData, _ := services.GetSettingInfo()
		ReturnData, err1 := services.GetUserBalance(userInfo.UserId)
		userBalanceData := map[string]interface{}{
			"id":             ReturnData.Id,
			"parent_id":      ReturnData.ParentId,
			"user_type_id":   ReturnData.UserTypeId,
			"name":           ReturnData.Name,
			"user_name":      ReturnData.UserName,
			"liability":      ReturnData.Exposure,
			"profit_loss":    ReturnData.ProfitLoss,
			"total_chip":     ReturnData.TotalChip,
			"available_chip": ReturnData.AvailableChip,
		}

		if err1 {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid User")
			return
		} else {
			util.SuccessResponse(c, http.StatusOK, userBalanceData, config.SuccessMessage)
			return
		}
	}
}

/*
Method:POST
{
 "id"               (number - Required)
 "user_type_id"     (number - Required)
 "old_password" 	(string - Required)
 "new_password"  	(string - Required)
 "confirm_password" (string - Required)
 "master_password" 	(string - Required)
*/
var UpdatePassword = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		// userAgent := c.GetHeader("User-Agent")
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		var user models.User
		err := json.NewDecoder(c.Request.Body).Decode(&user)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if user.NewPassword == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Old Password Is Required !")
			return
		}
		if user.ConfirmPassword == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "New Password Is Required!")
			return
		}
		if user.MasterPassword == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Transaction Password Is Required!")
			return
		}
		if user.OldPassword == "" && tokenData.UserTypeId == 1 {
			util.ErrorResponse(c, http.StatusOK, nil, "Old Password Is Required!")
			return
		}

		msg, status := services.GetTransactionPassword(tokenData.UserId, user.MasterPassword)
		if !status {
			util.ErrorResponse(c, http.StatusOK, nil, msg)
			return
		}
		// user.Id = tokenData.UserId
		// user.UserTypeId = tokenData.UserTypeId
		// user.PasswordChanged = tokenData.IsChangePassword
		ReturnData, status := services.UpdatePassword(user)
		if status {
			idstr := strconv.Itoa(int(tokenData.UserId))
			redisKeys := config.RedisGetAllLoginUser("login:" + idstr)
			for _, key := range redisKeys {
				cacheKey := "login:" + idstr + "_" + key
				_ = config.RedisDelete(cacheKey)
			}
			util.SuccessResponse(c, http.StatusOK, nil, ReturnData)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, ReturnData)
			return
		}
	}
}

/*
Method:POST
{
 new_password  			(string - Required)
 confirm_password  		(string - Required)
 master_password  		(string - Required)
}
*/
var ChangePassword = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		userAgent := c.GetHeader("User-Agent")
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		var user models.User
		err := json.NewDecoder(c.Request.Body).Decode(&user)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}

		if user.MasterPassword == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Master Password Is Required")
			return
		}
		if user.NewPassword == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Password Is Required")
			return
		} else {
			user.ConfirmPassword = user.NewPassword
		}
		// if user.ConfirmPassword == "" {
		// 	util.ErrorResponse(c, http.StatusOK, nil, "Confirm Password Is Required")
		// 	return
		// }

		changePasswordStatus := services.CheckForChangePasswordLocked(tokenData.UserId)
		if changePasswordStatus {
			util.ErrorResponse(c, http.StatusOK, nil, "Change password lock is enabled for User, Cannot change password currently")
			return
		}

		user.Id = tokenData.UserId
		returnMsg, tempTransactionPassword, status := services.ChangePassword(user)
		if status {
			var logs models.LoginDataLog
			logs.UserId = tokenData.UserId
			logs.ParentIds = tokenData.ParentIds
			logs.BrowserInfo = userAgent
			logs.IpAddress = ipAddress
			logs.UserName = tokenData.UserName
			logs.PasswordChangeTime = global.GetCurrentDateTime()
			logs.TimeStamp = global.GetCurrentTimeUnix()
			// log.Location = user.Location
			err = services.UserChangePasswordLog(logs)
			if err != nil {
				log.Println("Unable to set change password log with user_change_password_log")
				util.ErrorResponse(c, http.StatusOK, nil, "Unable to Login")
				return
			}

			data := map[string]string{
				"transaction_password": tempTransactionPassword,
			}
			idstr := strconv.Itoa(int(user.Id))
			redisKeys := config.RedisGetAllLoginUser("login:" + idstr)
			for _, key := range redisKeys {
				cacheKey := "login:" + idstr + "_" + key
				_ = config.RedisDelete(cacheKey)
			}
			util.SuccessResponse(c, http.StatusOK, data, returnMsg)
			return
		} else {
			util.ErrorResponse(c, http.StatusOK, nil, returnMsg)
			return
		}
	}
}

/*
Method:POST
{
 user_id 			(number - Required)
 user_type_id 		(number - Required)
 master_password 	(string - Required)
 user_status 		(string - Required)
 bet_status 		(string - Required)
}
*/
var UpdateUserAndBetStatus = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}

		type inputStruct struct {
			UserId         int64  `json:"user_id"`
			UserTypeId     int64  `json:"user_type_id"`
			UserStatus     string `json:"user_status"`
			BetStatus      string `json:"bet_status"`
			MasterPassword string `json:"master_password"`
		}

		var inputData inputStruct
		err := json.NewDecoder(c.Request.Body).Decode(&inputData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		if inputData.UserId == 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "User id is required")
			return
		}
		if inputData.UserTypeId == 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "User type id is required")
			return
		}
		if inputData.UserStatus == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "User status Is required")
			return
		}
		if inputData.BetStatus == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Bet status is required")
			return
		}
		if inputData.MasterPassword == "" {
			util.ErrorResponse(c, http.StatusOK, nil, "Master password is required")
			return
		}

		msg, status := services.GetTransactionPassword(tokenInfo.UserId, inputData.MasterPassword)
		if !status {
			util.ErrorResponse(c, http.StatusOK, nil, msg)
			return
		}

		parentResult, _ := services.GetUserDetailsByUserId(tokenInfo.UserId)
		if parentResult.BetLock == "1" {
			util.ErrorResponse(c, http.StatusOK, nil, "Parent already locked for betting")
			return
		}
		if parentResult.UserLock == "1" {
			util.ErrorResponse(c, http.StatusOK, nil, "Parent already locked")
			return
		}

		ReturnData, err1 := services.UpdateUserAndBetStatus(inputData.UserId, inputData.UserTypeId, tokenInfo.UserTypeId, tokenInfo.UserId, inputData.UserStatus, inputData.BetStatus, tokenInfo.UserName)
		if err1 {
			util.ErrorResponse(c, http.StatusOK, ReturnData, "Invalid User")
			return
		} else {

			if inputData.UserStatus == "0" {
				tokenInfo.UserId = inputData.UserId
				allUserData := services.GetAllUser(tokenInfo)
				if len(allUserData) > 0 {
					for _, userData := range allUserData {
						cacheKey := "login:" + strconv.Itoa(int(userData.UserId)) + "_" + userData.Token
						_ = config.RedisDelete(cacheKey)
					}
				}
			}
			util.SuccessResponse(c, http.StatusOK, ReturnData, config.SuccessMessage)
			return
		}
	}
}

/*
METHOD: POST
{
 id 						(number - Required)
 name						(string - Optional)
 mobile 					(string - Optional)
 is_active 					(string - Optional)	0= active, 1= inactive
 market_commission  		(number - Optional)
 session_commission 		(number - Optional)
 bookmaker_commission 		(number - Optional)
 market_delay 				(number - Optional)
 session_delay 				(number - Optional)
 bookmaker_delay 			(number - Optional)
 is_change_password_lock    (string - Optional)
 master_password    		(string - Required)
}
*/
var UpdateUserDetails = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		parentInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		// ipAddressArr := strings.Split(r.Header.Get("X-Forwarded-For"), ",")
		// var ipAddress string
		// if len(ipAddress) > 0 {
		// 	ipAddress = ipAddressArr[0]
		// }
		// if ipAddress == "" {
		// 	ipAddress = strings.Split(r.RemoteAddr, ":")[0]
		// }
		// ipAddress := global.GetIpAddress(r)
		var user models.User
		err := json.NewDecoder(c.Request.Body).Decode(&user)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		if user.Id == 0 {
			util.ErrorResponse(c, http.StatusOK, nil, "User Id Is Required")
			return
		}
		// if user.Name == "" {
		// 	util.ErrorResponse(c, http.StatusOK, nil, "Name Is Required")
		// 	return
		// }

		msg, status := services.GetTransactionPassword(parentInfo.UserId, user.MasterPassword)
		if !status {
			util.ErrorResponse(c, http.StatusOK, nil, msg)
			return
		} else {

			UserData, UserErr := services.GetUserDetailsByUserId(user.Id)
			if UserErr {
				util.ErrorResponse(c, http.StatusOK, nil, "User Not Found")
				return
			}
			isValidParent := false
			parentIdsArr := strings.Split(global.TrimCommaBothSide(UserData.ParentIds), ",")
			for _, idStr := range parentIdsArr {
				if idStr == strconv.Itoa(int(parentInfo.UserId)) {
					isValidParent = true
					break
				}
			}
			if !isValidParent {
				util.ErrorResponse(c, http.StatusOK, nil, "Invalid Parent")
				return
			}
			// isValid := services.ValidateUpdateUserSetting(UserData.UserTypeId, UserData.Id)
			// if !isValid {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "In Running Match User Setting Update Not Allowed")
			// 	return
			// }
			ParentData, _ := services.GetUserDetailsByUserId(UserData.ParentId)
			if UserData.UserTypeId == 1 {
				util.ErrorResponse(c, http.StatusOK, nil, "Invalid User Id")
				return
			}

			// if user.CricketPartnership > ParentData.CricketPartnership {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Partnership Allowed Is : "+fmt.Sprintf("%v", ParentData.CricketPartnership))
			// 	return
			// } else if user.SoccerPartnership > ParentData.SoccerPartnership {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Partnership Allowed Is : "+fmt.Sprintf("%v", ParentData.SoccerPartnership))
			// 	return
			// } else if user.TennisPartnership > ParentData.TennisPartnership {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Partnership Allowed Is : "+fmt.Sprintf("%v", ParentData.TennisPartnership))
			// 	return
			// }

			// if user.CricketCommission > ParentData.CricketCommission {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Commission Allowed Is : "+fmt.Sprintf("%v", ParentData.CricketCommission))
			// 	return
			// } else if user.SoccerCommission > ParentData.SoccerCommission {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Commission Allowed Is : "+fmt.Sprintf("%v", ParentData.SoccerCommission))
			// 	return
			// } else if user.TennisCommission > ParentData.TennisCommission {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Commission Allowed Is : "+fmt.Sprintf("%v", ParentData.TennisCommission))
			// 	return
			// }

			// if user.CricketMinBet < ParentData.CricketMinBet {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Commission Allowed Is : "+fmt.Sprintf("%v", ParentData.CricketMinBet))
			// 	return
			// } else if user.SoccerMinBet < ParentData.SoccerMinBet {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Commission Allowed Is : "+fmt.Sprintf("%v", ParentData.SoccerMinBet))
			// 	return
			// } else if user.TennisMinBet < ParentData.TennisMinBet {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Commission Allowed Is : "+fmt.Sprintf("%v", ParentData.TennisMinBet))
			// 	return
			// }

			// if user.CricketMaxBet > ParentData.CricketMaxBet {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Commission Allowed Is : "+fmt.Sprintf("%v", ParentData.CricketMaxBet))
			// 	return
			// } else if user.SoccerMaxBet > ParentData.SoccerMaxBet {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Commission Allowed Is : "+fmt.Sprintf("%v", ParentData.SoccerMaxBet))
			// 	return
			// } else if user.TennisMaxBet > ParentData.TennisMaxBet {
			// 	util.ErrorResponse(c, http.StatusOK, nil, "Maximum Commission Allowed Is : "+fmt.Sprintf("%v", ParentData.TennisMaxBet))
			// 	return
			// }

			returnMsg, updateErr := services.UpdateUserDetails(UserData, ParentData, user, parentInfo.Name)
			if updateErr {
				util.ErrorResponse(c, http.StatusOK, nil, returnMsg)
				return
			}
			util.SuccessResponse(c, http.StatusOK, nil, returnMsg)
			return
		}
	}
}

/*
Method:POST
{
 page        (number - Required)
 log_type 	 (string - Required : L=login,P=change password)
}
*/
// GetUserLogs :
var GetUserLogs = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		tokenData, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		if int(tokenData.UserTypeId) == config.USER {
			util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
			return
		}
		type input struct {
			Page    int64  `json:"page"`
			UserId  int64  `json:"user_id"`
			LogType string `json:"log_type"`
		}
		var reqData input
		err := json.NewDecoder(c.Request.Body).Decode(&reqData)
		if err != nil {
			util.ErrorResponse(c, http.StatusBadRequest, nil, config.InvalidJsonMessage)
			return
		}
		if reqData.LogType != "L" && reqData.LogType != "P" {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid log type")
			return
		}

		reqData.UserId = tokenData.UserId
		count, limit, returnData := services.GetUserLogs(reqData.Page, reqData.UserId, reqData.LogType)
		data := map[string]interface{}{
			"total":    count,
			"limit":    limit,
			"log_data": returnData,
		}
		util.SuccessResponse(c, http.StatusOK, data, config.SuccessMessage)
		return
	}
}

/*
Method:GET
*/
var AdminDashboard = func() func(c *gin.Context) {
	return func(c *gin.Context) {
		ipAddress := global.GetIpAddress(c.Request)
		userInfo, tokenMsg, tokenErr := global.VerifyUserToken(c.Request.Header.Get("Authorization"), ipAddress)
		if tokenErr {
			util.ErrorResponse(c, http.StatusForbidden, nil, tokenMsg)
			return
		}
		if int(userInfo.UserTypeId) == config.USER {
			util.ErrorResponse(c, http.StatusOK, nil, config.AccessErrorMessage)
			return
		}

		returnData, err1 := services.DashboardData(userInfo.UserId)
		dashboardData := map[string]interface{}{
			"id":             returnData.Id,
			"parent_id":      returnData.ParentId,
			"user_type_id":   returnData.UserTypeId,
			"name":           returnData.Name,
			"user_name":      returnData.UserName,
			"exposure":       returnData.Exposure,
			"profit_loss":    returnData.ProfitLoss,
			"total_chip":     returnData.TotalChip,
			"available_chip": returnData.AvailableChip,
			"credited_chip":  returnData.CreditedChip,
		}
		if err1 {
			util.ErrorResponse(c, http.StatusOK, nil, "Invalid User")
			return
		} else {
			util.SuccessResponse(c, http.StatusOK, dashboardData, config.SuccessMessage)
			return
		}
	}
}
