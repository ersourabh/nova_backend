package global

import (
	cryptorand "crypto/rand"
	"errors"
	"fmt"
	"math/big"
	"net"
	"net/http"
	"novaexchbackend/api/config"
	"novaexchbackend/api/models"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func GetCurrentDateTime() string {
	currentTimeVal := time.Now()
	return currentTimeVal.Format("2006-01-02 15:04:05")
}
func GetCurrentTimeUnix() int64 {
	return time.Now().Unix()
}
func ConvertUTCtoIST(value string) string {
	t, _ := time.Parse("2006-01-02T15:04:05Z", value)
	loc, _ := time.LoadLocation("Asia/Kolkata")
	newTime := t.In(loc)
	return newTime.Format("2006-01-02 15:04:05")
}

func FindMin(value []float64) (min float64) {
	min = value[0]
	for _, value := range value {
		if value < min {
			min = value
		}
	}
	return min
}

func FindMax(value []float64) (max float64) {
	max = value[0]
	for _, value := range value {
		if value > max {
			max = value
		}
	}
	return max
}

// RandNumString - Generates a numeric string of specified length
func RandNumString(length int) string {
	result := makeRandomString(length, "number")
	return result
}

// makeRandomString : Make the randon string of certain length
func makeRandomString(strlen int, stringType string) string {

	//rand.Seed(time.Now().UTC().UnixNano())
	var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

	switch strings.ToLower(stringType) {
	case "number":
		chars = "1234567890"
		break
	}

	result := make([]byte, strlen)
	for i := 0; i < strlen; i++ {
		max := *big.NewInt(int64(len(chars)))
		n, err := cryptorand.Int(cryptorand.Reader, &max)
		if err != nil {
			fmt.Println(err)
		}

		result[i] = chars[int(n.Int64())]
		//result[i] = chars[rand.Intn(len(chars))]
	}
	return string(result)
}

func TrimCommaBothSide(str string) string {
	newStr := strings.TrimLeft(str, ",")
	return strings.TrimRight(newStr, ",")
}

func GetIpAddress(r *http.Request) string {
	//Get IP from RemoteAddr
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	cfIP := r.Header.Get("Cf-Connecting-Ip")
	netCFIP := net.ParseIP(cfIP)
	if netCFIP != nil {
		return cfIP
	}
	//Get IP from the X-REAL-IP header
	ipXreal := r.Header.Get("X-REAL-IP")
	netIP := net.ParseIP(ipXreal)
	if netIP != nil {
		return ipXreal
	}

	//Get IP from X-FORWARDED-FOR header
	ips := r.Header.Get("X-FORWARDED-FOR")
	splitIps := strings.Split(ips, ",")
	for _, ipSingle := range splitIps {
		netIP := net.ParseIP(ipSingle)
		if netIP != nil {
			return ipSingle
		}
	}
	return ip

}

func VerifyUserToken(authHeader, ipAddress string) (userInfo models.TokenData, tokenMsg string, tokenErr bool) {
	hmacSecret := []byte{97, 48, 97, 50, 97, 98, 105, 49, 99, 102, 83, 53, 57, 98, 52, 54, 97, 102, 99, 12, 12, 13, 56, 34, 23, 16, 78, 67, 54, 34, 32, 21}
	//authHeader := c.Request.Header.Get("Authorization")
	//userInfo := models.TokenData{}
	if authHeader == "" {
		//log.Println(errAuthHeaderNotFound)
		return userInfo, "Authorization Failed", true
	}

	tokenSlice := strings.Split(authHeader, " ")
	if len(tokenSlice) != 2 {
		return userInfo, "Authorization Failed", true
	}
	tokenString := tokenSlice[1]
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {

		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return userInfo, errors.New("token format is invalid")
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return hmacSecret, nil
	})
	if err != nil {
		return userInfo, "Authorization Failed", true
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok && !token.Valid {
		return userInfo, "Authorization Failed", true
	}
	userInfo.IpAddress = fmt.Sprintf("%v", claims["ip_address"])
	if userInfo.IpAddress != ipAddress {
		return userInfo, "Phishing in process >> Another IP detected", true
	}
	userTypeId := fmt.Sprintf("%v", claims["user_type_id"])
	userId := fmt.Sprintf("%v", claims["user_id"])
	userIdInt, _ := strconv.Atoi(userId)
	userInfo.UserId = int64(userIdInt)
	userTypeIdInt, _ := strconv.Atoi(userTypeId)
	userInfo.UserTypeId = int64(userTypeIdInt)
	parentId := fmt.Sprintf("%v", claims["parent_id"])
	parentIdInt, _ := strconv.Atoi(parentId)
	userInfo.ParentId = int64(parentIdInt)
	userInfo.Name = fmt.Sprintf("%v", claims["name"])
	userInfo.ParentIds = fmt.Sprintf("%v", claims["parent_ids"])
	userInfo.UserName = fmt.Sprintf("%v", claims["user_name"])
	userInfo.LoginDateTime = fmt.Sprintf("%v", claims["login_date_time"])
	userInfo.UserAgent = fmt.Sprintf("%v", claims["browser_info"])
	userInfo.IsChangePassword = fmt.Sprintf("%v", claims["is_change_password"])
	cacheKey := "login:" + strconv.Itoa(int(userInfo.UserId)) + "_" + tokenString
	tokenStr := config.RedisGet(cacheKey)
	if tokenStr == "" {
		return userInfo, "Authorization Failed", true
	}
	if tokenString != tokenStr {
		return userInfo, "Authorization Failed", true
	}
	// if userInfo.UserTypeId != 1 {
	// 	status := IsSiteUnderMaitenance()
	// 	if status {
	// 		return userInfo, "Site Under Maintenance", true
	// 	}
	// }
	return userInfo, "", false
}
