package models

import "database/sql"

type AccountStatement struct {
	UserId           int64   `json:"user_id"`
	ParentId         int64   `json:"parent_id"`
	StatementType    int64   `json:"statement_type"`
	CrDe             int64   `json:"cr_de,omitempty"`
	Amount           float64 `json:"amount"`
	AvailableBalance float64 `json:"available_balance"`
	Type             string  `json:"type"`
	Description      string  `json:"description"`
	MatchId          string  `json:"match_id,omitempty"`
	MarketId         string  `json:"market_id,omitempty"`
	CreatedAt        string  `json:"created_at,omitempty"`
	Password         string  `json:"password,omitempty"`
	MasterPassword   string  `json:"master_password,omitempty"`
	OperatorName     string  `json:"operator_name,omitempty"`
	UserName         string  `json:"user_name,omitempty"`
}

type ChipInOutResult struct {
	Id           int64   `json:"id"`
	UserId       int64   `json:"user_id"`
	UserName     string  `json:"user_name"`
	ParentId     int64   `json:"parent_id"`
	OperatorName string  `json:"operator_name"`
	Description  string  `json:"description"`
	CreatedAt    string  `json:"created_at"`
	Amount       float64 `json:"amount"`
}
type ChipInOutResultInput struct {
	Id           sql.NullInt64
	UserId       sql.NullInt64
	UserName     sql.NullString
	ParentId     sql.NullInt64
	OperatorName sql.NullString
	Description  sql.NullString
	CreatedAt    sql.NullString
	Amount       sql.NullFloat64
}
type UserAccountStatementHistory struct {
	Date         string  `json:"date"`
	Description  string  `json:"description"`
	CreditDebit  float64 `json:"credit_debit"`
	Balance      float64 `json:"balance"`
	OperatorName string  `json:"operator_name"`
	UserName     string  `json:"user_name"`
	MatchId      string  `json:"match_id"`
	MarketId     string  `json:"market_id"`
}
type UserAccountStatementHistoryInput struct {
	UserName     sql.NullString
	Date         sql.NullString
	Description  sql.NullString
	CreditDebit  sql.NullFloat64
	Balance      sql.NullFloat64
	OperatorName sql.NullString
	MatchId      sql.NullString
	MarketId     sql.NullString
}

type InputGetUserAccountStatementHistory struct {
	Page         int64  `json:"page"`
	AccountType  string `json:"account_type"`
	SportId      string `json:"sport_id"`
	Filter       string `json:"filter"`
	FromDate     string `json:"from_date"`
	ToDate       string `json:"to_date"`
	SearchClient string `json:"search_client"`
	Search       string `json:"search"`
	IsDownload   int64  `json:"is_download"`
}
