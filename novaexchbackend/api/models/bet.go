package models

import "database/sql"

type Bet struct {
	UserId        int64   `json:"user_id"`
	ParentIds     string  `json:"parentIds,omitempty"`
	UserName      string  `json:"user_name"`
	SportId       string  `json:"sport_id"`
	SportName     string  `json:"sport_name"`
	SeriesId      string  `json:"series_id"`
	SeriesName    string  `json:"series_name"`
	MatchId       string  `json:"match_id"`
	MatchName     string  `json:"match_name"`
	MarketId      string  `json:"market_id"`
	MarketName    string  `json:"market_name"`
	WinnerName    string  `json:"winner_name,omitempty"`
	WinnerId      string  `json:"winner_id,omitempty"`
	ResultDate    string  `json:"result_date"`
	MarketType    string  `json:"market_type"`
	IsBack        string  `json:"is_back"`
	SelectionId   string  `json:"selection_id"`
	SelectionName string  `json:"selection_name"`
	Stack         int64   `json:"stack"`
	Odds          float64 `json:"odds"`
	Size          float64 `json:"size"`
	Exposure      float64 `json:"exposure"`
	Profit        float64 `json:"profit"`
	Winloss       float64 `json:"winloss,omitempty"`
	IsDeleted     string  `json:"is_deleted,omitempty"`
	DeletedBy     string  `json:"deleted_by,omitempty"`
	CreatedAt     string  `json:"created_at"`
}
type BetInput struct {
	UserId        sql.NullInt64
	UserTypeId    sql.NullInt64
	UserName      sql.NullString
	ParentIds     sql.NullString
	SportName     sql.NullString
	SeriesId      sql.NullString
	SportId       sql.NullString
	SeriesName    sql.NullString
	MatchId       sql.NullString
	MatchName     sql.NullString
	MarketId      sql.NullString
	MarketName    sql.NullString
	WinnerName    sql.NullString
	WinnerId      sql.NullString
	ResultDate    sql.NullString
	MarketType    sql.NullString
	IsBack        sql.NullString
	SelectionId   sql.NullString
	SelectionName sql.NullString
	Stack         sql.NullInt64
	Odds          sql.NullFloat64
	Size          sql.NullFloat64
	Exposure      sql.NullFloat64
	Profit        sql.NullFloat64
	Winloss       sql.NullFloat64
	IsDeleted     sql.NullString
	DeletedBy     sql.NullString
	CreatedAt     sql.NullString
}
type CalculatedDataForMarketBet struct {
	IsMatched    string  `json:"is_matched"`
	MarketStatus string  `json:"market_status"`
	PL           float64 `json:"p_l"`
	Liability    float64 `json:"liability"`
}

//OutputBetHistory :
type OutputBetHistory struct {
	Name           string  `json:"name,omitempty"`
	UserName       string  `json:"user_name,omitempty"`
	BetType        string  `json:"bet_type,omitempty"`
	Placed         string  `json:"placed,omitempty"`
	MatchName      string  `json:"match_name,omitempty"`
	SelectionName  string  `json:"selection_name,omitempty"`
	MarketName     string  `json:"market_name,omitempty"`
	BetId          int64   `json:"bet_id,omitempty"`
	BetMatchedAt   string  `json:"bet_matched_at,omitempty"`
	IsBack         string  `json:"is_back,omitempty"`
	Odds           float64 `json:"odds,omitempty"`
	Stack          int64   `json:"stack,omitempty"`
	SportId        string  `json:"sport_id,omitempty"`
	Status         string  `json:"status,omitempty"`
	SportName      string  `json:"sport_name,omitempty"`
	MarketId       string  `json:"market_id,omitempty"`
	MatchId        string  `json:"match_id,omitempty"`
	Liability      float64 `json:"liability,omitempty"`
	ProfitLoss     float64 `json:"profit_loss"`
	DeleteStatus   string  `json:"delete_status"`
	IpAddress      string  `json:"ip_address"`
	SelectionId    string  `json:"selection_id,omitempty"`
	IsMatched      string  `json:"is_matched,omitempty"`
	DeviceInfo     string  `json:"device_info"`
	ParentUserName string  `json:"parent_user_name,omitempty"`
}

//BetHistoryInput :
type BetHistoryInput struct {
	Name            sql.NullString
	UserName        sql.NullString
	BetType         sql.NullString
	Placed          sql.NullString
	MatchName       sql.NullString
	SelectionName   sql.NullString
	MarketName      sql.NullString
	BetId           sql.NullInt64
	BetMatchedAt    sql.NullString
	IsBack          sql.NullString
	Odds            sql.NullFloat64
	Stack           sql.NullInt64
	SportId         sql.NullString
	Status          sql.NullString
	SportName       sql.NullString
	MarketId        sql.NullString
	MatchId         sql.NullString
	Liability       sql.NullFloat64
	PotentialProfit sql.NullFloat64
	ProfitLoss      sql.NullFloat64
	DeleteStatus    sql.NullString
	IpAddress       sql.NullString
	SelectionId     sql.NullString
	IsMatched       sql.NullString
	DeviceInfo      sql.NullString
	ParentUserName  sql.NullString
	Location        sql.NullString
}
type InputBetHistory struct {
	UserId     int64  `json:"user_id"`
	Page       int64  `json:"page"`
	BetType    string `json:"bet_type"`
	UserCode   string `json:"user_code"`
	IpAddress  string `json:"ip_address"`
	FromAmount int64  `json:"from_amount"`
	ToAmount   int64  `json:"to_amount"`
	IsBack     string `json:"is_back"`
	MatchId    string `json:"match_id"`
	Search     string `json:"search"`
	MarketId   string `json:"market_id"`
	Limit      int64  `json:"limit"`
	Offset     int64  `json:"offset"`
	SportId    string `json:"sport_id"`
	FromDate   string `json:"from_date"`
	ToDate     string `json:"to_date"`
}

//CurrentBet :
type CurrentBet struct {
	UserName      string  `json:"user_name,omitempty"`
	BetType       string  `json:"bet_type,omitempty"`
	Placed        string  `json:"placed,omitempty"`
	SelectionName string  `json:"selection_name,omitempty"`
	MarketName    string  `json:"market_name,omitempty"`
	BetId         int64   `json:"bet_id,omitempty"`
	IsBack        string  `json:"is_back,omitempty"`
	Odds          float64 `json:"odds,omitempty"`
	Stack         int64   `json:"stack"`
	MarketId      string  `json:"market_id,omitempty"`
	ProfitLoss    float64 `json:"profit_loss"`
	IsDeleted     string  `json:"is_deleted"`
}
type ValidateFancyBetOutput struct {
	UserId                 int64   `json:"user_id"`
	UserTypeId             int64   `json:"user_type_id"`
	ProfitFancy            float64 `json:"profit_fancy"`
	LiabilityFancy         float64 `json:"liability_fancy"`
	LiabilityForBalance    float64 `json:"liability_for_balance"`
	FancyScorePositionJson string  `json:"fancy_score_position_json"`
}
type BetsOdd struct {
	Id                int64   `json:"id"`
	UserId            int64   `json:"user_id"`
	ParentIds         string  `json:"parent_ids"`
	SportId           string  `json:"sport_id"`
	MatchId           string  `json:"match_id"`
	MarketId          string  `json:"market_id"`
	SelectionId       string  `json:"selection_id"`
	MarketName        string  `json:"market_name"`
	SelectionName     string  `json:"selection_name"`
	WinnerName        string  `json:"winner_name"`
	WinnerSelectionId string  `json:"winner_selection_id"`
	ResultDeclaredAt  string  `json:"result_declared_at"`
	Odds              float64 `json:"odds"`
	Stack             float64 `json:"stack"`
	IsBack            string  `json:"is_back"`
	PL                float64 `json:"p_l"`
	Liability         float64 `json:"liability"`
	Chips             float64 `json:"chips"`
	BetResultId       string  `json:"bet_result_id"`
	IsMatched         string  `json:"is_matched"`
	BetMatchedAt      string  `json:"bet_matched_at"`
	IpAddress         string  `json:"ip_address"`
	DeviceInfo        string  `json:"device_info"`
	DeleteStatus      string  `json:"delete_status"`
	DeletedReason     string  `json:"deleted_reason"`
	DeletedBy         int64   `json:"deleted_by"`
	DeletedFromIp     string  `json:"deleted_from_ip"`
	CreatedAt         string  `json:"created_at"`
	UpdatedAt         string  `json:"updated_at"`
	DeletedAt         string  `json:"deleted_at"`
	TotalStack        float64 `json:"total_stack"`
	UserCommission    float64 `json:"user_commission"`
	UserIds           string  `json:"user_ids"`
	Partnership       int64   `json:"partnership"`
	Commission        float64 `json:"commission"`
	SelfCommission    float64 `json:"self_commission"`
	Description       string  `json:"description"`
}
