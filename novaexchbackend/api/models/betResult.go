package models

import "database/sql"

type ResultInput struct {
	MatchId       string `json:"match_id"`
	MarketId      string `json:"market_id"`
	SelectionId   string `json:"selection_id"`
	SelectionName string `json:"selection_name"`
	Result        string `json:"result"`
	FancyId       string `json:"fancy_id"`
}

type InputAbandonedMarket struct {
	SportId    string `json:"sport_id"`
	SeriesId   string `json:"series_id"`
	MatchId    string `json:"match_id"`
	MarketId   string `json:"market_id"`
	SportName  string `json:"sport_name"`
	SeriesName string `json:"series_name"`
	MatchName  string `json:"match_name"`
	MarketName string `json:"market_name"`
	IsRollback int    `json:"is_rollback"`
}

type DeclaredMarkets struct {
	BetResultId string `json:"bet_result_id"`
	SportId     string `json:"sport_id"`
	SeriesId    string `json:"series_id"`
	MatchId     string `json:"match_id"`
	MarketId    string `json:"market_id"`
	SelectionId string `json:"selection_id"`
	SportName   string `json:"sport_name"`
	SeriesName  string `json:"series_name"`
	MatchName   string `json:"match_name"`
	MarketName  string `json:"market_name"`
	WinnerName  string `json:"winner_name"`
	Type        string `json:"type"`
	MatchDate   string `json:"match_date"`
	CreatedAt   string `json:"created_at" gorm:"-"`
	CardData    string `json:"card_data"`
	FancyTypeId int64  `json:"fancy_type_id"`
}

type InputMarketResult struct {
	SportId                string                        `json:"sport_id"`
	SeriesId               string                        `json:"series_id"`
	MatchId                string                        `json:"match_id"`
	MarketId               string                        `json:"market_id"`
	SelectionId            string                        `json:"selection_id"`
	SportName              string                        `json:"sport_name"`
	SeriesName             string                        `json:"series_name"`
	MatchName              string                        `json:"match_name"`
	MarketName             string                        `json:"market_name"`
	SelectionName          string                        `json:"selection_name"`
	Result                 string                        `json:"result"`
	CardData               string                        `json:"card_data"`
	ResultOddsPerSelection []InputResultOddsPerSelection `json:"result_odds_per_selection"`
	FancyTypeId            int64                         `json:"fancy_type_id"`
	IsHalf                 int64                         `json:"is_half"`
}

type InputResultOddsPerSelection struct {
	SportId     string  `json:"sport_id"`
	MatchId     string  `json:"match_id"`
	MarketId    string  `json:"market_id"`
	SelectionId string  `json:"selection_id"`
	Odds        float64 `json:"odds"`
	IsWinner    int     `json:"isWinner"`
}

type UserProfitLossInput struct {
	UserId               sql.NullInt64
	SportId              sql.NullString
	MatchId              sql.NullString
	MarketId             sql.NullString
	Type                 sql.NullString
	Stack                sql.NullInt64
	Liability            sql.NullFloat64
	UserPL               sql.NullFloat64
	UserCommission       sql.NullFloat64
	UserAvailableBalance sql.NullFloat64
	CreatedAt            sql.NullString
	UserTypeId           sql.NullInt64
	ParentIds            sql.NullString
	FreeChips            sql.NullFloat64
}

type UserProfitLoss struct {
	UserId               int64   `json:"user_id"`
	SportId              string  `json:"sport_id"`
	MatchId              string  `json:"match_id"`
	MarketId             string  `json:"market_id"`
	Type                 string  `json:"type"`
	Stack                int64   `json:"stack"`
	Liability            float64 `json:"liability"`
	UserPL               float64 `json:"user_pl"`
	UserCommission       float64 `json:"user_commission"`
	UserAvailableBalance float64 `json:"user_available_balance"`
	CreatedAt            string  `json:"created_at"`
	UserTypeId           int64   `json:"user_type_id"`
	ParentIds            string  `json:"parent_ids"`
	FreeChips            float64
}

type InputMarketResultRollback struct {
	MatchId  string `json:"match_id"`
	MarketId string `json:"market_id"`
	IsFancy  int64  `json:"is_fancy"`
}
