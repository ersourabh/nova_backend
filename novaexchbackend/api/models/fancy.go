package models

import "database/sql"

// Fancy model
type Fancy struct {
	Id                     int64           `json:"id"`
	SportId                string          `json:"sport_id"`
	SeriesId               string          `json:"series_id"`
	MatchId                string          `json:"match_id"`
	FancyId                string          `json:"fancy_id"`
	SelectionId            string          `json:"selection_id"`
	Name                   string          `json:"name"`
	Active                 string          `json:"active"` //0=>Inactive, 1=>Active, 2=>Closed, 3=>Abandoned, 4=Suspended
	Result                 sql.NullFloat64 `json:"result"`
	SessionSizeNo          float64         `json:"session_size_no"`
	SessionSizeYes         float64         `json:"session_size_yes"`
	SuperAdminMessage      string          `json:"super_admin_message"`
	CreatedAt              string          `json:"created_at"`
	ScorePosition          float64         `json:"score_position" sql:"-"`
	FancyScorePositionJson string          `json:"fancy_score_position_json"`
	SuperAdminFacnyId      string          `json:"super_admin_fancy_id"`
}

type FancyData struct {
	FancyId                string  `json:"fancy_id"`
	Name                   string  `json:"name"`
	Active                 string  `json:"active"`
	SessionSizeNo          float64 `json:"session_size_no"`
	SessionSizeYes         float64 `json:"session_size_yes"`
	SessionValueNo         float64 `json:"session_value_no"`
	SessionValueYes        float64 `json:"session_value_yes"`
	DisplayMessage         string  `json:"display_message"`
	SuperAdminMessage      string  `json:"super_admin_message"`
	ScorePosition          float64 `json:"score_position"`
	FancyScorePositionJson string  `json:"fancy_score_position_json"`
}

type FancyListData struct {
	SportId           string `json:"sport_id"`
	SeriesId          string `json:"series_id"`
	MatchId           string `json:"match_id"`
	FancyId           string `json:"fancy_id"`
	SelectionId       string `json:"selection_id"`
	Name              string `json:"name"`
	MatchName         string `json:"match_name"`
	SeriesName        string `json:"series_name"`
	SportName         string `json:"sport_name"`
	Active            string `json:"active"`
	MatchDate         string `json:"match_date"`
	SuperAdminMessage string `json:"super_admin_message"`
}

type RedisFancyData struct {
	RunnerName  string  `json:"RunnerName"`
	SelectionId string  `json:"SelectionId"`
	LayPrice1   float64 `json:"LayPrice1"`
	LaySize1    float64 `json:"LaySize1"`
	BackPrice1  float64 `json:"BackPrice1"`
	BackSize1   float64 `json:"BackSize1"`
	GameStatus  string  `json:"GameStatus"`
	Added       int     `json:"added"`
}

type RemoteRedisFancy struct {
	RunnerName  string      `json:"RunnerName"`
	SelectionId string      `json:"SelectionId"`
	BackPrice1  interface{} `json:"BackPrice1"`
	BackSize1   interface{} `json:"BackSize1"`
	LayPrice1   interface{} `json:"LayPrice1"`
	LaySize1    interface{} `json:"LaySize1"`
	GameStatus  string      `json:"GameStatus"`
	Added       int         `json:"added"`
}

//FancyScorePosition :
type FancyScorePosition struct {
	UserParentId           string  `json:"user_parent_id"`
	UserId                 int64   `json:"user_id"`
	ParentId               int64   `json:"parent_id"`
	UserTypeId             int64   `json:"user_type_id"`
	UserIds                string  `json:"user_ids"`
	MatchId                string  `json:"match_id"`
	FancyId                string  `json:"fancy_id"`
	Liability              float64 `json:"liability"`
	Profit                 float64 `json:"profit"`
	FancyScorePositionJson string  `json:"fancy_score_position_json"`
}

//FancyScorePositionInput :
type FancyScorePositionInput struct {
	UserParentId           sql.NullString
	UserId                 sql.NullInt64
	ParentId               sql.NullInt64
	UserTypeId             sql.NullInt64
	UserIds                sql.NullString
	MatchId                sql.NullString
	FancyId                sql.NullString
	Liability              sql.NullFloat64
	Profit                 sql.NullFloat64
	FancyScorePositionJson sql.NullString
}

// func (FancyScorePosition) TableName() string {
// 	return "fancy_score_position"
// }

type FancyBetForUserPosition struct {
	IsBack     string  `json:"is_back"`
	Run        float64 `json:"run"`
	Size       float64 `json:"size"`
	Stack      float64 `json:"stack"`
	Percentage float64 `json:"percentage"`
}

type FancyBetForUserPositionInput struct {
	IsBack     sql.NullString
	Run        sql.NullFloat64
	Size       sql.NullFloat64
	Stack      sql.NullFloat64
	Percentage sql.NullFloat64
}

// type InputCreateFancyPosition struct {
// 	UserId     int     `json:"user_id"`
// 	UserTypeId int     `json:"user_type_id"`
// 	FancyId    string  `json:"fancy_id"`
// 	IsBack     string  `json:"is_back"`
// 	Run        float64 `json:"run"`
// 	Size       float64 `json:"size"`
// 	Stack      float64 `json:"stack"`
// }

// type RemoteRedisFancy struct {
// 	GameStatus string      `json:"GameStatus"`
// 	BackPrice1 interface{} `json:"BackPrice1"`
// 	BackSize1  interface{} `json:"BackSize1"`
// 	LayPrice1  interface{} `json:"LayPrice1"`
// 	LaySize1   interface{} `json:"LaySize1"`
// }

type FacnyResultValue struct {
	Key   string  `json:"key"`
	Value float64 `json:"value"`
}

type FacnyResultData struct {
	UserId        int64              `json:"user_id"`
	UserTypeId    int64              `json:"user_type_id"`
	FancyPosition []FacnyResultValue `json:"fancy_position"`
	MaxExposure   float64            `json:"max_exposure"`
	MaxProfit     float64            `json:"max_profit,omitempty"`
}

// type FancyPosition struct {
// 	Id                     int     `json:"id"`
// 	Liability              float64 `json:"liability"`
// 	Profit                 float64 `json:"profit"`
// 	FancyScorePositionJson string  `json:"fancy_score_position_json"`
// }
type FancyDataMongo struct {
	MatchId     string
	FancyId     string
	SportId     string
	SeriesId    string
	DateTimeNow string
	DateNow     string
	FancyData   Fancy
	Active      string
}

//FancyListStruct :
type FancyListStruct struct {
	Id                int    `json:"id"`
	SportId           string `json:"sport_id"`
	SeriesId          string `json:"series_id"`
	MatchId           string `json:"match_id"`
	MarketId          string `json:"market_id"`
	SportName         string `json:"sport_name"`
	SeriesName        string `json:"series_name"`
	MatchName         string `json:"match_name"`
	FancyId           string `json:"fancy_id"`
	SelectionId       string `json:"selection_id"`
	RunnerName        string `json:"runner_name"`
	LayPrice1         string `json:"lay_price1"`
	LaySize1          string `json:"lay_size1"`
	BackPrice1        string `json:"back_price1"`
	BackSize1         string `json:"back_size1"`
	GameStatus        string `json:"game_status"`
	IsActive          string `json:"is_active"`
	Active            string `json:"active"`
	IsResulted        int64  `json:"is_resulted"`
	SuperAdminMessage string `json:"super_admin_message"`
	Result            string `json:"result"`
}
