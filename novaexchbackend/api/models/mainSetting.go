package models

import (
	"database/sql"
	"time"
)

type Model struct {
	Id        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

// Global Structures for use in multiple models/services/controllers etc.
type ResultCount struct {
	TotalCount int `json:"total_count"`
}

type Ids struct {
	Ids string `json:"ids"`
}

type ResultWithPagination struct {
	TotalCount int         `json:"total_count"`
	Limit      int         `json:"limit"`
	Data       interface{} `json:"data"`
}

type GlobalSetting struct {
	Id                   int     `json:"id"`
	Logo                 string  `json:"logo"`
	MatchStack           string  `json:"match_stack"`
	FancyStack           string  `json:"fancy_stack,omitempty"`
	OddsLimit            float64 `json:"odds_limit,omitempty"`
	OneClickStack        string  `json:"one_click_stack,omitempty"`
	SiteTitle            string  `json:"site_title,omitempty"`
	SiteMessage          string  `json:"site_message,omitempty"`
	SiteUnderMaintenance string  `json:"site_under_maintenance,omitempty"`
	TermsConditions      string  `json:"terms_conditions,omitempty"`
	ThemeCode            string  `json:"theme_code,omitempty"`
	Favicon              string  `json:"favicon,omitempty"`
	MarketCommission     int64   `json:"market_commission"`
	SessionCommission    int64   `json:"session_commission"`
	BookmakerCommission  int64   `json:"bookmaker_commission"`
	MarketDelay          int64   `json:"market_delay"`
	SessionDelay         int64   `json:"session_delay"`
	BookmakerDelay       int64   `json:"bookmaker_delay"`
}

//GetMinMaxBets :
type GetMinMaxBets struct {
	CricketMinBet int64 `json:"cricket_min_bet"`
	SoccerMinBet  int64 `json:"soccer_min_bet"`
	TennisMinBet  int64 `json:"tennis_min_bet"`
	CricketMaxBet int64 `json:"cricket_max_bet"`
	SoccerMaxBet  int64 `json:"soccer_max_bet"`
	TennisMaxBet  int64 `json:"tennis_max_bet"`
	SoccerDelay   int64 `json:"soccer_delay"`
	TennisDelay   int64 `json:"tennis_delay"`
	CricketDelay  int64 `json:"cricket_delay"`
}

//GetMinMaxBetsInput :
type GetMinMaxBetsInput struct {
	CricketMinBet sql.NullInt64
	SoccerMinBet  sql.NullInt64
	TennisMinBet  sql.NullInt64
	CricketMaxBet sql.NullInt64
	SoccerMaxBet  sql.NullInt64
	TennisMaxBet  sql.NullInt64
	SoccerDelay   sql.NullInt64
	TennisDelay   sql.NullInt64
	CricketDelay  sql.NullInt64
}

//GetCommissionInput :
type GetCommissionInput struct {
	CricketOddsCommission      sql.NullInt64
	SoccerOddsCommission       sql.NullInt64
	TennisOddsCommission       sql.NullInt64
	CricketFancyCommission     sql.NullInt64
	SoccerFancyCommission      sql.NullInt64
	TennisFancyCommission      sql.NullInt64
	CricketBookmakerCommission sql.NullInt64
	SoccerBookmakerCommission  sql.NullInt64
	TennisBookmakerCommission  sql.NullInt64
}

//GetCommission :
type GetCommission struct {
	CricketOddsCommission      int64 `json:"cricket_odds_commission"`
	SoccerOddsCommission       int64 `json:"soccer_odds_commission"`
	TennisOddsCommission       int64 `json:"tennis_odds_commission"`
	CricketFancyCommission     int64 `json:"cricket_fancy_commission"`
	SoccerFancyCommission      int64 `json:"soccer_fancy_commission"`
	TennisFancyCommission      int64 `json:"tennis_fancy_commission"`
	CricketBookmakerCommission int64 `json:"cricket_bookmaker_commission"`
	SoccerBookmakerCommission  int64 `json:"soccer_bookmaker_commission"`
	TennisBookmakerCommission  int64 `json:"tennis_bookmaker_commission"`
}

//GetMatchMinMaxBets :
type GetMatchMinMaxBets struct {
	MarketMinBet    int64 `json:"market_min_bet"`
	SessionMinBet   int64 `json:"session_min_bet"`
	BookmakerMinBet int64 `json:"bookmaker_min_bet"`
	MarketMaxBet    int64 `json:"market_max_bet"`
	SessionMaxBet   int64 `json:"session_max_bet"`
	BookmakerMaxBet int64 `json:"bookmaker_max_bet"`
	MarketDelay     int64 `json:"market_delay"`
	SessionDelay    int64 `json:"session_delay"`
	BookmakerDelay  int64 `json:"bookmaker_delay"`
}

//GetMatchMinMaxBetsInput :
type GetMatchMinMaxBetsInput struct {
	MarketMinBet    sql.NullInt64
	SessionMinBet   sql.NullInt64
	BookmakerMinBet sql.NullInt64
	MarketMaxBet    sql.NullInt64
	SessionMaxBet   sql.NullInt64
	BookmakerMaxBet sql.NullInt64
	MarketDelay     sql.NullInt64
	SessionDelay    sql.NullInt64
	BookmakerDelay  sql.NullInt64
}
