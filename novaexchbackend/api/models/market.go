package models

import "database/sql"

type Market struct {
	SportId            string       `json:"sport_id"`
	SportName          string       `json:"Sport_name"`
	SeriesId           string       `json:"series_id"`
	SeriesName         string       `json:"series_name"`
	MatchId            string       `json:"match_id"`
	MatchName          string       `json:"match_name"`
	MarketId           string       `json:"market_id"`
	SelectionId        string       `json:"selection_id"`
	Name               string       `json:"name"`
	RunnerJson         []RunnerData `json:"runner_json"`
	IsResultDeclared   string       `json:"is_result_declared"` //0 default, 1 = declared, 2 declared
	Result             string       `json:"result"`
	WinnerId           string       `json:"winner_id"`
	ResultDate         string       `json:"result_date"`
	MarketStartTime    string       `json:"market_start_time"`
	IsActive           string       `json:"is_active"`
	IsBookmaker        string       `json:"is_bookmaker"`
	CreatedAt          string       `json:"created_at"`
	MaxBetLiability    float64      `json:"max_bet_liability"`
	MaxMarketLiability float64      `json:"max_market_liability"`
	MaxMarketProfit    float64      `json:"max_market_profit"`
	IsVisible          string       `json:"is_visible"`
	MarketType         string       `json:"market_type"`
	IsBookmakerMarket  string       `json:"is_bookmaker_market"`
	MatchDate          string       `json:"match_date"`
	LiabilityType      string       `json:"liability_type,omitempty"`
	OddsLimit          float64
}
type MarketInput struct {
	SportId          sql.NullString
	SportName        sql.NullString
	SeriesId         sql.NullString
	SeriesName       sql.NullString
	MatchId          sql.NullString
	MatchName        sql.NullString
	MarketId         sql.NullString
	SelectionId      sql.NullString
	Name             sql.NullString
	RunnerJson       sql.NullString
	IsResultDeclared sql.NullString
	Result           sql.NullString
	WinnerId         sql.NullString
	ResultDate       sql.NullString
	MarketStartTime  sql.NullString
	IsActive         sql.NullString
	IsBookmaker      sql.NullString
	CreatedAt        sql.NullString
	MatchDate        sql.NullString
	OddsLimit        sql.NullFloat64
}

type MarketDataMongo struct {
	MatchId    string
	MarketId   string
	MarketData string
}

type MarketData struct {
	MarketId           string       `json:"market_id"`
	Name               string       `json:"name"`
	IsBookmakerMarket  string       `json:"is_bookmaker_market"`
	IsVisible          string       `json:"is_visible"`
	MaxBetLiability    float64      `json:"max_bet_liability"`
	MaxMarketLiability float64      `json:"max_market_liability"`
	MaxMarketProfit    float64      `json:"max_market_profit"`
	Status             string       `json:"status"`
	RunnerJson         []RunnerData `json:"runner_json"`
	Cards              interface{}  `json:"cards"`
	CardsTotal         interface{}  `json:"cardsTotal"`
	TvUrl              string       `json:"tv_url"`
	Results            interface{}  `json:"results"`
	AutoTime           string       `json:"autotime"`
	MarketStartTime    string       `json:"market_start_time"`
	MarketType         string       `json:"market_type"`
}

type RunnerData struct {
	SelectionId   string        `json:"selection_id"`
	Name          string        `json:"name"`
	SortPriority  float64       `json:"sort_priority"`
	LiabilityType string        `json:"liability_type"`
	SortName      string        `json:"sort_name"`
	Status        interface{}   `json:"status"`
	Lay           []LayBackData `json:"lay"`
	Back          []LayBackData `json:"back"`
	WinLoss       float64       `json:"win_loss"`
}

type LayBackData struct {
	Size   string `json:"size"`
	Price  string `json:"price"`
	Status string `json:"status"`
}

type MarketList struct {
	SportId            string  `json:"sport_id"`
	SeriesId           string  `json:"series_id"`
	MatchId            string  `json:"match_id"`
	MarketId           string  `json:"market_id"`
	SportName          string  `json:"sport_name"`
	SeriesName         string  `json:"series_name"`
	MatchName          string  `json:"match_name"`
	MarketName         string  `json:"market_name"`
	MaxBetLiability    float64 `json:"max_bet_liability"`
	MaxMarketLiability float64 `json:"max_market_liability"`
	MaxMarketProfit    float64 `json:"max_market_profit"`
	IsActive           string  `json:"is_active"`
	IsVisible          string  `json:"is_visible"`
	IsBookmakerMarket  string  `json:"is_bookmaker_market"`
	MatchDate          string  `json:"match_date"`
	CreatedAt          string  `json:"created_at"`
}

type TeamPositionInput struct {
	UserId        sql.NullInt64
	UserName      sql.NullString
	MatchId       sql.NullString
	MarketId      sql.NullString
	MarketName    sql.NullString
	SelectionId   sql.NullString
	SelectionName sql.NullString
	LiabilityType sql.NullString
	SortPriority  sql.NullInt64
	Stack         sql.NullFloat64
	WinValue      sql.NullFloat64
	LossValue     sql.NullFloat64
	WinLoss       sql.NullFloat64
	UserTypeId    sql.NullInt64
	UserIds       sql.NullString
}
type TeamPosition struct {
	UserId        int64   `json:"user_id,omitempty"`
	UserName      string  `json:"user_name,omitempty"`
	MatchId       string  `json:"match_id,omitempty"`
	MarketId      string  `json:"market_id,omitempty"`
	MarketName    string  `json:"market_name,omitempty"`
	SelectionId   string  `json:"selection_id,omitempty"`
	SelectionName string  `json:"selection_name,omitempty"`
	LiabilityType string  `json:"liability_type,omitempty"`
	Stack         float64 `json:"stack,omitempty"`
	WinValue      float64 `json:"win_value"`
	LossValue     float64 `json:"loss_value"`
	WinLoss       float64 `json:"win_loss"`
	UserTypeId    int64   `json:"user_type_id,omitempty"`
	UserIds       string  `json:"user_ids,omitempty"`
}

type OnlineMarketSelectionStructStrSelection struct {
	SelectionId  string `json:"selectionId"`
	RunnerName   string `json:"runnerName"`
	Handicap     int64  `json:"handicap"`
	SortPriority int64  `json:"sortPriority"`
}
type OnlineMarketSelectionStructIntSelection struct {
	SelectionId  int64  `json:"selectionId"`
	RunnerName   string `json:"runnerName"`
	Handicap     int64  `json:"handicap"`
	SortPriority int64  `json:"sortPriority"`
}

type OnlineMarketStructInt struct {
	MarketId        string                                    `json:"marketId"`
	MarketName      string                                    `json:"marketName"`
	MarketStartTime string                                    `json:"marketStartTime"`
	TotalMatched    int                                       `json:"totalMatched"`
	Runners         []OnlineMarketSelectionStructIntSelection `json:"runners"`
}
type OnlineMarketStructStr struct {
	MarketId        string                                    `json:"marketId"`
	MarketName      string                                    `json:"marketName"`
	MarketStartTime string                                    `json:"marketStartTime"`
	TotalMatched    int                                       `json:"totalMatched"`
	Runners         []OnlineMarketSelectionStructStrSelection `json:"runners"`
}

// MarketSelection model
type MarketSelection struct {
	Id            int64  `json:"id,omitempty"`
	MatchId       string `json:"match_id,omitempty"`
	MarketId      string `json:"market_id,omitempty"`
	SelectionId   string `json:"selection_id,omitempty"`
	Name          string `json:"name,omitempty"`
	SortPriority  int64  `json:"sort_priority,omitempty"`
	SortName      string `json:"sort_name,omitempty"`
	LiabilityType string `json:"liability_type,omitempty"`
	CreatedAt     string `json:"created_at,omitempty"`
}

// MarketSelection model
type MarketSelectionInput struct {
	Id            sql.NullInt64
	MatchId       sql.NullString
	MarketId      sql.NullString
	SelectionId   sql.NullString
	Name          sql.NullString
	SortPriority  sql.NullInt64
	SortName      sql.NullString
	LiabilityType sql.NullString
	CreatedAt     sql.NullString
}

type OddsDataParsedRunnerExBackLayStruct struct {
	Price  float64 `json:"price"`
	Size   float64 `json:"size"`
	Status string  `json:"status"`
}

type OddsDataParsedRunnerExStruct struct {
	AvailableToBack []OddsDataParsedRunnerExBackLayStruct `json:"availableToBack"`
	AvailableToLay  []OddsDataParsedRunnerExBackLayStruct `json:"availableToLay"`
}

type OddsDataParsedRunnerStruct struct {
	SelectionId     int                          `json:"selectionId"`
	Handicap        int                          `json:"handicap"`
	Status          interface{}                  `json:"status"`
	LastPriceTraded float64                      `json:"lastPriceTraded"`
	TotalMatched    float64                      `json:"totalMatched"`
	Ex              OddsDataParsedRunnerExStruct `json:"ex"`
}

type OddsDataParsedStruct struct {
	MarketId              string                       `json:"marketId"`
	IsMarketDataDelayed   bool                         `json:"isMarketDataDelayed"`
	Status                string                       `json:"status"`
	BetDelay              int                          `json:"betDelay"`
	BspReconciled         bool                         `json:"bspReconciled"`
	Complete              bool                         `json:"complete"`
	Inplay                bool                         `json:"inplay"`
	NumberOfWinners       int                          `json:"numberOfWinners"`
	NumberOfRunners       int                          `json:"numberOfRunners"`
	NumberOfActiveRunners int                          `json:"numberOfActiveRunners"`
	LastMatchTime         string                       `json:"lastMatchTime"`
	TotalMatched          float64                      `json:"totalMatched"`
	TotalAvailable        float64                      `json:"totalAvailable"`
	CrossMatching         bool                         `json:"crossMatching"`
	RunnersVoidable       bool                         `json:"runnersVoidable"`
	Version               float64                      `json:"version"`
	SelectionType         string                       `json:"selectionType"`
	Runners               []OddsDataParsedRunnerStruct `json:"runners"`
	RunnersOrg            []OddsDataParsedRunnerStruct `json:"runners_org"`
	Cards                 interface{}                  `json:"cards"`
	TvUrl                 string                       `json:"tv_url"`
	Results               interface{}                  `json:"results"`
	AutoTime              string                       `json:"autotime"`
}

type MarketDataForAutoResult struct {
	SportId    string `json:"sport_id"`
	SeriesId   string `json:"series_id"`
	MatchId    string `json:"match_id"`
	MarketId   string `json:"market_id"`
	MarketName string `json:"market_name"`
	MatchName  string `json:"match_name"`
	SeriesName string `json:"series_name"`
	SportName  string `json:"sport_name"`
	IsActive   string `json:"is_active"`
}

//GetBetForPositionInput :
type GetBetForPositionInput struct {
	UserId             sql.NullInt64
	ParentIds          sql.NullString
	MatchId            sql.NullString
	MarketId           sql.NullString
	MarketName         sql.NullString
	SelectionId        sql.NullString
	SelectionName      sql.NullString
	LiabilityType      sql.NullString
	SortPriority       sql.NullInt64
	IsBack             sql.NullString
	CLiabilityType     sql.NullString
	ASelectionId       sql.NullString
	Stack              sql.NullInt64
	PL                 sql.NullFloat64
	IsMatched          sql.NullString
	Liability          sql.NullFloat64
	MatchLiabilityType sql.NullString
}
