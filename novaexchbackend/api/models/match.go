package models

import "database/sql"

// Match model
type Match struct {
	Id         int64  `json:"id,omitempty"`
	SportId    string `json:"sport_id"`
	SportName  string `json:"sport_name"`
	SeriesId   string `json:"series_id"`
	SeriesName string `json:"series_name"`
	MatchId    string `json:"match_id"`
	Name       string `json:"name"`
	MatchDate  string `json:"match_date"`
	StartDate  string `json:"start_date"`
	// OddLimit        float64 `json:"odd_limit"`
	MinStack        float64 `json:"min_stack"`
	MaxStack        float64 `json:"max_stack"`
	ScoreType       string  `json:"score_type"`
	ScoreKey        string  `json:"score_key"`
	IsActive        string  `json:"is_active"`
	LiabilityType   string  `json:"liability_type"`
	LiveSportTvUrl  string  `json:"live_sport_tv_url"`
	CreatedAt       string  `json:"created_at"`
	UpdatedAt       string  `json:"updated_at"`
	TvUrl           string  `json:"tv_url"`
	SessionMinStack float64 `json:"session_min_stack"`
	SessionMaxStack float64 `json:"session_max_stack"`
	// IsCup           string  `json:"is_cup"`
	// Country     string  `josn:"country_code"`
	// Country string `json:"country"`
	// Venue   string `json:"venue"`
	Added int `json:"added,omitempty"`
}

// MatchInput model
type MatchInput struct {
	Id                        sql.NullInt64
	SportId                   sql.NullString
	SportName                 sql.NullString
	SeriesId                  sql.NullString
	SeriesName                sql.NullString
	MatchId                   sql.NullString
	Name                      sql.NullString
	MatchDate                 sql.NullString
	StartDate                 sql.NullString
	OddLimit                  sql.NullFloat64
	MinStack                  sql.NullFloat64
	MaxStack                  sql.NullFloat64
	ScoreType                 sql.NullString
	ScoreKey                  sql.NullString
	IsActive                  sql.NullString
	LiabilityType             sql.NullString
	LiveSportTvUrl            sql.NullString
	CreatedAt                 sql.NullString
	UpdatedAt                 sql.NullString
	IsDeactivatedBySuperAdmin sql.NullString
	TvUrl                     sql.NullString
	SessionMinStack           sql.NullFloat64
	SessionMaxStack           sql.NullFloat64
	IsliveSport               sql.NullString
	IsCup                     sql.NullString
	CountryCode               sql.NullString
	Venue                     sql.NullString
}

type SportListOutput struct {
	SportName    string              `json:"sport_name"`
	SportId      string              `json:"sport_id,omitempty"`
	MatchesCount int                 `json:"matches_count"`
	SeriesList   []SeriesListForMenu `json:"series_list"`
}

type SeriesListForMenu struct {
	SeriesName   string             `json:"series_name"`
	SeriesId     string             `json:"series_id,omitempty"`
	SportId      string             `json:"sport_id,omitempty"`
	MatchesCount int                `json:"matches_count"`
	Matches      []MatchListForMenu `json:"matches"`
}

// type SportMatchesForMenu struct {
// 	SportName  string                 `json:"sport_name"`
// 	SportId    string                 `json:"sport_id,omitempty"`
// 	Matches    int                    `json:"matches_count"`
// 	SeriesList []SeriesMatchesForMenu `json:"series"`
// }

// type SeriesMatchesForMenu struct {
// 	SeriesName string `json:"series_name"`
// 	SeriesId   string `json:"series_id,omitempty"`
// 	Matches    int    `json:"matches_count"`
// }

type MatchListForMenu struct {
	MatchId   string `json:"match_id"`
	SportId   string `json:"sport_id,omitempty"`
	MatchName string `json:"match_name"`
	SeriesId  string `json:"series_id,omitempty"`
	MatchDate string `json:"match_date"`
	//Markets   []MarketListForMenu `json:"markets,omitempty"`
}
type MarketListForMenu struct {
	MarketId   string `json:"market_id"`
	MarketName string `json:"market_name"`
}

type MatchDetailInput struct {
	SportId          sql.NullString
	SeriesId         sql.NullString
	MatchId          sql.NullString
	SportName        sql.NullString
	SeriesName       sql.NullString
	MatchName        sql.NullString
	ScoreType        sql.NullString
	ScoreKey         sql.NullString
	IsShowLastResult sql.NullString
	IsShowTv         sql.NullString
	MatchDate        sql.NullString
	MaxStack         sql.NullFloat64
	MinStack         sql.NullFloat64
	SessionMaxStack  sql.NullFloat64
	SessionMinStack  sql.NullFloat64
	IsLiveSport      sql.NullString
	LiveSportTvUrl   sql.NullString
	LiabilityType    sql.NullString
	TvUrl            sql.NullString
}
type MatchDetail struct {
	SportId          string       `json:"sport_id"`
	SeriesId         string       `json:"series_id"`
	MatchId          string       `json:"match_id"`
	SportName        string       `json:"sport_name"`
	SeriesName       string       `json:"series_name"`
	MatchName        string       `json:"match_name"`
	ScoreType        string       `json:"score_type"`
	ScoreKey         string       `json:"score_key"`
	IsShowLastResult string       `json:"is_show_last_result"`
	IsShowTv         string       `json:"is_show_tv"`
	MatchDate        string       `json:"match_date"`
	MaxStack         float64      `json:"max_stack"`
	MinStack         float64      `json:"min_stack"`
	SessionMaxStack  float64      `json:"session_max_stack"`
	SessionMinStack  float64      `json:"session_min_stack"`
	Markets          []MarketData `json:"markets"`
	Fancies          []FancyData  `json:"fancies"`
	IsLiveSport      string       `json:"is_live_sport"`
	LiveSportTvUrl   string       `json:"live_sport_tv_url"`
	MinBetAmount     int64        `json:"min_bet_amount"`
	LiabilityType    string       `json:"liability_type"`
	BetLock          bool         `json:"bet_lock"`
	FancyLock        bool         `json:"fancy_lock"`
	TvUrl            string       `json:"tv_url"`
	IsActiveParent   bool         `json:"is_active_parent"`
	SelfActive       bool         `json:"self_active"`
}

type MarketWatchData struct {
	MatchId           string       `json:"match_id"`
	SportId           string       `json:"sport_id"`
	SeriesId          string       `json:"series_id"`
	SeriesName        string       `json:"series_name"`
	MatchName         string       `json:"match_name"`
	Inplay            bool         `json:"inplay"`
	MarketId          string       `json:"market_id,omitempty"`
	IsBookmakerMarket bool         `json:"is_bookmaker_market"`
	IsFancy           bool         `json:"is_fancy"`
	IsTv              bool         `josn:"is_tv"`
	RunnerJson        []RunnerData `json:"runner_json"`
	MatchDate         string       `json:"match_date"`
	TotalBet          int64        `json:"total_bet,omitempty"`
	IsCup             string       `json:"is_cup"`
}

type MarketWatchDataInput struct {
	SportId       sql.NullString
	SeriesId      sql.NullString
	SeriesName    sql.NullString
	MatchId       sql.NullString
	SportName     sql.NullString
	MatchName     sql.NullString
	MatchDate     sql.NullString
	StartDate     sql.NullString
	OddLimit      sql.NullFloat64
	MinStack      sql.NullFloat64
	MaxStack      sql.NullFloat64
	ScoreType     sql.NullString
	ScoreKey      sql.NullString
	LiabilityType sql.NullString
	IsBetExist    sql.NullInt64
	MarketId      sql.NullString
	Inplay        sql.NullInt64
	IsLiveSport   sql.NullString
	IsFavMarket   sql.NullString
	IsActive      sql.NullString
	TvUrl         sql.NullString
	IsCup         sql.NullString
}

type MatchSettingInput struct {
	MatchId       sql.NullString
	ScoreKey      sql.NullString
	ScoreType     sql.NullString
	IsActive      sql.NullString
	LiabilityType sql.NullString
}
type MatchSetting struct {
	MatchId       string `json:"match_id"`
	ScoreKey      string `json:"score_key"`
	ScoreType     string `json:"score_type"`
	IsActive      string `json:"is_active"`
	LiabilityType string `json:"liability_type"`
}

type InputMatchSetting struct {
	MatchId string `json:"match_id"`
	// OddLimit        float64 `json:"odd_limit"`
	MinStack          float64 `json:"min_stack"`
	MaxStack          float64 `json:"max_stack"`
	TvUrl             string  `json:"tv_url"`
	ScoreKey          string  `json:"score_key"`
	ScoreType         string  `json:"score_type"`
	SessionMinStack   float64 `json:"session_min_stack"`
	SessionMaxStack   float64 `json:"session_max_stack"`
	BookmakerMinStack float64 `json:"bookmaker_min_stack"`
	BookmakerMaxStack float64 `json:"bookmaker_max_stack"`
}

type UpcomingFixtures struct {
	MatchId    string `json:"match_id"`
	SportId    string `json:"sport_id"`
	SeriesId   string `json:"series_id"`
	SeriesName string `json:"series_name"`
	MatchName  string `json:"match_name"`
	MatchDate  string `json:"match_date"`
}

type ScoreboardData struct {
	Data []ScoreboardMatchData `json:"data"`
}

type ScoreboardMatchData struct {
	MatchDate string           `json:"match_date"`
	MatchId   int64            `json:"match_id"`
	MatchName string           `json:"match_name"`
	Venue     string           `json:"venue"`
	Teams     []ScoreboardTeam `json:"teams"`
}
type ScoreboardTeam struct {
	Name string `json:"name"`
}
