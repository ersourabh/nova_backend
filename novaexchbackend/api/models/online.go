package models

import (
	"database/sql"
	"time"
)

// SportSeries
type SportSeries struct {
	Competition struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"competition"`
	MarketCount       int    `json:"marketCount"`
	CompetitionRegion string `json:"competitionRegion"`
	Added             int    `json:"added"`
}

// SportSeriesMatch
type SportSeriesMatch struct {
	Event struct {
		ID          string    `json:"id"`
		Name        string    `json:"name"`
		CountryCode string    `json:"countryCode"`
		Timezone    string    `json:"timezone"`
		OpenDate    time.Time `json:"openDate"`
	} `json:"event"`
	MarketCount int `json:"marketCount"`
	Added       int `json:"added"`
}

// SportSeriesMatchMarket
type SportSeriesMatchMarket struct {
	MarketID        string          `json:"marketId,omitempty"`
	MarketName      string          `json:"marketName,omitempty"`
	MarketStartTime string          `json:"marketStartTime,omitempty"`
	TotalMatched    float64         `json:"totalMatched,omitempty"`
	Runners         []MarketRunners `json:"runners,omitempty"`
	Added           int             `json:"added"`
}

// MarketRunners
type MarketRunners struct {
	SelectionID  int    `json:"selectionId,omitempty"`
	RunnerName   string `json:"runnerName,omitempty"`
	SortPriority int    `json:"sortPriority,omitempty"`
}

//-------------------------------
type SportsDBInput struct {
	SportID   sql.NullInt64
	SportName sql.NullString
	IsActive  sql.NullString
}

type SportsDB struct {
	SportID     string `json:"eventType"`
	SportName   string `json:"name"`
	MarketCount int64  `json:"marketCount"`
	Added       int    `json:"added"`
}

type SportSeriesDBInput struct {
	SeriesID   sql.NullInt64
	SeriesName sql.NullString
}

type SportSeriesMatchesDBInput struct {
	MatchID   sql.NullString
	MatchName sql.NullString
}

type SportSeriesMatchMarketDBInput struct {
	MarketID   sql.NullString
	MarketName sql.NullString
}

// SportSeriesMatchMarket
type MarketOdds struct {
	MarketID     string              `json:"marketId"`
	Status       string              `json:"status"`
	Inplay       bool                `json:"inplay"`
	TotalMatched float64             `json:"totalMatched"`
	Runners      []MarketRunnersOdds `json:"runners"`
}

// MarketRunnersOdds
type MarketRunnersOdds struct {
	SelectionID int    `json:"selectionId"`
	Handicap    int    `json:"handicap,omitempty"`
	Status      string `json:"status,omitempty"`
	Ex          OddsEx `json:"ex"`
}
type OddsEx struct {
	AvailableToBack []AvailableToBackLay `json:"availableToBack"`
	AvailableToLay  []AvailableToBackLay `json:"availableToLay"`
}
type AvailableToBackLay struct {
	Price float64 `json:"price"`
	// Size  float64 `json:"size"`
	Size interface{} `json:"size"`
}

// type MarketOddsBookmaker struct {
// 	MarketID     string                       `json:"marketId"`
// 	Status       string                       `json:"status"`
// 	Inplay       bool                         `json:"inplay"`
// 	TotalMatched float64                      `json:"totalMatched"`
// 	Runners      []MarketRunnersOddsBookmaker `json:"runners"`
// }

// type MarketRunnersOddsBookmaker struct {
// 	SelectionID int             `json:"selectionId"`
// 	Handicap    int             `json:"handicap,omitempty"`
// 	Status      string          `json:"status,omitempty"`
// 	Ex          OddsExBookmaker `json:"ex"`
// }
// type OddsExBookmaker struct {
// 	AvailableToBack []AvailableToBackLayBookmaker `json:"availableToBack"`
// 	AvailableToLay  []AvailableToBackLayBookmaker `json:"availableToLay"`
// }
// type AvailableToBackLayBookmaker struct {
// 	Price float64 `json:"price"`
// 	Size  string  `json:"size"`
// }
