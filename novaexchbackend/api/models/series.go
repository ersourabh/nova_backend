package models

import (
	"database/sql"
)

type SeriesInput struct {
	Id        sql.NullInt64
	IsActive  sql.NullString
	Name      sql.NullString
	SeriesId  sql.NullString
	SportId   sql.NullString
	SportName sql.NullString
	CreatedAt sql.NullString
	UpdatedAt sql.NullString
}

type Series struct {
	Id        int64  `json:"id"`
	IsActive  string `json:"is_active"`
	Name      string `json:"name"`
	SeriesId  string `json:"series_id"`
	SportId   string `json:"sport_id"`
	SportName string `json:"sport_name"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}
