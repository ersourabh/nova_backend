package models

import "database/sql"

type Sport struct {
	SportId     string `json:"sport_id"`
	Name        string `json:"name"`
	IsActive    string `json:"is_active"`
	IsLiveSport string `json:"is_live_sport"`
	CreatedAt   string `json:"created_at,omitempty"`
	UpdatedAt   string `json:"updated_at,omitempty"`
}

type SportInput struct {
	Id                        sql.NullInt64
	SportId                   sql.NullString
	Name                      sql.NullString
	IsActive                  sql.NullString
	IsDeactivatedBySuperAdmin sql.NullString
	IsAllowBookmakerMarket    sql.NullString
	IsLiveSport               sql.NullString
	IsShowLastResult          sql.NullString
	IsShowTv                  sql.NullString
	OrderBy                   sql.NullInt64
	CreatedAt                 sql.NullString
	UpdatedAt                 sql.NullString
}

type IsLiveSportByMatchId struct {
	IsLiveSport string `json:"is_live_sport"`
}
