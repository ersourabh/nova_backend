package models

import (
	"database/sql"
)

type UserInput struct {
	Id                  sql.NullInt64
	UserName            sql.NullString
	Name                sql.NullString
	Password            sql.NullString
	MasterPassword      sql.NullString
	OldPassword         sql.NullString
	NewPassword         sql.NullString
	ConfirmPassword     sql.NullString
	ParentId            sql.NullInt64
	ParentName          sql.NullString
	ParentIds           sql.NullString
	UserTypeId          sql.NullInt64
	ParentUserTypeId    sql.NullInt64
	Mobile              sql.NullString
	City                sql.NullString
	Exposure            sql.NullFloat64
	ProfitLoss          sql.NullFloat64
	AvailableChip       sql.NullFloat64
	TotalChip           sql.NullFloat64
	CreditedChip        sql.NullFloat64
	Partnership         sql.NullInt64
	MarketDelay         sql.NullInt64
	SessionDelay        sql.NullInt64
	BookmakerDelay      sql.NullInt64
	MarketCommission    sql.NullInt64
	SessionCommission   sql.NullInt64
	BookmakerCommission sql.NullInt64
	PasswordChanged     sql.NullString
	BetLock             sql.NullString
	UserLock            sql.NullString
	CreatedAt           sql.NullString
	UpdatedAt           sql.NullString
	Remark              sql.NullString
	IsActive            sql.NullString
	ChangePasswordLock  sql.NullString
}

type User struct {
	Id                  int64   `json:"id"`
	UserName            string  `json:"user_name"`
	Name                string  `json:"name"`
	Password            string  `json:"password,omitempty"`
	MasterPassword      string  `json:"master_password,omitempty"`
	OldPassword         string  `json:"old_password,omitempty"`
	NewPassword         string  `json:"new_password,omitempty"`
	ConfirmPassword     string  `json:"confirm_password,omitempty"`
	ParentId            int64   `json:"parent_id"`
	ParentName          string  `json:"parent_name"`
	ParentIds           string  `json:"parent_ids"`
	UserTypeId          int64   `json:"user_type_id"`
	ParentUserTypeId    int64   `json:"parent_user_type_id"`
	Mobile              string  `json:"mobile"`
	City                string  `json:"city"`
	Exposure            float64 `json:"exposure"`
	ProfitLoss          float64 `json:"profit_loss"`
	AvailableChip       float64 `json:"available_chip"`
	ParentAvailableChip float64 `json:"parent_available_chip,omitempty"`
	TotalChip           float64 `json:"total_chip"`
	CreditedChip        float64 `json:"credited_chip"`
	Partnership         int64   `json:"partnership"`
	MarketDelay         int64   `json:"market_delay"`
	SessionDelay        int64   `json:"session_delay"`
	BookmakerDelay      int64   `json:"bookmaker_delay"`
	MarketCommission    int64   `json:"market_commission"`
	SessionCommission   int64   `json:"session_commission"`
	BookmakerCommission int64   `json:"bookmaker_commission"`
	PasswordChanged     string  `json:"password_changed,omitempty"`
	UserLock            string  `json:"user_lock,omitempty"`
	BetLock             string  `json:"bet_lock,omitempty"`
	CreatedAt           string  `json:"created_at"`
	UpdatedAt           string  `json:"updated_at"`
	Remark              string  `json:"remark"`
	IsActive            string  `json:"is_active"`
	ChangePasswordLock  string  `json:"is_change_password_lock"`
	CrDe                int64   `json:"cr_de,omitempty"`
}

type ManageUsers struct {
	Id        int64  `json:"id"`
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
	Password  string `json:"password,omitempty"`
	Mobile    string `json:"phone,omitempty"`
	City      string `json:"city,omitempty"`
	Email     string `json:"email,omitempty"`
	Country   string `json:"country,omitempty"`
	Role      string `json:"role,omitempty"`
	Active    string `json:"active,omitempty"`
	Edit      bool   `json:"edit"`
	Delete    bool   `json:"delete"`
}

type LoginData struct {
	UserId     int64  `json:"user_id"`
	UserTypeId int64  `json:"user_type_id"`
	UserName   string `json:"user_name"`
	Name       string `json:"name"`
	City       string `json:"city"`
	Mobile     string `json:"mobile"`
	Token      string `json:"token"`
}

type TokenData struct {
	UserId           int64  `json:"user_id"`
	UserTypeId       int64  `json:"user_type_id"`
	ParentId         int64  `json:"parent_id"`
	ParentIds        string `json:"parent_ids,omitempty"`
	UserName         string `json:"user_name"`
	Name             string `json:"name"`
	LoginDateTime    string `json:"login_date_time"`
	IpAddress        string `json:"ip_address"`
	City             string `json:"city"`
	Mobile           string `json:"mobile"`
	Token            string `json:"token"`
	UserAgent        string `json:"browser_info"`
	IsChangePassword string `json:"is_change_password"`
}

type LoginDataLog struct {
	UserId             int64  `json:"user_id"`
	ParentIds          string `json:"parent_ids,omitempty"`
	UserName           string `json:"user_name"`
	LoginTime          string `json:"login_time,omitempty"`
	PasswordChangeTime string `json:"password_change_time,omitempty"`
	IpAddress          string `json:"ip_address"`
	BrowserInfo        string `json:"browser_info"`
	TimeStamp          int64  `json:"timestamp"`
	Location           string `json:"location"`
}

type LoginDataLogInput struct {
	UserId             sql.NullInt64
	ParentIds          sql.NullString
	UserName           sql.NullString
	LoginTime          sql.NullString
	PasswordChangeTime sql.NullString
	IpAddress          sql.NullString
	BrowserInfo        sql.NullString
	TimeStamp          sql.NullInt64
	Location           sql.NullString
}
