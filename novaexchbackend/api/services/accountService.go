package services

import (
	"log"
	"novaexchbackend/api/config"
	"novaexchbackend/api/models"
	"strconv"
)

//DepositWithdrawl :
func DepositWithdrawl(account models.AccountStatement, userData models.User, parentData models.User) (string, bool) {

	// var message, description string
	var message string
	var accountStatementParent models.AccountStatement

	amount := account.Amount

	if account.CrDe == 2 {
		updateQuery := "UPDATE users SET available_chip = available_chip - ?, total_chip = total_chip - ? WHERE id = ?"
		stmt, err := config.MySQL.Prepare(updateQuery)
		if err != nil {
			return "Unable to Debit Amount", false
		}
		defer stmt.Close()
		_, err = stmt.Exec(account.Amount, account.Amount, account.UserId)
		if err != nil {
			return "Unable to Debit Amount", false
		}
		account.Amount = -(account.Amount)
		message = "Amount Debited Successfully"
	} else {
		updateQuery := "UPDATE users SET available_chip = available_chip + ?, total_chip = total_chip + ? WHERE id = ?"
		stmt, err := config.MySQL.Prepare(updateQuery)
		if err != nil {
			return "Unable to Credit Amount", false
		}
		defer stmt.Close()
		_, err = stmt.Exec(account.Amount, account.Amount, account.UserId)
		if err != nil {
			return "Unable to Credit Amount", false
		}

		message = "Amount Credited Successfully"
	}
	if account.Description == "" {
		if account.CrDe == 1 {
			account.Description = "Amount Credited"
			accountStatementParent.Description = "Amount Debited"
		} else {
			account.Description = "Amount Debited"
			accountStatementParent.Description = "Amount Credited"
		}
	} else {
		accountStatementParent.Description = account.Description
	}
	account.AvailableBalance = userData.AvailableChip + account.Amount
	insertStr := `INSERT INTO account_statements (user_id, parent_id, description, statement_type, amount, available_balance, transaction_for, operator_name, user_name, created_at) VALUES (?,?,?,?,?,?,"1",?,?,NOW())`
	stmt, err := config.MySQL.Prepare(insertStr)
	if err != nil {
		log.Println("DepositWithdrawl >> Unable create Account Statement", err.Error())
	}
	defer stmt.Close()
	_, err = stmt.Exec(account.UserId, account.ParentId, account.Description, account.StatementType, account.Amount, account.AvailableBalance, account.OperatorName, account.UserName)
	if err != nil {
		log.Println("DepositWithdrawl >> Unable create Account Statement", err.Error())
	}

	if userData.UserTypeId != 1 {
		accountStatementParent.UserId = parentData.Id
		accountStatementParent.ParentId = parentData.ParentId
		accountStatementParent.StatementType = account.StatementType
		accountStatementParent.Type = account.Type
		accountStatementParent.Amount = amount

		if account.CrDe == 2 {
			updateQuery := "UPDATE users SET available_chip = available_chip + ? WHERE id = ?"
			stmt, err := config.MySQL.Prepare(updateQuery)
			if err != nil {
				log.Println("DepositWithdrawl >> Unable to update users", err.Error())
			}
			defer stmt.Close()
			_, err = stmt.Exec(accountStatementParent.Amount, accountStatementParent.UserId)
			if err != nil {
				log.Println("DepositWithdrawl >> Unable to update users", err.Error())
			}
		} else {
			updateQuery := "UPDATE users SET available_chip = available_chip - ? WHERE id = ?"
			stmt, err := config.MySQL.Prepare(updateQuery)
			if err != nil {
				log.Println("DepositWithdrawl >> Unable to update users", err.Error())
			}
			defer stmt.Close()
			_, err = stmt.Exec(accountStatementParent.Amount, accountStatementParent.UserId)
			if err != nil {
				log.Println("DepositWithdrawl >> Unable to update users", err.Error())
			}
			accountStatementParent.Amount = -(accountStatementParent.Amount)
		}
		accountStatementParent.AvailableBalance = parentData.AvailableChip + accountStatementParent.Amount
		insertStr := `INSERT INTO account_statements (user_id, parent_id, description, statement_type, amount, available_balance, transaction_for, operator_name, user_name, created_at) VALUES (?,?,?,?,?,?,"2",?,?,NOW())`
		stmt, err := config.MySQL.Prepare(insertStr)
		if err != nil {
			log.Println("DepositWithdrawl >> Unable create Account Statement", err.Error())

		}
		defer stmt.Close()
		_, err = stmt.Exec(accountStatementParent.UserId, accountStatementParent.ParentId, accountStatementParent.Description, accountStatementParent.StatementType, accountStatementParent.Amount, accountStatementParent.AvailableBalance, account.OperatorName, account.UserName)
		if err != nil {
			log.Println("DepositWithdrawl >> Unable create Account Statement", err.Error())
		}
	}

	return message, true
}

//DepositWithdrawlHistory :
func DepositWithdrawlHistory(userId int64, page int64, fromDate, toDate string, isDownload int64) (count int64, limit int64, result []models.ChipInOutResult, status bool) {
	if page == 0 {
		page = 1
	}
	limit = int64(config.PageLimit)
	offset := (page - 1) * limit
	var pageCount models.ResultCount
	var whereCondition string
	if fromDate != "" {
		whereCondition += " AND DATE(created_at) >= DATE('" + fromDate + "')"
	}
	if toDate != "" {
		whereCondition += " AND DATE(created_at) <= DATE('" + toDate + "')"
	}
	if page == 1 && isDownload != 1 {
		err := config.MySQL.QueryRow("Select count(id) AS total_count FROM account_statements Where user_id  = ?"+whereCondition, userId).Scan(&pageCount.TotalCount)
		if err != nil {
			log.Println("DepositWithdrawlHistory >> get_history", err.Error())
		}
	}

	if isDownload == 1 {
		rows, err := config.MySQL.Query(" Select description, parent_id, created_at, user_id, amount, id,user_name,operator_name FROM account_statements Where user_id = ? and statement_type=? "+whereCondition+" ORDER BY id desc", userId, "1")
		if err != nil {
			log.Println("DepositWithdrawlHistory >> get_history", err.Error())
			return int64(pageCount.TotalCount), limit, result, true
		}
		defer rows.Close()
		for rows.Next() {
			var content models.ChipInOutResultInput
			err := rows.Scan(
				&content.Description,
				&content.ParentId,
				&content.CreatedAt,
				&content.UserId,
				&content.Amount,
				&content.Id,
				&content.UserName,
				&content.OperatorName,
			)
			if err != nil {
				log.Println("DepositWithdrawlHistory >> get_history", err.Error())
			}
			var singleHistory models.ChipInOutResult
			singleHistory.Description = content.Description.String
			singleHistory.ParentId = content.ParentId.Int64
			singleHistory.CreatedAt = content.CreatedAt.String
			singleHistory.UserId = content.UserId.Int64
			singleHistory.Amount = content.Amount.Float64
			singleHistory.Id = content.Id.Int64
			singleHistory.UserName = content.UserName.String
			singleHistory.OperatorName = content.OperatorName.String

			result = append(result, singleHistory)
		}
	} else {
		rows, err := config.MySQL.Query(" Select description, parent_id, created_at, user_id, amount, id,user_name,operator_name FROM account_statements Where user_id = ? and statement_type=? "+whereCondition+" ORDER BY id desc LIMIT ?, ?", userId, "1", offset, limit)
		if err != nil {
			log.Println("DepositWithdrawlHistory >> get_history", err.Error())
			return int64(pageCount.TotalCount), limit, result, true
		}
		defer rows.Close()
		for rows.Next() {
			var content models.ChipInOutResultInput
			err := rows.Scan(
				&content.Description,
				&content.ParentId,
				&content.CreatedAt,
				&content.UserId,
				&content.Amount,
				&content.Id,
				&content.UserName,
				&content.OperatorName,
			)
			if err != nil {
				log.Println("DepositWithdrawlHistory >> get_history", err.Error())
			}
			var singleHistory models.ChipInOutResult
			singleHistory.Description = content.Description.String
			singleHistory.ParentId = content.ParentId.Int64
			singleHistory.CreatedAt = content.CreatedAt.String
			singleHistory.UserId = content.UserId.Int64
			singleHistory.Amount = content.Amount.Float64
			singleHistory.Id = content.Id.Int64
			singleHistory.UserName = content.UserName.String
			singleHistory.OperatorName = content.OperatorName.String
			result = append(result, singleHistory)
		}
	}

	return int64(pageCount.TotalCount), limit, result, status
}

// GetUserAccountStatementHistory :
func GetUserAccountStatementHistory(inputData models.InputGetUserAccountStatementHistory, userId int64) (count, limit int64, result []models.UserAccountStatementHistory, openingBalance float64) {
	userData := models.UserInput{}
	var sqlStr, countStr, subStr, sportStr, whereStr, searchStr1, searchStr2 string
	userID := strconv.Itoa(int(userId))
	if inputData.Page == 0 {
		inputData.Page = 1
	}
	limitQry := " LIMIT ?, ? "
	if inputData.IsDownload == 1 {
		limitQry = ""
	}
	limit = int64(config.PageLimit)
	offset := (inputData.Page - 1) * limit
	accountStatementSearch := ""
	if inputData.SearchClient != "" {
		userStr := "select id from users u where u.user_name='" + inputData.SearchClient + "' and FIND_IN_SET ('" + userID + "',u.parent_ids)"
		//accountStatementSearch = " AND (a.parent_id = " + userID + " OR a.user_id = " + userID + ") AND a.user_name='" + inputData.SearchClient + "'"
		err := config.MySQL.QueryRow(userStr).Scan(&userData.Id)
		if err != nil {
			// log.Println("GetUserAccountStatementHistory", err.Error())
		}
		userID = strconv.Itoa(int(userData.Id.Int64))
	}

	if inputData.Search != "" {
		searchStr1 = " AND (a.created_at LIKE '%" + inputData.Search + "%' OR a.description LIKE '%" + inputData.Search + "%' OR a.user_available_balance LIKE '%" + inputData.Search + "%')"

		searchStr2 = " AND (a.created_at LIKE '%" + inputData.Search + "%' OR a.description LIKE '%" + inputData.Search + "%' OR a.amount LIKE '%" + inputData.Search + "%' OR a.available_balance LIKE '%" + inputData.Search + "%' OR a.operator_name LIKE '%" + inputData.Search + "%' OR a.user_name LIKE '%" + inputData.Search + "%') "
	}
	accountStatementSearch = " AND a.user_id = " + userID
	if inputData.AccountType == "GR" {
		if inputData.SportId != "All" && inputData.SportId != "" && inputData.SportId != "U" && inputData.SportId != "D" && inputData.SportId != "0" {
			sportStr = " AND a.sport_id = " + inputData.SportId
		}
	}

	if inputData.FromDate != "" {
		subStr = " AND DATE(a.created_at) >= date('" + inputData.FromDate + "')"
	}
	if inputData.ToDate != "" {
		subStr = subStr + " AND DATE(a.created_at) <= date('" + inputData.ToDate + "')"
	}

	if inputData.AccountType == "GR" {
		if inputData.Page == 1 && inputData.IsDownload != 1 {
			countStr = "SELECT count(*) FROM (SELECT a.id,SUM(user_pl + user_commission) AS credit_debit, created_at FROM profit_loss a WHERE a.user_id = " + userID + subStr + sportStr + searchStr1 + " GROUP BY a.market_id, a.type) zz"
		}
		sqlStr = `SELECT created_at AS date, description, SUM(user_pl + user_commission) AS credit_debit, user_available_balance AS closing_balance, '' AS operator_name, '' AS user_name, match_id, market_id
		FROM profit_loss a WHERE user_id = ` + userID + subStr + sportStr + searchStr1 + ` GROUP BY a.market_id, a.type ORDER BY created_at ` + limitQry
	} else if inputData.AccountType == "BR" {
		if inputData.SportId == "upper" {
			whereStr = ` AND transaction_for = "1" `
		} else if inputData.SportId == "lower" {
			whereStr = ` AND transaction_for = "2" `
			// accountStatementSearch = " AND a.parent_id = " + userID
		}
		if inputData.Page == 1 && inputData.IsDownload != 1 {
			countStr = "SELECT count(a.id)" +
				" FROM account_statements AS a" +
				" WHERE statement_type = 1 " + accountStatementSearch + whereStr + subStr + searchStr2
		}
		sqlStr = "SELECT created_at AS date, description, amount AS credit_debit, available_balance AS closing_balance, operator_name, user_name, '' AS match_id,'' AS market_id" +
			" FROM account_statements AS a" +
			" WHERE statement_type = 1 " + accountStatementSearch + whereStr + subStr + searchStr2 +
			" ORDER BY a.created_at " + limitQry

	} else {
		if inputData.Page == 1 && inputData.IsDownload != 1 {
			countStr = "SELECT count(*) FROM (	SELECT a.user_id,a.amount AS credit_debit FROM account_statements AS a" +
				" WHERE 1=1" + accountStatementSearch + whereStr + " AND a.statement_type IN (1, 6) " + subStr + searchStr2 + " UNION ALL" +
				" SELECT a.user_id,SUM(a.user_pl + a.user_commission) AS credit_debit FROM profit_loss a" +
				" WHERE a.user_id = " + userID + subStr + sportStr + searchStr1 +
				" GROUP BY a.market_id, a.type) AS xx"
		}
		sqlStr = `SELECT xx.* FROM (
			SELECT created_at AS date, description, amount AS credit_debit, available_balance AS closing_balance, operator_name, user_name,'' AS match_id,'' AS market_id
			FROM account_statements AS a
			WHERE 1=1` + accountStatementSearch + whereStr + ` AND statement_type IN (1, 6)` + subStr + searchStr2 + ` UNION ALL
			SELECT a.created_at AS date, a.description as description, SUM(a.user_pl + a.user_commission) AS credit_debit, a.user_available_balance AS closing_balance,'' as operator_name, '' as user_name, a.match_id, a.market_id
			FROM profit_loss a
			WHERE user_id = ` + userID + subStr + sportStr + searchStr1 + ` GROUP BY a.market_id, a.type
			) AS xx ORDER BY xx.date` + limitQry

	}
	// fmt.Println(sqlStr)
	if inputData.IsDownload == 1 {
		rows, err := config.MySQL.Query(sqlStr)
		if err != nil {
			// log.Println("GetUserAccountStatementHistory >> statement history", err.Error())
			return count, limit, result, openingBalance
		}
		defer rows.Close()
		for rows.Next() {
			var content models.UserAccountStatementHistoryInput
			err := rows.Scan(
				&content.Date,
				&content.Description,
				&content.CreditDebit,
				&content.Balance,
				&content.OperatorName,
				&content.UserName,
				&content.MatchId,
				&content.MarketId,
			)
			if err != nil {
				// log.Println("GetUserAccountStatementHistory >> statement history", err.Error())
			}
			var singleHistory models.UserAccountStatementHistory
			singleHistory.Date = content.Date.String
			singleHistory.Description = content.Description.String
			singleHistory.CreditDebit = content.CreditDebit.Float64
			singleHistory.Balance = content.Balance.Float64
			singleHistory.OperatorName = content.OperatorName.String
			singleHistory.UserName = content.UserName.String
			singleHistory.MatchId = content.MatchId.String
			singleHistory.MarketId = content.MarketId.String
			if singleHistory != (models.UserAccountStatementHistory{}) {
				result = append(result, singleHistory)
			}
		}
	} else {
		rows, err := config.MySQL.Query(sqlStr, offset, limit)
		if err != nil {
			// log.Println("GetUserAccountStatementHistory >> statement history", err.Error())
			return count, limit, result, openingBalance
		}
		defer rows.Close()
		for rows.Next() {
			var content models.UserAccountStatementHistoryInput
			err := rows.Scan(
				&content.Date,
				&content.Description,
				&content.CreditDebit,
				&content.Balance,
				&content.OperatorName,
				&content.UserName,
				&content.MatchId,
				&content.MarketId,
			)
			if err != nil {
				// log.Println("GetUserAccountStatementHistory >> statement history", err.Error())
			}
			var singleHistory models.UserAccountStatementHistory
			singleHistory.Date = content.Date.String
			singleHistory.Description = content.Description.String
			singleHistory.CreditDebit = content.CreditDebit.Float64
			singleHistory.Balance = content.Balance.Float64
			singleHistory.OperatorName = content.OperatorName.String
			singleHistory.UserName = content.UserName.String
			singleHistory.MatchId = content.MatchId.String
			singleHistory.MarketId = content.MarketId.String
			if singleHistory != (models.UserAccountStatementHistory{}) {
				result = append(result, singleHistory)
			}
		}
	}
	if inputData.Page == 1 && inputData.IsDownload != 1 {
		//fmt.Println(countStr)
		err := config.MySQL.QueryRow(countStr).Scan(&count)
		if err != nil {
			// log.Println("GetUserAccountStatementHistory >> history count", err.Error())
		}
	}
	// if inputData.Page != 1 {
	// 	if len(result) > 0 {
	// 		inputData.FromDate = result[0].Date
	// 	}
	// }
	// // Openeing Balance
	// if inputData.FromDate != "" {
	// 	// var balanceInput sql.NullFloat64
	// 	// balanceStr := `SELECT available_balance AS opening_balance FROM account_statements
	// 	// 	WHERE user_id = ?
	// 	// 	ORDER BY created_at asc LIMIT 1`
	// 	// err := config.MySQL.QueryRow(balanceStr, userID).Scan(&balanceInput)
	// 	// if err != nil && err != sql.ErrNoRows {
	// 	// 	// log.Println("GetUserAccountStatementHistory >> Unable to get opening balance", err.Error())
	// 	// }
	// 	if inputData.Page == 1 {
	// 		subStr = " AND DATE(a.created_at) < date('" + inputData.FromDate + "')"
	// 	} else {
	// 		subStr = " AND a.created_at < '" + inputData.FromDate + "'"
	// 	}
	// 	if inputData.AccountType == "GR" {
	// 		sqlStr = `SELECT created_at AS date, SUM(user_pl + user_commission) AS credit_debit
	// 		FROM user_profit_loss a WHERE user_id = ` + userID + subStr + sportStr + searchStr1 + ` GROUP BY a.market_id, a.type ORDER BY created_at `
	// 	} else if inputData.AccountType == "BR" {
	// 		if inputData.SportId == "upper" {
	// 			whereStr = ` AND transaction_for = "1" `
	// 		} else if inputData.SportId == "lower" {
	// 			whereStr = ` AND transaction_for = "2" `
	// 			// accountStatementSearch = " AND a.parent_id = " + userID
	// 		}
	// 		sqlStr = "SELECT created_at AS date, amount AS credit_debit" +
	// 			" FROM account_statements AS a" +
	// 			" WHERE statement_type = 1 " + accountStatementSearch + subStr + whereStr + searchStr2 +
	// 			" ORDER BY a.created_at "

	// 	} else {
	// 		sqlStr = `SELECT xx.* FROM (
	// 			SELECT created_at AS date, amount AS credit_debit
	// 			FROM account_statements AS a
	// 			WHERE 1=1` + accountStatementSearch + whereStr + subStr + ` AND statement_type IN (1, 6)` + searchStr2 + ` UNION ALL
	// 			SELECT a.created_at AS date, SUM(a.user_pl + a.user_commission) AS credit_debit
	// 			FROM user_profit_loss a
	// 			WHERE user_id = ` + userID + subStr + sportStr + searchStr1 + ` GROUP BY a.market_id, a.type) AS xx ORDER BY xx.date`
	// 	}
	// 	rows, err := config.MySQL.Query(sqlStr)
	// 	if err != nil {
	// 		// log.Println("GetUserAccountStatementHistory >> statement history", err.Error())
	// 		return count, limit, result, openingBalance
	// 	}
	// 	defer rows.Close()
	// 	for rows.Next() {
	// 		var content models.UserAccountStatementHistoryInput
	// 		err := rows.Scan(
	// 			&content.Date,
	// 			&content.CreditDebit,
	// 		)
	// 		if err != nil {
	// 			// log.Println("GetUserAccountStatementHistory >> statement history", err.Error())
	// 		}
	// 		openingBalance += content.CreditDebit.Float64
	// 	}
	// 	//openingBalance = balanceInput.Float64
	// }

	// if inputData.FromDate != "" {
	// 	if inputData.Page != 1 && inputData.LastBalance != 0 {
	// 		openingBalance = inputData.LastBalance
	// 	} else {
	// 		var balanceInput sql.NullFloat64
	// 		balanceStr := `SELECT available_balance AS opening_balance FROM account_statements
	// 	WHERE user_id = ? AND statement_type != 0 AND Date(created_at) < Date('` + inputData.FromDate + `')
	// 	ORDER BY created_at desc LIMIT 1`
	// 		if inputData.Page != 1 {
	// 			balanceStr = `SELECT available_balance AS opening_balance FROM account_statements
	// 	WHERE user_id = ? AND statement_type != 0 AND created_at < '` + inputData.FromDate + `'
	// 	ORDER BY created_at desc LIMIT 1`
	// 		}
	// 		err := config.MySQL.QueryRow(balanceStr, userID).Scan(&balanceInput)
	// 		if err != nil && err != sql.ErrNoRows {
	// 			// log.Println("GetUserAccountStatementHistory >> Unable to get opening balance", err.Error())
	// 		}
	// 		openingBalance = balanceInput.Float64
	// 	}
	// }
	closingBalance := openingBalance
	for i, singleRecord := range result {
		//if singleRecord.CreditDebit > 0 {
		closingBalance += singleRecord.CreditDebit
		// } else {
		// 	closingBalance -= singleRecord.CreditDebit
		// }
		result[i].Balance = closingBalance
	}

	return count, limit, result, openingBalance
}
