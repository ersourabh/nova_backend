package services

import (
	"database/sql"
	"fmt"
	"log"
	"math"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"strconv"
	"strings"
)

//DeclareMarket :
func DeclareMarket(betData models.ResultInput) (string, bool) {
	var marketName, matchName sql.NullString
	config.MySQL.QueryRow(`SELECT name, match_name FROM markets where market_id = ? AND match_id = ? AND is_result_declared = '0' AND is_fancy = '0'`, betData.MarketId, betData.MatchId).Scan(&marketName, &matchName)
	if marketName.String == "" {
		return "Market Not Found/Already Declared", true
	}
	tx, err := config.MySQL.Begin()
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	marketUpdateStr := `UPDATE markets SET winner_id = ?, result = ?, result_date = NOW(), is_result_declared = '1' WHERE market_id = ? AND match_id = ?`
	marketStmt, err := tx.Prepare(marketUpdateStr)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	defer marketStmt.Close()
	_, err = marketStmt.Exec(betData.SelectionId, betData.SelectionName, betData.MarketId, betData.MatchId)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	if strings.ToUpper(marketName.String) == "MATCH ODDS" || strings.ToUpper(marketName.String) == "WINNER" || strings.ToUpper(marketName.String) == "BOOKMAKER" {
		matchUpdateStr := `UPDATE matches SET is_resulted = '1' WHERE match_id = ?`
		matchStmt, err := tx.Prepare(matchUpdateStr)
		if err != nil {
			tx.Rollback()
			return "Unable Declare", true
		}
		defer matchStmt.Close()
		_, err = matchStmt.Exec(betData.MatchId)
		if err != nil {
			tx.Rollback()
			return "Unable Declare", true
		}
	}
	betUpdateStr := `UPDATE bets SET 
						winloss = CASE WHEN selection_id = ? THEN
							CASE WHEN is_back = '0' THEN exposure ELSE profit END
  							ELSE
	 						CASE WHEN is_back = '0' THEN stack ELSE - stack END
  						END,
						winner_name = ?, winner_id = ?, result_date = NOW(), is_result_declared='1'
					WHERE match_id = ? AND result_date is null AND market_id = ?`
	betStmt, err := tx.Prepare(betUpdateStr)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	defer betStmt.Close()
	_, err = betStmt.Exec(betData.SelectionId, betData.SelectionName, betData.SelectionId, betData.MatchId, betData.MarketId)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	tx, err = setResultMarket(betData.MatchId, betData.MarketId, matchName.String, marketName.String, "0", tx)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	tx.Commit()
	return "Declared Successfully", false
}
func setResultMarket(matchId, marketId, marketName, matchName, liabilityType string, tx *sql.Tx) (*sql.Tx, error) {
	// Get all users and users-bets details
	userBetsCalc := map[string]models.BetsOdd{}
	usersMap, userBets, err := GetUserBetsForCalc(matchId, marketId, tx)
	if err != nil {
		// log.Println("GetUserBetsForCalc >> get usersbets and users", err.Error())
		return tx, err
	} else {
		for _, userBet := range userBets {

			for _, bet := range userBet {
				userCalc := userBetsCalc[strconv.Itoa(int(bet.UserId.Int64))+"_"+strconv.Itoa(int(bet.UserId.Int64))]

				users := usersMap[bet.UserId.Int64]

				// Users parents calculation for user-profit loss
				// matchCommission := 0
				parentIDs := strings.Split(global.TrimCommaBothSide(bet.ParentIds.String), ",")

				for _, pID := range parentIDs {
					parentID, _ := strconv.Atoi(pID)

					if usr, ok := users[int64(parentID)]; ok {
						parentCalc := userBetsCalc[pID+"_"+strconv.Itoa(int(bet.UserId.Int64))]
						parentCalc.UserId = int64(parentID)
						parentCalc.SportId = bet.SportId.String
						parentCalc.MatchId = bet.MatchId.String
						parentCalc.MarketId = bet.MarketId.String
						parentCalc.SelectionId = bet.SelectionId.String
						parentCalc.MarketName = bet.MarketName.String
						parentCalc.SelectionName = bet.SelectionName.String
						parentCalc.WinnerName = bet.WinnerName.String
						parentCalc.WinnerSelectionId = bet.WinnerId.String
						parentCalc.ResultDeclaredAt = bet.ResultDate.String
						parentCalc.Odds = bet.Odds.Float64
						parentCalc.Stack += float64(bet.Stack.Int64)
						parentCalc.IsBack = bet.IsBack.String
						parentCalc.Chips = bet.Winloss.Float64

						if bet.Winloss.Float64 > 0 {
							if marketName != "Bookmaker" {
								// comm := (math.Abs(bet.Winloss.Float64) * float64(matchCommission) / 100)
								parentCalc.UserCommission += ((bet.Winloss.Float64 * math.Abs(float64(usr.MarketCommission))) / 100)
								parentCalc.Commission = float64(usr.MarketCommission)
							} else {
								// comm := (math.Abs(bet.Winloss.Float64) * float64(matchCommission) / 100)
								parentCalc.UserCommission += ((bet.Winloss.Float64 * math.Abs(float64(usr.BookmakerCommission))) / 100)
								parentCalc.Commission = float64(usr.BookmakerCommission)

							}
						} else {
							if marketName != "Bookmaker" {
								userCalc.Commission = float64(usr.MarketCommission)
							} else {
								userCalc.Commission = float64(usr.BookmakerCommission)
							}
						}

						if bet.Winloss.Float64 < 0 {
							parentCalc.PL += math.Abs((bet.Winloss.Float64 * float64(usr.Partnership)) / 100)
						} else {
							parentCalc.PL += -(math.Abs((bet.Winloss.Float64 * float64(usr.Partnership)) / 100))
						}

						plStr := ""
						if parentCalc.Chips >= 0 {
							plStr = " Profit "
						} else {
							plStr = " Loss "
						}

						parentCalc.UserIds = bet.ParentIds.String + strconv.Itoa(int(bet.UserId.Int64)) + ","
						parentCalc.Partnership = usr.Partnership
						if bet.SportId.String == "-1" {
							parentCalc.Description = matchName + marketName + "(" + bet.MarketId.String + ")" + plStr + " [ Winner : " + bet.WinnerName.String + " ]"
						} else {
							parentCalc.Description = matchName + "->" + marketName + plStr + " [ Winner : " + bet.WinnerName.String + " ]"
						}

						userBetsCalc[pID+"_"+strconv.Itoa(int(bet.UserId.Int64))] = parentCalc
					}
				}

				// Users calculation for user-profit loss
				userCalc.UserId = bet.UserId.Int64
				userCalc.SportId = bet.SportId.String
				userCalc.MatchId = bet.MatchId.String
				userCalc.MarketId = bet.MarketId.String
				userCalc.SelectionId = bet.SelectionId.String
				userCalc.MarketName = bet.MarketName.String
				userCalc.SelectionName = bet.SelectionName.String
				userCalc.WinnerName = bet.WinnerName.String
				userCalc.WinnerSelectionId = bet.WinnerId.String
				userCalc.ResultDeclaredAt = bet.ResultDate.String
				userCalc.Odds = bet.Odds.Float64
				userCalc.Stack += (float64(bet.Stack.Int64))
				userCalc.IsBack = bet.IsBack.String
				userCalc.PL += bet.Winloss.Float64
				userCalc.Chips = bet.Winloss.Float64
				userCalc.UserIds = bet.ParentIds.String + strconv.Itoa(int(bet.UserId.Int64)) + ","

				if userValue, userValid := users[userCalc.UserId]; userValid {
					if bet.Winloss.Float64 > 0 {
						if marketName != "Bookmaker" {
							comm := (math.Abs(bet.Winloss.Float64) * float64(userValue.MarketCommission) / 100)
							userCalc.UserCommission += comm * -1
							userCalc.Commission = float64(userValue.MarketCommission)
						} else {
							comm := (math.Abs(bet.Winloss.Float64) * float64(userValue.MarketCommission) / 100)
							userCalc.UserCommission += comm * -1
							userCalc.Commission = float64(userValue.MarketCommission)
						}
					} else {
						if marketName != "Bookmaker" {
							userCalc.Commission = float64(userValue.MarketCommission)
						} else {
							userCalc.Commission = float64(userValue.BookmakerCommission)
						}
					}
					userCalc.Partnership = userValue.Partnership
				}

				plStr := ""
				if userCalc.Chips >= 0 {
					plStr = " Profit "
				} else {
					plStr = " Loss "
				}

				// userCalc.Description = matchName + " : " + marketName + plStr + " [ Winner : " + selectionName + " ]"
				if bet.SportId.String == "-1" {
					userCalc.Description = matchName + " " + bet.MarketId.String + plStr + " [ Winner : " + bet.WinnerName.String + " ]"
				} else {
					userCalc.Description = matchName + " " + marketName + plStr + " [ Winner : " + bet.WinnerName.String + " ]"
				}
				userBetsCalc[strconv.Itoa(int(bet.UserId.Int64))+"_"+strconv.Itoa(int(bet.UserId.Int64))] = userCalc
			}
		}
	}
	// Insert calculation in user-profit-loss
	chunksize := 1000
	valueStrings := []string{}
	valueArgs := []interface{}{}

	// Set user profit loss for match_id+market_id with user_profit_loss table
	insertStr := `INSERT INTO profit_loss (user_id, sport_id, match_id, market_id, type, bet_result_id, stack, user_pl, user_commission, user_parent_ids, partnership, commission, description, created_at) VALUES %s`
	for _, content := range userBetsCalc {
		if len(valueStrings) == chunksize {
			insertStr = fmt.Sprintf(insertStr, strings.Join(valueStrings, ","))
			_, err = tx.Exec(insertStr, valueArgs...)
			if err != nil {
				log.Println("UserProfitLoss >> Insertion failed for User Profit loss for user ", err.Error())
				return tx, err
			}
			valueStrings = []string{}
			valueArgs = []interface{}{}
		}
		valueStrings = append(valueStrings, "(?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())")

		valueArgs = append(valueArgs, content.UserId)
		valueArgs = append(valueArgs, content.SportId)
		valueArgs = append(valueArgs, content.MatchId)
		valueArgs = append(valueArgs, content.MarketId)
		valueArgs = append(valueArgs, "1")
		valueArgs = append(valueArgs, content.BetResultId)
		valueArgs = append(valueArgs, content.Stack)
		valueArgs = append(valueArgs, content.PL)
		valueArgs = append(valueArgs, content.UserCommission)
		valueArgs = append(valueArgs, content.UserIds)
		valueArgs = append(valueArgs, content.Partnership)
		valueArgs = append(valueArgs, content.Commission)
		valueArgs = append(valueArgs, content.Description)
	}
	if len(valueArgs) > 0 {
		insertStr = fmt.Sprintf(insertStr, strings.Join(valueStrings, ","))
		_, err = tx.Exec(insertStr, valueArgs...)
		if err != nil {
			log.Println("UserProfitLoss >> Insertion failed for User Profit loss for user ", err.Error())
			return tx, err
		}
	}
	tx, err = UpdateBalanceLiability(matchId, marketId, "0", liabilityType, 0, tx)
	if err != nil {
		// log.Println("UpdateBalanceLiability >> Updatation failed for Users ", err.Error())
		return tx, err
	}
	// Set/Update user_profit_loss for match_id+market_id with bet result
	tx, err = UpdateBalanceOnResult(matchId, marketId, 0, 0, tx)
	if err != nil {
		tx.Rollback()
		// log.Println("UpdateBalanceOnResult >> Updatation failed for Users and Users Profit Loss", err.Error())
		return tx, err
	}
	return tx, nil
}
func setResultSession(matchId, marketId, marketName, matchName, liabilityType string, tx *sql.Tx) (*sql.Tx, error) {
	// Get all users and users-bets details
	userBetsCalc := map[string]models.BetsOdd{}
	usersMap, userBets, err := GetUserBetsForCalc(matchId, marketId, tx)
	if err != nil {
		// log.Println("GetUserBetsForCalc >> get usersbets and users", err.Error())
		return tx, err
	} else {
		for _, userBet := range userBets {

			for _, bet := range userBet {
				userCalc := userBetsCalc[strconv.Itoa(int(bet.UserId.Int64))+"_"+strconv.Itoa(int(bet.UserId.Int64))]

				users := usersMap[bet.UserId.Int64]

				// Users parents calculation for user-profit loss
				// matchCommission := 0
				parentIDs := strings.Split(global.TrimCommaBothSide(bet.ParentIds.String), ",")

				for _, pID := range parentIDs {
					parentID, _ := strconv.Atoi(pID)

					if usr, ok := users[int64(parentID)]; ok {
						parentCalc := userBetsCalc[pID+"_"+strconv.Itoa(int(bet.UserId.Int64))]
						parentCalc.UserId = int64(parentID)
						parentCalc.SportId = bet.SportId.String
						parentCalc.MatchId = bet.MatchId.String
						parentCalc.MarketId = bet.MarketId.String
						parentCalc.SelectionId = bet.SelectionId.String
						parentCalc.MarketName = bet.MarketName.String
						parentCalc.SelectionName = bet.SelectionName.String
						parentCalc.WinnerName = bet.WinnerName.String
						parentCalc.WinnerSelectionId = bet.WinnerId.String
						parentCalc.ResultDeclaredAt = bet.ResultDate.String
						parentCalc.Odds = bet.Odds.Float64
						parentCalc.Stack += float64(bet.Stack.Int64)
						parentCalc.IsBack = bet.IsBack.String
						parentCalc.Chips = bet.Winloss.Float64

						if bet.Winloss.Float64 > 0 {
							parentCalc.UserCommission += ((bet.Winloss.Float64 * math.Abs(float64(usr.MarketCommission))) / 100)
							parentCalc.Commission = float64(usr.SessionCommission)

						} else {
							userCalc.Commission = float64(usr.SessionCommission)
						}

						if bet.Winloss.Float64 < 0 {
							parentCalc.PL += math.Abs((bet.Winloss.Float64 * float64(usr.Partnership)) / 100)
						} else {
							parentCalc.PL += -(math.Abs((bet.Winloss.Float64 * float64(usr.Partnership)) / 100))
						}

						plStr := ""
						if parentCalc.Chips >= 0 {
							plStr = " Profit "
						} else {
							plStr = " Loss "
						}

						parentCalc.UserIds = bet.ParentIds.String + strconv.Itoa(int(bet.UserId.Int64)) + ","
						parentCalc.Partnership = usr.Partnership

						parentCalc.Description = matchName + "->" + marketName + plStr + " [ Winner : " + bet.WinnerName.String + " ]"

						userBetsCalc[pID+"_"+strconv.Itoa(int(bet.UserId.Int64))] = parentCalc
					}
				}

				// Users calculation for user-profit loss
				userCalc.UserId = bet.UserId.Int64
				userCalc.SportId = bet.SportId.String
				userCalc.MatchId = bet.MatchId.String
				userCalc.MarketId = bet.MarketId.String
				userCalc.SelectionId = bet.SelectionId.String
				userCalc.MarketName = bet.MarketName.String
				userCalc.SelectionName = bet.SelectionName.String
				userCalc.WinnerName = bet.WinnerName.String
				userCalc.WinnerSelectionId = bet.WinnerId.String
				userCalc.ResultDeclaredAt = bet.ResultDate.String
				userCalc.Odds = bet.Odds.Float64
				userCalc.Stack += (float64(bet.Stack.Int64))
				userCalc.IsBack = bet.IsBack.String
				userCalc.PL += bet.Winloss.Float64
				userCalc.Chips = bet.Winloss.Float64
				userCalc.UserIds = bet.ParentIds.String + strconv.Itoa(int(bet.UserId.Int64)) + ","

				if userValue, userValid := users[userCalc.UserId]; userValid {
					if bet.Winloss.Float64 > 0 {
						comm := (math.Abs(bet.Winloss.Float64) * float64(userValue.MarketCommission) / 100)
						userCalc.UserCommission += comm * -1
						userCalc.Commission = float64(userValue.SessionCommission)

					} else {
						userCalc.Commission = float64(userValue.SessionCommission)
					}

					userCalc.Partnership = userValue.Partnership
				}

				plStr := ""
				if userCalc.Chips >= 0 {
					plStr = " Profit "
				} else {
					plStr = " Loss "
				}

				userCalc.Description = matchName + " " + marketName + plStr + " [ Winner : " + bet.WinnerName.String + " ]"

				userBetsCalc[strconv.Itoa(int(bet.UserId.Int64))+"_"+strconv.Itoa(int(bet.UserId.Int64))] = userCalc
			}
		}
	}
	// Insert calculation in user-profit-loss
	chunksize := 1000
	valueStrings := []string{}
	valueArgs := []interface{}{}

	// Set user profit loss for match_id+market_id with user_profit_loss table
	insertStr := `INSERT INTO profit_loss (user_id, sport_id, match_id, market_id, type, bet_result_id, stack, user_pl, user_commission, user_parent_ids, partnership, commission, description, created_at) VALUES %s`
	for _, content := range userBetsCalc {
		if len(valueStrings) == chunksize {
			insertStr = fmt.Sprintf(insertStr, strings.Join(valueStrings, ","))
			_, err = tx.Exec(insertStr, valueArgs...)
			if err != nil {
				log.Println("UserProfitLoss >> Insertion failed for User Profit loss for user ", err.Error())
				return tx, err
			}
			valueStrings = []string{}
			valueArgs = []interface{}{}
		}
		valueStrings = append(valueStrings, "(?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())")

		valueArgs = append(valueArgs, content.UserId)
		valueArgs = append(valueArgs, content.SportId)
		valueArgs = append(valueArgs, content.MatchId)
		valueArgs = append(valueArgs, content.MarketId)
		valueArgs = append(valueArgs, "2")
		valueArgs = append(valueArgs, content.BetResultId)
		valueArgs = append(valueArgs, content.Stack)
		valueArgs = append(valueArgs, content.PL)
		valueArgs = append(valueArgs, content.UserCommission)
		valueArgs = append(valueArgs, content.UserIds)
		valueArgs = append(valueArgs, content.Partnership)
		valueArgs = append(valueArgs, content.Commission)
		valueArgs = append(valueArgs, content.Description)
	}
	if len(valueArgs) > 0 {
		insertStr = fmt.Sprintf(insertStr, strings.Join(valueStrings, ","))
		_, err = tx.Exec(insertStr, valueArgs...)
		if err != nil {
			log.Println("UserProfitLoss >> Insertion failed for User Profit loss for user ", err.Error())
			return tx, err
		}
	}
	// Set/Update user_profit_loss for match_id+market_id with bet result
	tx, err = UpdateBalanceOnResult(matchId, marketId, 1, 0, tx)
	if err != nil {
		tx.Rollback()
		// log.Println("UpdateBalanceOnResult >> Updatation failed for Users and Users Profit Loss", err.Error())
		return tx, err
	}
	return tx, nil
}

//GetUserBetsForCalc :
func GetUserBetsForCalc(matchId, marketId string, tx *sql.Tx) (map[int64]map[int64]models.User, map[int64][]models.BetInput, error) {
	usersBets := map[int64][]models.BetInput{}
	users := map[int64]map[int64]models.User{}

	sqlStr := `SELECT b.user_id, u.parent_ids, u.user_type_id, b.sport_id, b.match_id, b.market_id, sum(b.stack) as stack, sum(b.winloss) as winloss, b.winner_id, b.winner_name
	FROM bets AS b
	INNER JOIN users AS u ON u.id = b.user_id
	WHERE b.match_id = ? and b.market_id = ? AND b.is_deleted = '0' AND is_matched='1' GROUP BY b.user_id`
	rows, err := tx.Query(sqlStr, matchId, marketId)
	if err != nil && err != sql.ErrNoRows {
		tx.Rollback()
		// log.Println("GetUserBetsForCalc", err.Error())
		return users, usersBets, err
	}
	defer rows.Close()
	for rows.Next() {
		var userBet models.BetInput
		err := rows.Scan(
			&userBet.UserId,
			&userBet.ParentIds,
			&userBet.UserTypeId,
			&userBet.SportId,
			&userBet.MatchId,
			&userBet.MarketId,
			&userBet.Stack,
			&userBet.Winloss,
			&userBet.WinnerId,
			&userBet.WinnerName,
		)
		if err != nil {
			// log.Println("GetUserBetsForCalc", err.Error())
		}

		users[userBet.UserId.Int64] = GetUserPartnership(userBet.ParentIds.String, userBet.UserId.Int64)

		usersBets[userBet.UserId.Int64] = append(usersBets[userBet.UserId.Int64], userBet)
	}
	return users, usersBets, nil
}

//UpdateBalanceLiability :
func UpdateBalanceLiability(matchId, marketId, isAdd, liabilityType string, userId int64, tx *sql.Tx) (*sql.Tx, error) {
	subStr := ""
	if userId != 0 {
		subStr = " AND user_id = " + strconv.Itoa(int(userId))
	}
	result, err := config.MySQL.Query("SELECT user_id, user_type_id, liability_type, stack, win_value, loss_value, win_loss FROM odds_profit_loss WHERE match_id = ? AND market_id = ? AND user_type_id = ?"+subStr, matchId, marketId, config.USER)
	if err != nil {
		// log.Println("UpdateBalanceLiability:", err.Error())
		return tx, err
	}

	positionMap := map[int64][]models.TeamPosition{}
	for result.Next() {
		var singlePositionInput models.TeamPositionInput
		result.Scan(
			&singlePositionInput.UserId,
			&singlePositionInput.UserTypeId,
			&singlePositionInput.LiabilityType,
			&singlePositionInput.Stack,
			&singlePositionInput.WinValue,
			&singlePositionInput.LossValue,
			&singlePositionInput.WinLoss,
		)
		var singlePosition models.TeamPosition
		singlePosition.UserId = singlePositionInput.UserId.Int64
		singlePosition.UserTypeId = singlePositionInput.UserTypeId.Int64
		singlePosition.LiabilityType = singlePositionInput.LiabilityType.String
		singlePosition.Stack = singlePositionInput.Stack.Float64
		singlePosition.WinValue = singlePositionInput.WinValue.Float64
		singlePosition.LossValue = singlePositionInput.LossValue.Float64
		singlePosition.WinLoss = singlePositionInput.WinLoss.Float64
		positionMap[singlePosition.UserId] = append(positionMap[singlePosition.UserId], singlePosition)
	}

	for key, positions := range positionMap {

		var liabilityTotalStack float64
		var liabilityMinVal []float64
		for _, position := range positions {
			if isAdd == "1" {
				if liabilityType == "1" && position.LiabilityType == "1" {
					liabilityTotalStack += -(position.Stack)
				} else {
					liabilityTotalStack += 0
				}

				if position.LiabilityType == "0" {
					liabilityMinVal = append(liabilityMinVal, position.WinValue+position.LossValue)
				} else {
					liabilityMinVal = append(liabilityMinVal, 0)
				}
			} else {
				if liabilityType == "1" && position.LiabilityType == "1" {
					liabilityTotalStack += -(position.Stack)
				} else {
					liabilityTotalStack += 0
				}

				if position.LiabilityType == "0" {
					liabilityMinVal = append(liabilityMinVal, position.WinValue+position.LossValue)
				} else {
					liabilityMinVal = append(liabilityMinVal, 0)
				}
			}

		}

		//Update data to users table for free chip and liability ==>
		var liabilityMinValue float64
		if len(liabilityMinVal) > 0 {
			liabilityMinValue = global.FindMin(liabilityMinVal)
		}

		if liabilityType == "1" || (liabilityTotalStack+liabilityMinValue) < 0 {
			var updateQuery string
			if isAdd == "1" {

				updateQuery = "UPDATE users SET available_chip = available_chip + (?), exposure = exposure + (?) WHERE id = ? "
			} else {

				updateQuery = "UPDATE users SET available_chip = available_chip - (?), exposure = exposure - (?) WHERE id = ?"
			}

			stmt, err := tx.Prepare(updateQuery)
			if err != nil {
				// log.Println("UpdateBalanceLiability >> Update users", err.Error())
				return tx, err
			}
			defer stmt.Close()
			_, err = stmt.Exec(liabilityTotalStack+liabilityMinValue, liabilityTotalStack+liabilityMinValue, key)
			if err != nil {
				// log.Println("UpdateBalanceLiability >> Update users", err.Error())
				return tx, err
			}

		}
	}

	return tx, nil
}

//UpdateBalanceOnResult :
func UpdateBalanceOnResult(matchId, marketId string, isFancy, isRollback int64, tx *sql.Tx) (*sql.Tx, error) {
	var userProfitLoss []models.UserProfitLoss
	if isFancy == 1 {
		userFancyLiabilityMap := map[int64]float64{}

		result, err := config.MySQL.Query("SELECT user_id, liability FROM session_possition WHERE match_id = ? AND fancy_id = ? AND user_type_id = ?", matchId, marketId, config.USER)
		if err != nil {
			// log.Println("UpdateBalanceOnResult:", err.Error())
			return tx, err
		}
		defer result.Close()
		for result.Next() {
			var fancyPosition models.FancyScorePositionInput
			result.Scan(
				&fancyPosition.UserId,
				&fancyPosition.Liability,
			)
			userFancyLiabilityMap[fancyPosition.UserId.Int64] += fancyPosition.Liability.Float64
		}
		//Get fancy score position from mongo as per user_id, match_id, market_id --------------------------------
		//Get Users for per fancy bet calculation -----------------------------------------------------------------
		sqlStr := `SELECT a.user_id, a.sport_id, a.match_id, a.market_id, a.type, a.stack, SUM(a.user_pl) as user_pl, SUM(a.user_commission) as user_commission, a.created_at, u.available_chip FROM profit_loss as a INNER JOIN users as u on u.id = a.user_id  WHERE  a.match_id = ? AND a.market_id = ?  AND  a.type = '2' GROUP BY  a.user_id`

		rows, err := tx.Query(sqlStr, matchId, marketId)
		if err != nil && err != sql.ErrNoRows {
			// log.Println("UpdateBalanceOnResult", err.Error())
			return tx, err
		}
		defer rows.Close()
		for rows.Next() {
			var data models.UserProfitLossInput
			var content models.UserProfitLoss
			err := rows.Scan(
				&data.UserId,
				&data.SportId,
				&data.MatchId,
				&data.MarketId,
				&data.Type,
				&data.Stack,
				&data.UserPL,
				&data.UserCommission,
				&data.CreatedAt,
				&data.FreeChips,
			)
			if err != nil {
				// log.Println("UpdateBalanceOnResult", err.Error())
			}

			content.UserId = data.UserId.Int64
			content.SportId = data.SportId.String
			content.MatchId = data.MatchId.String
			content.MarketId = data.MarketId.String
			content.Type = data.Type.String
			content.Stack = data.Stack.Int64
			content.UserPL = data.UserPL.Float64
			content.UserCommission = data.UserCommission.Float64
			content.CreatedAt = data.CreatedAt.String
			content.UserTypeId = data.UserTypeId.Int64
			content.ParentIds = data.ParentIds.String
			content.FreeChips = data.FreeChips.Float64
			if value, OK := userFancyLiabilityMap[data.UserId.Int64]; OK {

				content.Liability = value
			}
			userProfitLoss = append(userProfitLoss, content)
		}
		for _, pl := range userProfitLoss {
			var _, totalBalance, liability, profitLoss float64
			subStr := ""
			if isRollback == 1 {
				subStr = "available_chip = available_chip - ? - ? + ?,total_chip = total_chip - ?,profit_loss = profit_loss - ?,exposure = exposure + ?"
				totalBalance = pl.UserPL + pl.UserCommission
				liability = pl.Liability
				profitLoss = pl.UserPL + pl.UserCommission
			} else {
				subStr = "available_chip = available_chip + ? + ? - ?,total_chip = total_chip + ?,profit_loss = profit_loss + ?,exposure = exposure - ?"
				totalBalance = pl.UserPL + pl.UserCommission
				liability = pl.Liability
				profitLoss = pl.UserPL + pl.UserCommission
			}

			//Update users as per fancy bet calculation for user -----------------------------------------------------------------
			updateQuery := "UPDATE users SET " + subStr + " WHERE id = ?"
			stmt, err := tx.Prepare(updateQuery)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				return tx, err
			}
			defer stmt.Close()
			// _, err = stmt.Exec(freeChip, totalBalance, profitLoss, liability, pl.UserId)
			_, err = stmt.Exec(pl.UserPL, pl.UserCommission, pl.Liability, totalBalance, profitLoss, liability, pl.UserId)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				return tx, err
			}
			if isRollback == 0 {
				// insertStr := `INSERT INTO account_statements (user_id, description, statement_type, match_id, market_id, amount, available_balance, created_at) VALUES (?,? ,? ,?,?,?,?,NOW())`
				// stmt1, err := tx.Prepare(insertStr)
				// if err != nil {
				// 	// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				// 	return err
				// }
				// defer stmt1.Close()
				// _, err = stmt1.Exec(pl.UserId, "Session P/L", "4", matchId, marketId, profitLoss, (pl.FreeChips + pl.UserPL + pl.UserCommission - pl.Liability))
				// if err != nil {
				// 	// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				// 	return err
				// }
			} else {
				// deletetStr := `DELETE FROM account_statements WHERE user_id = ? AND match_id = ? AND market_id = ? AND statement_type = ?`
				// stmt1, err := tx.Prepare(deletetStr)
				// if err != nil {
				// 	// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				// 	return err
				// }
				// defer stmt1.Close()
				// _, err = stmt1.Exec(pl.UserId, matchId, marketId, "4")
				// if err != nil {
				// 	// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				// 	return err
				// }
			}
		}
		// }
		if isRollback == 1 {
			deletetStr := `DELETE FROM profit_loss WHERE match_id = ? AND market_id = ?`
			stmt1, err := tx.Prepare(deletetStr)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				return tx, err
			}
			defer stmt1.Close()
			_, err = stmt1.Exec(matchId, marketId)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				return tx, err
			}
		} else {
			updateQuery := `UPDATE profit_loss AS pl
		INNER JOIN users u ON u.id = pl.user_id
		SET pl.user_available_balance = (u.available_chip - u.exposure)
		WHERE pl.match_id = ? AND pl.market_id = ?`
			stmt, err := tx.Prepare(updateQuery)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update user_profit_loss", err.Error())
				return tx, err
			}
			defer stmt.Close()
			_, err = stmt.Exec(matchId, marketId)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update user_profit_loss", err.Error())
				return tx, err
			}
		}
	} else {
		sqlStr := `SELECT user_id,sport_id,match_id,market_id,user_pl,user_commission, u.available_chip FROM profit_loss AS pl
		INNER JOIN users AS u ON u.id = pl.user_id
		WHERE pl.match_id = ? AND market_id = ?`

		rows, err := tx.Query(sqlStr, matchId, marketId)
		if err != nil && err != sql.ErrNoRows {
			// log.Println("UpdateBalanceOnResult", err.Error())
			return tx, err
		}
		defer rows.Close()
		for rows.Next() {
			var data models.UserProfitLossInput
			var content models.UserProfitLoss
			err := rows.Scan(
				&data.UserId,
				&data.SportId,
				&data.MatchId,
				&data.MarketId,
				&data.UserPL,
				&data.UserCommission,
				&data.FreeChips,
			)
			if err != nil {
				// log.Println("UpdateBalanceOnResult", err.Error())
			}

			content.UserId = data.UserId.Int64
			content.SportId = data.SportId.String
			content.MatchId = data.MatchId.String
			content.MarketId = data.MarketId.String
			content.Type = data.Type.String
			content.Stack = data.Stack.Int64
			content.UserPL = data.UserPL.Float64
			content.UserCommission = data.UserCommission.Float64
			content.CreatedAt = data.CreatedAt.String
			content.UserTypeId = data.UserTypeId.Int64
			content.FreeChips = data.FreeChips.Float64

			userProfitLoss = append(userProfitLoss, content)
		}
		for _, pl := range userProfitLoss {
			var freeChip, totalBalance, profitLoss float64
			subStr := ""
			if isRollback == 1 {
				subStr = "available_chip = available_chip - ?,total_chip = total_chip - ?,profit_loss = profit_loss - ?"
				freeChip = pl.UserPL + pl.UserCommission
				totalBalance = pl.UserPL + pl.UserCommission
				profitLoss = pl.UserPL + pl.UserCommission
			} else {
				subStr = "available_chip = available_chip + ?,total_chip = total_chip + ?,profit_loss = profit_loss + ?"
				freeChip = pl.UserPL + pl.UserCommission
				totalBalance = pl.UserPL + pl.UserCommission
				profitLoss = pl.UserPL + pl.UserCommission
			}

			//Update users as per other bet calculation -----------------------------------------------------------------
			updateQuery := "UPDATE users SET " + subStr + " WHERE id = ?"
			stmt, err := tx.Prepare(updateQuery)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				return tx, err
			}
			defer stmt.Close()
			_, err = stmt.Exec(freeChip, totalBalance, profitLoss, pl.UserId)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				return tx, err
			}
			if isRollback == 0 {
				// insertStr := `INSERT INTO account_statements (user_id, description, statement_type, match_id, market_id, amount, available_balance, created_at) VALUES (?,? ,? ,?,?,?,?,NOW())`
				// stmt1, err := tx.Prepare(insertStr)
				// if err != nil {
				// 	// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				// 	return tx, err
				// }
				// defer stmt1.Close()
				// _, err = stmt1.Exec(pl.UserId, "Match P/L", "2", matchId, marketId, profitLoss, (pl.FreeChips + freeChip))
				// if err != nil {
				// 	// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				// 	return tx, err
				// }
			} else {

				// deletetStr := `DELETE FROM account_statements WHERE user_id = ? AND match_id = ? AND market_id = ? AND statement_type = ?`
				// stmt1, err := tx.Prepare(deletetStr)
				// if err != nil {
				// 	// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				// 	return tx, err
				// }
				// defer stmt1.Close()
				// _, err = stmt1.Exec(pl.UserId, matchId, marketId, "2")
				// if err != nil {
				// 	// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				// 	return tx, err
				// }
			}
		}
		if isRollback == 1 {
			deletetStr := `DELETE FROM profit_loss WHERE match_id = ? AND market_id = ?`
			stmt1, err := tx.Prepare(deletetStr)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				return tx, err
			}
			defer stmt1.Close()
			_, err = stmt1.Exec(matchId, marketId)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update users", err.Error())
				return tx, err
			}
		} else {
			updateQuery := `UPDATE profit_loss AS pl
		INNER JOIN users u ON u.id = pl.user_id
		SET pl.user_available_balance = (u.available_chip - u.exposure)
		WHERE pl.match_id = ? AND market_id = ?`
			stmt, err := tx.Prepare(updateQuery)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update user_profit_loss", err.Error())
				return tx, err
			}
			defer stmt.Close()
			_, err = stmt.Exec(matchId, marketId)
			if err != nil {
				// log.Println("UpdateBalanceOnResult >> Update user_profit_loss", err.Error())
				return tx, err
			}
		}
	}
	return tx, nil
}

//DeclareSession :
func DeclareSession(betData models.ResultInput) (string, bool) {
	var marketName, matchName sql.NullString
	config.MySQL.QueryRow(`SELECT name, match_name FROM markets where market_id = ? AND match_id = ? AND is_result_declared = '0' AND is_fancy = '1'`, betData.FancyId, betData.MatchId).Scan(&marketName, &matchName)
	if marketName.String == "" {
		return "Session Not Found/Already Declared", true
	}
	tx, err := config.MySQL.Begin()
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	marketUpdateStr := `UPDATE markets SET winner_id = ?, result = ?, result_date = NOW(), is_result_declared = '1' WHERE market_id = ? AND match_id = ?`
	marketStmt, err := tx.Prepare(marketUpdateStr)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	defer marketStmt.Close()
	_, err = marketStmt.Exec(betData.Result, betData.Result, betData.FancyId, betData.MatchId)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	updateStr1 := `UPDATE bets SET winloss = profit, winner_name = ?, winner_id = ?, result_date = NOW(), is_result_declared = '1' WHERE match_id = ? AND market_id = ? AND ((is_back = '1' AND odds <= ?) OR (is_back = '0' AND odds > ?))`

	stmt1, err := tx.Prepare(updateStr1)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	defer stmt1.Close()
	_, err = stmt1.Exec(betData.Result, betData.Result, betData.MatchId, betData.FancyId, betData.Result, betData.Result)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	updateStr2 := `UPDATE bets SET winloss = exposure, winner_name = ?, winner_id = ?, result_date = NOW(), is_result_declared = '1' WHERE match_id = ? AND market_id = ? AND ((is_back = '1' AND odds > ?) OR(is_back = '0' AND odds <= ?))`
	stmt2, err := tx.Prepare(updateStr2)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	defer stmt2.Close()
	_, err = stmt2.Exec(betData.Result, betData.Result, betData.MatchId, betData.FancyId, betData.Result, betData.Result)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	tx, err = setResultSession(betData.MatchId, betData.FancyId, marketName.String, matchName.String, "0", tx)
	if err != nil {
		tx.Rollback()
		return "Unable Declare", true
	}
	tx.Commit()
	return "Declared Successfully", false
}

//AbandonedMarketFancy :
func AbandonedMarketFancy(inputData models.InputAbandonedMarket) (message string, status bool) {
	// var err error
	matchDetail, err := GetMatchContent(inputData.MatchId)
	if err != nil {
		log.Println("AbandonedMarketFancy >> GetMatchContent >>> Unable to get match liability type", err.Error())
	}
	if inputData.IsRollback == 0 {
		marketData, _, _, _, _, err := GetMarketById("1", inputData.MarketId, inputData.MatchId)
		if err == nil {
			// marketData := models.Market{}
			// if len(marketDataRecords) > 0 {
			// 	marketData = marketDataRecords[0]
			// }
			validMarket, _ := CheckMarketStatus(inputData.MatchId, inputData.MarketId)
			if validMarket.TotalCount > 0 {
				matchDetail, err := GetMatchContent(inputData.MatchId)
				if err != nil {
					message = "Result Not Declared - Something went wrong"
					return message, status
				}
				if matchDetail.MatchId == "" {
					return "Match Not Available", false
				}
				// inputData.MatchName = matchDetail.MatchName

				// var marketInput models.InputMarketResult
				// marketInput.MarketId = inputData.MarketId
				// marketInput.MarketName = inputData.MarketName
				// marketInput.MatchId = inputData.MatchId
				// marketInput.MatchName = inputData.MatchName
				// marketInput.SelectionId = "Abandoned"
				// marketInput.SelectionName = "Abandoned"
				// marketInput.Result = "Abandoned"
				// marketInput.SeriesId = inputData.SeriesId
				// marketInput.SeriesName = inputData.SeriesName
				// marketInput.SportId = inputData.SportId
				// marketInput.SportName = inputData.SportName
				// betResultId := inputData.MatchId + "_" + inputData.MarketId

				// InsertBetResultsMongo(marketInput, marketData, "1", betResultId, 0, 0, "")
				insertBetResultStr := `UPDATE markets SET winner_id = ?, result = ?, result_date = NOW(), is_result_declared = '2' WHERE market_id = ? AND match_id = ?`
				stmt, err := config.MySQL.Prepare(insertBetResultStr)
				if err != nil {
					return "Unable Abandoned", false
				}
				_, err = stmt.Exec("Abandoned", "Abandoned", marketData.MarketId, marketData.MatchId)
				if err != nil {
					return "Unable Abandoned", false
				}
				isBetExist := GetExistBetStatus(inputData.MatchId, inputData.MarketId)
				if isBetExist != 0 {
					err = AbandonedMarketCalc(inputData.MatchId, inputData.MarketId, inputData.SportId, 0, matchDetail)
					if err == nil {
						// DeleteMarket(inputData.MarketId)
						// DeleteExistedBet(inputData.MatchId, inputData.MarketId)
						return "Result Abandoned Successfully", true
					} else {
						// DeleteBetResultsMongo(betResultId)
						return "Result Abandoned Failed", false
					}
				} else {
					// DeleteMarket(inputData.MarketId)
					// DeleteExistedBet(inputData.MatchId, inputData.MarketId)
					return "Result Abandoned Successfully", true
				}
			} else {
				return "Result Already Decleared", false
			}
		} else {
			return "Market Not Available", false
		}
	} else {
		// var betResultData models.BetResult
		// filter := bson.M{"matchid": inputData.MatchId, "marketid": inputData.MarketId, "result": "Abandoned"}
		// err := data.MongoClient.Collection("bet_results").FindOne(context.TODO(), filter).Decode(&betResultData)
		// if err != nil && err.Error() != "" {
		// 	// log.Println("AbandonedMarket >>", err.Error())

		// }
		// if betResultData != (models.BetResult{}) {
		// DeleteBetResultsMongo(betResultData.BetResultId)
		err := AbandonedMarketCalc(inputData.MatchId, inputData.MarketId, inputData.SportId, 1, matchDetail)
		if err == nil {
			// RestoreMarketAfterRollback(betResultData)
			// RestoreMarketAfterRollback(inputData.MarketId)
			// RestoreExistedBetAfterRollback(inputData.MatchId, inputData.MarketId)
			// var betId int
			// err = config.MySQL.QueryRow("SELECT id as bet_id FROM bets_odds WHERE match_id = ? AND market_id = ? LIMIT 1", inputData.MatchId, inputData.MarketId).Scan(&betId)
			// if err != nil {
			// 	// log.Println("AbandonedMarket >>", err.Error())
			// }
			// if betId > 0 {
			// 	SetMongoForIsBetExist(inputData.MatchId, inputData.MarketId)
			// }
			return "Result Rollback Successfully", true
		} else {
			// var resultDataInput models.InputMarketResult
			// var ResultmarketData models.Market
			// resultDataInput.SportId = betResultData.SportId
			// resultDataInput.SeriesId = betResultData.SeriesId
			// resultDataInput.MatchId = betResultData.MatchId
			// resultDataInput.MarketId = betResultData.MarketId
			// resultDataInput.SportName = betResultData.SportName
			// resultDataInput.SeriesName = betResultData.SeriesName
			// resultDataInput.MatchName = betResultData.MatchName
			// resultDataInput.MarketName = betResultData.MarketName
			// resultDataInput.SelectionId = betResultData.SelectionId
			// resultDataInput.Result = betResultData.Result
			// resultDataInput.SelectionName = betResultData.WinnerName
			// resultDataInput.CardData = betResultData.CardData
			// ResultmarketData.IsBookmakerMarket = betResultData.IsBookmakerMarket
			// ResultmarketData.MaxBetLiability = betResultData.MaxBetLiability
			// ResultmarketData.MaxMarketLiability = betResultData.MaxMarketLiability
			// ResultmarketData.MaxMarketProfit = betResultData.MaxMarketProfit
			// InsertBetResultsMongo(resultDataInput, ResultmarketData, "1", betResultData.BetResultId, betResultData.SessionSizeYes, betResultData.SessionSizeNo, betResultData.SuperAdminessage)
			return "Result Rollback Failed", false
		}
		// } else {
		// 	return "Match Not Abandoned Already", false
		// }
	}
}

//CheckMarketStatus :
func CheckMarketStatus(matchId, marketId string) (models.ResultCount, error) {
	var resultCount models.ResultCount
	err := config.MySQL.QueryRow(`SELECT count(id) AS total_count FROM markets WHERE match_id=? AND market_id=? AND is_result_declared='0' AND is_active='1' `, matchId, marketId).Scan(&resultCount.TotalCount)
	if err != nil && err != sql.ErrNoRows {
		return resultCount, err
	}
	return resultCount, nil
}

//GetExistBetStatus :
func GetExistBetStatus(matchId, marketId string) int64 {
	var betCount sql.NullInt64
	err := config.MySQL.QueryRow(`SELECT count(id) AS total_count FROM bets WHERE match_id=? AND market_id=? AND is_result_declared='0' AND is_active='1' `, matchId, marketId).Scan(&betCount)
	if err != nil && err != sql.ErrNoRows {
		return 0
	}
	return betCount.Int64
}

//AbandonedMarektCalc :
func AbandonedMarketCalc(matchId, marketId, sportId string, isRollback int64, matchDetail models.MatchDetail) error {
	rows, err := config.MySQL.Query(`SELECT user_id, user_type_id, user_parent_ids, match_id, market_id, selection_id, selection_name, liability_type, stack, win_value, loss_value, win_loss
	FROM odds_profit_loss
	WHERE match_id=? AND market_id=? AND user_type_id=?`, matchId, marketId, config.USER)
	if err != nil {
		return err
	} else {
		positionMap := map[int64][]models.TeamPosition{}
		defer rows.Close()
		for rows.Next() {
			var content models.TeamPositionInput
			err := rows.Scan(
				&content.UserId,
				&content.UserTypeId,
				&content.UserIds,
				&content.MatchId,
				&content.MarketId,
				&content.SelectionId,
				&content.SelectionName,
				&content.LiabilityType,
				&content.Stack,
				&content.WinValue,
				&content.LossValue,
				&content.WinLoss,
			)
			if err != nil {
				log.Println("AbandonedMarketCalc >> Unable get odds profit loss ", err.Error())
			}
			var singleTeamPosition models.TeamPosition
			singleTeamPosition.UserId = content.UserId.Int64
			singleTeamPosition.UserTypeId = content.UserTypeId.Int64
			singleTeamPosition.UserIds = content.UserIds.String
			singleTeamPosition.MatchId = content.MatchId.String
			singleTeamPosition.MarketId = content.MarketId.String
			singleTeamPosition.SelectionId = content.SelectionId.String
			singleTeamPosition.SelectionName = content.SelectionName.String
			singleTeamPosition.LiabilityType = content.LiabilityType.String
			singleTeamPosition.Stack = content.Stack.Float64
			singleTeamPosition.WinValue = content.WinValue.Float64
			singleTeamPosition.LossValue = content.LossValue.Float64
			singleTeamPosition.WinLoss = content.WinLoss.Float64

			positionMap[singleTeamPosition.UserId] = append(positionMap[singleTeamPosition.UserId], singleTeamPosition)
		}

		for key, positions := range positionMap {
			var liabilityTotalStack float64
			var liabilityMinVal []float64
			for _, position := range positions {
				if matchDetail.LiabilityType == "1" && position.LiabilityType == "1" {
					liabilityTotalStack += -(position.Stack)
				} else {
					liabilityTotalStack += 0
				}

				if position.LiabilityType == "0" {
					liabilityMinVal = append(liabilityMinVal, position.WinValue+position.LossValue)
				} else {
					liabilityMinVal = append(liabilityMinVal, 0)
				}
			}

			//Update data to users table for free chip and liability ==>
			var liabilityMinValue float64
			if len(liabilityMinVal) > 0 {
				liabilityMinValue = global.FindMin(liabilityMinVal)
			}

			if matchDetail.LiabilityType == "1" || (liabilityTotalStack+liabilityMinValue) < 0 {
				var parentIds sql.NullString
				sqlStr := `SELECT parent_ids FROM users WHERE id = ? and user_type_id = ?`

				err := config.MySQL.QueryRow(sqlStr, key, config.USER).Scan(&parentIds)
				if err != nil && err != sql.ErrNoRows {
					log.Println("AbandonedMarketCalc >> UpdateBalanceOnResult", err.Error())
					return err
				}
				if parentIds.String != "" {
					parentDetailsMap := GetUserPartnership(parentIds.String, key)
					parentIdsArr := strings.Split(global.TrimCommaBothSide(parentIds.String), ",")
					for _, pId := range parentIdsArr {
						var partnership int64
						pIdInt, _ := strconv.Atoi(pId)
						if value, OK := parentDetailsMap[int64(pIdInt)]; OK {
							partnership = value.Partnership
						}

						parentLiability := ((liabilityTotalStack + liabilityMinValue) * float64(partnership) / 100)
						if isRollback == 1 {
							_, err = config.MySQL.Query("UPDATE users SET exposure = exposure + (?) WHERE id = ? AND user_type_id != "+strconv.Itoa(config.USER), parentLiability, pIdInt)
							if err != nil {
								log.Println("AbandonedMarketCalc >> UpdateBalanceLiability >> Error Update users for liability for user_id ", err.Error())
								return err
							}
						} else {
							_, err = config.MySQL.Query("UPDATE users SET exposure = exposure - (?) WHERE id = ? AND user_type_id != "+strconv.Itoa(config.USER), parentLiability, pIdInt)
							if err != nil {
								log.Println("AbandonedMarketCalc >> UpdateBalanceLiability >> Error Update users for liability for user_id ", err.Error())
								return err
							}
						}
					}
				}

				var updateQuery string
				if isRollback == 0 {
					updateQuery = "UPDATE users SET available_chip = available_chip - (?), exposure = exposure - (?) WHERE id = ? AND user_type_id = " + strconv.Itoa(config.USER)
				} else {
					updateQuery = "UPDATE users SET available_chip = available_chip + (?), exposure = exposure + (?) WHERE id = ? AND user_type_id = " + strconv.Itoa(config.USER)
				}

				stmt, err := config.MySQL.Prepare(updateQuery)
				if err != nil {
					log.Println("AbandonedMarketCalc >> UpdateBalanceLiability >> Update users", err.Error())
					return err
				}
				defer stmt.Close()
				_, err = stmt.Exec(liabilityTotalStack+liabilityMinValue, liabilityTotalStack+liabilityMinValue, key)
				if err != nil {
					log.Println("AbandonedMarketCalc >> UpdateBalanceLiability >> Update users", err.Error())
					return err
				}
			}
		}
		if isRollback == 0 {
			updateQuery := "UPDATE bets SET winner_name = 'Abandoned', winner_id = 'Abandoned', result_date = NOW(), is_result_declared = '2' WHERE match_id = ? AND market_id = ?"

			stmt, err := config.MySQL.Prepare(updateQuery)
			if err != nil {
				log.Println("AbandonedMarketCalc >> UpdateBalanceLiability >> Update users", err.Error())
				return err
			}
			defer stmt.Close()
			_, err = stmt.Exec(matchId, marketId)
			if err != nil {
				log.Println("AbandonedMarketCalc >> UpdateBalanceLiability >> Update users", err.Error())
				return err
			}
		} else {
			updateQuery := "UPDATE bets SET  winner_name = NULL, winner_id = NULL, result_date = NULL, is_result_declared = '0' WHERE match_id = ? AND market_id = ?"

			stmt, err := config.MySQL.Prepare(updateQuery)
			if err != nil {
				log.Println("AbandonedMarketCalc >> UpdateBalanceLiability >> Update users", err.Error())
				return err
			}
			defer stmt.Close()
			_, err = stmt.Exec(matchId, marketId)
			if err != nil {
				log.Println("AbandonedMarketCalc >> UpdateBalanceLiability >> Update users", err.Error())
				return err
			}
		}
	}
	return nil
}

/*
//DeleteMarket :
func DeleteMarket(marketId string) bool {
	updateMarketStr := `UPDATE markets SET is_active ='0', updated_at = NOW() WHERE market_id = ? `
	stmt, err := config.MySQL.Prepare(updateMarketStr)
	if err != nil {
		return false
	}
	_, err = stmt.Exec(marketId)
	if err != nil {
		return false
	}
	return true
}

//DeleteExistedBet :
func DeleteExistedBet(matchId, marketId string) bool {
	updateMarketStr := `UPDATE bets SET is_active ='0', updated_at = NOW() WHERE match_id =? AND market_id = ? `
	stmt, err := config.MySQL.Prepare(updateMarketStr)
	if err != nil {
		return false
	}
	_, err = stmt.Exec(matchId, marketId)
	if err != nil {
		return false
	}
	return true
}

//RestoreMarketAfterRollback :
func RestoreMarketAfterRollback(marketId string) bool {
	updateMarketStr := `UPDATE markets SET is_active ='1', updated_at = NOW() WHERE market_id = ? `
	stmt, err := config.MySQL.Prepare(updateMarketStr)
	if err != nil {
		return false
	}
	_, err = stmt.Exec(marketId)
	if err != nil {
		return false
	}
	return true
}

//RestoreExistedBetAfterRollback :
func RestoreExistedBetAfterRollback(matchId, marketId string) bool {
	updateMarketStr := `UPDATE bets SET is_active ='1', updated_at = NOW() WHERE match_id =? AND market_id = ? `
	stmt, err := config.MySQL.Prepare(updateMarketStr)
	if err != nil {
		return false
	}
	_, err = stmt.Exec(matchId, marketId)
	if err != nil {
		return false
	}
	return true
}
*/

//RollbackMarketFancyResult :
func RollbackMarketFancyResult(inputData models.InputMarketResultRollback) (message string, status bool) {
	betResultData, _ := CheckExistedBetStatus(inputData.MatchId, inputData.MarketId)
	if betResultData.TotalCount != 0 {
		matchDetail, _ := GetMatchContent(inputData.MatchId)
		//DeleteBetResultsMongo(inputData.BetResultId)
		err := RollBackResultOdds(inputData.MatchId, inputData.MarketId, inputData.IsFancy, matchDetail)
		if err != nil {
			// log.Println("RollbackMarketResult >> sp_rollback_result_odds >>", err.Error())
			// var resutDataInput models.InputMarketResult
			// var ResultmarketData models.Market
			// resutDataInput.SportId = betResultData.SportId
			// resutDataInput.SeriesId = betResultData.SeriesId
			// resutDataInput.MatchId = betResultData.MatchId
			// resutDataInput.MarketId = betResultData.MarketId
			// resutDataInput.SportName = betResultData.SportName
			// resutDataInput.SeriesName = betResultData.SeriesName
			// resutDataInput.MatchName = betResultData.MatchName
			// resutDataInput.MarketName = betResultData.MarketName
			// resutDataInput.SelectionId = betResultData.SelectionId
			// resutDataInput.Result = betResultData.Result
			// resutDataInput.SelectionName = betResultData.WinnerName
			// resutDataInput.CardData = betResultData.CardData
			// ResultmarketData.IsBookmakerMarket = betResultData.IsBookmakerMarket
			// ResultmarketData.MaxBetLiability = betResultData.MaxBetLiability
			// ResultmarketData.MaxMarketLiability = betResultData.MaxMarketLiability
			// ResultmarketData.MaxMarketProfit = betResultData.MaxMarketProfit
			// ResultmarketData.MarketStartTime = betResultData.MarketStartTime
			// ResultmarketData.MarketType = betResultData.MarketType

			// InsertBetResultsMongo(resutDataInput, ResultmarketData, "1", betResultData.BetResultId, 0, 0, "")
			return "Unable to Rollback Result", false
		} else {
			// DeleteBetResultsMongo(inputData.BetResultId)
			// RestoreMarketAfterRollback(betResultData)
			// var betId int
			// err = config.MySQL.QueryRow("SELECT id as bet_id FROM bets_odds WHERE match_id = ? AND market_id = ? LIMIT 1", inputData.MatchId, inputData.MarketId).Scan(&betId)
			// if err != nil {
			// 	// log.Println("RollbackMarketResult >>", err.Error())
			// }
			// if betId > 0 {
			// 	SetMongoForIsBetExist(inputData.MatchId, inputData.MarketId)
			// }
			return "Match Rollback Successfully", true
		}
	} else {
		return "Result Not Declared", false
	}
}

//CheckExistedBetStatus :
func CheckExistedBetStatus(matchId, marketId string) (models.ResultCount, error) {
	var resultCount models.ResultCount
	err := config.MySQL.QueryRow(`SELECT count(id) AS total_count FROM markets WHERE match_id=? AND market_id=? AND is_result_declared='1' AND is_active='1' `, matchId, marketId).Scan(&resultCount.TotalCount)
	if err != nil && err != sql.ErrNoRows {
		return resultCount, err
	}
	return resultCount, nil
}

//RollBackResultOdds :
func RollBackResultOdds(matchId, marketId string, isFancy int64, matchDetail models.MatchDetail) error {
	// Set/Update user liability with odds_profit_loss for match_id+market_id
	tx, err := config.MySQL.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}
	tx, err = UpdateBalanceLiability(matchId, marketId, "1", "0", 0, tx)
	if err != nil {
		tx.Rollback()
		// log.Println("UpdateBalanceLiability >> Updatation failed for Users ", err.Error())
		return err
	}

	// Set/Update user_profit_loss for match_id+market_id with bet result
	tx, err = UpdateBalanceOnResult(matchId, marketId, isFancy, 1, tx)
	if err != nil {
		tx.Rollback()
		// log.Println("UpdateBalanceOnResult >> Updatation failed for Users and Users Profit Loss", err.Error())
		return err
	}
	updateStr := `UPDATE bets SET stack = 0, winner_name = NULL, winner_id = NULL, result_date = NULL WHERE match_id = ? AND market_id = ?`

	stmt1, err := tx.Prepare(updateStr)
	if err != nil {
		tx.Rollback()
		// log.Println("RollBackResultOdds >> Update bet odds", err.Error())
		return err
	}
	defer stmt1.Close()
	_, err = stmt1.Exec(matchId, marketId)
	if err != nil {
		tx.Rollback()
		// log.Println("RollBackResultOdds >> Update bet odds", err.Error())
		return err
	}
	deleteStr := `DELETE FROM profit_loss WHERE match_id = ? AND market_id = ?`

	stmt2, err := tx.Prepare(deleteStr)
	if err != nil {
		tx.Rollback()
		// log.Println("RollBackResultOdds >> delete user_profit_loss", err.Error())
		return err
	}
	defer stmt2.Close()
	_, err = stmt2.Exec(matchId, marketId)
	if err != nil {
		tx.Rollback()
		// log.Println("RollBackResultOdds >> delete user_profit_loss", err.Error())
		return err
	}
	deleteStr1 := `DELETE FROM account_statements WHERE market_id = ?`

	stmt3, err := tx.Prepare(deleteStr1)
	if err != nil {
		tx.Rollback()
		// log.Println("RollBackResultOdds >> delete user_profit_loss", err.Error())
		return err
	}
	defer stmt3.Close()
	_, err = stmt3.Exec(marketId)
	if err != nil {
		tx.Rollback()
		// log.Println("RollBackResultOdds >> delete user_profit_loss", err.Error())
		return err
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		// log.Println("RollBackResultOdds >> commit >>", err.Error())
		return err
	}
	return nil
}
