package services

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"sort"
	"strconv"
	"strings"
)

func ValidateMarketBet(userDetail models.User, inputData models.Bet, calculatedDataForMarketBet models.CalculatedDataForMarketBet, selectionData models.RunnerData, isLiveSport string, matchDetail models.Market) (liabilityForBalance float64, message string, status bool) {
	teamPosition := GetTeamPosition(userDetail.Id, userDetail.UserTypeId, inputData.MatchId, inputData.MarketId, isLiveSport)
	var runTimeSumWinLoss []float64
	var oldSumWinLoss []float64

	if inputData.IsBack == "1" {
		for i := 0; i < len(teamPosition); i++ {
			oldSumWinLoss = append(oldSumWinLoss, teamPosition[i].WinValue+teamPosition[i].LossValue)
			if teamPosition[i].SelectionId == inputData.SelectionId {
				teamPosition[i].WinValue = teamPosition[i].WinValue + calculatedDataForMarketBet.PL
			} else {
				teamPosition[i].LossValue = teamPosition[i].LossValue - float64(inputData.Stack)
			}
			var sumWinLoss float64
			if teamPosition[i].LiabilityType == "0" {
				sumWinLoss = teamPosition[i].WinValue + teamPosition[i].LossValue
			} else {
				sumWinLoss = 0
			}
			runTimeSumWinLoss = append(runTimeSumWinLoss, sumWinLoss)
		}
	} else {
		for i := 0; i < len(teamPosition); i++ {
			oldSumWinLoss = append(oldSumWinLoss, teamPosition[i].WinValue+teamPosition[i].LossValue)
			if teamPosition[i].SelectionId == inputData.SelectionId {
				teamPosition[i].WinValue = teamPosition[i].WinValue + calculatedDataForMarketBet.Liability
			} else {
				teamPosition[i].LossValue = teamPosition[i].LossValue + calculatedDataForMarketBet.PL
			}
			var sumWinLoss float64
			if teamPosition[i].LiabilityType == "0" {
				sumWinLoss = teamPosition[i].WinValue + teamPosition[i].LossValue
			} else {
				sumWinLoss = 0
			}
			runTimeSumWinLoss = append(runTimeSumWinLoss, sumWinLoss)
		}
	}

	var userMaxLoss float64
	var oldUserMaxLoss float64

	if matchDetail.LiabilityType == "1" {
		if selectionData.LiabilityType == "1" {
			for i := 0; i < len(teamPosition); i++ {
				if teamPosition[i].LiabilityType == "1" {
					oldUserMaxLoss = oldUserMaxLoss + teamPosition[i].Stack
				} else {
					oldUserMaxLoss = oldUserMaxLoss + 0
				}
			}
			userMaxLoss = -(oldUserMaxLoss + float64(inputData.Stack))
			oldUserMaxLoss = -(oldUserMaxLoss)
		}
		if selectionData.LiabilityType == "0" {
			if len(oldSumWinLoss) == 0 {
				return liabilityForBalance, "An Error Occurred, Try Again", false
			}
			oldSumWinLossValue := global.FindMin(oldSumWinLoss)
			if oldSumWinLossValue >= 0 {
				oldUserMaxLoss = 0
			} else {
				oldUserMaxLoss = oldSumWinLossValue
			}
			if len(runTimeSumWinLoss) == 0 {
				return liabilityForBalance, "An Error Occurred, Try Again", false
			}
			userMaxLoss = global.FindMin(runTimeSumWinLoss)
		}
	} else {
		if len(oldSumWinLoss) == 0 {
			return liabilityForBalance, "An Error Occurred, Try Again", false
		}
		oldSumWinLossValue := global.FindMin(oldSumWinLoss)
		if oldSumWinLossValue >= 0 {
			oldUserMaxLoss = 0
		} else {
			oldUserMaxLoss = oldSumWinLossValue
		}
		if len(runTimeSumWinLoss) == 0 {
			return liabilityForBalance, "An Error Occurred, Try Again", false
		}
		userMaxLoss = global.FindMin(runTimeSumWinLoss)
	}

	if userMaxLoss >= 0 {
		liabilityForBalance = math.Abs(oldUserMaxLoss)
	} else {
		liabilityForBalance = math.Abs(oldUserMaxLoss) - math.Abs(userMaxLoss)
	}

	userBalance := userDetail.AvailableChip + math.Abs(oldUserMaxLoss)
	var tempUserBalance float64
	if userMaxLoss > 0 {
		tempUserBalance = 0
	} else {
		tempUserBalance = userMaxLoss
	}
	if math.Abs(tempUserBalance) > userBalance {
		return liabilityForBalance, "Insufficient Balance", false
	}
	return liabilityForBalance, message, status
}
func PlaceMarketBet(betData models.Bet, liabilityForBalance float64) (string, bool) {
	tx, err := config.MySQL.Begin()
	if err != nil {
		tx.Rollback()
		log.Println("PlaceMarketBet >> transaction connection", err.Error())
		return "Unable place bet", true
	}
	query := `INSERT INTO bets	(user_id, user_name, parent_ids, sport_id, sport_name, series_id, series_name, match_id, match_name, market_id, market_name, selection_id, selection_name, odds, stack, is_back, market_type, is_matched, profit, exposure, created_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())`
	stmt, err := tx.Prepare(query)
	if err != nil {
		tx.Rollback()
		log.Println("PlaceMarketBet >> transaction connection", err.Error())
		return "Unable place bet", true
	}
	defer stmt.Close()
	_, err = stmt.Exec(betData.UserId, betData.UserName, betData.ParentIds, betData.SportId, betData.SportName, betData.SeriesId, betData.SeriesName, betData.MatchId, betData.MatchName, betData.MarketId, betData.MarketName, betData.SelectionId, betData.SelectionName, betData.Odds, betData.Stack, betData.IsBack, "0", "1", betData.Profit, betData.Exposure)
	if err != nil {
		tx.Rollback()
		log.Println("PlaceMarketBet >> Insert bet", err.Error())
		return "Unable Place bet", true
	}

	_, err = tx.Exec("UPDATE users SET exposure = exposure + ?, available_chip = available_chip + ? WHERE id = ?", liabilityForBalance, liabilityForBalance, betData.UserId)
	if err != nil {
		tx.Rollback()
		log.Println("PlaceMarketBet >> Error Update users liablitiy >> ", err.Error())
		return "Unable Place bet", true
	}

	err, tx = CreateMarketPosition(betData.UserId, betData.ParentIds, betData.MatchId, betData.MarketId, 0, tx)
	if err != nil {
		tx.Rollback()
		return "Unable Place bet", true
	}
	er := tx.Commit()
	if er != nil {
		tx.Rollback()
		return "Unable Place bet", true
	}
	return "Placed Successfully", false
}

//ValidateFancyBet :
func ValidateFancyBet(userDetail models.User, inputData models.Bet, fancyData models.Market) (outputData []models.ValidateFancyBetOutput, message string, status bool) {

	teamPosition, _ := GetFancyPosition(userDetail.Id, fancyData.MarketId)

	oldUserMaxLoss := teamPosition.Liability
	// oldUserMaxProfit := teamPosition.Profit
	dataObj := models.FancyBetForUserPosition{
		IsBack:     inputData.IsBack,
		Run:        inputData.Odds,
		Size:       inputData.Size,
		Stack:      float64(inputData.Stack),
		Percentage: 100,
	}
	var createFancyPosition []models.FacnyResultData
	// if fancyData.FancyTypeId == 3 || fancyData.FancyTypeId == 4 {
	// 	createFancyPosition = CreateFancyPositionForFancyMarket(userDetail.Id, fancyData.FancyId, fancyData.SportId, userDetail.UserTypeId, dataObj, userDetail.ParentIds)
	// } else {
	createFancyPosition = CreateFancyPosition(userDetail.Id, fancyData.MarketId, fancyData.SportId, userDetail.UserTypeId, dataObj, userDetail.ParentIds)
	// }

	for _, singleUserPosition := range createFancyPosition {
		singleUserPositionOutput := models.ValidateFancyBetOutput{}
		if singleUserPosition.UserId == userDetail.Id {
			userMaxLoss := singleUserPosition.MaxExposure
			userMaxProfit := singleUserPosition.MaxProfit
			userBalance := userDetail.AvailableChip + math.Abs(oldUserMaxLoss)

			if userMaxLoss >= 0 {
				if oldUserMaxLoss >= 0 {
					singleUserPositionOutput.LiabilityForBalance = 0
				} else {
					singleUserPositionOutput.LiabilityForBalance = math.Abs(oldUserMaxLoss)
				}
				singleUserPositionOutput.LiabilityFancy = 0
			} else {
				singleUserPositionOutput.LiabilityForBalance = math.Abs(oldUserMaxLoss) - math.Abs(userMaxLoss)
				singleUserPositionOutput.LiabilityFancy = userMaxLoss
			}
			singleUserPositionOutput.ProfitFancy = userMaxProfit
			fancyScorePositionJsonMarshal, _ := json.Marshal(singleUserPosition.FancyPosition)
			singleUserPositionOutput.FancyScorePositionJson = string(fancyScorePositionJsonMarshal)
			singleUserPositionOutput.UserId = singleUserPosition.UserId
			singleUserPositionOutput.UserTypeId = singleUserPosition.UserTypeId
			if math.Abs(userMaxLoss) > userBalance {
				return outputData, "Insufficient Balance", true
			}
		} else {
			singleUserPositionOutput.ProfitFancy = singleUserPosition.MaxProfit
			singleUserPositionOutput.LiabilityFancy = singleUserPosition.MaxExposure
			fancyScorePositionJsonMarshal, _ := json.Marshal(singleUserPosition.FancyPosition)
			singleUserPositionOutput.FancyScorePositionJson = string(fancyScorePositionJsonMarshal)
			singleUserPositionOutput.UserId = singleUserPosition.UserId
			singleUserPositionOutput.UserTypeId = singleUserPosition.UserTypeId
		}
		outputData = append(outputData, singleUserPositionOutput)
	}
	////////////

	return outputData, message, status
}

//PlaceFancyBet :
func PlaceFancyBet(betData models.Bet, fancyScorePositionRecord []models.FancyScorePosition, liabilityForBalance float64) (string, bool) {
	tx, err := config.MySQL.Begin()
	if err != nil {
		log.Println("PlaceMarketBet >> transaction connection", err.Error())
		return "Unable place bet", true
	}

	query := `INSERT INTO bets	(user_id, user_name, parent_ids, sport_id, sport_name, series_id, series_name, match_id, match_name, market_id, market_name, selection_id, selection_name, odds, size, stack, is_back, market_type, is_matched, profit, exposure, created_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())`
	stmt, err := tx.Prepare(query)
	if err != nil {
		tx.Rollback()
		log.Println("PlaceMarketBet >> transaction connection", err.Error())
		return "Unable place bet", true
	}
	defer stmt.Close()
	_, err = stmt.Exec(betData.UserId, betData.UserName, betData.ParentIds, betData.SportId, betData.SportName, betData.SeriesId, betData.SeriesName, betData.MatchId, betData.MatchName, betData.MarketId, betData.MarketName, betData.SelectionId, betData.SelectionName, betData.Odds, betData.Size, betData.Stack, betData.IsBack, "1", "1", betData.Profit, betData.Exposure)
	if err != nil {
		tx.Rollback()
		log.Println("PlaceMarketBet >> Insert bet", err.Error())
		return "Unable Place bet", true
	}

	_, err = tx.Exec("UPDATE users SET exposure = exposure + ?, available_chip = available_chip + ? WHERE id = ?", liabilityForBalance, liabilityForBalance, betData.UserId)
	if err != nil {
		tx.Rollback()
		log.Println("PlaceMarketBet >> Error Update users for liability for user_id >> ", betData.UserId, " liablitiy >> ", liabilityForBalance, err.Error())
		return "Unable Place bet", true
	}
	for _, singleData := range fancyScorePositionRecord {
		sqlStr := `INSERT INTO session_possition (user_id, parent_id, user_type_id, user_ids, match_id, fancy_id, liability, profit, fancy_score_position_json) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE liability = ?, profit = ?, fancy_score_position_json = ?`
		stmt, err := tx.Prepare(sqlStr)
		if err != nil {
			tx.Rollback()
			log.Println("PlaceMarketBet >> Error Update users for liability for user_id >> ", betData.UserId, " liablitiy >> ", liabilityForBalance, err.Error())
			return "Unable Place bet", true
		}
		defer stmt.Close()
		_, err = stmt.Exec(singleData.UserId, singleData.ParentId, singleData.UserTypeId, singleData.UserIds, singleData.MatchId, singleData.FancyId, singleData.Liability, singleData.Profit, singleData.FancyScorePositionJson, singleData.Liability, singleData.Profit, singleData.FancyScorePositionJson)
		if err != nil {
			tx.Rollback()
			log.Println("PlaceMarketBet >> Error Update users for liability for user_id >> ", betData.UserId, " liablitiy >> ", liabilityForBalance, err.Error())
			return "Unable Place bet", true
		}
	}
	er := tx.Commit()
	if er != nil {
		tx.Rollback()
		log.Println("PlaceMarketBet >> Error Update users for liability for user_id >> ", betData.UserId, " liablitiy >> ", liabilityForBalance, err.Error())
		return "Unable Place bet", true
	}
	return "Placed Successfully", false
}

//DeleteBetOdds :
func DeleteBetOdds(betId, loginId int64, marketId, isVoid string) (message string, status bool) {
	var checkBet, liabilityType, matchId sql.NullString
	var userId sql.NullInt64
	var parentIds sql.NullString
	err := config.MySQL.QueryRow("SELECT user_id, parent_ids, is_deleted, match_id FROM bets WHERE id = ? AND result_date IS NULL LIMIT 1", betId).Scan(&userId, &parentIds, &checkBet, &matchId)
	if err != nil {
		// log.Println("DeleteBetOdds", err.Error())
		return "Unable Delete Bet", false
	}

	if checkBet.String == "0" {
		// parentIdsStr := global.TrimCommaBothSide(parentIds.String) + "," + strconv.Itoa(int(userId.Int64))

		tx, err := config.MySQL.Begin()
		if err != nil {
			tx.Rollback()
			log.Println("DeleteBetOdds", err.Error())
			return "Unable Delete Bet", false
		}
		if isVoid == "1" {
			sqlStr := `UPDATE bets SET is_deleted = '2', deleted_by = ?, deleted_at = NOW() WHERE id = ? LIMIT 1`

			stmt, err := tx.Prepare(sqlStr)
			if err != nil {
				tx.Rollback()
				log.Println("DeleteBetOdds >> Update bet_odds", err.Error())

				return "Unable Delete Bet", false
			}
			defer stmt.Close()
			_, err = stmt.Exec(loginId, betId)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds >> Update bet_odds", err.Error())
				return "Unable Delete Bet", false
			}
			// tx.Commit()
			// return "Bet Void Successfully", true
		} else {
			sqlStr := `UPDATE bets SET is_deleted = '1', deleted_by = ?, deleted_at = NOW() WHERE id = ? LIMIT 1`

			stmt, err := tx.Prepare(sqlStr)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds >> Update bet_odds", err.Error())
				return "Unable Delete Bet", false
			}
			defer stmt.Close()
			_, err = stmt.Exec(loginId, betId)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds >> Update bet_odds", err.Error())
				return "Unable Delete Bet", false
			}
			// tx.Commit()
			//  return "Bet Deleted Successfully", true
		}

		err = tx.QueryRow("SELECT m.liability_type FROM matches m WHERE m.match_id = ?  LIMIT 1", matchId.String).Scan(&liabilityType)
		if err != nil {
			tx.Rollback()
			// log.Println("DeleteBetOdds", err.Error())
			return "Unable Delete Bet", false
		}
		if liabilityType.String == "1" {
			var stackLibility int64
			err, tx = CreateMarketPosition(userId.Int64, parentIds.String, matchId.String, marketId, 1, tx)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds", err.Error())
				return "Unable Delete Bet", false
			}
			err = tx.QueryRow("SELECT ROUND(-k.stack, 2) FROM bets_odds k  WHERE k.id = ?  LIMIT 1", betId).Scan(&stackLibility)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds", err.Error())
				return "Unable Delete Bet", false
			}

			sqlStr := `UPDATE users a SET a.exposure = ROUND((a.exposure - (?)), 2), a.available_chip = ROUND((a.available_chip - (?)), 2) WHERE a.id = ?`

			stmt, err := tx.Prepare(sqlStr)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds >> Update bet_odds", err.Error())
				return "Unable Delete Bet", false
			}
			defer stmt.Close()
			_, err = stmt.Exec(stackLibility, stackLibility, userId.Int64)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds >> Update bet_odds", err.Error())
				return "Unable Delete Bet", false
			}
		} else {
			var oldLiability, newLiability []float64
			var oLiability, nLiability float64

			// filter := bson.M{}

			// filter = bson.M{"marketid": inputData.MarketId, "userid": inputData.UserId}

			// result, err := data.MongoClient.Collection("odds_profit_loss").Find(context.TODO(), filter)
			// if err != nil {
			// 	// log.Println("DeleteBetOdds >> Update bet_odds", err.Error())
			// 	return "Unable Delete Bet", parentIdsStr, false
			// } else {
			// 	for result.Next(context.TODO()) {
			// 		var singlePosition models.TeamPosition
			// 		err = result.Decode(&singlePosition)

			// 		oldLiability = append(oldLiability, (singlePosition.WinValue + singlePosition.LossValue))
			// 	}
			// }
			res, err := tx.Query(`SELECT (win_value+loss_value) oldLiability FROM odds_profit_loss WHERE user_id = ? AND match_id = ? AND market_id = ?`, userId.Int64, matchId.String, marketId)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds >> Update bet_odds", err.Error())
				return "Unable Delete Bet", false
			}
			defer res.Close()
			for res.Next() {
				var winloss sql.NullFloat64
				res.Scan(&winloss)
				oldLiability = append(oldLiability, winloss.Float64)
			}
			if len(oldLiability) > 0 {
				if global.FindMin(oldLiability) > 0 {
					oLiability = 0
				} else {
					oLiability = global.FindMin(oldLiability)
				}
			} else {
				oLiability = 0
			}

			err, tx = CreateMarketPosition(userId.Int64, parentIds.String, matchId.String, marketId, 1, tx)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds", err.Error())
				return "Unable Delete Bet", false
			}
			res1, err := tx.Query(`SELECT (win_value+loss_value) oldLiability FROM odds_profit_loss WHERE user_id = ? AND match_id = ? AND market_id = ?`, userId.Int64, matchId.String, marketId)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds >> Update bet_odds", err.Error())
				return "Unable Delete Bet", false
			}
			defer res1.Close()
			for res1.Next() {
				var winloss sql.NullFloat64
				res1.Scan(&winloss)
				newLiability = append(newLiability, winloss.Float64)
			}
			if len(newLiability) > 0 {
				if global.FindMin(newLiability) > 0 {
					nLiability = 0
				} else {
					nLiability = global.FindMin(newLiability)
				}
			} else {
				nLiability = 0
			}
			sqlStr := `UPDATE users a SET a.exposure = ROUND((a.exposure - (abs(?)-abs(?))), 2), a.available_chip = ROUND((a.available_chip - (abs(?) - abs(?))), 2) WHERE  a.id=?`

			stmt, err := tx.Prepare(sqlStr)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds >> Update bet_odds", err.Error())
				return "Unable Delete Bet", false
			}
			defer stmt.Close()
			_, err = stmt.Exec(nLiability, oLiability, nLiability, oLiability, userId.Int64)
			if err != nil {
				tx.Rollback()
				// log.Println("DeleteBetOdds >> Update bet_odds", err.Error())
				return "Unable Delete Bet", false
			}
		}
		tx.Commit()
		if isVoid == "1" {
			return "Bet Void Successfully", true
		}
		return "Bet Deleted Successfully", true
	} else {
		return "Result already decleared or bet deleted", false

	}

}

//BetHistory :
func BetHistory(inputData models.InputBetHistory) (resultCount models.ResultCount, limit int64, result []models.OutputBetHistory) {

	limit = int64(config.PageLimit)
	if inputData.Page == 0 {
		inputData.Page = 1
	}
	offset := (inputData.Page - 1) * limit
	limitQry := " LIMIT " + strconv.Itoa(int(offset)) + ", " + strconv.Itoa(int(limit))

	var searchQuery, whereStrBet, betTypeStr, finalQry string

	if inputData.UserCode != "" {
		searchQuery = " AND bets.user_name = '" + inputData.UserCode + "' "
	}

	if inputData.BetType == "D" {
		betTypeStr = " AND bets.is_deleted = '1' "
	} else if inputData.BetType == "M" {
		betTypeStr = " AND bets.is_deleted != '1' "
	}

	// if inputData.IpAddress != "" {
	// 	whereStrBet += " AND bets.ip_address = '" + inputData.IpAddress + "' "
	// }

	if inputData.IsBack != "" && (inputData.IsBack == "0" || inputData.IsBack == "1") {
		whereStrBet += " AND bets.is_back = '" + inputData.IsBack + "' "
	}

	if inputData.FromAmount != 0 {
		whereStrBet += " AND bets.stack >= " + strconv.Itoa(int(inputData.FromAmount))
	}

	if inputData.ToAmount != 0 {
		whereStrBet += " AND bets.stack <= " + strconv.Itoa(int(inputData.ToAmount))
	}

	finalQry = `SELECT bets.id AS bet_id, bets.market_id, bets.user_name,bets.created_at AS placed,
		bets.match_name,bets.selection_name,bets.bet_matched_at,bets.is_back,bets.odds,bets.stack, '1' AS bet_type, bets.is_deleted FROM bets
		WHERE bets.sport_id !='-1' AND (FIND_IN_SET(` + strconv.Itoa(int(inputData.UserId)) + `,bets.parent_ids) OR bets.user_id = ?) ` + betTypeStr + searchQuery + whereStrBet + ` ORDER BY bets.created_at DESC ` + limitQry

	// fmt.Println(finalQry)
	rows, err := config.MySQL.Query(finalQry, inputData.UserId)
	if err != nil {
		log.Println("BetHistory >> ", err.Error())
		return resultCount, 0, result
	}
	for rows.Next() {
		var content models.BetHistoryInput

		err := rows.Scan(
			&content.BetId,
			&content.MarketId,
			// &content.Name,
			&content.UserName,
			&content.Placed,
			&content.MatchName,
			&content.SelectionName,
			&content.BetMatchedAt,
			&content.IsBack,
			&content.Odds,
			&content.Stack,
			// &content.IpAddress,
			// &content.DeviceInfo,
			&content.BetType,
			&content.DeleteStatus,
		)
		if err != nil {
			log.Println("BetHistory >> ", err.Error())
		}

		var SingleBet models.OutputBetHistory
		SingleBet.BetId = content.BetId.Int64
		SingleBet.MarketId = content.MarketId.String
		// SingleBet.Name = content.Name.String
		SingleBet.UserName = content.UserName.String
		SingleBet.Placed = content.Placed.String
		SingleBet.MatchName = content.MatchName.String
		SingleBet.SelectionName = content.SelectionName.String
		SingleBet.BetMatchedAt = content.BetMatchedAt.String
		SingleBet.IsBack = content.IsBack.String
		SingleBet.Odds = content.Odds.Float64
		SingleBet.Stack = content.Stack.Int64
		// SingleBet.IpAddress = content.IpAddress.String
		// SingleBet.DeviceInfo = content.DeviceInfo.String
		SingleBet.BetType = content.BetType.String
		SingleBet.DeleteStatus = content.DeleteStatus.String
		result = append(result, SingleBet)
	}

	var countQry string
	if inputData.Page == 1 {
		countQry = `SELECT COUNT(bets.id) AS total_count FROM bets
		WHERE bets.sport_id !='-1' AND (FIND_IN_SET(` + strconv.Itoa(int(inputData.UserId)) + `,bets.parent_ids) OR bets.user_id = ?) ` + betTypeStr + searchQuery + whereStrBet
		//fmt.Println(countQry)
		err := config.MySQL.QueryRow(countQry, inputData.UserId).Scan(&resultCount.TotalCount)
		if err != nil {
			log.Println("BetHistory >> count ", err.Error())
		}
	}

	return resultCount, limit, result
}

//GetCurrentBet :
func GetCurrentBet(userId int64, matchId string) (result []models.CurrentBet) {
	finalQry := `SELECT id as bet_id, market_id, market_name, user_name, created_at AS placed,selection_name, is_back,odds,stack, market_type, is_deleted	FROM bets WHERE is_matched = '1' AND is_deleted = '0' AND (FIND_IN_SET(?,parent_ids) OR user_id = ?) AND match_id = ? AND is_result_declared = '0' ORDER BY created_at DESC LIMIT 50`
	rows, err := config.MySQL.Query(finalQry, strconv.Itoa(int(userId)), userId, matchId)
	if err != nil {
		log.Println("BetHistory >> ", err.Error())
		return result
	}
	defer rows.Close()
	for rows.Next() {
		var content models.BetHistoryInput
		err := rows.Scan(
			&content.BetId,
			&content.MarketId,
			&content.MarketName,
			&content.UserName,
			&content.Placed,
			&content.SelectionName,
			&content.IsBack,
			&content.Odds,
			&content.Stack,
			&content.BetType,
			&content.DeleteStatus,
		)
		if err != nil {
			log.Println("BetHistory >> ", err.Error())
		}
		var SingleBet models.CurrentBet
		SingleBet.BetId = content.BetId.Int64
		SingleBet.MarketId = content.MarketId.String
		SingleBet.MarketName = content.MarketName.String
		SingleBet.UserName = content.UserName.String
		SingleBet.Placed = content.Placed.String
		SingleBet.SelectionName = content.SelectionName.String
		SingleBet.IsBack = content.IsBack.String
		SingleBet.Odds = content.Odds.Float64
		SingleBet.Stack = content.Stack.Int64
		SingleBet.BetType = content.BetType.String
		SingleBet.IsDeleted = content.DeleteStatus.String
		result = append(result, SingleBet)
	}

	return result
}
func GetFancyPosition(userId int64, fancyId string) (fancyScorePosition models.FancyScorePosition, status bool) {
	var content models.FancyScorePositionInput
	err := config.MySQL.QueryRow("SELECT user_id, parent_id, user_type_id, match_id, fancy_id, liability, profit, fancy_score_position_json, user_ids FROM session_possition WHERE user_id = ? AND fancy_id = ?", userId, fancyId).Scan(&content.UserId, &content.ParentId, &content.UserTypeId, &content.MatchId, &content.FancyId, &content.Liability, &content.Profit, &content.FancyScorePositionJson)
	if err != nil && err != sql.ErrNoRows {
		return fancyScorePosition, false
	}
	fancyScorePosition.FancyId = content.FancyId.String
	fancyScorePosition.MatchId = content.MatchId.String
	fancyScorePosition.UserId = content.UserId.Int64
	fancyScorePosition.ParentId = content.ParentId.Int64
	fancyScorePosition.UserTypeId = content.UserTypeId.Int64
	fancyScorePosition.Liability = content.Liability.Float64
	fancyScorePosition.Profit = content.Profit.Float64
	fancyScorePosition.FancyScorePositionJson = content.FancyScorePositionJson.String

	return fancyScorePosition, true
}

//CreateFancyPosition :
func CreateFancyPosition(userId int64, fancyId, sportId string, userTypeId int64, dataObj models.FancyBetForUserPosition, parentIds string) []models.FacnyResultData {
	var notInIds []interface{}
	finalData := []models.FacnyResultData{}

	fancyList := GetFancyBetForUserPosition(userId, fancyId, userTypeId, notInIds)
	userData := models.FacnyResultData{}
	isExist := false
	for i, fancy := range fancyList {
		if fancy.IsBack == dataObj.IsBack && fancy.Run == dataObj.Run && fancy.Size == dataObj.Size {
			isExist = true
			fancyList[i].Stack += dataObj.Stack
		}
	}
	if !isExist {
		if dataObj != (models.FancyBetForUserPosition{}) {
			fancyList = append(fancyList, dataObj)
		}
	}
	if len(fancyList) > 1 {
		sort.Sort(FancyListSort(fancyList))
	}
	var run []float64
	var lastPosition float64
	maxExposure := 0.0
	maxProfit := 0.0
	resultValues := []models.FacnyResultValue{}
	runMap := map[float64]float64{}
	for _, fancy := range fancyList {
		runMap[fancy.Run-1] = fancy.Run - 1
		//run = append(run, fancy.Run-1)
	}
	for _, singleRun := range runMap {

		run = append(run, singleRun)
	}
	run = append(run, fancyList[len(fancyList)-1].Run)
	for ind, r := range run {
		var tempTotal float64
		for _, f := range fancyList {
			//stack := (f.Stack * f.Percentage) / 100
			if f.IsBack == "1" {
				if r < f.Run {
					tempTotal = tempTotal - f.Stack

				} else {
					tempTotal = tempTotal + f.Stack*(f.Size/100)
				}
			} else {
				if r >= f.Run {
					tempTotal = tempTotal - f.Stack*(f.Size/100)
				} else {
					tempTotal = tempTotal + f.Stack
				}
			}
		}
		resultValue := models.FacnyResultValue{}
		if len(run)-1 == ind {
			resultValue.Key = strconv.Itoa(int(lastPosition)) + "+"
			resultValue.Value = math.Round(tempTotal)
			resultValues = append(resultValues, resultValue)
		} else {
			if lastPosition == r {
				resultValue.Key = strconv.Itoa(int(lastPosition))
				resultValue.Value = math.Round(tempTotal)
				resultValues = append(resultValues, resultValue)
			} else {
				resultValue.Key = strconv.Itoa(int(lastPosition)) + "-" + strconv.Itoa(int(r))
				resultValue.Value = math.Round(tempTotal)
				resultValues = append(resultValues, resultValue)
			}
		}
		lastPosition = r + 1
		if maxExposure > tempTotal {
			maxExposure = tempTotal
		}
		if maxProfit < tempTotal {
			maxProfit = tempTotal
		}
	}
	userData.FancyPosition = resultValues
	userData.MaxExposure = maxExposure
	userData.MaxProfit = maxProfit
	userData.UserId = userId
	userData.UserTypeId = userTypeId
	finalData = append(finalData, userData)
	parentDetailsMap := GetUserPartnership(parentIds, userId)
	parentIdsArr := strings.Split(global.TrimCommaBothSide(parentIds), ",")
	for _, pId := range parentIdsArr {
		parentData := models.FacnyResultData{}
		var partnership int64
		pIdInt, _ := strconv.Atoi(pId)
		if partnershipValue, Valid := parentDetailsMap[int64(pIdInt)]; Valid {
			partnership = partnershipValue.Partnership
		}
		var lastPositionParent float64
		maxExposureParent := 0.0
		resultValuesParent := []models.FacnyResultValue{}

		for ind, r := range run {
			var tempTotalParent float64
			for _, f := range fancyList {
				stack := (f.Stack * float64(partnership)) / 100
				if f.IsBack == "1" {
					if f.Run <= r {
						tempTotalParent = tempTotalParent - stack*(f.Size/100)
					} else {
						tempTotalParent = tempTotalParent + stack
					}
				} else {
					if f.Run > r {
						tempTotalParent = tempTotalParent - stack
					} else {
						tempTotalParent = tempTotalParent + stack*(f.Size/100)
					}
				}
			}
			resultValueParent := models.FacnyResultValue{}
			if len(run)-1 == ind {
				resultValueParent.Key = strconv.Itoa(int(lastPositionParent)) + "+" //lastPostionStr + "+"
				resultValueParent.Value = math.Round(tempTotalParent)
				resultValuesParent = append(resultValuesParent, resultValueParent)
			} else {
				if lastPositionParent == r {
					resultValueParent.Key = strconv.Itoa(int(lastPositionParent))
					resultValueParent.Value = math.Round(tempTotalParent)
					resultValuesParent = append(resultValuesParent, resultValueParent)
				} else {
					resultValueParent.Key = strconv.Itoa(int(lastPositionParent)) + "-" + strconv.Itoa(int(r))
					resultValueParent.Value = math.Round(tempTotalParent)
					resultValuesParent = append(resultValuesParent, resultValueParent)
				}
			}
			lastPositionParent = r + 1
			if maxExposureParent > tempTotalParent {
				maxExposureParent = tempTotalParent
			}
		}
		parentData.FancyPosition = resultValuesParent
		parentData.MaxExposure = math.Round(maxExposureParent)
		parentData.UserId = int64(pIdInt)
		parentData.UserTypeId = parentDetailsMap[int64(pIdInt)].UserTypeId
		finalData = append(finalData, parentData)
	}

	return finalData
}

//GetFancyBetForUserPosition :
func GetFancyBetForUserPosition(userId int64, fancyId string, userTypeId int64, notInIds []interface{}) (fancyBetForUserPosition []models.FancyBetForUserPosition) {

	var condition string

	if len(notInIds) > 0 {
		var notInIdsStrArr []string
		for _, x := range notInIds {
			notInIdsStrArr = append(notInIdsStrArr, fmt.Sprintf("%v", x))
		}
		notInIdsStr := strings.Join(notInIdsStrArr, ",")
		condition = condition + " AND id NOT IN (" + notInIdsStr + ") "
	}
	rows, err := config.MySQL.Query("Select odds, is_back, size, SUM(stack) AS stack FROM bets Where is_deleted='0' AND market_id = ? AND result_date IS NULL AND user_id = ? "+condition+" GROUP BY odds, is_back, size ORDER BY odds", fancyId, userId)
	if err != nil {
		log.Println("GetFancyBetForUserPosition", err.Error())
		return fancyBetForUserPosition
	}
	defer rows.Close()
	for rows.Next() {
		var content models.FancyBetForUserPositionInput
		err := rows.Scan(
			&content.Run,
			&content.IsBack,
			&content.Size,
			&content.Stack,
		)
		if err != nil {
			log.Println("GetFancyBetForUserPosition", err.Error())
		}
		var singlePosition models.FancyBetForUserPosition
		singlePosition.Run = content.Run.Float64
		singlePosition.IsBack = content.IsBack.String
		singlePosition.Size = content.Size.Float64
		singlePosition.Stack = content.Stack.Float64
		fancyBetForUserPosition = append(fancyBetForUserPosition, singlePosition)
	}
	return fancyBetForUserPosition
}

// FancyListSort :
type FancyListSort []models.FancyBetForUserPosition

func (a FancyListSort) Len() int      { return len(a) }
func (a FancyListSort) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a FancyListSort) Less(i, j int) bool {
	return a[i].Run < a[j].Run
}
