package services

import (
	"database/sql"
	"log"
	"novaexchbackend/api/config"
	"novaexchbackend/api/models"
)

//CreateFancy :
func CreateFancy(fancy models.Fancy) (bool, string) {
	var resultCount models.ResultCount

	err := config.MySQL.QueryRow(`SELECT COUNT(matches.id) AS total_count FROM matches INNER JOIN series ON matches.series_id=series.series_id INNER JOIN sports ON matches.sport_id=sports.sport_id WHERE matches.match_id = ? AND matches.is_active = ? AND series.is_active = ? AND sports.is_active = ?`, fancy.MatchId, "1", "1", "1").Scan(&resultCount.TotalCount)
	if err != nil {
		log.Println("CreateFancy >> Unable to Create Fancy", err.Error())
		return false, "Unable to create Fancy"
	}
	if resultCount.TotalCount > 0 {
		totalCount := 0
		// totalCount, _ := GetDecalaredMarketCountDB("", "", fancy.MatchId, fancy.FancyId)
		if totalCount == 0 {
			fancycount := GetFanciesCountDB(fancy.MatchId, fancy.FancyId)
			if fancycount == 0 {
				var resultContent models.MarketInput
				err := config.MySQL.QueryRow(`SELECT m.name as match_name, s.sport_id, s.name as sport_name, sr.series_id, sr.name as series_name FROM matches m 
				INNER JOIN sports s on (s.sport_id = m.sport_id AND s.is_active='1')
				INNER JOIN series sr on (sr.series_id = m.series_id AND sr.is_active='1') WHERE m.match_id = ? AND m.is_active = '1'`, fancy.MatchId).Scan(&resultContent.MatchName, &resultContent.SportId, &resultContent.SportName, &resultContent.SeriesId, &resultContent.SeriesName)
				if err != nil && err != sql.ErrNoRows {
					log.Println("CreateFancy", err.Error())
					return false, "Unable to create Fancy"
				}
				if resultContent.MatchName.String == "" {
					return false, "Sport/Series/Match not active"
				}

				query := `INSERT INTO markets (sport_id, sport_name, series_id, series_name, match_id, match_name, market_id, name, is_fancy, fancy_selection_id, created_at,updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,NOW(),NOW())`
				stmt, err := config.MySQL.Prepare(query)
				if err != nil {
					log.Println("CreateFancy >> Insert error", err.Error())
					return false, "Unable to create Fancy"
				}
				defer stmt.Close()
				_, err = stmt.Exec(resultContent.SportId, resultContent.SportName, resultContent.SeriesId, resultContent.SeriesName, fancy.MatchId, resultContent.MatchName, fancy.FancyId, fancy.Name, "1", fancy.SelectionId)
				if err == nil {
					return true, "Fancy Created Successfully"
				} else {
					log.Println("CreateFancy >> Insert error", err.Error())
					return false, "Something went wrong, Please Try Again !"
				}
			} else {
				return false, "Fancy Already Exists !"
			}
		} else {
			return false, "Fancy Already Exists !"
		}
	} else {
		return false, "Match/Series/Sport Not Available !"
	}
}

//GetDecalaredMarketCountDB :
func GetDecalaredMarketCountDB(sportId, seriesId, matchId, marketId string) (result int64, err error) {
	// var result int64
	// var err error
	// filter := bson.M{}
	// if sportId != "" {
	// 	filter = bson.M{"sportid": sportId}
	// }
	// if seriesId != "" {
	// 	if sportId != "" {
	// 		filter = bson.M{"sportid": sportId, "seriesid": seriesId}
	// 	} else {
	// 		filter = bson.M{"seriesid": seriesId}
	// 	}
	// }
	// if matchId != "" {
	// 	if sportId != "" {
	// 		filter = bson.M{"sportid": sportId, "matchid": matchId}
	// 	} else if seriesId != "" {
	// 		filter = bson.M{"seriesid": seriesId, "matchid": matchId}
	// 	} else if seriesId != "" && sportId != "" {
	// 		filter = bson.M{"sportid": sportId, "seriesid": seriesId, "matchid": matchId}
	// 	} else {
	// 		filter = bson.M{"matchid": matchId}
	// 	}
	// }
	// if marketId != "" {
	// 	if sportId != "" {
	// 		filter = bson.M{"sportid": sportId, "marketid": marketId}
	// 	} else if seriesId != "" {
	// 		filter = bson.M{"seriesid": seriesId, "marketid": marketId}
	// 	} else if seriesId != "" && sportId != "" {
	// 		filter = bson.M{"sportid": sportId, "seriesid": seriesId, "marketid": marketId}
	// 	} else if seriesId != "" && sportId != "" && matchId != "" {
	// 		filter = bson.M{"sportid": sportId, "seriesid": seriesId, "matchid": matchId, "marketid": marketId}
	// 	} else if seriesId == "" && sportId == "" && matchId != "" {
	// 		filter = bson.M{"matchid": matchId, "marketid": marketId}
	// 	} else if seriesId != "" && sportId == "" && matchId != "" {
	// 		filter = bson.M{"seriesid": seriesId, "matchid": matchId, "marketid": marketId}
	// 	} else if seriesId == "" && sportId != "" && matchId != "" {
	// 		filter = bson.M{"sportid": sportId, "matchid": matchId, "marketid": marketId}
	// 	} else {
	// 		filter = bson.M{"marketid": marketId}
	// 	}
	// }
	// result, err := data.MongoClient.Collection("bet_results").CountDocuments(context.TODO(), filter)
	// if err != nil && err.Error() != "mongo: no documents in result" {
	// 	fmt.Println("error:", err.Error())
	// 	return -1, err
	// }
	// return result, nil
	return result, nil
}

//GetFanciesCountDB :
func GetFanciesCountDB(matchId, fancyId string) int64 {
	var resultCount models.ResultCount
	whereStr := " AND match_id = '" + matchId + "'"
	if fancyId != "" {
		whereStr = " AND match_id = '" + matchId + "' AND market_id = '" + fancyId + "'"
	}

	err := config.MySQL.QueryRow(`SELECT count(id) AS total_count FROM markets WHERE is_fancy='1' ` + whereStr).Scan(&resultCount.TotalCount)
	if err != nil && err != sql.ErrNoRows {
		log.Println("GetFanciesCountDB >> ", err.Error())
		return -1
	}
	return int64(resultCount.TotalCount)
}

//UpdateFancyStatus :
func UpdateFancyStatus(fancyId string, isActive string) string {

	updateStr := "UPDATE markets SET is_active = ? WHERE market_id = ? AND is_fancy='1'"
	stmt, err := config.MySQL.Prepare(updateStr)
	if err != nil {
		log.Println("UpdateFancyStatus >> Unable to update fancy status", err.Error())
		return "Unable to update fancy status"
	}
	_, err = stmt.Exec(isActive, fancyId)
	if err != nil {
		log.Println("UpdateFancyStatus >> Unable to update fancy status", err.Error())
		return "Unable to update fancy status"
	} else {
		if isActive == "0" {
			return "Fancy Deactivated Successfully"
		} else if isActive == "4" {
			return "Fancy Suspended Successfully"
		} else if isActive == "2" {
			return "Fancy Closed Successfully"
		} else {
			return "Fancy Activated Successfully"
		}
	}
}

// GetFancies :
func GetFancies(sportId, seriesId, matchId, isActive, search string) (dataOutput []models.FancyListData, err error) {
	whereStr := ""
	if sportId != "" {
		whereStr += " AND m.sport_id = '" + sportId + "'"
	}
	if seriesId != "" {
		whereStr += " AND m.series_id = '" + seriesId + "'"
	}
	if isActive == "0" || isActive == "1" || isActive == "2" || isActive == "4" {
		whereStr += " AND mr.is_active = '" + isActive + "'"
	}
	if matchId != "" {
		whereStr += " AND m.match_id ='" + matchId + "'"
	}
	if search != "" {
		whereStr += " AND mr.name ='" + search + "'"
	}

	// fmt.Println("Select mr.sport_id,mr.sport_name, mr.series_id,mr.series_name, mr.match_id, mr.match_name, mr.market_id, mr.fancy_selection_id, mr.name, m.start_date, mr.is_active FROM markets mr INNER JOIN sports sp ON (mr.sport_id=sp.sport_id AND sp.is_active = ?) INNER JOIN series sr ON (mr.series_id=sr.series_id AND sr.is_active = ?) INNER JOIN matches m ON (mr.match_id=m.match_id AND m.is_active = ?) WHERE mr.is_fancy = '1' " + whereStr + " ORDER BY mr.created_at")
	rows, err := config.MySQL.Query("Select mr.sport_id,mr.sport_name, mr.series_id,mr.series_name, mr.match_id, mr.match_name, mr.market_id, mr.fancy_selection_id, mr.name, m.start_date, mr.is_active FROM markets mr INNER JOIN sports sp ON (mr.sport_id=sp.sport_id AND sp.is_active = ?) INNER JOIN series sr ON (mr.series_id=sr.series_id AND sr.is_active = ?) INNER JOIN matches m ON (mr.match_id=m.match_id AND m.is_active = ?) WHERE mr.is_fancy = '1' "+whereStr+" ORDER BY mr.created_at", "1", "1", "1")
	if err != nil {
		log.Println("GetFancies >> Unable get fancies", err.Error())
		return dataOutput, err
	}
	defer rows.Close()
	for rows.Next() {
		var content models.MarketInput
		err := rows.Scan(
			&content.SportId,
			&content.SportName,
			&content.SeriesId,
			&content.SeriesName,
			&content.MatchId,
			&content.MatchName,
			&content.MarketId,
			&content.SelectionId,
			&content.Name,
			&content.MatchDate,
			&content.IsActive,
		)
		if err != nil {
			log.Println("GetFancies >> Unable get fancies", err.Error())
		}
		var singleMatch models.FancyListData
		singleMatch.SportId = content.SportId.String
		singleMatch.SportName = content.SportName.String
		singleMatch.SeriesId = content.SeriesId.String
		singleMatch.SeriesName = content.SeriesName.String
		singleMatch.MatchId = content.MatchId.String
		singleMatch.MatchName = content.MatchName.String
		singleMatch.FancyId = content.MarketId.String
		singleMatch.Name = content.Name.String
		singleMatch.MatchDate = content.MatchDate.String
		singleMatch.Active = content.IsActive.String
		singleMatch.SelectionId = content.SelectionId.String

		dataOutput = append(dataOutput, singleMatch)
	}

	return dataOutput, nil
}
