package services

import (
	"database/sql"
	"log"
	"novaexchbackend/api/config"
	"novaexchbackend/api/models"
)

//GetSettingInfo :
func GetSettingInfo() (result models.GlobalSetting, err error) {
	err = config.MySQL.QueryRow("SELECT id,site_message,market_commission,session_commission,bookmaker_commission,market_delay,session_delay,bookmaker_delay,logo,match_stack,is_maintenance FROM main_settings").Scan(&result.Id, &result.SiteMessage, &result.MarketCommission, &result.SessionCommission, &result.BookmakerCommission, &result.MarketDelay, &result.SessionDelay, &result.BookmakerDelay, &result.Logo, &result.MatchStack, &result.SiteUnderMaintenance)
	if err != nil {
		log.Println("GetSettingInfo", err.Error())
		return models.GlobalSetting{}, err
	}
	return result, err
}

// //UpdateBySuperAdmin :
// func UpdateBySuperAdmin(data1 models.GlobalSetting) (message string, status bool) {
// 	updateQuery := "Update global_settings SET site_message = ?, match_stack = ?, fancy_stack = ?, cricket_odds_commission = ?, cricket_fancy_commission = ?, cricket_bookmaker_commission = ?, soccer_odds_commission = ?, soccer_fancy_commission = ?, soccer_bookmaker_commission = ?, tennis_odds_commission = ?, tennis_fancy_commission = ?, tennis_bookmaker_commission = ?, tennis_min_bet = ?, soccer_min_bet = ?, tennis_min_bet = ?, cricket_max_bet = ?, soccer_max_bet = ?, tennis_max_bet = ?, cricket_odds_delay = ?, soccer_odds_delay = ?, tennis_odds_delay = ?, cricket_fancy_delay = ?, soccer_fancy_delay = ?, tennis_fancy_delay = ?"
// 	stmt, err := config.MySQL.Prepare(updateQuery)
// 	if err != nil {
// 		log.Println("UpdateBySuperAdmin", err.Error())
// 	}
// 	defer stmt.Close()
// 	_, err = stmt.Exec(data1.SiteMessage, data1.MatchStack, data1.FancyStack, data1.CricketOddsCommission, data1.CricketFancyCommission, data1.CricketBookmakerCommission, data1.SoccerOddsCommission, data1.SoccerFancyCommission, data1.SoccerBookmakerCommission, data1.TennisOddsCommission, data1.TennisFancyCommission, data1.TennisBookmakerCommission, data1.CricketMinBet, data1.SoccerMinBet, data1.TennisMinBet, data1.CricketMaxBet, data1.SoccerMaxBet, data1.TennisMaxBet, data1.CricketOddsDelay, data1.SoccerOddsDelay, data1.TennisOddsDelay, data1.CricketFancyDelay, data1.SoccerFancyDelay, data1.TennisFancyDelay)
// 	if err != nil {
// 		log.Println("UpdateBySuperAdmin", err.Error())

// 	}
// 	return "", true
// }

// func GetMatchAndSessionCommission() (models.GetCommission, error) {
// 	var commission models.GetCommission
// 	var content models.GetCommissionInput
// 	err := config.MySQL.QueryRow(`SELECT cricket_odds_commission, soccer_odds_commission, tennis_odds_commission,
// 	cricket_fancy_commission, soccer_fancy_commission, tennis_fancy_commission,
// 	cricket_bookmaker_commission, soccer_bookmaker_commission, tennis_bookmaker_commission
// 	FROM global_settings`).Scan(&content.CricketOddsCommission, &content.SoccerOddsCommission, &content.TennisOddsCommission, &content.CricketFancyCommission, &content.SoccerFancyCommission, &content.TennisFancyCommission, &content.CricketBookmakerCommission, &content.SoccerBookmakerCommission, &content.TennisBookmakerCommission)
// 	if err != nil && err != sql.ErrNoRows {
// 		log.Println("IsSiteUnderMantance", err.Error())
// 		return commission, err
// 	}

// 	commission.CricketOddsCommission = content.CricketOddsCommission.Int64
// 	commission.SoccerOddsCommission = content.SoccerOddsCommission.Int64
// 	commission.TennisOddsCommission = content.TennisOddsCommission.Int64
// 	commission.CricketFancyCommission = content.CricketFancyCommission.Int64
// 	commission.SoccerFancyCommission = content.SoccerFancyCommission.Int64
// 	commission.TennisFancyCommission = content.TennisFancyCommission.Int64
// 	commission.CricketBookmakerCommission = content.CricketBookmakerCommission.Int64
// 	commission.SoccerBookmakerCommission = content.SoccerBookmakerCommission.Int64
// 	commission.TennisBookmakerCommission = content.TennisBookmakerCommission.Int64

// 	return commission, nil
// }

// func GetUserSetting(userId int64, matchId string) (models.GetMinMaxBets, error) {
// 	var inputData models.GetMinMaxBetsInput
// 	var outputData models.GetMinMaxBets

// 	err := config.MySQL.QueryRow("SELECT cricket_min_bet,soccer_min_bet,tennis_min_bet,cricket_max_bet,soccer_max_bet,tennis_max_bet, cricket_delay, soccer_delay, tennis_delay FROM user_settings WHERE user_id = ?", userId).Scan(&inputData.CricketMinBet, &inputData.SoccerMinBet, &inputData.TennisMinBet, &inputData.CricketMaxBet, &inputData.SoccerMaxBet, &inputData.TennisMaxBet, &inputData.CricketDelay, &inputData.SoccerDelay, &inputData.TennisDelay)
// 	if err != nil && err != sql.ErrNoRows {
// 		log.Println("GetMinMaxBetStack", err.Error())
// 		return outputData, err
// 	}
// 	outputData.CricketMinBet = inputData.CricketMinBet.Int64
// 	outputData.SoccerMinBet = inputData.SoccerMinBet.Int64
// 	outputData.TennisMinBet = inputData.TennisMinBet.Int64
// 	outputData.CricketMaxBet = inputData.CricketMaxBet.Int64
// 	outputData.SoccerMaxBet = inputData.SoccerMaxBet.Int64
// 	outputData.TennisMaxBet = inputData.TennisMaxBet.Int64
// 	outputData.SoccerDelay = inputData.SoccerDelay.Int64
// 	outputData.TennisDelay = inputData.TennisDelay.Int64
// 	outputData.CricketDelay = inputData.CricketDelay.Int64
// 	return outputData, err
// }

func GetMatchSettingInfo(sportId string) (result models.GetMatchMinMaxBets, err error) {
	var inputData models.GetMatchMinMaxBetsInput
	var outputData models.GetMatchMinMaxBets

	err = config.MySQL.QueryRow("SELECT market_min_bet,market_max_bet,session_min_bet,session_max_bet,bookmaker_min_bet,bookmaker_max_bet FROM sports WHERE sport_id = ?", sportId).Scan(&inputData.MarketMinBet, &inputData.MarketMaxBet, &inputData.SessionMinBet, &inputData.SessionMaxBet, &inputData.BookmakerMinBet, &inputData.BookmakerMaxBet)
	if err != nil && err != sql.ErrNoRows {
		log.Println("GetMinMaxBetStack", err.Error())
		return outputData, err
	}
	outputData.MarketMinBet = inputData.MarketMinBet.Int64
	outputData.MarketMaxBet = inputData.MarketMaxBet.Int64
	outputData.SessionMinBet = inputData.SessionMinBet.Int64
	outputData.SessionMaxBet = inputData.SessionMaxBet.Int64
	outputData.BookmakerMinBet = inputData.BookmakerMinBet.Int64
	outputData.BookmakerMaxBet = inputData.BookmakerMaxBet.Int64

	return outputData, err
}
