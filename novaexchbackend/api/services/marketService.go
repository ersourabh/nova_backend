package services

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"novaexchbackend/api/util"
	"strconv"
	"strings"
)

// CreateSeries :
func CreateMarket(content models.Market) (message string, status bool) {
	var resultCount models.ResultCount
	err := config.MySQL.QueryRow(`SELECT count(id) AS total_count FROM markets WHERE market_id = ?`, content.MarketId).Scan(&resultCount.TotalCount)
	if err != nil && err != sql.ErrNoRows {
		log.Println("CreateMarket", err.Error())
		return "Market Allready Created", false
	}
	if resultCount.TotalCount == 0 {
		var resultContent models.MarketInput
		err := config.MySQL.QueryRow(`SELECT m.name as match_name, s.sport_id, s.name as sport_name, sr.series_id, sr.name as series_name FROM matches m 
		INNER JOIN sports s on (s.sport_id = m.sport_id AND s.is_active='1')
		INNER JOIN series sr on (sr.series_id = m.series_id AND sr.is_active='1') WHERE m.match_id = ? AND m.is_active = '1'`, content.MatchId).Scan(&resultContent.MatchName, &resultContent.SportId, &resultContent.SportName, &resultContent.SeriesId, &resultContent.SeriesName)
		if err != nil && err != sql.ErrNoRows {
			log.Println("CreateMarket", err.Error())
			return "Unable Create Market", false
		}
		if resultContent.MatchName.String == "" {
			return "Sport/Series/Match not active", false
		}
		content.SportId = resultContent.SportId.String
		content.SportName = resultContent.SportName.String
		content.SeriesId = resultContent.SeriesId.String
		content.SeriesName = resultContent.SeriesName.String
		content.MatchName = resultContent.MatchName.String
		marshalRunnerJson, _ := json.Marshal(content.RunnerJson)

		query := `INSERT INTO markets (sport_id, sport_name, series_id, series_name, match_id, match_name, market_id, name, market_start_time, is_bookmaker, runner_json, is_fancy, created_at,updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,'0',NOW(),NOW())`
		stmt, err := config.MySQL.Prepare(query)
		if err != nil {
			log.Println("CreateMarket >> Insert error", err.Error())
			return "Unable Create Market", false
		}
		defer stmt.Close()
		_, err = stmt.Exec(content.SportId, content.SportName, content.SeriesId, content.SeriesName, content.MatchId, content.MatchName, content.MarketId, content.Name, content.MarketStartTime, content.IsBookmaker, string(marshalRunnerJson))
		if err != nil {
			log.Println("CreateMarket >> Insert error", err.Error())
			return "Unable Create Market", false
		}
		message = "Market Created Successfully"
		status = true
		return message, status

	} else {
		return "Market Allready Created", false
	}
}

//GetOnlineSelection :
func GetOnlineSelection(marketID, matchID string) bool {
	sportSeriesMatchMarkets := []models.SportSeriesMatchMarket{}

	content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineMarketsRunnersURL + marketID)
	if statusCode != 200 || err != nil {
		log.Println("GetOnlineSelection >> statuscode >> ", statusCode, ": error >> ", err.Error())
		return false
	}

	err = json.Unmarshal(content, &sportSeriesMatchMarkets)
	if err != nil {
		return false
	}
	query := "INSERT INTO market_selections (match_id, market_id, selection_id, name, sort_priority, sort_name, created_at) VALUES "
	var vals string
	var queryVals []string
	if len(sportSeriesMatchMarkets) > 0 {
		for _, content := range sportSeriesMatchMarkets[0].Runners {
			vals = "('" + matchID + "','" + marketID + "','" + strconv.Itoa(content.SelectionID) + "','" + content.RunnerName + "'," + strconv.Itoa(int(content.SortPriority)) + ",'" + content.RunnerName + "',NOW())"
			queryVals = append(queryVals, vals)
		}
	}
	//prepare the statement
	stmt, err := config.MySQL.Prepare(query + strings.Join(queryVals, ","))
	if err != nil {
		log.Println("GetOnlineSelection >> CreateMarketSelections >> Unable create market selections", err.Error())
		return false
	}
	defer stmt.Close()
	//format all vals at once
	_, err = stmt.Exec()
	if err != nil {
		log.Println("GetOnlineSelection >> CreateMarketSelections >> Unable create market selections", err.Error())
		return false
	}
	return true
}

//GetOnlineSelectionBookmaker :
func GetOnlineSelectionBookmaker(marketID, matchID string) bool {
	sportSeriesMatchMarkets := []models.SportSeriesMatchMarket{}

	content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineBookmakerMarketsRunnersURL + marketID)
	if statusCode != 200 || err != nil {
		log.Println("GetOnlineSelectionBookmaker >> statuscode >> ", statusCode, ": error >> ", err.Error())
		return false
	}

	err = json.Unmarshal(content, &sportSeriesMatchMarkets)
	if err != nil {
		return false
	}
	query := "INSERT INTO market_selections (match_id, market_id, selection_id, name, sort_priority, sort_name, created_at) VALUES "
	var vals string
	var queryVals []string
	if len(sportSeriesMatchMarkets) > 0 {
		for _, content := range sportSeriesMatchMarkets[0].Runners {
			vals = "('" + matchID + "','" + marketID + "','" + strconv.Itoa(content.SelectionID) + "','" + content.RunnerName + "'," + strconv.Itoa(int(content.SortPriority)) + ",'" + content.RunnerName + "',NOW())"
			queryVals = append(queryVals, vals)
		}
	}
	//prepare the statement
	stmt, err := config.MySQL.Prepare(query + strings.Join(queryVals, ","))
	if err != nil {
		log.Println("GetOnlineSelectionBookmaker >> CreateMarketSelections >> Unable create market selections", err.Error())
		return false
	}
	defer stmt.Close()
	//format all vals at once
	_, err = stmt.Exec()
	if err != nil {
		log.Println("GetOnlineSelectionBookmaker >> CreateMarketSelections >> Unable create market selections", err.Error())
		return false
	}
	return true
}

//UpdateMarketStatus :
func UpdateMarketStatus(contant models.Market) string {

	updateStr := "UPDATE markets SET is_active = ? WHERE market_id = ?"
	stmt, err := config.MySQL.Prepare(updateStr)
	if err != nil {
		log.Println("UpdateMarketStatus >> Unable update market status", err.Error())
		return "Unable update status"
	}
	_, err = stmt.Exec(contant.IsActive, contant.MarketId)
	if err != nil {
		log.Println("UpdateMarketStatus >> Unable update market status", err.Error())
		return "Unable update status"
	}
	if contant.IsActive == "0" {
		return "Market Deactivated Successfully"
	} else {
		return "Market Activated Successfully"
	}
}

// GetMarkets :
func GetMarkets(sportId, seriesId, isActive, matchId, isResulted string) (dataOutput []models.Market, err error) {
	whereStr := ""

	if sportId != "" {
		whereStr += " AND m.sport_id = '" + sportId + "'"
	}
	if seriesId != "" {

		whereStr += " AND m.series_id = '" + seriesId + "'"
	}
	if isActive == "0" || isActive == "1" {
		whereStr += " AND m.is_active = '" + isActive + "'"
	}
	if matchId != "" {
		whereStr += " AND m.match_id ='" + matchId + "'"
	}
	if isResulted != "" {
		if isResulted == "1" {
			whereStr += " AND (mr.is_result_declared ='1' OR mr.is_result_declared ='2')"
		} else {
			whereStr += " AND mr.is_result_declared ='0'"
		}

	}
	rows, err := config.MySQL.Query("Select mr.sport_id,mr.sport_name, mr.series_id,mr.series_name, mr.match_id, mr.match_name, mr.market_id, mr.name, mr.is_bookmaker, mr.result_date, mr.market_start_time, mr.is_active, mr.created_at, mr.result, mr.winner_id, m.start_date, mr.runner_json FROM markets mr INNER JOIN sports sp ON (mr.sport_id=sp.sport_id AND sp.is_active = ?) INNER JOIN series sr ON (mr.series_id=sr.series_id AND sr.is_active = ?) INNER JOIN matches m ON (mr.match_id=m.match_id AND m.is_active = ?) WHERE mr.is_fancy = '0' "+whereStr+" ORDER BY mr.created_at", "1", "1", "1")
	if err != nil {
		log.Println("GetMarkets >> Unable get markets", err.Error())
		return dataOutput, err
	}
	defer rows.Close()
	for rows.Next() {
		var content models.MarketInput
		err := rows.Scan(
			&content.SportId,
			&content.SportName,
			&content.SeriesId,
			&content.SeriesName,
			&content.MatchId,
			&content.MatchName,
			&content.MarketId,
			&content.Name,
			&content.IsBookmaker,
			&content.ResultDate,
			&content.MarketStartTime,
			&content.IsActive,
			&content.CreatedAt,
			&content.Result,
			&content.WinnerId,
			&content.MatchDate,
			&content.RunnerJson,
		)
		if err != nil {
			log.Println("GetMarket >> Unable get market", err.Error())
		}
		var singleMatch models.Market
		singleMatch.SportId = content.SportId.String
		singleMatch.SportName = content.SportName.String
		singleMatch.SeriesId = content.SeriesId.String
		singleMatch.SeriesName = content.SeriesName.String
		singleMatch.MatchId = content.MatchId.String
		singleMatch.MatchName = content.MatchName.String
		singleMatch.MarketId = content.MarketId.String
		singleMatch.Name = content.Name.String
		singleMatch.IsBookmaker = content.IsBookmaker.String
		singleMatch.ResultDate = content.ResultDate.String
		singleMatch.MarketStartTime = content.MarketStartTime.String
		singleMatch.IsActive = content.IsActive.String
		singleMatch.CreatedAt = content.CreatedAt.String
		singleMatch.Result = content.Result.String
		singleMatch.WinnerId = content.WinnerId.String
		singleMatch.MatchDate = content.MatchDate.String
		json.Unmarshal([]byte(content.RunnerJson.String), &singleMatch.RunnerJson)
		dataOutput = append(dataOutput, singleMatch)
	}

	return dataOutput, nil
}

// GetSessionAndMarketOdds :
func GetSessionAndMarketOdds(ids string, callType string) (dataOutput []models.MarketOdds, fancyDataOutput []models.RemoteRedisFancy) {
	idsArr := strings.Split(ids, ",")
	var keys []string
	for _, marketid := range idsArr {
		if callType == "1" {
			keys = append(keys, "MARKET_"+marketid)
		} else if callType == "2" {
			keys = append(keys, "SESSION_"+marketid)
		}
	}
	val := config.GetValuesFromRedis(keys)
	if callType == "1" {
		for _, v := range val {
			var market models.MarketOdds
			err := json.Unmarshal([]byte(fmt.Sprint(v)), &market)
			if err != nil && !strings.Contains(fmt.Sprint(v), "nil") {
				// fmt.Println(fmt.Sprint(v))
				log.Println("GetSessionAndMarketOdds >> market >>", err.Error())
			}
			dataOutput = append(dataOutput, market)
		}
	} else if callType == "2" {
		for _, v := range val {
			var fancy models.RemoteRedisFancy
			err := json.Unmarshal([]byte(fmt.Sprint(v)), &fancy)
			if err != nil && !strings.Contains(fmt.Sprint(v), "nil") {
				// fmt.Println(fmt.Sprint(v))
				log.Println("GetSessionAndMarketOdds >> session >>", err.Error())
			}
			fancyDataOutput = append(fancyDataOutput, fancy)
		}
	}
	return dataOutput, fancyDataOutput
}

// GetMatches :
func GetMarketById(isActive, marketID, matchId string) (models.Market, float64, float64, float64, float64, error) {
	whereStr := ""
	var content models.MarketInput
	if isActive == "0" || isActive == "1" {
		whereStr += " AND mr.is_active = '" + isActive + "'"
	}
	if matchId != "" {
		whereStr += " AND mr.match_id = '" + matchId + "'"
	}
	var singleMatch models.Market
	var maxStack, minStack, sessionMinStack, sessinMaxStack sql.NullFloat64
	err := config.MySQL.QueryRow("Select mr.sport_id,mr.sport_name, mr.series_id,mr.series_name, mr.match_id, mr.match_name, mr.market_id, mr.name, mr.is_bookmaker, mr.result_date, mr.market_start_time, mr.is_active, mr.created_at, mr.result, mr.winner_id, mr.is_result_declared, m.min_stack, m.max_stack, m.session_min_stack, m.session_max_stack, mr.runner_json, sp.odds_limit FROM markets mr INNER JOIN sports sp ON (mr.sport_id=sp.sport_id AND sp.is_active = ?) INNER JOIN series sr ON (mr.series_id=sr.series_id AND sr.is_active = ?) INNER JOIN matches m ON (mr.match_id=m.match_id AND m.is_active = ?) WHERE market_id = ? "+whereStr+" ORDER BY mr.created_at", "1", "1", "1", marketID).Scan(&content.SportId, &content.SportName, &content.SeriesId, &content.SeriesName, &content.MatchId, &content.MatchName, &content.MarketId, &content.Name, &content.IsBookmaker, &content.ResultDate, &content.MarketStartTime, &content.IsActive, &content.CreatedAt, &content.Result, &content.WinnerId, &content.IsResultDeclared, &minStack, &maxStack, &sessionMinStack, &sessinMaxStack, &content.RunnerJson, &content.OddsLimit)
	if err != nil {
		log.Println("GetMarketsById >> Unable get markets", err.Error())
		return singleMatch, minStack.Float64, maxStack.Float64, sessionMinStack.Float64, sessinMaxStack.Float64, err
	}
	singleMatch.SportId = content.SportId.String
	singleMatch.SportName = content.SportName.String
	singleMatch.SeriesId = content.SeriesId.String
	singleMatch.SeriesName = content.SeriesName.String
	singleMatch.MatchId = content.MatchId.String
	singleMatch.MatchName = content.MatchName.String
	singleMatch.MarketId = content.MarketId.String
	singleMatch.Name = content.Name.String
	singleMatch.IsBookmaker = content.IsBookmaker.String
	singleMatch.ResultDate = content.ResultDate.String
	singleMatch.MarketStartTime = content.MarketStartTime.String
	singleMatch.IsActive = content.IsActive.String
	singleMatch.CreatedAt = content.CreatedAt.String
	singleMatch.Result = content.Result.String
	singleMatch.WinnerId = content.WinnerId.String
	singleMatch.IsResultDeclared = content.IsResultDeclared.String
	singleMatch.OddsLimit = content.OddsLimit.Float64
	if content.RunnerJson.String != "" {
		err = json.Unmarshal([]byte(content.RunnerJson.String), &singleMatch.RunnerJson)
		if err != nil {
			// fmt.Println(err.Error())
			log.Println("GetMarket >> Unable get market", err.Error())
		}
	}

	return singleMatch, minStack.Float64, maxStack.Float64, sessionMinStack.Float64, sessinMaxStack.Float64, nil
}

//GetTeamPosition :
func GetTeamPosition(userId, userTypeId int64, matchId string, marketId string, isLiveSport string) (teamPosition []models.TeamPosition) {

	rows, err := config.MySQL.Query(`Select match_id,market_id,selection_id,selection_name,liability_type, stack,win_loss,win_value,loss_value FROM odds_profit_loss WHERE user_id = ? AND market_id =?`, userId, marketId)
	if err != nil {
		log.Println("GetTeamPosition", err.Error())
		return teamPosition
	}
	defer rows.Close()
	for rows.Next() {

		var singlePosition models.TeamPositionInput
		err := rows.Scan(
			&singlePosition.MatchId,
			&singlePosition.MarketId,
			&singlePosition.SelectionId,
			&singlePosition.SelectionName,
			&singlePosition.LiabilityType,
			&singlePosition.Stack,
			&singlePosition.WinLoss,
			&singlePosition.WinValue,
			&singlePosition.LossValue,
		)
		if err != nil {
			log.Println("GetMarket >> Unable get market", err.Error())
		}

		positionMap := map[string]models.TeamPosition{}
		var position models.TeamPosition
		if userTypeId != 7 {
			position.MatchId = singlePosition.MatchId.String
			position.MarketId = singlePosition.MarketId.String
			position.SelectionId = singlePosition.SelectionId.String
			position.SelectionName = singlePosition.SelectionName.String
			position.LiabilityType = singlePosition.LiabilityType.String
			position.Stack = singlePosition.Stack.Float64
			position.WinLoss += singlePosition.WinLoss.Float64
		} else {
			position.MatchId = singlePosition.MatchId.String
			position.MarketId = singlePosition.MarketId.String
			position.SelectionId = singlePosition.SelectionId.String
			position.SelectionName = singlePosition.SelectionName.String
			position.LiabilityType = singlePosition.LiabilityType.String
			position.WinLoss = singlePosition.WinValue.Float64 + singlePosition.LossValue.Float64
			position.WinValue = singlePosition.WinValue.Float64
			position.LossValue = singlePosition.LossValue.Float64
		}
		positionMap[singlePosition.SelectionId.String] = position
		for _, record := range positionMap {
			teamPosition = append(teamPosition, record)
		}
	}
	if len(teamPosition) == 0 {
		marketSelectionList := []models.MarketSelection{}
		if isLiveSport == "1" {
			marketSelectionList = MarketSelectionListByMatchId(matchId)
		} else {
			marketSelectionList = MarketSelectionList(matchId, marketId)
		}

		// marketSelectionList := MarketSelectionList(matchId, marketId)

		for i := 0; i < len(marketSelectionList); i++ {
			teamPosition = append(teamPosition, models.TeamPosition{
				MatchId:       matchId,
				MarketId:      marketId,
				SelectionId:   marketSelectionList[i].SelectionId,
				SelectionName: marketSelectionList[i].Name,
				LiabilityType: marketSelectionList[i].LiabilityType,
				WinValue:      0,
				LossValue:     0,
				WinLoss:       0,
			})
		}
	}
	return teamPosition
}

//MarketSelectionList :
func MarketSelectionList(matchId string, marketId string) (marketData []models.MarketSelection) {
	rows, err := config.MySQL.Query("Select id, match_id, market_id, selection_id, name, sort_priority, sort_name, liability_type, created_at FROM market_selections WHERE match_id = ? AND market_id = ? ORDER BY sort_priority ASC", matchId, marketId)
	if err != nil {
		log.Println("MarketSelectionList", err.Error())
	}
	defer rows.Close()
	for rows.Next() {
		var content models.MarketSelectionInput
		err := rows.Scan(
			&content.Id,
			&content.MatchId,
			&content.MarketId,
			&content.SelectionId,
			&content.Name,
			&content.SortPriority,
			&content.SortName,
			&content.LiabilityType,
			&content.CreatedAt,
		)
		if err != nil {
			log.Println("MarketSelectionList", err.Error())
		}
		var singlePosition models.MarketSelection
		singlePosition.Id = content.Id.Int64
		singlePosition.MatchId = content.MatchId.String
		singlePosition.MarketId = content.MarketId.String
		singlePosition.SelectionId = content.SelectionId.String
		singlePosition.Name = content.Name.String
		singlePosition.SortPriority = content.SortPriority.Int64
		singlePosition.SortName = content.SortName.String
		singlePosition.LiabilityType = content.LiabilityType.String
		singlePosition.CreatedAt = content.CreatedAt.String
		marketData = append(marketData, singlePosition)
	}
	return marketData
}

//MarketSelectionListByMatchId :
func MarketSelectionListByMatchId(matchId string) (marketData []models.MarketSelection) {

	rows, err := config.MySQL.Query("Select id, match_id, market_id, selection_id, name, sort_priority, sort_name, liability_type, created_at FROM market_selections WHERE match_id = ? ORDER BY sort_priority ASC", matchId)
	if err != nil {
		log.Println("MarketSelectionlistByMatchId", err.Error())
	}
	defer rows.Close()
	for rows.Next() {
		var content models.MarketSelectionInput
		err := rows.Scan(
			&content.Id,
			&content.MatchId,
			&content.MarketId,
			&content.SelectionId,
			&content.Name,
			&content.SortPriority,
			&content.SortName,
			&content.LiabilityType,
			&content.CreatedAt,
		)
		if err != nil {
			log.Println("MarketSelectionlistByMatchId", err.Error())
		}
		var singlePosition models.MarketSelection
		singlePosition.Id = content.Id.Int64
		singlePosition.MatchId = content.MatchId.String
		singlePosition.MarketId = content.MarketId.String
		singlePosition.SelectionId = content.SelectionId.String
		singlePosition.Name = content.Name.String
		singlePosition.SortPriority = content.SortPriority.Int64
		singlePosition.SortName = content.SortName.String
		singlePosition.LiabilityType = content.LiabilityType.String
		singlePosition.CreatedAt = content.CreatedAt.String
		marketData = append(marketData, singlePosition)
	}
	return marketData
}

//CreateMarketPosition
func CreateMarketPosition(userId int64, parentIds, matchId, marketId string, IsDeleteBet int64, tx *sql.Tx) (error, *sql.Tx) {
	var Bets []models.GetBetForPositionInput
	rows, err := tx.Query(`SELECT b.user_id, b.match_id, b.market_id, b.selection_id, b.market_name, a.name AS selection_name, a.liability_type,b.is_back, c.liability_type as c_liability_type, a.selection_id as a_selection_id, b.stack,b.profit, b.is_matched, b.exposure, m.liability_type as match_liability_type
	FROM bets b
	INNER JOIN sports s ON (s.sport_id = b.sport_id)
	INNER JOIN matches m ON (b.match_id = m.match_id)
	INNER JOIN market_selections a ON(a.match_id = b.match_id)
	INNER JOIN market_selections c ON (b.match_id = c.match_id AND b.selection_id = c.selection_id)
	WHERE
	 b.is_deleted="0" AND b.user_id = ? AND b.market_id = ? AND ((s.is_live_sport = '0' AND a.market_id = ?) OR (s.is_live_sport = '1')) AND ((s.is_live_sport = '0' AND c.market_id = ?) OR (s.is_live_sport = '1'))
	`, userId, marketId, marketId, marketId)
	if err != nil {
		log.Println("CreateMarketPosition", err.Error())
		return err, tx
	}
	defer rows.Close()
	for rows.Next() {
		var content models.GetBetForPositionInput
		err := rows.Scan(
			&content.UserId,
			&content.MatchId,
			&content.MarketId,
			&content.SelectionId,
			&content.MarketName,
			&content.SelectionName,
			&content.LiabilityType,
			&content.IsBack,
			&content.CLiabilityType,
			&content.ASelectionId,
			&content.Stack,
			&content.PL,
			&content.IsMatched,
			&content.Liability,
			&content.MatchLiabilityType,
		)
		if err != nil {
			log.Println("CreateMarketPosition", err.Error())
			return err, tx
		}
		Bets = append(Bets, content)
	}
	parentDetailsMap := GetUserPartnership(parentIds, userId)
	if len(Bets) > 0 {
		var parentIdsArr []string
		singleUserMap := map[string]models.TeamPosition{}
		for _, bet := range Bets {
			singleUser := singleUserMap[strconv.Itoa(int(bet.UserId.Int64))+"_"+bet.ASelectionId.String]
			parentIdsArr = strings.Split(global.TrimCommaBothSide(parentIds), ",")
			// stack or winvalue for user
			if bet.IsMatched.String == "1" {
				if bet.SelectionId == bet.ASelectionId {
					if bet.IsBack.String == "0" {
						singleUser.Stack += bet.Liability.Float64
						singleUser.WinValue += bet.Liability.Float64
					} else {
						singleUser.Stack += float64(bet.Stack.Int64)
						singleUser.WinValue += bet.PL.Float64
					}
				} else {
					singleUser.Stack += 0
					singleUser.WinValue += 0
				}
			} else {
				singleUser.Stack += 0
				singleUser.WinValue += 0
			}
			// lossvalue for user
			if bet.IsMatched.String == "1" && bet.CLiabilityType.String == "0" && bet.LiabilityType.String == "0" {
				if bet.SelectionId == bet.ASelectionId {
					singleUser.LossValue += 0
				} else {
					if bet.MatchLiabilityType.String == "1" && bet.LiabilityType.String == "1" {
						singleUser.LossValue += 0
					} else {
						if bet.IsBack.String == "0" {
							singleUser.LossValue += bet.PL.Float64
						} else {
							singleUser.LossValue -= float64(bet.Stack.Int64)
						}
					}
				}

			} else {
				singleUser.LossValue += 0
			}
			singleUser.UserId = bet.UserId.Int64
			singleUser.MatchId = bet.MatchId.String
			singleUser.MarketId = bet.MarketId.String
			singleUser.MarketName = bet.MarketName.String
			singleUser.SelectionId = bet.ASelectionId.String
			singleUser.SelectionName = bet.SelectionName.String
			singleUser.LiabilityType = bet.LiabilityType.String
			if value, OK := parentDetailsMap[bet.UserId.Int64]; OK {
				singleUser.UserTypeId = value.UserTypeId
			}
			singleUser.UserIds = parentIds + strconv.Itoa(int(bet.UserId.Int64)) + ","
			singleUserMap[strconv.Itoa(int(bet.UserId.Int64))+"_"+bet.ASelectionId.String] = singleUser
			for _, pId := range parentIdsArr {
				var partnership, parentUserTypeId int64
				singleParentPostion := singleUserMap[pId+"_"+bet.ASelectionId.String]
				pIdInt, _ := strconv.Atoi(pId)
				if value, OK := parentDetailsMap[int64(pIdInt)]; OK {
					partnership = value.Partnership
					parentUserTypeId = value.UserTypeId
				}

				// winloss for parent
				if bet.IsMatched.String == "1" {
					var winloss1, winloss2, sumWinloss float64
					if bet.SelectionId == bet.ASelectionId {
						if bet.MatchLiabilityType.String == "1" && bet.LiabilityType.String == "1" {
							winloss1 += bet.Liability.Float64
						} else {
							if bet.IsBack.String == "0" {
								winloss1 += bet.Liability.Float64
							} else {
								winloss1 += bet.PL.Float64
							}
						}
					} else {
						winloss1 += 0
					}
					sumWinloss += (winloss1 * -1 * float64(partnership) / 100)
					if bet.IsMatched.String == "1" && bet.CLiabilityType.String != "1" && bet.LiabilityType.String != "1" {
						if bet.SelectionId == bet.ASelectionId {
							winloss2 += 0
						} else {
							if bet.IsBack.String == "0" {
								winloss2 += bet.PL.Float64
							} else {
								winloss2 += -(float64(bet.Stack.Int64))
							}
						}
					} else {
						winloss2 += 0
					}
					sumWinloss += (winloss2 * -1 * float64(partnership) / 100)
					singleParentPostion.WinLoss += sumWinloss
				} else {
					singleParentPostion.WinLoss += 0
				}
				singleParentPostion.UserId = int64(pIdInt)
				singleParentPostion.MatchId = bet.MatchId.String
				singleParentPostion.MarketId = bet.MarketId.String
				singleParentPostion.MarketName = bet.MarketName.String
				singleParentPostion.SelectionId = bet.ASelectionId.String
				singleParentPostion.SelectionName = bet.SelectionName.String
				singleParentPostion.LiabilityType = bet.LiabilityType.String
				singleParentPostion.UserTypeId = parentUserTypeId
				singleParentPostion.UserIds = parentIds + strconv.Itoa(int(bet.UserId.Int64)) + ","
				singleUserMap[pId+"_"+bet.ASelectionId.String] = singleParentPostion
			}
		}
		for _, singleMap := range singleUserMap {
			sqlStr := `INSERT INTO odds_profit_loss (user_id, user_type_id, user_parent_ids, match_id, market_id, market_name, selection_id, selection_name, liability_type, stack, win_value, loss_value, win_loss) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE stack = ?, win_value = ?, loss_value = ?, win_loss = ?`
			stmt, err := tx.Prepare(sqlStr)
			if err != nil {
				log.Println("Error Insert Position", err.Error())
				return err, tx
			}
			defer stmt.Close()
			_, err = stmt.Exec(singleMap.UserId, singleMap.UserTypeId, singleMap.UserIds, singleMap.MatchId, singleMap.MarketId, singleMap.MarketName, singleMap.SelectionId, singleMap.SelectionName, singleMap.LiabilityType, singleMap.Stack, singleMap.WinValue, singleMap.LossValue, singleMap.WinLoss, singleMap.Stack, singleMap.WinValue, singleMap.LossValue, singleMap.WinLoss)
			if err != nil {
				log.Println("Error Insert Position", err.Error())
				return err, tx
			}
		}
	} else if IsDeleteBet == 1 {
		sqlStr := `DELETE FROM odds_profit_loss WHERE match_id = ?, market_id = ?, user_ids = ?`
		stmt, err := tx.Prepare(sqlStr)
		if err != nil {
			log.Println("Error Insert Position", err.Error())
			return err, tx
		}
		defer stmt.Close()
		_, err = stmt.Exec(matchId, marketId, parentIds+strconv.Itoa(int(userId))+",")
		if err != nil {
			log.Println("Error Insert Position", err.Error())
			return err, tx
		}
	}
	return nil, tx
}

//GetUserPartnerShip : Get ParentPartnership for calculation
func GetUserPartnership(parentIds string, userId int64) map[int64]models.User {
	//parentidStr := strings.Join(parentIdsArr, ",")
	usersData := models.User{}
	//parentIds = strconv.Itoa(int(userId))
	if parentIds != "" {
		parentIds = global.TrimCommaBothSide(parentIds) + "," + strconv.Itoa(int(userId))
	} else {
		parentIds = strconv.Itoa(int(userId))
	}
	parentDataMap := map[int64]models.User{}
	parentIdMap := map[int64]models.User{}
	userIdMap := map[int64]models.User{}
	//fmt.Println(`SELECT id, parent_id, user_type_id, partnership, match_commission, session_commission, session_commission_type, is_custom_partnership FROM users WHERE id in (` + parentIds + `) order by desc`)
	rows, err := config.MySQL.Query(`SELECT id, parent_id, user_type_id, partnership, market_commission, bookmaker_commission, session_commission FROM users WHERE id in (` + parentIds + `) order by id desc`)
	//fmt.Println(`SELECT id, parent_id, partnership FROM users WHERE id in (` + parentIds + `)`)
	if err != nil {
		log.Println("CreateMarketPosition", err.Error())
		return nil
	}
	defer rows.Close()
	for rows.Next() {
		var content models.UserInput
		err := rows.Scan(
			&content.Id,
			&content.ParentId,
			&content.UserTypeId,
			&content.Partnership,
			&content.MarketCommission,
			&content.BookmakerCommission,
			&content.SessionCommission,
		)
		if err != nil {
			log.Println("CreateMarketPosition", err.Error())
			return parentDataMap
		}
		contentOutput := models.User{}
		contentOutput.Id = content.Id.Int64
		contentOutput.ParentId = content.ParentId.Int64
		contentOutput.Partnership = content.Partnership.Int64
		contentOutput.UserTypeId = content.UserTypeId.Int64
		contentOutput.SessionCommission = content.SessionCommission.Int64
		contentOutput.MarketCommission = content.MarketCommission.Int64
		contentOutput.BookmakerCommission = content.BookmakerCommission.Int64
		if contentOutput.UserTypeId == 7 {
			usersData = contentOutput
		} else {
			userIdMap[contentOutput.Id] = contentOutput
			parentIdMap[contentOutput.ParentId] = contentOutput
		}
	}
	if parentValue, validOK := userIdMap[usersData.ParentId]; validOK {
		parentValue.Partnership = usersData.Partnership
		userIdMap[parentValue.Id] = parentValue
		parentIdMap[parentValue.ParentId] = parentValue
	}
	for _, singleUSer := range userIdMap {
		user := singleUSer
		if child, childValid := parentIdMap[singleUSer.Id]; childValid {
			user.Partnership -= child.Partnership
			parentDataMap[singleUSer.Id] = user
		} else {
			parentDataMap[singleUSer.Id] = user
		}
	}
	parentDataMap[usersData.Id] = usersData
	return parentDataMap
}
