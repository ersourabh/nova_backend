package services

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"novaexchbackend/api/config"
	"novaexchbackend/api/models"
	"strconv"
	"strings"
)

// CreateMatch :
func CreateMatch(matchData models.Match) (bool, string) {
	var seriesID sql.NullString

	err := config.MySQL.QueryRow("Select series.id, sports.name, series.name FROM series INNER JOIN sports on series.sport_id = sports.sport_id Where series.series_id = ? AND series.is_active = ? AND sports.is_active = ?", matchData.SeriesId, "1", "1").Scan(&seriesID, &matchData.SportName, &matchData.SeriesName)
	if err != nil {
		return false, "Unable to create Match"
	} else {
		if seriesID.String != "" {
			var matchCount sql.NullInt64
			err := config.MySQL.QueryRow("Select count(id) AS total_count FROM matches Where match_id = ? ", matchData.MatchId).Scan(&matchCount)
			if err != nil {
				return false, "Unable to create Match"
			}
			if matchCount.Int64 == 0 {
				insertStr := `INSERT INTO matches (sport_id,sport_name,series_id,series_name,match_id, name, match_date, start_date, score_type, score_key, is_active, liability_type, max_stack, min_stack, session_min_stack, session_max_stack, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW(),NOW())`
				stmt, err := config.MySQL.Prepare(insertStr)
				if err != nil {
					return false, "Unable to create Match"
				}
				_, err = stmt.Exec(matchData.SportId, matchData.SportName, matchData.SeriesId, matchData.SeriesName, matchData.MatchId, matchData.Name, matchData.MatchDate, matchData.StartDate, matchData.ScoreType, matchData.ScoreKey, matchData.IsActive, matchData.LiabilityType, matchData.MaxStack, matchData.MinStack, matchData.SessionMinStack, matchData.SessionMaxStack)
				if err != nil {
					return false, "Unable to create Match"
				}
				return true, "Match Created Successfully"
			} else {
				return false, "Match Already Exists"
			}
		} else {
			return false, "Sport/Series Not Available"
		}
	}
}

// GetMatches :
func GetMatches(sportId, seriesId, isActive, search string, userTypeId int64) (dataOutput []models.Match, err error) {
	whereStr := ""
	if sportId != "" || seriesId != "" || isActive != "" || search != "" {
		whereStr += " WHERE"
	}

	if sportId != "" {
		whereStr += " m.sport_id = '" + sportId + "'"
	}
	if seriesId != "" {
		if sportId != "" {
			whereStr += " AND"
		}
		whereStr += " m.series_id = '" + seriesId + "'"
	}
	if isActive == "0" || isActive == "1" {
		if sportId != "" || seriesId != "" {
			whereStr += " AND"
		}
		whereStr += " m.is_active = '" + isActive + "'"
	}
	if search != "" {
		if sportId != "" || seriesId != "" || isActive != "" {
			whereStr += " AND"
		}
		whereStr += " m.name LIKE %" + search + "%'"
	}
	//fmt.Println("Select m.sport_id, m.series_id, m.match_id, m.name, m.match_date, m.start_date, m.odd_limit, m.min_stack, m.max_stack, m.score_type, m.score_key, m.is_active, m.liability_type, m.is_deactivated_by_super_admin FROM matches m INNER JOIN sports sp ON (m.sport_id=sp.sport_id AND sp.is_active = ?) INNER JOIN series sr ON (m.series_id=sr.series_id AND sr.is_active = ?)"+whereStr+" ORDER BY m.start_date DESC", "1", "1")
	rows, err := config.MySQL.Query("Select m.sport_id,m.sport_name, m.series_id,m.sport_name, m.match_id, m.name, m.match_date, m.start_date, m.min_stack, m.max_stack, m.score_type, m.score_key, m.is_active, m.liability_type, m.tv_url, m.session_min_stack, m.session_max_stack FROM matches m INNER JOIN sports sp ON (m.sport_id=sp.sport_id AND sp.is_active = ?) INNER JOIN series sr ON (m.series_id=sr.series_id AND sr.is_active = ?)"+whereStr+" ORDER BY m.match_date ASC", "1", "1")
	if err != nil {
		log.Println("GetMatches >> Unable get GetMatches", err.Error())
		return dataOutput, err
	}
	defer rows.Close()
	for rows.Next() {
		var content models.MatchInput
		err := rows.Scan(
			&content.SportId,
			&content.SportName,
			&content.SeriesId,
			&content.SeriesName,
			&content.MatchId,
			&content.Name,
			&content.MatchDate,
			&content.StartDate,
			&content.MinStack,
			&content.MaxStack,
			&content.ScoreType,
			&content.ScoreKey,
			&content.IsActive,
			&content.LiabilityType,
			&content.TvUrl,
			&content.SessionMinStack,
			&content.SessionMaxStack,
		)
		if err != nil {
			log.Println("GetMatches >> Unable get GetMatches", err.Error())
		}
		var singleMatch models.Match
		singleMatch.SportId = content.SportId.String
		singleMatch.SportName = content.SportName.String
		singleMatch.SeriesId = content.SeriesId.String
		singleMatch.SeriesName = content.SeriesName.String
		singleMatch.MatchId = content.MatchId.String
		singleMatch.Name = content.Name.String
		singleMatch.MatchDate = content.MatchDate.String
		singleMatch.StartDate = content.StartDate.String
		singleMatch.MinStack = content.MinStack.Float64
		singleMatch.MaxStack = content.MaxStack.Float64
		singleMatch.ScoreType = content.ScoreType.String
		singleMatch.ScoreKey = content.ScoreKey.String
		singleMatch.IsActive = content.IsActive.String
		singleMatch.LiabilityType = content.LiabilityType.String
		singleMatch.TvUrl = content.TvUrl.String

		singleMatch.SessionMinStack = content.SessionMinStack.Float64
		singleMatch.SessionMaxStack = content.SessionMaxStack.Float64

		// if isHideDeclaredMatch == "1" {
		// 	marketData, _, _ := GetMarketsMongo(singleMatch.MatchId, "")
		// 	if len(marketData) > 0 {
		// 		dataOutput = append(dataOutput, singleMatch)
		// 	}
		// } else {
		dataOutput = append(dataOutput, singleMatch)
		// }
	}

	return dataOutput, nil
}

//UpdateMatchStatus :
func UpdateMatchStatus(contant models.Match) string {

	updateStr := "UPDATE matches SET is_active = ? WHERE match_id = ?"
	stmt, err := config.MySQL.Prepare(updateStr)
	if err != nil {
		log.Println("UpdateMatchStatus >> Unable update match status", err.Error())
		return "Unable update status"
	}
	_, err = stmt.Exec(contant.IsActive, contant.MatchId)
	if err != nil {
		log.Println("UpdateMatchStatus >> Unable update match status", err.Error())
		return "Unable update status"
	}
	if contant.IsActive == "0" {
		return "Match Deactivated Successfully"
	} else {
		return "Match Activated Successfully"
	}
}

//GetSportsListForMenu :
func GetSportsListForMenu(search string) (output []models.SportListOutput, err error) {

	var sports []models.SportListOutput
	var seriesIds []string

	isSportExitMap := map[string]string{}
	seriesMap := map[string][]models.SeriesListForMenu{}
	matchMap := map[string][]models.MatchListForMenu{}

	var whereStr string
	if search != "" {
		whereStr = ` AND name LIKE '%` + search + `%' `
	}

	rows, err := config.MySQL.Query(`SELECT s.sport_id, s.sport_name, s.series_id, s.name AS series_name
	FROM series s
	INNER JOIN sports sp ON (sp.sport_id=s.sport_id AND sp.is_active='1')
	WHERE s.is_active='1'
	ORDER BY s.sport_id ASC, s.created_at ASC`)
	if err != nil {
		log.Println("GetSportsListForMenu >> Unable to get sport/series", err.Error())
		return output, err
	}

	defer rows.Close()
	for rows.Next() {
		var sportId, sportName, seriesId, seriesName sql.NullString
		err := rows.Scan(
			&sportId,
			&sportName,
			&seriesId,
			&seriesName,
		)
		if err != nil {
			log.Println("GetSportsListForMenu >> Unable get sport/series", err.Error())
		}
		seriesIds = append(seriesIds, seriesId.String)

		var sportDetails models.SportListOutput
		sportDetails.SportName = sportName.String
		sportDetails.SportId = sportId.String

		var seriesDetail models.SeriesListForMenu
		seriesDetail.SeriesName = seriesName.String
		seriesDetail.SeriesId = seriesId.String

		seriesMap[sportId.String] = append(seriesMap[sportId.String], seriesDetail)
		if _, valid := isSportExitMap[sportId.String]; !valid {
			sports = append(sports, sportDetails)
			isSportExitMap[sportId.String] = sportId.String
		}
	}

	if len(seriesIds) > 0 {
		rows1, err := config.MySQL.Query(`SELECT series_id, match_id, name, DATE(match_date) FROM matches WHERE is_active='1' AND series_id IN (` + strings.Join(seriesIds, ",") + `) ` + whereStr + ` ORDER BY match_date ASC`)
		if err != nil {
			log.Println("GetSportsListForMenu >> Unable to get series/matches", err.Error())
			return output, err
		}
		defer rows1.Close()
		for rows1.Next() {
			var seriesId, matchId, matchName, matchDate sql.NullString
			err := rows1.Scan(
				&seriesId,
				&matchId,
				&matchName,
				&matchDate,
			)
			if err != nil {
				log.Println("GetMatches >> Unable to get series/matches", err.Error())
			}

			// var markets []models.MarketListForMenu
			// rows1, err := config.MySQL.Query(`SELECT market_id, name, is_active FROM markets WHERE match_id=? AND is_active='1' AND is_result_declared='0'`, matchId.String)
			// if err != nil {
			// 	log.Println("GetSportsListForMenu >> Unable to get match/markets", err.Error())
			// 	return output, err
			// }
			// defer rows1.Close()
			// for rows1.Next() {
			// 	var content models.MarketInput
			// 	err := rows1.Scan(
			// 		&content.MarketId,
			// 		&content.Name,
			// 		&content.IsActive,
			// 	)
			// 	if err != nil {
			// 		log.Println("GetSportsListForMenu >> Unable to get match/markets", err.Error())
			// 	}

			// 	var market models.MarketListForMenu
			// 	if content.IsActive.String == "1" {
			// 		if content.Name.String == "Match Odds" || content.Name.String == "Complete" || content.Name.String == "Completed Match" || content.Name.String == "Tie" || content.Name.String == "Tied Match" {
			// 			market.MarketName = content.Name.String
			// 			market.MarketId = content.MarketId.String
			// 			markets = append(markets, market)
			// 		}
			// 	}
			// }

			// if len(markets) > 0 {
			// 	var singleMatch models.MatchListForMenu
			// 	singleMatch.MatchId = matchId.String
			// 	singleMatch.MatchName = matchName.String
			// 	singleMatch.MatchDate = matchDate.String
			// 	singleMatch.Markets = markets
			// 	singleMatch.SeriesId = seriesId.String
			// 	matchMap[seriesId.String] = append(matchMap[seriesId.String], singleMatch)
			// }

			var singleMatch models.MatchListForMenu
			singleMatch.MatchId = matchId.String
			singleMatch.MatchName = matchName.String
			singleMatch.MatchDate = matchDate.String
			singleMatch.SeriesId = seriesId.String
			matchMap[seriesId.String] = append(matchMap[seriesId.String], singleMatch)
		}
	}

	totalMatchCount := 0
	seriesMatchCount := 0

	for _, sport := range sports {
		var seriesRecord []models.SeriesListForMenu
		if record, valid := seriesMap[sport.SportId]; valid {
			for _, series := range record {
				// dateGroup := map[string]models.MatchListDateGroupForMenu{}
				if matches, exit := matchMap[series.SeriesId]; exit {
					for _, match := range matches {
						if match.SeriesId == series.SeriesId {
							match.SeriesId = ""
							// if content, Ok := dateGroup[match.MatchDate]; Ok {
							// 	content.Matches = append(content.Matches, match)
							// 	dateGroup[match.MatchDate] = content
							// } else {
							// 	content.MatchDate = match.MatchDate
							// 	content.Matches = append(content.Matches, match)
							// 	dateGroup[match.MatchDate] = content
							// }
							seriesMatchCount++
							series.MatchesCount = seriesMatchCount
							series.Matches = append(series.Matches, match)
						}
						totalMatchCount++
					}
					seriesMatchCount = 0
				}

				// series.SeriesId = ""
				// for _, matchSingleDate := range dateGroup {
				// 	series.MatchDates = append(series.MatchDates, matchSingleDate)
				// }
				if len(series.Matches) > 0 {
					seriesRecord = append(seriesRecord, series)
				}
			}
		}

		// sport.SportId = ""
		if len(seriesRecord) > 0 {
			sport.SeriesList = seriesRecord
			sport.MatchesCount = totalMatchCount
		}
		output = append(output, sport)
	}
	return output, err
}

// //GetSportsSeriesMatchesCount :
// func GetSportsSeriesMatchesCount() (output []models.SportMatchesForMenu, err error) {

// 	var sports []models.SportMatchesForMenu
// 	var seriesIds []string

// 	isSportExitMap := map[string]string{}
// 	seriesMap := map[string][]models.SeriesMatchesForMenu{}
// 	matchMap := map[string][]models.MatchListForMenu{}

// 	rows, err := config.MySQL.Query(`SELECT s.sport_id, s.sport_name, s.series_id, s.name AS series_name
// 	FROM series s
// 	INNER JOIN sports sp ON (sp.sport_id=s.sport_id AND s.is_active='1')
// 	WHERE s.is_active='1'
// 	ORDER BY s.sport_id ASC, s.created_at ASC`)
// 	if err != nil {
// 		log.Println("GetSportsSeriesMatchesCount >> Unable to get sport/series", err.Error())
// 		return output, err
// 	}

// 	defer rows.Close()
// 	for rows.Next() {
// 		var sportId, sportName, seriesId, seriesName sql.NullString
// 		err := rows.Scan(
// 			&sportId,
// 			&sportName,
// 			&seriesId,
// 			&seriesName,
// 		)
// 		if err != nil {
// 			log.Println("GetSportsSeriesMatchesCount >> Unable get sport/series", err.Error())
// 		}
// 		seriesIds = append(seriesIds, seriesId.String)

// 		var sportDetails models.SportMatchesForMenu
// 		sportDetails.SportName = sportName.String
// 		sportDetails.SportId = sportId.String

// 		var seriesDetail models.SeriesMatchesForMenu
// 		seriesDetail.SeriesName = seriesName.String
// 		seriesDetail.SeriesId = seriesId.String

// 		seriesMap[sportId.String] = append(seriesMap[sportId.String], seriesDetail)
// 		if _, valid := isSportExitMap[sportId.String]; !valid {
// 			sports = append(sports, sportDetails)
// 			isSportExitMap[sportId.String] = sportId.String
// 		}
// 	}

// 	if len(seriesIds) > 0 {
// 		rows1, err := config.MySQL.Query(`SELECT series_id, match_id, name, DATE(match_date) FROM matches WHERE is_resulted ='0' AND series_id IN (` + strings.Join(seriesIds, ",") + `) ORDER BY match_date ASC`)
// 		if err != nil {
// 			log.Println("GetSportsSeriesMatchesCount >> Unable to get series/matches", err.Error())
// 			return output, err
// 		}
// 		defer rows1.Close()
// 		for rows1.Next() {
// 			var seriesId, matchId, matchName, matchDate sql.NullString
// 			err := rows1.Scan(
// 				&seriesId,
// 				&matchId,
// 				&matchName,
// 				&matchDate,
// 			)
// 			if err != nil {
// 				log.Println("GetSportsSeriesMatchesCount >> Unable to get series/matches", err.Error())
// 			}

// 			var singleMatch models.MatchListForMenu
// 			singleMatch.MatchId = matchId.String
// 			singleMatch.MatchName = matchName.String
// 			singleMatch.MatchDate = matchDate.String
// 			singleMatch.SeriesId = seriesId.String
// 			matchMap[seriesId.String] = append(matchMap[seriesId.String], singleMatch)
// 		}
// 	}

// 	for _, sport := range sports {
// 		var seriesRecord []models.SeriesMatchesForMenu
// 		var sportMatchCount int
// 		if record, valid := seriesMap[sport.SportId]; valid {
// 			for _, series := range record {
// 				var seriesMatchCount int

// 				if matches, exit := matchMap[series.SeriesId]; exit {
// 					for _, match := range matches {
// 						if match.SeriesId == series.SeriesId {

// 							seriesMatchCount++
// 						}
// 					}
// 					series.Matches = seriesMatchCount
// 				}

// 				if series.Matches > 0 {
// 					seriesRecord = append(seriesRecord, series)
// 					sportMatchCount += series.Matches
// 				}
// 			}
// 		}

// 		if len(seriesRecord) > 0 {
// 			sport.Matches = sportMatchCount
// 			sport.SeriesList = seriesRecord
// 		}
// 		output = append(output, sport)
// 	}
// 	return output, err
// }

//GetMatchDetail :
// func GetMatchDetail(matchId string, userId int64, userTypeId int64, isShow100Percent int64, marketId string) (models.MatchDetail, bool) {
func GetMatchDetail(matchId string, userId, userTypeId int64) (models.MatchDetail, bool) {
	var matchDetail models.MatchDetail
	var matchContant models.MatchDetailInput
	err := config.MySQL.QueryRow("Select m.sport_id, m.series_id, m.match_id, sp.name AS sport_name, sr.name AS series_name, m.name match_name, m.score_type, m.score_key, case when (sp.is_live_sport = '1') then now() else m.match_date end AS match_date, m.max_stack, m.min_stack, sp.is_live_sport, m.session_min_stack, m.session_max_stack FROM matches m INNER JOIN sports sp on m.sport_id=sp.sport_id INNER JOIN series sr on m.series_id=sr.series_id Where m.match_id= ? AND sp.is_active = '1' AND sr.is_active = '1' AND m.is_active = '1' AND m.is_resulted = '0' ", matchId).Scan(&matchContant.SportId, &matchContant.SeriesId, &matchContant.MatchId, &matchContant.SportName, &matchContant.SeriesName, &matchContant.MatchName, &matchContant.ScoreType, &matchContant.ScoreKey, &matchContant.MatchDate, &matchContant.MaxStack, &matchContant.MinStack, &matchContant.IsLiveSport, &matchContant.SessionMinStack, &matchContant.SessionMaxStack)
	if err != nil && err != sql.ErrNoRows {
		log.Println("GetMatchDetail", err.Error())
		return matchDetail, false
	} else {
		matchDetail.SportId = matchContant.SportId.String
		matchDetail.SeriesId = matchContant.SeriesId.String
		matchDetail.MatchId = matchContant.MatchId.String
		matchDetail.SportName = matchContant.SportName.String
		matchDetail.SeriesName = matchContant.SeriesName.String
		matchDetail.MatchName = matchContant.MatchName.String
		matchDetail.ScoreType = matchContant.ScoreType.String
		matchDetail.ScoreKey = matchContant.ScoreKey.String
		// matchDetail.IsShowLastResult = matchContant.IsShowLastResult.String
		// matchDetail.IsShowTv = matchContant.IsShowTv.String
		matchDetail.MatchDate = matchContant.MatchDate.String
		matchDetail.MaxStack = matchContant.MaxStack.Float64
		matchDetail.MinStack = matchContant.MinStack.Float64
		matchDetail.SessionMaxStack = matchContant.SessionMaxStack.Float64
		matchDetail.SessionMinStack = matchContant.SessionMinStack.Float64
		// matchDetail.IsLiveSport = matchContant.IsLiveSport.String
		// matchDetail.LiveSportTvUrl = matchContant.LiveSportTvUrl.String
		matchDetail.TvUrl = matchContant.TvUrl.String
	}

	if matchDetail.MatchId != "" {
		var marketDataResult, marketsListRedis, _ = GetMarketsDB(matchId, "", "0")
		var isMatchOddsMarket bool
		var marketData []models.Market
		//if marketId != "" {
		// marketsListRedis = []string{}
		for _, market := range marketDataResult {
			// if marketId == market.MarketId && market.Name == "Match Odds" {
			if market.Name == "Match Odds" {
				isMatchOddsMarket = true
			}
		}
		for _, market := range marketDataResult {
			if isMatchOddsMarket && market.Name != "Complete" && market.Name != "Completed Match" && market.Name != "Tie" && market.Name != "Tied Match" {
				// marketsListRedis = append(marketsListRedis, config.RedisMarketKey+market.MarketId)
				marketData = append(marketData, market)
			}
			// else {
			// 	if marketId == market.MarketId {
			// 		marketsListRedis = append(marketsListRedis, "ODDS_"+market.MarketId)
			// 		marketData = append(marketData, market)
			// 	}
			// }
			// if marketId == market.MarketId && market.Name == "Match Odds" {
			// 	isMatchOddsMarket = true
			// }
		}
		//}
		//  else {
		// 	isMatchOddsMarket = true
		// 	marketData = marketDataResult
		// }
		// if matchDetail.IsLiveSport == "1" {
		// 	if len(marketData) > 1 {
		// 		sort.Sort(MarketsSort(marketData))
		// 	}
		// 	if len(marketData) > 0 {
		// 		letestMarket := marketData[0]
		// 		marketData = []models.Market{}
		// 		marketData = append(marketData, letestMarket)
		// 	}
		// }

		marketDataFromRedis, _ := GetSessionAndMarketOdds((strings.Join(marketsListRedis, ",")), "1")
		marketsList := []models.MarketData{}
		fancyList := []models.FancyData{}
		for _, arr := range marketData {
			if arr.IsActive == "1" {

				teamPosition := GetTeamPosition(userId, userTypeId, matchId, arr.MarketId, "0")
				marketStatus := "SUSPENDED"
				var oddsDataParsed models.OddsDataParsedStruct
				var oddsRunnerJsonArr []models.OddsDataParsedRunnerStruct
				var tvUrl, autoTime string
				var cards, results interface{}
				// for ii := 0; ii < len(marketDataFromRedis); ii++ {
				for _, oddsData := range marketDataFromRedis {
					if oddsData.MarketID != "" {
						// oddsData := marketDataFromRedis[ii]
						data, _ := json.Marshal(oddsData)
						json.Unmarshal(data, &oddsDataParsed)
						if oddsDataParsed.MarketId == arr.MarketId {
							if oddsDataParsed.MarketId != "" {
								marketStatus = "SUSPENDED"
								if matchDetail.IsLiveSport == "1" {
									marketStatus = oddsDataParsed.Status
								} else {
									if oddsDataParsed.Status != "" {
										marketStatus = oddsDataParsed.Status
									}
								}
								if oddsDataParsed.SelectionType == "outer" {
									oddsRunnerJsonArr = oddsDataParsed.RunnersOrg
								} else {
									oddsRunnerJsonArr = oddsDataParsed.Runners
								}
								cards = oddsDataParsed.Cards
								tvUrl = oddsDataParsed.TvUrl
								results = oddsDataParsed.Results
								autoTime = oddsDataParsed.AutoTime
							}
							break
						}
					} else {
						oddsRunnerJsonArr = []models.OddsDataParsedRunnerStruct{}
					}
				}
				var runnerJsonArr = arr.RunnerJson
				runnersList := SetRunnerOdds(runnerJsonArr, oddsDataParsed, oddsRunnerJsonArr, teamPosition)

				marketsList = append(marketsList, models.MarketData{
					MarketId:           arr.MarketId,
					Name:               arr.Name,
					IsBookmakerMarket:  arr.IsBookmakerMarket,
					IsVisible:          arr.IsVisible,
					MaxBetLiability:    arr.MaxBetLiability,
					MaxMarketLiability: arr.MaxMarketLiability,
					MaxMarketProfit:    arr.MaxMarketProfit,
					Status:             marketStatus,
					RunnerJson:         runnersList,
					Cards:              cards,
					TvUrl:              tvUrl,
					Results:            results,
					AutoTime:           autoTime,
					MarketStartTime:    arr.MarketStartTime,
					MarketType:         arr.MarketType,
				})
			}
		}

		// var singleDeactiveBet models.DeactiveUsers
		// data.MongoClient.Collection("deactive_market_bet").FindOne(context.TODO(), bson.M{"userid": userId, "matchid": matchId}).Decode(&singleDeactiveBet)
		// if singleDeactiveBet != (models.DeactiveUsers{}) {
		// 	matchDetail.BetLock = true
		// 	singleDeactiveBet = models.DeactiveUsers{}
		// }

		// if isMatchOddsMarket {
		// fancyData, fancyIds := GetFancyByMatchId(matchId, userId, userTypeId)
		var fancyData, fancyIds, _ = GetMarketsDB(matchId, "", "1")
		if len(fancyIds) > 0 {
			fancyDataFromRedis := RedisGetFancyByFancyIds(fancyIds)

			for fi := 0; fi < len(fancyData); fi++ {

				displayMessage := "Result Awaiting"
				var sessionSizeNo, sessionSizeYes, sessionValueNo, sessionValueYes float64 = 0, 0, 0, 0
				if fancyDataFromRedis[fancyData[fi].MarketId].SelectionId != "" {
					displayMessage = fancyDataFromRedis[fancyData[fi].MarketId].GameStatus
					sessionSizeNo = fancyDataFromRedis[fancyData[fi].MarketId].LaySize1
					sessionSizeYes = fancyDataFromRedis[fancyData[fi].MarketId].BackSize1
					sessionValueNo = fancyDataFromRedis[fancyData[fi].MarketId].LayPrice1
					sessionValueYes = fancyDataFromRedis[fancyData[fi].MarketId].BackPrice1
				}
				// var notInIds []interface{}
				// notInIds = append(notInIds, 0)
				if fancyData[fi].IsActive == "0" {
					displayMessage = "Closed"
				}
				fancyList = append(fancyList, models.FancyData{
					FancyId:         fancyData[fi].MarketId,
					Name:            fancyData[fi].Name,
					Active:          fancyData[fi].IsActive,
					SessionSizeNo:   sessionSizeNo,
					SessionSizeYes:  sessionSizeYes,
					SessionValueNo:  sessionValueNo,
					SessionValueYes: sessionValueYes,
					DisplayMessage:  displayMessage,
					// SuperAdminMessage:      fancyData[fi].SuperAdminMessage,
					// ScorePosition:          fancyData[fi].ScorePosition,
					// FancyScorePositionJson: fancyData[fi].FancyScorePositionJson,
				})
			}
			// data.MongoClient.Collection("deactive_fancy_bet").FindOne(context.TODO(), bson.M{"userid": userId, "matchid": matchId}).Decode(&singleDeactiveBet)
			// if singleDeactiveBet != (models.DeactiveUsers{}) {
			// 	matchDetail.FancyLock = true
			// }
			matchDetail.Fancies = fancyList
		}
		// }

		matchDetail.Markets = marketsList

		// matchDetail.MinBetAmount = int64(global.MinimumBetStack)
		return matchDetail, true
	} else {
		return matchDetail, false
	}
}

//GetMarketsDB :
func GetMarketsDB(matchId, marketId, isFancy string) (marketDataResult []models.Market, marketsListRedis []string, status bool) {
	whereStr := ""
	if matchId != "" {
		whereStr += " AND m.match_id ='" + matchId + "'"
	}
	if marketId != "" {
		whereStr += " AND m.market_id ='" + marketId + "'"
	}
	if marketId != "" && matchId != "" {
		whereStr += " AND m.match_id ='" + matchId + "' AND m.market_id ='" + marketId + "'"
	}

	// result, err := data.MongoClient.Collection("markets").Find(context.TODO(), filter)
	// if err != nil {
	// 	fmt.Println("error:", err.Error())
	// 	return marketDataResult, marketsListRedis, false
	// } else {
	// 	for result.Next(context.TODO()) {
	// 		var singleMarket models.MarketDataMongo
	// 		err = result.Decode(&singleMarket)
	// 		var market models.Market
	// 		json.Unmarshal([]byte(singleMarket.MarketData), &market)
	// 		if market.IsActive == "1" {
	// 			marketsListRedis = append(marketsListRedis, "ODDS_"+market.MarketId)
	// 		}
	// 		marketDataResult = append(marketDataResult, market)
	// 	}

	// }

	rows, err := config.MySQL.Query("Select mr.sport_id,mr.sport_name, mr.series_id,mr.series_name, mr.match_id, mr.match_name, mr.market_id, mr.name, mr.is_bookmaker, mr.result_date, mr.market_start_time, mr.is_active, mr.created_at, mr.runner_json FROM markets mr INNER JOIN sports sp ON (mr.sport_id=sp.sport_id AND sp.is_active = ?) INNER JOIN series sr ON (mr.series_id=sr.series_id AND sr.is_active = ?) INNER JOIN matches m ON (mr.match_id=m.match_id AND m.is_active = ?) WHERE 1=1 AND mr.is_active = '1' AND mr.is_fancy=? AND mr.is_result_declared='0' "+whereStr+" ORDER BY mr.created_at", "1", "1", "1", isFancy)
	if err != nil {
		log.Println("GetMarkets >> Unable get markets", err.Error())
		return marketDataResult, marketsListRedis, false
	}
	defer rows.Close()
	for rows.Next() {
		var content models.MarketInput
		err := rows.Scan(
			&content.SportId,
			&content.SportName,
			&content.SeriesId,
			&content.SeriesName,
			&content.MatchId,
			&content.MatchName,
			&content.MarketId,
			&content.Name,
			&content.IsBookmaker,
			&content.ResultDate,
			&content.MarketStartTime,
			&content.IsActive,
			&content.CreatedAt,
			&content.RunnerJson,
		)
		if err != nil {
			log.Println("GetMarket >> Unable get market", err.Error())
		}
		var singleMarket models.Market
		singleMarket.SportId = content.SportId.String
		singleMarket.SportName = content.SportName.String
		singleMarket.SeriesId = content.SeriesId.String
		singleMarket.SeriesName = content.SeriesName.String
		singleMarket.MatchId = content.MatchId.String
		singleMarket.MatchName = content.MatchName.String
		singleMarket.MarketId = content.MarketId.String
		singleMarket.Name = content.Name.String
		singleMarket.IsBookmaker = content.IsBookmaker.String
		singleMarket.ResultDate = content.ResultDate.String
		singleMarket.MarketStartTime = content.MarketStartTime.String
		singleMarket.IsActive = content.IsActive.String
		singleMarket.CreatedAt = content.CreatedAt.String

		if content.RunnerJson.String != "" {
			err = json.Unmarshal([]byte(content.RunnerJson.String), &singleMarket.RunnerJson)
			if err != nil {
				// fmt.Println(err.Error())
				log.Println("GetMarket >> Unable get market", err.Error())
			}
		}

		if singleMarket.IsActive == "1" {
			marketsListRedis = append(marketsListRedis, singleMarket.MarketId)
		}
		marketDataResult = append(marketDataResult, singleMarket)
	}

	return marketDataResult, marketsListRedis, true
}

//SetRunnerOdds :
func SetRunnerOdds(runnerJsonArr []models.RunnerData, oddsDataParsed models.OddsDataParsedStruct, oddsRunnerJsonArr []models.OddsDataParsedRunnerStruct, teamPosition []models.TeamPosition) (runnersList []models.RunnerData) {
	for _, runnerJson := range runnerJsonArr {
		var layList, backList []models.LayBackData
		var runnerStatus interface{}
		runnerStatus = "SUSPENDED"
		if oddsDataParsed.MarketId != "" {
			for _, oddsRunnerJson := range oddsRunnerJsonArr {
				if runnerJson.SelectionId == fmt.Sprintf("%v", oddsRunnerJson.SelectionId) {
					runnerStatus = oddsRunnerJson.Status

					for _, avlToLay := range oddsRunnerJson.Ex.AvailableToLay {
						layList = append(layList, models.LayBackData{
							Size:   fmt.Sprintf("%v", avlToLay.Size),
							Price:  fmt.Sprintf("%v", avlToLay.Price),
							Status: avlToLay.Status,
						})
					}
					for _, avlToBack := range oddsRunnerJson.Ex.AvailableToBack {
						backList = append(backList, models.LayBackData{
							Size:   fmt.Sprintf("%v", avlToBack.Size),
							Price:  fmt.Sprintf("%v", avlToBack.Price),
							Status: avlToBack.Status,
						})
					}
				}
			}
		}

		if len(layList) == 0 {
			layList = runnerJson.Lay
		}

		if len(backList) == 0 {
			backList = runnerJson.Back
		}

		var winLoss float64
		for iii := 0; iii < len(teamPosition); iii++ {
			if runnerJson.SelectionId == teamPosition[iii].SelectionId {
				winLoss = teamPosition[iii].WinLoss
			}
		}

		runnersList = append(runnersList, models.RunnerData{
			SelectionId:   runnerJson.SelectionId,
			Name:          runnerJson.Name,
			SortPriority:  runnerJson.SortPriority,
			LiabilityType: runnerJson.LiabilityType,
			SortName:      runnerJson.SortName,
			Status:        runnerStatus,
			Lay:           layList,
			Back:          backList,
			WinLoss:       winLoss,
		})
	}
	return runnersList
}

//Get from remote server
func RedisGetFancyByFancyIds(fancyIds []string) map[string]models.RedisFancyData {
	//var fancyList map[interface{}]interface{}
	fancyList := map[string]models.RedisFancyData{}

	if len(fancyIds) > 0 {
		// val, err := config.Redis.MGet(fancyIds...).Result()
		_, val := GetSessionAndMarketOdds(strings.Join(fancyIds, ","), "2")
		if len(val) > 0 {
			type onlineFancyStruct struct {
				RunnerName  string      `json:"RunnerName"`
				SelectionId interface{} `json:"SelectionId"`
				LayPrice1   interface{} `json:"LayPrice1"`
				LaySize1    interface{} `json:"LaySize1"`
				BackPrice1  interface{} `json:"BackPrice1"`
				BackSize1   interface{} `json:"BackSize1"`
				GameStatus  string      `json:"GameStatus"`
			}
			var oddsDataParsed onlineFancyStruct

			for i := 0; i < len(fancyIds); i++ {
				// if val[i] != nil {
				oddsData, _ := json.Marshal(val[i])
				json.Unmarshal(oddsData, &oddsDataParsed)

				layPrice1, _ := strconv.ParseFloat(fmt.Sprintf("%v", oddsDataParsed.LayPrice1), 64)
				laySize1, _ := strconv.ParseFloat(fmt.Sprintf("%v", oddsDataParsed.LaySize1), 64)
				backPrice1, _ := strconv.ParseFloat(fmt.Sprintf("%v", oddsDataParsed.BackPrice1), 64)
				backSize1, _ := strconv.ParseFloat(fmt.Sprintf("%v", oddsDataParsed.BackSize1), 64)

				fancyList[fancyIds[i]] = models.RedisFancyData{
					RunnerName:  oddsDataParsed.RunnerName,
					SelectionId: fmt.Sprintf("%v", oddsDataParsed.SelectionId),
					LayPrice1:   layPrice1,
					LaySize1:    laySize1,
					BackPrice1:  backPrice1,
					BackSize1:   backSize1,
					GameStatus:  oddsDataParsed.GameStatus,
				}
				// }
			}
		} else {
			log.Println("RedisGetFancyByFancyIds >> fancy records not available")
		}
	}
	return fancyList
}

//HomePageMatches :
func HomePageMatches(sportId, seriesId string, userId, userTypeId, isInPlayPage int64, isCup string) []models.MarketWatchData {

	var matches []models.MarketWatchData

	var whereStr string
	if sportId != "" {
		whereStr += ` AND m.sport_id = '` + sportId + `'`
	}
	if seriesId != "" {
		whereStr += ` AND m.series_id = '` + seriesId + `'`
	}
	// if isCup == "1" {
	// 	whereStr += ` AND m.is_cup = '1'`
	// } else {
	// 	whereStr += ` AND m.is_cup = '0'`
	// }
	query := `SELECT m.match_id, m.sport_id, m.name AS match_name, m.match_date as match_date, m.tv_url, m.series_name
	FROM matches m
	INNER JOIN sports sp ON m.sport_id=sp.sport_id AND sp.is_active = ?
	INNER JOIN series sr ON m.series_id=sr.series_id AND sr.is_active = ?
	WHERE m.is_active = ? AND m.is_resulted = '0' ` + whereStr + ` ORDER BY m.start_date ASC `
	rows, err := config.MySQL.Query(query, "1", "1", "1")
	if err != nil && err != sql.ErrNoRows {
		log.Println("MarketWatch", err.Error())
		return matches
	}
	defer rows.Close()
	for rows.Next() {
		var matchInput models.MarketWatchDataInput
		var matchOutput models.MarketWatchData

		err := rows.Scan(
			&matchInput.MatchId,
			&matchInput.SportId,
			&matchInput.MatchName,
			&matchInput.MatchDate,
			&matchInput.TvUrl,
			&matchInput.SeriesName,
		)
		if err != nil {
			log.Println("MarketWatch", err.Error())
		}

		matchOutput.MatchId = matchInput.MatchId.String
		matchOutput.MatchName = matchInput.MatchName.String
		matchOutput.MatchDate = matchInput.MatchDate.String
		matchOutput.SportId = matchInput.SportId.String
		matchOutput.SeriesName = matchInput.SeriesName.String

		//-------------------Market Data Addon ---------------
		if matchInput.TvUrl.String != "" {
			matchOutput.IsTv = true
		}
		MarketData, _, _ := GetMarketsDB(matchOutput.MatchId, "", "0")
		isMarketExist := false
		for _, arr := range MarketData {
			if arr.IsActive == "1" {
				isMarketExist = true
				if arr.Name == "Match Odds" || arr.Name == "Winner" {
					matchOutput.MarketId = arr.MarketId
					matchOutput.RunnerJson = arr.RunnerJson
				}
				if arr.Name == "Bookmaker" {
					matchOutput.IsBookmakerMarket = true
					if matchOutput.MarketId == "" {
						matchOutput.MarketId = arr.MarketId
						matchOutput.RunnerJson = arr.RunnerJson
					}
				}
			}
		}
		if isMarketExist {
			if userTypeId == int64(config.USER) || (userTypeId != int64(config.USER) && (seriesId != "" || sportId != "")) {
				fancyCount := GetFanciesCountDB(matchOutput.MatchId, "")
				if fancyCount > 0 {
					matchOutput.IsFancy = true
				}
				if isInPlayPage == 1 {
					marketDataFromRedis := config.RedisGet(config.RedisMarketKey + matchOutput.MarketId)

					var oddsDataParsed models.OddsDataParsedStruct
					var oddsRunnerJsonArr []models.OddsDataParsedRunnerStruct
					if marketDataFromRedis != "" {
						json.Unmarshal([]byte(marketDataFromRedis), &oddsDataParsed)
						if oddsDataParsed.SelectionType == "outer" {
							oddsRunnerJsonArr = oddsDataParsed.RunnersOrg
						} else {
							oddsRunnerJsonArr = oddsDataParsed.Runners
						}

						matchOutput.Inplay = oddsDataParsed.Inplay
						var runnerJsonArr = matchOutput.RunnerJson
						var teamPosition []models.TeamPosition
						runnersList := SetRunnerOdds(runnerJsonArr, oddsDataParsed, oddsRunnerJsonArr, teamPosition)
						matchOutput.RunnerJson = runnersList
					}
					if matchOutput.Inplay {
						matches = append(matches, matchOutput)
					}

				} else {
					marketDataFromRedis := config.RedisGet(config.RedisMarketKey + matchOutput.MarketId)

					var oddsDataParsed models.OddsDataParsedStruct
					var oddsRunnerJsonArr []models.OddsDataParsedRunnerStruct
					if marketDataFromRedis != "" {
						json.Unmarshal([]byte(marketDataFromRedis), &oddsDataParsed)
						if oddsDataParsed.SelectionType == "outer" {
							oddsRunnerJsonArr = oddsDataParsed.RunnersOrg
						} else {
							oddsRunnerJsonArr = oddsDataParsed.Runners
						}

						matchOutput.Inplay = oddsDataParsed.Inplay
						var runnerJsonArr = matchOutput.RunnerJson
						var teamPosition []models.TeamPosition
						runnersList := SetRunnerOdds(runnerJsonArr, oddsDataParsed, oddsRunnerJsonArr, teamPosition)
						matchOutput.RunnerJson = runnersList
					}
					matches = append(matches, matchOutput)
				}
			} else {
				// // get Market bet Count
				// var marketBetCount sql.NullInt64
				// config.MySQL.QueryRow(`SELECT COUNT(b.id) as total_bet from bets_odds b
				// INNER JOIN users u ON (u.id=b.user_id AND FIND_IN_SET(?, u.parent_ids)) WHERE b.match_id = ?`, strconv.Itoa(int(userId)), matchOutput.MatchId).Scan(&marketBetCount)
				// matchOutput.TotalBet = marketBetCount.Int64
				// // get fancy bet Count
				// var fancyBetCount sql.NullInt64
				// config.MySQL.QueryRow(`SELECT COUNT(f.id) as total_bet from bets_fancy f
				// INNER JOIN users u ON (u.id=f.user_id AND FIND_IN_SET(?, u.parent_ids)) WHERE f.match_id = ?`, strconv.Itoa(int(userId)), matchOutput.MatchId).Scan(&fancyBetCount)
				// matchOutput.TotalBet += fancyBetCount.Int64
				// if matchOutput.TotalBet > 0 {
				// 	if matchOutput.MarketId != "" {
				// 		selecationMap := GetMarketPositionForInplay(matchOutput.MarketId, userId, userTypeId)
				// 		if len(selecationMap) > 0 {
				// 			for i, runner := range matchOutput.RunnerJson {
				// 				if position, Valid := selecationMap[runner.SelectionId]; Valid {
				// 					matchOutput.RunnerJson[i].WinLoss = position.WinLoss
				// 				}
				// 			}
				// 		}
				// 	}
				// 	matches = append(matches, matchOutput)
				// }
			}
		}
	}

	return matches
}

//UpdateMatchDetails :
func UpdateMatchDetails(content models.InputMatchSetting) (message string, status bool) {
	var isLiveSports sql.NullString
	config.MySQL.QueryRow("SELECT sp.is_live_sport FROM matches m INNER JOIN sports sp ON sp.sport_id = m.sport_id WHERE match_id = ? LIMIT 1", content.MatchId).Scan(&isLiveSports)

	query := `UPDATE matches SET score_type = ?, score_key = ?, min_stack = ?, max_stack = ?, tv_url = ?, session_min_stack = ?, session_max_stack = ?, bookmaker_min_stack = ?, bookmaker_max_stack = ? WHERE match_id = ?`
	if isLiveSports.String == "1" {
		query = `UPDATE matches SET score_type = ?, score_key = ?, min_stack = ?, max_stack = ?, live_sport_tv_url = ?, session_min_stack = ?, session_max_stack = ?, bookmaker_min_stack = ?, bookmaker_max_stack = ? WHERE match_id = ?`
	}
	stmt, err := config.MySQL.Prepare(query)
	if err != nil {
		return "UpdateMatchDetails >> Insert error", false
	}
	defer stmt.Close()
	_, err = stmt.Exec(content.ScoreType, content.ScoreKey, content.MinStack, content.MaxStack, content.TvUrl, content.SessionMinStack, content.SessionMaxStack, content.BookmakerMinStack, content.BookmakerMaxStack, content.MatchId)
	if err != nil {
		return "UpdateMatchDetails >> Insert error", false
	}
	return "Match details updated successfully", true
}

//GetUpcomingFixtures :
func GetUpcomingFixtures() []models.UpcomingFixtures {
	var matches []models.UpcomingFixtures

	query := `SELECT m.match_id, m.sport_id, m.name AS match_name, m.start_date as match_date, m.series_name, m.series_id
	FROM matches m
	INNER JOIN sports sp ON m.sport_id=sp.sport_id AND sp.is_active = ?
	INNER JOIN series sr ON m.series_id=sr.series_id AND sr.is_active = ?
	WHERE m.is_active = ? AND m.is_resulted = ? ORDER BY m.start_date ASC `
	rows, err := config.MySQL.Query(query, "1", "1", "1", "0")
	if err != nil && err != sql.ErrNoRows {
		log.Println("GetUpcomingFixtures", err.Error())
		return matches
	}
	defer rows.Close()
	for rows.Next() {
		var matchInput models.MarketWatchDataInput
		var matchOutput models.UpcomingFixtures

		err := rows.Scan(
			&matchInput.MatchId,
			&matchInput.SportId,
			&matchInput.MatchName,
			&matchInput.MatchDate,
			&matchInput.SeriesName,
			&matchInput.SeriesId,
		)
		if err != nil {
			log.Println("GetUpcomingFixtures", err.Error())
		}

		matchOutput.MatchId = matchInput.MatchId.String
		matchOutput.MatchName = matchInput.MatchName.String
		matchOutput.MatchDate = matchInput.MatchDate.String
		matchOutput.SportId = matchInput.SportId.String
		matchOutput.SeriesName = matchInput.SeriesName.String
		matchOutput.SeriesId = matchInput.SeriesId.String

		matches = append(matches, matchOutput)
	}

	return matches
}

//GetMatchContent :
func GetMatchContent(matchId string) (models.MatchDetail, error) {
	var matchDetail models.MatchDetail
	var matchContant models.MatchDetailInput
	err := config.MySQL.QueryRow(`SELECT sport_id, series_id, match_id, name match_name, liability_type, max_stack FROM matches WHERE match_id= ? AND is_active = '1'`, matchId).Scan(&matchContant.SportId, &matchContant.SeriesId, &matchContant.MatchId, &matchContant.MatchName, &matchContant.LiabilityType, &matchContant.MaxStack)
	if err != nil {
		log.Println("GetMatchContent", err.Error())
		return matchDetail, err
	} else {
		matchDetail.SportId = matchContant.SportId.String
		matchDetail.SeriesId = matchContant.SeriesId.String
		matchDetail.MatchId = matchContant.MatchId.String
		matchDetail.MatchName = matchContant.MatchName.String
		matchDetail.LiabilityType = matchContant.LiabilityType.String
		matchDetail.MaxStack = matchContant.MaxStack.Float64
	}
	return matchDetail, nil
}

// CreateMatchesAuto :
func CreateMatchesAuto(seriesId string, matchDataInput []models.Match) (bool, string) {

	var seriesID, sportID, sportName, seriesName sql.NullString
	if len(matchDataInput) > 0 {
		err := config.MySQL.QueryRow("Select series_id, sport_id, sport_name, name FROM series Where series_id = ? AND is_active = ? ", seriesId, "1").Scan(&seriesID, &sportID, &sportName, &seriesName)
		if err != nil {
			return false, "Unable to create Match"
		} else {
			for _, matchData := range matchDataInput {
				if seriesID.String != "" {
					var matchCount sql.NullInt64
					err := config.MySQL.QueryRow("Select count(id) AS total_count FROM matches Where match_id = ? ", matchData.MatchId).Scan(&matchCount)
					if err != nil {
						return false, "Unable to create Match"
					}
					if matchCount.Int64 == 0 {
						insertStr := `INSERT INTO matches (sport_id,sport_name,series_id,series_name,match_id, name, match_date, start_date, score_type, score_key, is_active, liability_type, max_stack, min_stack, session_min_stack, session_max_stack, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW(),NOW())`
						stmt, err := config.MySQL.Prepare(insertStr)
						if err != nil {
							return false, "Unable to create Match"
						}
						_, err = stmt.Exec(sportID, sportName.String, seriesID.String, seriesName.String, matchData.MatchId, matchData.Name, matchData.MatchDate, matchData.StartDate, matchData.ScoreType, matchData.ScoreKey, matchData.IsActive, matchData.LiabilityType, matchData.MaxStack, matchData.MinStack, matchData.SessionMinStack, matchData.SessionMaxStack)
						if err != nil {
							return false, "Unable to create Match"
						}

					} else {
						return false, "Match Already Exists"
					}
				}
			}
			return true, "Matches Created Successfully"
		}
	}

	return false, "Sport/Series Not Available"
}

//UpdateScoreboardMatchKey :
func UpdateScoreboardMatchKey(content models.ScoreboardMatchData) (status bool) {

	query := `UPDATE matches SET score_key = ? WHERE start_date = ? AND (name LIKE '%` + content.Teams[0].Name + `%' AND name LIKE '%` + content.Teams[1].Name + `%') `

	stmt, err := config.MySQL.Prepare(query)
	if err != nil {
		return false
	}
	defer stmt.Close()
	_, err = stmt.Exec(content.MatchId, content.MatchDate)
	if err != nil {
		return false
	}
	return true
}
