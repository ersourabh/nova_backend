package services

import (
	"encoding/json"
	"log"
	"novaexchbackend/api/config"
	"novaexchbackend/api/models"
	"novaexchbackend/api/util"
	"strconv"
	"strings"
	"time"
)

// GetSports:
func GetSports() ([]models.SportsDB, error) {
	var sportsList []models.SportsDB

	content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineSportURL)
	if statusCode != 200 || err != nil {
		log.Println("GetSports >> statuscode >> ", statusCode, ": error >> ", err.Error())
	}

	err = json.Unmarshal(content, &sportsList)
	if err != nil {
		return sportsList, err
	}

	sportsDB, err := GetSportsDB()
	if err != nil {
		log.Println("GetSports - Unable to get sports from DB")
	}

	for i, v := range sportsList {
		if _, ok := sportsDB[v.SportID]; ok {
			sportsList[i].Added = 1
		}
	}
	return sportsList, nil
}

// GetSportsDB :
func GetSportsDB() (map[string]string, error) {
	finalResult := map[string]string{}

	sql := `SELECT sport_id,name FROM sports`
	rows, err := config.MySQL.Query(sql)
	if err != nil {
		log.Println("GetSportsDB", err.Error())
		return finalResult, err
	}
	defer rows.Close()
	for rows.Next() {
		var content models.SportsDBInput
		err := rows.Scan(
			&content.SportID,
			&content.SportName,
		)
		if err != nil {
			log.Println("GetSportsDB", err.Error())
		}
		finalResult[strconv.Itoa(int(content.SportID.Int64))] = content.SportName.String
	}
	return finalResult, nil
}

// GetSportSeries:
func GetSportSeries(sportID string) ([]models.SportSeries, error) {
	var sportSeries []models.SportSeries

	content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineSeriesURL + sportID)
	if statusCode != 200 || err != nil {
		log.Println("GetSportSeries >> statuscode >> ", statusCode, ": error >> ", err.Error())
	}

	// content, err := ioutil.ReadFile("sports.json")
	// if err != nil {
	// 	fmt.Println(err)
	// }

	err = json.Unmarshal(content, &sportSeries)
	if err != nil {
		return sportSeries, err
	}
	sportsSeriesDB, err := GetSportSeriesDB(sportID)
	if err != nil {
		log.Println("GetSportSeries - Unable to get series from DB")
	}

	for i, v := range sportSeries {
		if _, ok := sportsSeriesDB[v.Competition.ID]; ok {
			sportSeries[i].Added = 1
		}
	}
	return sportSeries, nil
}

// GetSportSeriesDB :
func GetSportSeriesDB(sportID string) (map[string]string, error) {
	finalResult := map[string]string{}

	sql := `SELECT series_id,name FROM series WHERE sport_id = ?`
	rows, err := config.MySQL.Query(sql, sportID)
	if err != nil {
		log.Println("GetSportSeriesDB", err.Error())
		return finalResult, err
	}
	defer rows.Close()
	for rows.Next() {
		var content models.SportSeriesDBInput

		err := rows.Scan(
			&content.SeriesID,
			&content.SeriesName,
		)
		if err != nil {
			log.Println("GetSportSeriesDB", err.Error())
		}
		finalResult[strconv.Itoa(int(content.SeriesID.Int64))] = content.SeriesName.String
	}
	return finalResult, nil
}

// GetSportSeriesMatches:
func GetSportSeriesMatches(sportID, seriesID string) ([]models.SportSeriesMatch, error) {
	var sportSeriesMatches []models.SportSeriesMatch

	content, statusCode, err := util.RequestAPIDataClientURL(strings.Replace(config.OnlineMatchURL, "matchid", sportID, -1) + seriesID)
	if statusCode != 200 || err != nil {
		log.Println("GetSportSeriesMatches >> statuscode >> ", statusCode, ": error >> ", err.Error())
	}

	err = json.Unmarshal(content, &sportSeriesMatches)
	if err != nil {
		return sportSeriesMatches, err
	}

	sportsSeriesMatchesDB, err := GetSportSeriesMatchesDB(sportID, seriesID)
	if err != nil {
		log.Println("GetSportSeriesMatches - Unable to get matches from DB")
	}

	for i, v := range sportSeriesMatches {
		if _, ok := sportsSeriesMatchesDB[v.Event.ID]; ok {
			sportSeriesMatches[i].Added = 1
		}
	}

	return sportSeriesMatches, nil
}

// GetSportSeriesMatchesDB :
func GetSportSeriesMatchesDB(sportID, seriesID string) (map[string]string, error) {
	finalResult := map[string]string{}

	sql := `SELECT match_id,name FROM matches WHERE sport_id = ? AND series_id = ?`
	rows, err := config.MySQL.Query(sql, sportID, seriesID)
	if err != nil {
		log.Println("GetSportSeriesMatchesDB", err.Error())
		return finalResult, err
	}

	defer rows.Close()
	for rows.Next() {
		var content models.SportSeriesMatchesDBInput

		err := rows.Scan(
			&content.MatchID,
			&content.MatchName,
		)
		if err != nil {
			log.Println("GetSportSeriesMatchesDB", err.Error())
		}
		finalResult[content.MatchID.String] = content.MatchName.String
	}
	return finalResult, nil
}

// GetSportSeriesMatchMarkets:
func GetSportSeriesMatchMarkets(matchID string) ([]models.SportSeriesMatchMarket, error) {
	sportSeriesMatchMarkets := []models.SportSeriesMatchMarket{}

	content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineMarketURL + matchID)
	if statusCode != 200 || err != nil {
		log.Println("GetSportSeriesMatchMarkets >> statuscode >> ", statusCode, ": error >> ", err.Error())
	}

	err = json.Unmarshal(content, &sportSeriesMatchMarkets)
	if err != nil {
		return sportSeriesMatchMarkets, err
	}

	sportsSeriesMatchMarketsDB, err := GetSportSeriesMatchMarketsDB(matchID)
	if err != nil {
		log.Println("GetSportSeriesMatchesResponse - Unable to get matches from DB")
	}

	for i, v := range sportSeriesMatchMarkets {
		if _, ok := sportsSeriesMatchMarketsDB[v.MarketID]; ok {
			sportSeriesMatchMarkets[i].Added = 1
		}
	}
	return sportSeriesMatchMarkets, nil
}

// GetSportSeriesMatchMarketsDB :
func GetSportSeriesMatchMarketsDB(matchID string) (map[string]string, error) {
	finalResult := map[string]string{}

	sql := `SELECT market_id,name FROM markets WHERE match_id = ? AND is_result_declared = '0' AND is_active = '1'`
	rows, err := config.MySQL.Query(sql, matchID)
	if err != nil {
		log.Println("GetSportSeriesMatchMarketsDB", err.Error())
		return finalResult, err
	}
	defer rows.Close()
	for rows.Next() {
		var content models.SportSeriesMatchMarketDBInput

		err := rows.Scan(
			&content.MarketID,
			&content.MarketName,
		)
		if err != nil {
			log.Println("GetSportSeriesMatchMarketsDB", err.Error())
		}
		finalResult[content.MarketID.String] = content.MarketName.String
	}
	return finalResult, nil
}

// GetSportSeriesMatchSessions:
func GetSportSeriesMatchSessions(matchID string) ([]models.RemoteRedisFancy, error) {
	sportSeriesMatchSessions := []models.RemoteRedisFancy{}

	content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineSessionURL + matchID)
	if statusCode != 200 {
		log.Println("GetSportSeriesMatchSessions >> statuscode >> ", statusCode)
		return sportSeriesMatchSessions, err
	}
	if err != nil {
		log.Println("GetSportSeriesMatchSessions >> statuscode >> ", statusCode, ": error >> ", err.Error())
		return sportSeriesMatchSessions, err
	}

	err = json.Unmarshal(content, &sportSeriesMatchSessions)
	if err != nil {
		log.Println(err.Error())
		return sportSeriesMatchSessions, err
	}

	sportsSeriesMatchMarketsDB, err := GetSportSeriesMatchMarketsDB(matchID)
	if err != nil {
		log.Println("GetSportSeriesMatchSessions - Unable to get matches from DB")
		return sportSeriesMatchSessions, err
	}

	for i, v := range sportSeriesMatchSessions {
		if _, ok := sportsSeriesMatchMarketsDB[matchID+"_"+v.SelectionId]; ok {
			sportSeriesMatchSessions[i].Added = 1
		}
	}
	return sportSeriesMatchSessions, nil
}

// GetSportSeriesMatchBookmakerMarkets:
func GetSportSeriesMatchBookmakerMarkets(matchID string) ([]models.SportSeriesMatchMarket, error) {
	sportSeriesMatchMarkets := []models.SportSeriesMatchMarket{}

	content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineBookmakerURL + matchID)
	if statusCode != 200 || err != nil {
		log.Println("GetSportSeriesMatchBookmakerMarkets >> statuscode >> ", statusCode, ": error >> ", err.Error())
	}

	err = json.Unmarshal(content, &sportSeriesMatchMarkets)
	if err != nil {
		return sportSeriesMatchMarkets, err
	}

	sportsSeriesMatchMarketsDB, err := GetSportSeriesMatchMarketsDB(matchID)
	if err != nil {
		log.Println("GetSportSeriesMatchBookmakerMarkets - Unable to get matches from DB")
	}

	for i, v := range sportSeriesMatchMarkets {
		if _, ok := sportsSeriesMatchMarketsDB[v.MarketID]; ok {
			sportSeriesMatchMarkets[i].Added = 1
		}
	}
	return sportSeriesMatchMarkets, nil
}

// GetActiveMatchIds :
func GetActiveMatchIds() []string {
	var matchIds []string

	sql := `SELECT match_id FROM matches WHERE is_active = '1' AND is_resulted = '0'`
	rows, err := config.MySQL.Query(sql)
	if err != nil {
		log.Println("GetActiveMatchIds", err.Error())
		return matchIds
	}

	defer rows.Close()
	for rows.Next() {
		var match models.MatchInput
		err := rows.Scan(
			&match.MatchId,
		)
		if err != nil {
			log.Println("GetActiveMatchIds >> Unable get active matches", err.Error())
		}
		matchIds = append(matchIds, match.MatchId.String)
	}

	return matchIds
}

// GetActiveMarketIds :
func GetActiveMarketIds(isBookmaker string) []string {
	var marketIds []string
	var marketIdsStr, isBookmakerStr string
	var count int

	if isBookmaker == "1" {
		isBookmakerStr = ` AND is_bookmaker = '1'`
	} else {
		isBookmakerStr = ` AND is_bookmaker = '0'`
	}

	sql := `SELECT market_id FROM markets WHERE is_active = '1' AND is_result_declared = '0' AND is_fancy = '0' ` + isBookmakerStr
	rows, err := config.MySQL.Query(sql)
	if err != nil {
		log.Println("GetActiveMarketIds", err.Error())
		return marketIds
	}

	defer rows.Close()
	for rows.Next() {
		var markets models.MarketInput

		err := rows.Scan(
			&markets.MarketId,
		)
		if err != nil {
			log.Println("GetActiveMarketIds >> Unable get active markets", err.Error())
		}
		count++
		if marketIdsStr == "" {
			marketIdsStr = markets.MarketId.String
		} else {
			marketIdsStr += "," + markets.MarketId.String
		}
		if count > 100 {
			marketIds = append(marketIds, marketIdsStr)
			count = 0
			marketIdsStr = ""
		}
	}
	if marketIdsStr != "" {
		marketIds = append(marketIds, marketIdsStr)
	}
	return marketIds
}

func FetchOnlineMarketsSessionsBookmakers() {
	go GetOnlineMarketOdds()
	go GetOnlineSessionOdds()
	go GetOnlineBookmakerMarketOdds()
}

// GetOnlineMarketOdds :
func GetOnlineMarketOdds() {
	marketIds := GetActiveMarketIds("0")
	// fmt.Println(marketIds)
	// for _, markets := range marketIds {
	// fmt.Println(markets)
	// fmt.Println(config.ONLINEMARKETODDSURL + markets)
	content, statusCode, err := util.RequestAPIDataClientURL(config.ONLINEMARKETODDSURL + strings.Join(marketIds, ","))
	if statusCode != 200 || err != nil {
		if err != nil {
			// fmt.Println(err.Error())
			log.Println("GetOnlineMarketOdds >> statuscode >> ", statusCode, ": error >> ", err.Error())
		} else {
			// fmt.Println(statusCode)
			log.Println("GetOnlineMarketOdds >> statuscode >> ", statusCode)
		}
	}

	var marketOdds []models.MarketOdds
	err = json.Unmarshal(content, &marketOdds)
	if err != nil {
		// fmt.Println(err.Error())
		log.Println("GetOnlineMarketOdds >>", err.Error())
	}
	// }

	go SetMarketOddsRedis(marketOdds)
}

// SetMarketOddsRedis :
func SetMarketOddsRedis(marketOdds []models.MarketOdds) {
	for _, singleMarketOdds := range marketOdds {
		// fmt.Println(singleMarketOdds)
		redisKey := config.RedisMarketKey + singleMarketOdds.MarketID
		// strValue, _ := json.Marshal(singleMarketOdds)
		// config.RedisSetExp(redisKey, string(strValue), 432000*time.Second)
		config.RedisSetExp(redisKey, singleMarketOdds, 432000*time.Second)
	}
}

// GetOnlineSessionOdds :
func GetOnlineSessionOdds() {
	matchIds := GetActiveMatchIds()
	// fmt.Println(marketIds)
	for _, matchId := range matchIds {
		content, statusCode, err := util.RequestAPIDataClientURL(config.OnlineSessionURL + matchId)
		if statusCode != 200 || err != nil {
			if err != nil && statusCode != 409 {
				// fmt.Println(err.Error())
				log.Println("GetOnlineSessionOdds >> statuscode >> ", statusCode, ": error >> ", err.Error())
			} else if statusCode != 409 {
				// fmt.Println(statusCode)
				log.Println("GetOnlineSessionOdds >> statuscode >> ", statusCode)
			}
		}
		// fmt.Println(string(content))

		var sessionOdds []models.RemoteRedisFancy
		err = json.Unmarshal(content, &sessionOdds)
		if err != nil && statusCode != 409 {
			// fmt.Println(err.Error())
			log.Println("GetOnlineSessionOdds >>", err.Error())
		}
		go SetSessionOddsRedis(matchId, sessionOdds)
	}
}

// SetSessionOddsRedis :
func SetSessionOddsRedis(matchId string, sessionOdds []models.RemoteRedisFancy) {
	for _, singleSessionOdds := range sessionOdds {
		// fmt.Println(singleMarketOdds)
		redisKey := config.RedisSessionKey + matchId + "_" + singleSessionOdds.SelectionId
		// strValue, _ := json.Marshal(singleMarketOdds)
		// config.RedisSetExp(redisKey, string(strValue), 432000*time.Second)
		config.RedisSetExp(redisKey, singleSessionOdds, 432000*time.Second)
	}
}

// GetOnlineBookmakerMarketOdds :
func GetOnlineBookmakerMarketOdds() {
	marketIds := GetActiveMarketIds("1")
	// fmt.Println(marketIds)
	// for _, markets := range marketIds {
	// fmt.Println(markets)
	// fmt.Println(config.ONLINEBOOKMAKERMARKETODDSURL + markets)
	content, statusCode, err := util.RequestAPIDataClientURL(config.ONLINEBOOKMAKERMARKETODDSURL + strings.Join(marketIds, ","))
	if statusCode != 200 || err != nil {
		if err != nil {
			// fmt.Println(err.Error())
			log.Println("GetOnlineBookmakerMarketOdds >> statuscode >> ", statusCode, ": error >> ", err.Error())
		} else {
			// fmt.Println(statusCode)
			log.Println("GetOnlineBookmakerMarketOdds >> statuscode >> ", statusCode)
		}
	}
	// fmt.Println(string(content))

	var marketOdds []models.MarketOdds
	err = json.Unmarshal(content, &marketOdds)
	if err != nil {
		// fmt.Println(err.Error())
		log.Println("GetOnlineBookmakerMarketOdds >>", err.Error())
	}
	go SetBookmakerMarketOddsRedis(marketOdds)
	// }
}

// SetBookmakerMarketOddsRedis :
func SetBookmakerMarketOddsRedis(marketOdds []models.MarketOdds) {
	for _, singleMarketOdds := range marketOdds {
		// fmt.Println(singleMarketOdds)
		redisKey := config.RedisMarketKey + singleMarketOdds.MarketID
		// strValue, _ := json.Marshal(singleMarketOdds)
		// config.RedisSetExp(redisKey, string(strValue), 432000*time.Second)
		config.RedisSetExp(redisKey, singleMarketOdds, 432000*time.Second)
	}
}

/*
// GetMarketsOddsRedis :
func GetMarketsOddsRedis(markets string) []models.MarketOdds {
	marketIds := strings.Split(markets, ",")
	var marketOdds []models.MarketOdds

	for _, market := range marketIds {
		var marketOdd models.MarketOdds
		content := config.RedisGet(config.RedisMarketKey + market)

		err := json.Unmarshal([]byte(content), &marketOdd)
		if err != nil {
			log.Println("GetMarketsOddsRedis >>", err.Error())
		}
		marketOdds = append(marketOdds, marketOdd)
	}
	return marketOdds
}
*/
