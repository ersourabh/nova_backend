package services

import (
	"database/sql"
	"log"
	"novaexchbackend/api/config"
	"novaexchbackend/api/models"
	// "go.mongodb.org/mongo-driver/bson"
)

// CreateSeries :
func CreateSeries(content models.Series) (message string, status bool) {
	var sportID sql.NullString
	err := config.MySQL.QueryRow(`SELECT sport_id,name FROM sports WHERE sport_id = ? AND is_active = ?`, content.SportId, "1").Scan(&sportID, &content.SportName)
	if err != nil && err != sql.ErrNoRows {
		return "Unable to create Series", false
	}
	if sportID.String != "" {
		content.IsActive = "1"
		if content.SeriesId != "" {
			query := `INSERT INTO series (is_active,name,series_id,sport_id,sport_name,created_at,updated_at) VALUES (?,?,?,?,?,NOW(),NOW())`
			stmt, err := config.MySQL.Prepare(query)
			if err != nil {
				return "Unable to create Series", false
			}
			defer stmt.Close()
			result, err := stmt.Exec(content.IsActive, content.Name, content.SeriesId, content.SportId, content.SportName)
			if err != nil {
				return "Unable to create Series", false
			}
			if result != nil {
				rowCount, _ := result.RowsAffected()
				if rowCount > 0 {
					message = "Series Created Successfully"
					status = true
				} else {
					message = "Series Already Exist"
					status = false
				}
			}
		}
	} else {
		message = "Sport Not Available"
		status = false
	}
	return message, status
}

// GetSeries :
func GetSeries(sportId string, isActive string, search string) (contents []models.Series) {

	var whereStr string
	if sportId != "" || isActive != "" || search != "" {
		whereStr = " WHERE "
	}
	if sportId != "" {
		whereStr += ` sport_id = ` + sportId
	}
	if isActive != "" {
		if sportId != "" {
			whereStr += " AND "
		}
		whereStr += ` is_active = ` + isActive
	}
	if search != "" {
		if sportId != "" || isActive != "" {
			whereStr += " AND "
		}
		whereStr += ` name LIKE '%` + search + `%' `
	}
	query := `SELECT is_active,name,series_id,sport_id FROM series ` + whereStr + ` ORDER BY name`
	rows, err := config.MySQL.Query(query)
	if err != nil && err != sql.ErrNoRows {
		log.Println("GetSeries", err.Error())
	}
	defer rows.Close()
	for rows.Next() {
		var content models.SeriesInput
		var contentOutput models.Series
		err := rows.Scan(
			&content.IsActive,
			&content.Name,
			&content.SeriesId,
			&content.SportId,
		)
		if err != nil {
			log.Println("GetSeries", err.Error())
		}

		contentOutput.IsActive = content.IsActive.String
		contentOutput.Name = content.Name.String
		contentOutput.SeriesId = content.SeriesId.String
		contentOutput.SportId = content.SportId.String
		contents = append(contents, contentOutput)
	}

	return contents
}

// UpdateSeriesStatus :
func UpdateSeriesStatus(content models.Series) string {
	query := `UPDATE series SET is_active =? WHERE series_id =?`
	stmt, err := config.MySQL.Prepare(query)
	if err != nil {
		log.Println("UpdateSeriesStatus", err.Error())
	}
	defer stmt.Close()
	_, err = stmt.Exec(content.IsActive, content.SeriesId)
	if err != nil {
		log.Println("UpdateSeriesStatus", err.Error())
	}
	if content.IsActive == "0" {
		return "Series Deactivated Successfully"
	} else {
		return "Series Activated Successfully"
	}
}
