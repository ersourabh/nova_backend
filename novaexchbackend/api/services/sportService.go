package services

import (
	"database/sql"
	"log"
	"novaexchbackend/api/config"
	"novaexchbackend/api/models"
)

type Sport struct {
	SportId  string
	IsActive string
}

// GetSportsList :
func GetSportsList(isActive string) (finalResult []models.Sport) {
	sqlStr := "SELECT is_active, name,sport_id,is_live_sport FROM sports"
	whereStr := ""
	if isActive == "0" || isActive == "1" {
		whereStr += " Where is_active = '" + isActive + "'"
	}

	whereStr += " ORDER BY sport_id"
	rows, err := config.MySQL.Query(sqlStr + whereStr)
	if err != nil && err != sql.ErrNoRows {
		log.Println("GetSportsList", err.Error())
		return []models.Sport{}
	}
	defer rows.Close()
	for rows.Next() {
		var content models.SportInput
		err := rows.Scan(
			&content.IsActive,
			&content.Name,
			&content.SportId,
			&content.IsLiveSport,
		)
		if err != nil {
			log.Println("GetSportsList", err.Error())
		}

		var singleSport models.Sport
		singleSport.IsActive = content.IsActive.String
		singleSport.Name = content.Name.String
		singleSport.SportId = content.SportId.String
		singleSport.IsLiveSport = content.IsLiveSport.String
		finalResult = append(finalResult, singleSport)
	}
	return finalResult
}

// UpdateSportStatus :
func UpdateSportStatus(content models.Sport) string {
	// isSuperAdminDeactive := "0"
	// if userTypeId == 1 {
	// 	if content.IsActive == "0" {
	// 		isSuperAdminDeactive = "1"
	// 	}
	// }
	updateStr := "UPDATE sports SET is_active = ? WHERE sport_id = ?"
	stmt, err := config.MySQL.Prepare(updateStr)
	if err != nil {
		log.Println("UpdateSportStatus >> update failed for status", err.Error())
		return "Unable to update status"
	}
	_, err = stmt.Exec(content.IsActive, content.SportId)
	if err != nil {
		log.Println("UpdateSportStatus >> update failed for status", err.Error())
		return "Unable to update status"
	}
	if content.IsActive == "0" {
		return "Sport Deactivated Successfully"
	} else {
		return "Sport Activated Successfully"
	}

}

// //GetSportBySportId :
// func GetSportBySportId(sportId string) (result models.Sport) {
// 	var isLiveSport sql.NullString
// 	err := config.MySQL.QueryRow("SELECT is_live_sport FROM sports Where sport_id=?", sportId).Scan(&isLiveSport)
// 	if err != nil && err != sql.ErrNoRows {
// 		log.Println("GetSportBySportId", err.Error())
// 		return result
// 	}
// 	result.IsLiveSport = isLiveSport.String
// 	return result
// }

// //GetLiveSportsDetails :
// func GetLiveSportsDetails() (returnData models.Sport) {
// 	var sportsDetailInput models.SportInput
// 	err := config.MySQL.QueryRow("SELECT sport_id, name FROM sports Where is_live_sport=?", "1").Scan(&sportsDetailInput.SportId, &sportsDetailInput.Name)
// 	if err != nil && err != sql.ErrNoRows {
// 		log.Println("GetLiveSportsId", err.Error())
// 		return returnData
// 	}
// 	returnData.SportId = sportsDetailInput.SportId.String
// 	returnData.Name = sportsDetailInput.Name.String
// 	return returnData
// }

// //CheckIsLiveSportByMatchId :
// func CheckIsLiveSportByMatchId(matchId string) (result models.IsLiveSportByMatchId) {
// 	var isLiveSport sql.NullString
// 	err := config.MySQL.QueryRow("Select sp.is_live_sport as is_live_sport FROM matches m INNER JOIN sports sp on sp.sport_id = m.sport_id Where m.match_id = ?", matchId).Scan(&isLiveSport)
// 	if err != nil {
// 		log.Println("CheckIsLiveSportByMatchId", err.Error())
// 	}
// 	return result
// }
