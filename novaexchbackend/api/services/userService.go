package services

import (
	"database/sql"
	"fmt"
	"log"
	"novaexchbackend/api/config"
	"novaexchbackend/api/global"
	"novaexchbackend/api/models"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

var (
	hmacSecret = []byte{97, 48, 97, 50, 97, 98, 105, 49, 99, 102, 83, 53, 57, 98, 52, 54, 97, 102, 99, 12, 12, 13, 56, 34, 23, 16, 78, 67, 54, 34, 32, 21}
)

// VerifyUserLogin :
func VerifyUserLogin(userName, password string) (result models.User, message string, status bool) {
	var content models.UserInput

	err := config.MySQL.QueryRow("SELECT id, user_type_id, parent_id, parent_ids, user_name, name, password, city, mobile FROM users WHERE user_name = ? LIMIT 1", userName).Scan(&content.Id, &content.UserTypeId, &content.ParentId, &content.ParentIds, &content.UserName, &content.Name, &content.Password, &content.City, &content.Mobile)

	if err != nil {
		message = "Invalid Username Or Password"
		status = false
		return models.User{}, message, status
	} else {
		result.Id = content.Id.Int64
		result.ParentId = content.ParentId.Int64
		result.ParentIds = content.ParentIds.String
		result.UserTypeId = content.UserTypeId.Int64
		result.Name = content.Name.String
		result.UserName = content.UserName.String
		result.Password = content.Password.String
		result.City = content.City.String
		result.Mobile = content.Mobile.String

		err := bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(password))
		if err == nil {
			message = "Login Successfully"
			status = true
			return result, message, status
		} else {
			message = "Invalid Username Or Password"
			status = false
			return models.User{}, message, status
		}
	}
}

// EncodeToken :
func EncodeToken(data models.TokenData) string {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user_id":         data.UserId,
		"user_type_id":    data.UserTypeId,
		"parent_id":       data.ParentId,
		"name":            data.Name,
		"user_name":       data.UserName,
		"login_date_time": data.LoginDateTime,
		"ip_address":      data.IpAddress,
		"mobile":          data.Mobile,
		"nbf":             time.Date(2015, 10, 10, 12, 0, 0, 0, time.UTC).Unix(),
		"exp":             time.Now().Add((time.Second * 60 * 60 * 24) * 365).Unix(),
	})

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString(hmacSecret)

	if err != nil {
		log.Println("EncodeToken", err.Error())
	}
	return tokenString
}

// ExistUser :
func ExistUser(UserName string) int64 {
	var count sql.NullInt64
	err := config.MySQL.QueryRow("Select count(id) AS total_count from users WHERE user_name = ? ", UserName).Scan(&count)
	if err != nil {
		log.Println("ExistUser", err.Error())
	}

	return count.Int64
}

// GetAdminId :
func GetAdminId() int64 {
	var id sql.NullInt64
	err := config.MySQL.QueryRow("Select id AS default_admin from main_settings").Scan(&id)
	if err != nil {
		log.Println("ExistUser", err.Error())
	}

	return id.Int64
}

// CreateUser :
func CreateUser(input, parentData models.User) (message string, status bool) {
	tx, err := config.MySQL.Begin()
	if err != nil {
		log.Println("CreateUser >> Insertion failed for Create user", err.Error())
		tx.Rollback()
		message = "Cannot add user, try again"
		status = false
		return message, status
	}
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(input.Password), 8)
	input.Password = string(hashedPassword)

	// hashedPassword2, _ := bcrypt.GenerateFromPassword([]byte(global.RandNumString(6)), 8)
	// hashedPassword2, _ := bcrypt.GenerateFromPassword([]byte(strconv.Itoa(12345)), 8)
	// transactionPassword := hashedPassword2
	sql := "INSERT INTO users (parent_id,parent_ids,parent_name,user_type_id,user_name,name,password,mobile,city,remark,available_chip,total_chip,credited_chip,partnership,created_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())"

	stmt, err := tx.Prepare(sql)
	if err != nil {
		tx.Rollback()
		log.Println("CreateUser >> Insertion failed for Create user", err.Error())
		message = "Cannot add user, try again"
		status = false
		return message, status
	}
	defer stmt.Close()
	res, err := stmt.Exec(input.ParentId, input.ParentIds, input.ParentName, input.UserTypeId, input.UserName, input.Name, input.Password, input.Mobile, input.City, input.Remark, input.AvailableChip, input.AvailableChip, input.AvailableChip, input.Partnership)
	if err != nil {
		tx.Rollback()
		log.Println("CreateUser >> Insertion failed for Create user", err.Error())
		message = "Cannot add user, try again"
		status = false
		return message, status
	} else {
		// Account statment entry for user
		var userCrDeDescription, parentCrDeDescription string
		if input.CrDe == 1 {
			userCrDeDescription = "Amount Credited"
			parentCrDeDescription = "Amount Debited"
			input.ParentAvailableChip = parentData.AvailableChip - input.AvailableChip

			insertStr := `INSERT INTO account_statements (user_id, parent_id, description, statement_type, amount, available_balance, transaction_for, operator_name, user_name, created_at) VALUES (?,?,?,?,?,?,"1",?,?,NOW())`
			stmtInsrt, err := tx.Prepare(insertStr)
			if err != nil {
				tx.Rollback()
				log.Println("CreateUser >> Insertion failed for Create user credit amount >> account statement entry", err.Error())
				message = "Cannot add user, try again"
				status = false
				return message, status
			}
			defer stmtInsrt.Close()

			input.Id, _ = res.LastInsertId()
			_, err = stmtInsrt.Exec(input.Id, input.ParentId, userCrDeDescription, 1, input.AvailableChip, input.AvailableChip, parentData.UserName, input.UserName)
			if err != nil {
				tx.Rollback()
				log.Println("CreateUser >> Insertion failed for Create user credit amount >> account statement entry", err.Error())
				message = "Cannot add user, try again"
				status = false
				return message, status
			}

			// Updation for Parent
			sqlUpdate := "UPDATE users SET available_chip = available_chip - ?, total_chip = total_chip - ? WHERE id = ?"

			stmtUpdate, err := tx.Prepare(sqlUpdate)
			if err != nil {
				tx.Rollback()
				log.Println("CreateUser >> Insertion failed for Create user", err.Error())
				message = "Cannot add user, try again"
				status = false
				return message, status
			}

			defer stmtUpdate.Close()
			_, err = stmtUpdate.Exec(input.AvailableChip, input.AvailableChip, input.ParentId)
			if err != nil {
				tx.Rollback()
				log.Println("CreateUser >> Insertion failed for Create user", err.Error())
				message = "Cannot add user, try again"
				status = false
				return message, status
			}

			// Account statment entry for Parent
			insertStr2 := `INSERT INTO account_statements (user_id, parent_id, description, statement_type, amount, available_balance, transaction_for, operator_name, user_name, created_at) VALUES (?,?,?,?,?,?,"2",?,?,NOW())`
			stmtInsrt2, err := tx.Prepare(insertStr2)
			if err != nil {
				tx.Rollback()
				log.Println("CreateUser >> Insertion failed for Create user credit amount >> account statement entry", err.Error())
				message = "Cannot add user, try again"
				status = false
				return message, status
			}

			defer stmtInsrt2.Close()
			_, err = stmtInsrt2.Exec(input.ParentId, parentData.ParentId, parentCrDeDescription, 1, -(input.AvailableChip), input.ParentAvailableChip, parentData.UserName, input.UserName)
			if err != nil {
				tx.Rollback()
				log.Println("CreateUser >> Insertion failed for Create user credit amount >> account statement entry", err.Error())
				message = "Cannot add user, try again"
				status = false
				return message, status
			}

			tx.Commit()
		}

		message = "User added successfully"
		status = true
		return message, status
	}
}

// GetUserDetailsByUserId :
func GetUserDetailsByUserId(id int64) (finalResult models.User, err bool) {
	var result models.UserInput
	errQuery := config.MySQL.QueryRow(`SELECT u.id, u.parent_id, parent_ids, user_name, name, u.user_type_id, mobile, partnership, exposure, profit_loss, available_chip, total_chip, remark, parent_name, parent_user_type_id, u.created_at, GREATEST(self_lock, parent_lock) user_lock, GREATEST(bet_lock, parent_bet_lock) bet_lock, market_delay, session_delay, bookmaker_delay
	FROM users AS u
	WHERE u.id = ?`, id).Scan(&result.Id, &result.ParentId, &result.ParentIds, &result.UserName, &result.Name, &result.UserTypeId, &result.Mobile, &result.Partnership, &result.Exposure, &result.ProfitLoss, &result.AvailableChip, &result.TotalChip, &result.Remark, &result.ParentName, &result.ParentUserTypeId, &result.CreatedAt, &result.UserLock, &result.BetLock, &result.MarketDelay, &result.SessionDelay, &result.BookmakerDelay)

	if errQuery == nil {
		if result != (models.UserInput{}) {
			finalResult.Id = result.Id.Int64
			finalResult.ParentId = result.ParentId.Int64
			finalResult.ParentIds = result.ParentIds.String
			finalResult.UserName = result.UserName.String
			finalResult.Name = result.Name.String
			finalResult.Mobile = result.Mobile.String
			finalResult.UserTypeId = result.UserTypeId.Int64
			finalResult.Exposure = result.Exposure.Float64
			finalResult.Partnership = result.Partnership.Int64
			finalResult.AvailableChip = result.AvailableChip.Float64
			finalResult.TotalChip = result.TotalChip.Float64
			finalResult.PasswordChanged = result.PasswordChanged.String
			finalResult.CreatedAt = result.CreatedAt.String
			finalResult.ParentName = result.ParentName.String
			finalResult.ParentUserTypeId = result.ParentUserTypeId.Int64
			finalResult.Remark = result.Remark.String
			finalResult.BetLock = result.BetLock.String
			finalResult.UserLock = result.UserLock.String
			finalResult.MarketDelay = result.MarketDelay.Int64
			finalResult.BookmakerDelay = result.BookmakerDelay.Int64
			finalResult.SessionDelay = result.SessionDelay.Int64
		}
		// subStr := ""
		// if finalResult.ParentIds != "" {
		// 	subStr = " AND (FIND_IN_SET('" + strconv.Itoa(int(id)) + "', parent_ids))"
		// }
		// if finalResult.UserTypeId != 7 {
		// 	errr := config.MySQL.QueryRow("SELECT SUM(exposure) AS exposure from users Where user_type_id = 7 " + subStr).Scan(&result.Exposure)
		// 	if errr != nil && errr != sql.ErrNoRows {
		// 		log.Println("GetUserDetailsByUserId", errr)
		// 	} else {
		// 		finalResult.Exposure = result.Exposure.Float64
		// 	}
		// }
		err = false
	} else if errQuery != sql.ErrNoRows {
		log.Println("GetUserDetailsByUserId", errQuery)
		err = true
	}
	return finalResult, err
}

// GetUserDetailsByUserId :
func GetChildDetailById(id int64) (finalResult []models.User, err bool) {
	rows, errQuery := config.MySQL.Query(`SELECT u.id, u.parent_id, parent_ids, user_name, name, u.user_type_id, mobile, partnership, exposure, profit_loss, available_chip, total_chip, remark, parent_name, parent_user_type_id, u.created_at, GREATEST(self_lock, parent_lock) user_lock, GREATEST(bet_lock, parent_bet_lock) bet_lock, is_active, is_change_password_lock
	FROM users AS u
	WHERE u.parent_id = ?`, id)
	if errQuery != nil {
		return finalResult, true
	}
	defer rows.Close()
	for rows.Next() {
		var result models.UserInput
		rows.Scan(&result.Id, &result.ParentId, &result.ParentIds, &result.UserName, &result.Name, &result.UserTypeId, &result.Mobile, &result.Partnership, &result.Exposure, &result.ProfitLoss, &result.AvailableChip, &result.TotalChip, &result.Remark, &result.ParentName, &result.ParentUserTypeId, &result.CreatedAt, &result.UserLock, &result.BetLock, &result.IsActive, &result.ChangePasswordLock)
		var singleResult models.User
		singleResult.Id = result.Id.Int64
		singleResult.ParentId = result.ParentId.Int64
		singleResult.ParentIds = result.ParentIds.String
		singleResult.UserName = result.UserName.String
		singleResult.Name = result.Name.String
		singleResult.Mobile = result.Mobile.String
		singleResult.UserTypeId = result.UserTypeId.Int64
		singleResult.Exposure = result.Exposure.Float64
		singleResult.Partnership = result.Partnership.Int64
		singleResult.AvailableChip = result.AvailableChip.Float64
		singleResult.TotalChip = result.TotalChip.Float64
		singleResult.PasswordChanged = result.PasswordChanged.String
		singleResult.CreatedAt = result.CreatedAt.String
		singleResult.ParentName = result.ParentName.String
		singleResult.ParentUserTypeId = result.ParentUserTypeId.Int64
		singleResult.Remark = result.Remark.String
		singleResult.BetLock = result.BetLock.String
		singleResult.UserLock = result.UserLock.String
		singleResult.IsActive = result.UserLock.String
		singleResult.ChangePasswordLock = result.UserLock.String
		finalResult = append(finalResult, singleResult)
	}
	// subStr := ""
	// if finalResult.ParentIds != "" {
	// 	subStr = " AND (FIND_IN_SET('" + strconv.Itoa(int(id)) + "', parent_ids))"
	// }
	// if finalResult.UserTypeId != 7 {
	// 	errr := config.MySQL.QueryRow("SELECT SUM(exposure) AS exposure from users Where user_type_id = 7 " + subStr).Scan(&result.Exposure)
	// 	if errr != nil && errr != sql.ErrNoRows {
	// 		log.Println("GetUserDetailsByUserId", errr)
	// 	} else {
	// 		finalResult.Exposure = result.Exposure.Float64
	// 	}
	// }
	err = false
	return finalResult, err
}

// GetUserBalance :
func GetUserBalance(id int64) (result models.User, err bool) {
	var contant models.UserInput
	er := config.MySQL.QueryRow("SELECT id, parent_id, user_type_id, name, user_name, exposure, profit_loss, total_chip, available_chip FROM users where id = ? AND self_lock='0' AND parent_lock='0'", id).Scan(&contant.Id, &contant.ParentId, &contant.UserTypeId, &contant.Name, &contant.UserName, &contant.Exposure, &contant.ProfitLoss, &contant.TotalChip, &contant.AvailableChip)
	if er != nil && er != sql.ErrNoRows {
		log.Println("GetUserBalance", er.Error())
		return models.User{}, true
	}
	result.Id = contant.Id.Int64
	result.ParentId = contant.ParentId.Int64
	result.UserTypeId = contant.UserTypeId.Int64
	result.Name = contant.Name.String
	result.UserName = contant.UserName.String
	result.Exposure = contant.Exposure.Float64
	result.ProfitLoss = contant.ProfitLoss.Float64
	result.TotalChip = contant.TotalChip.Float64
	result.AvailableChip = contant.AvailableChip.Float64

	return result, err
}

func GetTransactionPassword(parentId int64, masterPassword string) (string, bool) {
	var result models.UserInput
	err := config.MySQL.QueryRow("SELECT transaction_password FROM users where id = ?", parentId).Scan(&result.MasterPassword)
	if err != nil && err != sql.ErrNoRows {
		return "Something Went Wrong", false
	}

	err = bcrypt.CompareHashAndPassword([]byte(result.MasterPassword.String), []byte(masterPassword))
	if err != nil {
		return "Transaction Password Is Incorrect", false
	}
	return "", true
}

// UpdatePassword :
func UpdatePassword(data models.User) (string, bool) {
	result := models.UserInput{}
	message := "Password Updated Successfully"
	status := true
	if data.NewPassword == data.ConfirmPassword {
		err := config.MySQL.QueryRow("SELECT password FROM users where id = ? LIMIT 1", data.Id).Scan(&result.Password)
		if err != nil && err != sql.ErrNoRows {
			// log.Println("UpdatePassword >> err update password get", err1.Error())
			return "Unable to update password", false
		}
		if result.Password.String != "" {
			err := bcrypt.CompareHashAndPassword([]byte(result.Password.String), []byte(data.OldPassword))
			if err == nil || data.UserTypeId != 1 {
				hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(data.NewPassword), 8)
				data.NewPassword = string(hashedPassword)
				//update passowrd for user
				updateQuery := "Update users SET password = ?, password_changed = '0' where id = ?"
				stmt, err := config.MySQL.Prepare(updateQuery)
				if err != nil {
					// log.Println("UpdatePassword >> Update failed for UpdatePassword", err.Error())
					return "Unable to update password", false
				}
				defer stmt.Close()
				_, err = stmt.Exec(hashedPassword, data.Id)
				if err != nil {
					// log.Println("UpdatePassword >> Update failed for UpdatePassword", err.Error())
					return "Unable to update password", false
				}

			} else {
				message = "Incorrect Old Password"
				status = false
			}
		} else {
			message = "Invalid User"
			status = false
		}
	} else {
		message = "Confirm And New Password Not Matched"
		status = false
	}
	return message, status
}

func CheckForChangePasswordLocked(userId int64) bool {
	var IsChangePasswordLock sql.NullString
	err := config.MySQL.QueryRow("SELECT is_change_password_lock FROM users where id = ?", userId).Scan(&IsChangePasswordLock)
	if err != nil {
		log.Println("CheckForChangePasswordLocked", err.Error())
	}

	if IsChangePasswordLock.String == "1" {
		return true
	}
	return false
}

// ChangePassword :
func ChangePassword(userData models.User) (string, string, bool) {
	result := models.UserInput{}
	tempTransactionPassword := ""
	message := "OK"
	status := true
	if userData.NewPassword == userData.ConfirmPassword {
		err1 := config.MySQL.QueryRow("SELECT password FROM users where id = ? LIMIT 1", userData.Id).Scan(&result.Password)
		if err1 != nil && err1 != sql.ErrNoRows {
			return "Unable to update password", tempTransactionPassword, false
		}
		if result.Password.String != "" {
			err := bcrypt.CompareHashAndPassword([]byte(result.Password.String), []byte(userData.MasterPassword))
			if err == nil {
				hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(userData.NewPassword), 8)
				userData.NewPassword = string(hashedPassword)
				var isTxPasswordStatic, txPassword sql.NullString
				config.MySQL.QueryRow("SELECT tx_password_static, tx_password FROM main_settings LIMIT 1").Scan(&isTxPasswordStatic, &txPassword)
				if isTxPasswordStatic.String == "1" {
					tempTransactionPassword = txPassword.String
				} else {
					tempTransactionPassword = global.RandNumString(6)
				}

				transactionPassword, _ := bcrypt.GenerateFromPassword([]byte(tempTransactionPassword), 8)
				hashedTransactionPassword := string(transactionPassword)
				//update passowrd for user
				updateQuery := "Update users SET password = ?, transaction_password = ?, password_changed = '0' where id = ?"
				stmt, err := config.MySQL.Prepare(updateQuery)
				if err != nil {
					return "Unable to update password", tempTransactionPassword, false
				}
				defer stmt.Close()
				_, err = stmt.Exec(hashedPassword, hashedTransactionPassword, userData.Id)
				if err != nil {
					return "Unable to update password", tempTransactionPassword, false
				}
				message = "Password Updated Successfully"
			} else {
				message = "Incorrect Old Password"
				status = false
			}
		} else {
			message = "Invalid User"
			status = false
		}
	} else {
		message = "Confirm And New Password Not Matched"
		status = false
	}
	return message, tempTransactionPassword, status
}

// UpdateUserAndBetStatus :
func UpdateUserAndBetStatus(userId, UserTypeId, loginUserTypeId, parentId int64, userStatus, betStatus, loginUserName string) (message string, status bool) {
	updateQuery := "Update users SET self_lock = ?, bet_lock = ? where id = ? AND (FIND_IN_SET(?, parent_ids) OR parent_id = ?)"
	stmt, err := config.MySQL.Prepare(updateQuery)
	if err != nil {
		log.Println("UpdateUserAndBetStatus", err.Error())
		return "Unable Update user", true
	}
	defer stmt.Close()
	_, err = stmt.Exec(userStatus, betStatus, userId, parentId, parentId)
	if err != nil {
		log.Println("UpdateUserAndBetStatus", err.Error())
		return "Unable Update user", true
	}
	if UserTypeId != int64(config.USER) {
		updateParentQuery := "Update users SET parent_lock = ?, parent_bet_lock = ? where FIND_IN_SET(?, parent_ids)"

		stmt, err := config.MySQL.Prepare(updateParentQuery)
		if err != nil {
			return "Unable Update user", true
		}
		defer stmt.Close()
		_, err = stmt.Exec(userStatus, betStatus, userId)
		if err != nil {
			log.Println("UpdateUserAndBetStatus", err.Error())
			return "Unable Update user", true
		}
		if userStatus == "0" || betStatus == "0" {
			rows, err := config.MySQL.Query(`SELECT id, self_lock, bet_lock from users WHERE (bet_lock = '1' OR self_lock = '1') AND FIND_IN_SET(?, parent_ids)`, strconv.Itoa(int(userId)))
			if err != nil {
				log.Println("UpdateUserAndBetStatus >> Error in Get user >> LockBetMatchWise", err.Error())
				return "Unable to lock User", true
			}
			defer rows.Close()
			for rows.Next() {
				var contentId sql.NullInt64
				var lockBetting, lockUser sql.NullString
				err = rows.Scan(&contentId,
					&lockUser,
					&lockBetting)
				if err != nil {
					log.Println("Error", err.Error())
				}
				stmt1, err := config.MySQL.Prepare(updateParentQuery)
				if err != nil {
					log.Println("UpdateUserAndBetStatus", err.Error())
					return "Unable Update user", true
				}
				defer stmt1.Close()
				_, err = stmt1.Exec(lockUser.String, lockBetting.String, strconv.Itoa(int(contentId.Int64)))
				if err != nil {
					log.Println("UpdateUserAndBetStatus", err.Error())
					return "Unable Update user", true
				}
			}
		}
	}

	// var userLockHistroy models.UserLockDetails
	// userLockHistroy.UserId = userId
	// userLockHistroy.UserName = loginUserName
	// userLockHistroy.UserTypeId = loginUserTypeId
	// if betStatus == "1" {
	// 	userLockHistroy.BetActive = true
	// } else {
	// 	userLockHistroy.BetActive = false
	// }
	// if userStatus == "1" {
	// 	userLockHistroy.UserActive = true
	// } else {
	// 	userLockHistroy.UserActive = false
	// }
	// data.MongoClient.Collection("user_and_bet_lock_histroy").InsertOne(context.TODO(), userLockHistroy)
	return "User Updated Successfully", false
}

//GetAllUser :
func GetAllUser(data models.TokenData) []models.TokenData {
	allUserData := []models.TokenData{}
	loginUserData := config.RedisGetAllLoginUser("login")

	for _, userToken := range loginUserData {
		userSingleData := models.TokenData{}
		claim, err := DecodeToken(userToken)
		if err != nil {
			log.Println("GetAllUser", err.Error())
		}
		userTypeId := fmt.Sprintf("%v", claim["user_type_id"])
		userId := fmt.Sprintf("%v", claim["user_id"])
		userIdInt, _ := strconv.Atoi(userId)
		userSingleData.UserId = int64(userIdInt)
		userTypeIdInt, _ := strconv.Atoi(userTypeId)
		userSingleData.UserTypeId = int64(userTypeIdInt)
		parentId := fmt.Sprintf("%v", claim["parent_id"])
		parentIdInt, _ := strconv.Atoi(parentId)
		userSingleData.ParentId = int64(parentIdInt)
		userSingleData.Name = fmt.Sprintf("%v", claim["name"])
		userSingleData.UserName = fmt.Sprintf("%v", claim["user_name"])
		userSingleData.LoginDateTime = fmt.Sprintf("%v", claim["login_date_time"])
		userSingleData.Token = userToken
		userSingleData.IpAddress = fmt.Sprintf("%v", claim["ip_address"])
		userSingleData.UserAgent = fmt.Sprintf("%v", claim["browser_info"])
		userSingleData.ParentIds = fmt.Sprintf("%v", claim["parent_ids"])

		if strings.Contains(userSingleData.ParentIds, strconv.Itoa(int(data.UserId))) {
			allUserData = append(allUserData, userSingleData)
		}

	}
	return allUserData
}

//DecodeToken :
func DecodeToken(tokenString string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return hmacSecret, nil
	})
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		return claims, nil
	} else {
		return nil, err
	}
}

//UpdateUserDetails :
func UpdateUserDetails(content, parentData models.User, user models.User, loginUserName string) (message string, status bool) {

	userUpdateStr := "UPDATE users SET name = ?, mobile = ?, is_change_password_lock = ? WHERE id = ?"
	stmt, err := config.MySQL.Prepare(userUpdateStr)
	if err != nil {
		return "Unable to update user", true
	}
	defer stmt.Close()
	_, err = stmt.Exec(user.Name, user.Mobile, user.ChangePasswordLock, user.Id)
	if err != nil {
		log.Println("UpdateUserDetails", err.Error())
		return "Unable to update user", true
	}
	return "User updated successfully", false
}

//UserLoginLog:
func UserLoginLog(userLog models.LoginDataLog) error {
	sql := "INSERT INTO user_logs (user_id,user_name,parent_ids,login_time,ip_address,browser_info,timestamp,location) VALUES (?,?,?,?,?,?,?,?)"

	stmt, err := config.MySQL.Prepare(sql)
	if err != nil {
		log.Println("UserLoginLog >> Insertion failed", err.Error())
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(userLog.UserId, userLog.UserName, userLog.ParentIds, userLog.LoginTime, userLog.IpAddress, userLog.BrowserInfo, userLog.TimeStamp, userLog.Location)
	if err != nil {
		log.Println("UserLoginLog >> Insertion failed", err.Error())
	}
	return nil
}

//UserChangePasswordLog:
func UserChangePasswordLog(userLog models.LoginDataLog) error {
	sql := "INSERT INTO user_logs (user_id,user_name,parent_ids,password_change_time,ip_address,browser_info,timestamp,location) VALUES (?,?,?,?,?,?,?,?)"

	stmt, err := config.MySQL.Prepare(sql)
	if err != nil {
		log.Println("UserChangePasswordLog >> Insertion failed", err.Error())
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(userLog.UserId, userLog.UserName, userLog.ParentIds, userLog.PasswordChangeTime, userLog.IpAddress, userLog.BrowserInfo, userLog.TimeStamp, userLog.Location)
	if err != nil {
		log.Println("UserChangePasswordLog >> Insertion failed", err.Error())
	}
	return nil
}

//GetUserLogs :
func GetUserLogs(page, userID int64, logType string) (resultCount models.ResultCount, limit int64, result []models.LoginDataLog) {

	count := 0
	limit = int64(config.PageLimit)
	if page == 0 {
		page = 1
	}
	offset := (page - 1) * limit
	limitQry := " LIMIT " + strconv.Itoa(int(offset)) + ", " + strconv.Itoa(int(limit))

	finalQry := `SELECT user_id,user_name,login_time,password_change_time,ip_address,browser_info FROM user_logs
		WHERE 1=1 AND (FIND_IN_SET(` + strconv.Itoa(int(userID)) + `,parent_ids) OR user_id = ?) ORDER BY timestamp DESC ` + limitQry

	// fmt.Println(finalQry)
	rows, err := config.MySQL.Query(finalQry, userID)
	if err != nil {
		log.Println("BetHistory >> ", err.Error())
		return resultCount, 0, result
	}
	for rows.Next() {
		var content models.LoginDataLogInput

		err := rows.Scan(
			&content.UserId,
			&content.UserName,
			&content.LoginTime,
			&content.PasswordChangeTime,
			&content.IpAddress,
			&content.BrowserInfo,
		)
		if err != nil {
			log.Println("BetHistory >> ", err.Error())
		}

		var logData models.LoginDataLog

		if logType == "L" && content.LoginTime.String != "" {
			logData.UserId = content.UserId.Int64
			logData.UserName = content.UserName.String
			logData.IpAddress = content.IpAddress.String
			logData.BrowserInfo = content.BrowserInfo.String
			logData.LoginTime = content.LoginTime.String
		} else if logType == "P" && content.PasswordChangeTime.String != "" {
			logData.UserId = content.UserId.Int64
			logData.UserName = content.UserName.String
			logData.IpAddress = content.IpAddress.String
			logData.BrowserInfo = content.BrowserInfo.String
			logData.PasswordChangeTime = content.PasswordChangeTime.String
		}
		if logData != (models.LoginDataLog{}) {
			count++
			result = append(result, logData)
		}
	}

	// var countQry string
	// if page == 1 {
	// 	countQry = `SELECT COUNT(id) AS total_count FROM user_logs
	// 	WHERE 1=1 AND (FIND_IN_SET(` + strconv.Itoa(int(userID)) + `,parent_ids) OR user_id = ?) ORDER BY timestamp DESC ` + limitQry
	// 	//fmt.Println(countQry)
	// 	err := config.MySQL.QueryRow(countQry, userID).Scan(&resultCount.TotalCount)
	// 	if err != nil {
	// 		log.Println("BetHistory >> count ", err.Error())
	// 	}
	// }
	resultCount.TotalCount = count
	return resultCount, limit, result
}

// DashboardData :
func DashboardData(id int64) (result models.User, err bool) {
	var contant models.UserInput
	er := config.MySQL.QueryRow("SELECT id, parent_id, user_type_id, name, user_name, exposure, profit_loss, total_chip, available_chip, credited_chip FROM users where id = ? AND self_lock='0' AND parent_lock='0'", id).Scan(&contant.Id, &contant.ParentId, &contant.UserTypeId, &contant.Name, &contant.UserName, &contant.Exposure, &contant.ProfitLoss, &contant.TotalChip, &contant.AvailableChip, &contant.CreditedChip)
	if er != nil && er != sql.ErrNoRows {
		log.Println("DashboardData", er.Error())
		return models.User{}, true
	}
	result.Id = contant.Id.Int64
	result.ParentId = contant.ParentId.Int64
	result.UserTypeId = contant.UserTypeId.Int64
	result.Name = contant.Name.String
	result.UserName = contant.UserName.String
	result.Exposure = contant.Exposure.Float64
	result.ProfitLoss = contant.ProfitLoss.Float64
	result.TotalChip = contant.TotalChip.Float64
	result.AvailableChip = contant.AvailableChip.Float64
	result.CreditedChip = contant.CreditedChip.Float64

	return result, err
}
