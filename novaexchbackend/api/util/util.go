package util

import (
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

type ResponseHandler struct {
	Status  bool        `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func WebResponse(data interface{}, message string, status bool) interface{} {
	jsonData := &ResponseHandler{
		Status:  status,
		Message: message,
		Data:    data,
	}
	return jsonData
}

// ErrorResponse is a wrapper function for returning a web response that includes a standard text message.
func ErrorResponse(c *gin.Context, code int, data interface{}, message string) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:5001")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Accept, Origin, Cache-Control, X-Requested-With, content-type")
	c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, HEAD, PATCH, OPTIONS, GET, PUT")
	c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Range,X-Total-Count")

	c.JSON(code, WebResponse(data, message, false))
}

// SuccessResponse is a wrapper function for returning a web response that includes a standard text message.
func SuccessResponse(c *gin.Context, code int, data interface{}, message string) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:5001")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Accept, Origin, Cache-Control, X-Requested-With, content-type")
	c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, HEAD, PATCH, OPTIONS, GET, PUT")
	c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Range,X-Total-Count")

	c.JSON(code, WebResponse(data, message, true))
}

// SuccessResponseWithCache is a wrapper function for returning a web response that includes a standard text message.
func SuccessResponseWithCache(c *gin.Context, code int, data interface{}, message string) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:5001")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Accept, Origin, Cache-Control, X-Requested-With, content-type")
	c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, HEAD, PATCH, OPTIONS, GET, PUT")
	c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Range,X-Total-Count")

	c.JSON(code, WebResponse(data, message, true))
}

// RequestAPIData:  Calls a (API) URL and return the data from the request.
func RequestAPIData(method, url, postdata string, headers map[string]string) ([]byte, int, error) {

	req, err := http.NewRequest(method, url, strings.NewReader(postdata))
	if err != nil {
		//log.Print("RequestAPIData >> statusCode >>", 500, "\n SportID >> "+sportID+" >> URL >>", url, "\n Payload >>", postdata)
		return nil, 500, err
	}

	for key, value := range headers {
		req.Header.Add(key, value)
	}

	// dump, err := httputil.DumpRequestOut(req, true)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// if reqHeadersBytes, err := json.Marshal(req.Header); err != nil {
	// 	log.Print("Could not Marshal Req Headers")
	// } else {
	// 	log.Print("****REQUEST dump ****\n\n", string(dump), "****REQUEST headers ****\n\n", string(reqHeadersBytes))
	// }

	client := &http.Client{Timeout: 2 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		// log.Print("RequestAPIData >> statusCode >>", 408, " error>>", err.Error(), "\n SportID >> "+sportID+" >> URL >>", url, "\n Payload >>", postdata)
		return nil, 408, err
	}

	// read body
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		// log.Print("RequestAPIData >> statusCode >>", resp.StatusCode, " error>>", err.Error(), "\n SportID >> "+sportID+" >> URL >>", url, "\n Payload >>", postdata)
		return nil, resp.StatusCode, err
	}
	return body, resp.StatusCode, nil
}

// RequestAPIDataClientURL:  Calls a (API) URL and return the data from the request.
func RequestAPIDataClientURL(url string) ([]byte, int, error) {
	client := &http.Client{Timeout: 2 * time.Second}
	resp, err := client.Get(url)
	if err != nil {
		// log.Print("RequestAPIDataClientURL >> statusCode >>", 408, " error>>", err.Error(), "\n SportID >> "+sportID+" >> URL >>", url)
		return nil, 408, err
	}
	// read body
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		// log.Print("RequestAPIDataClientURL >> statusCode >>", resp.StatusCode, " error>>", err.Error(), "\n SportID >> "+sportID+" >> URL >>", url)
		return nil, resp.StatusCode, err
	}
	return body, resp.StatusCode, nil
}
