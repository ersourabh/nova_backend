module novaexchbackend

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.7
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/leemcloughlin/logfile v0.0.0-20201123203928-cff1c8a30a10
	github.com/robfig/cron v1.2.0
	go.mongodb.org/mongo-driver v1.8.2 // indirect
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce
	gopkg.in/mail.v2 v2.3.1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
