package handler

import (
	"novaexchbackend/api/controllers"

	"github.com/gin-gonic/gin"
)

// RoutesHandler adds routes for various APIs.
func RoutesHandler(e *gin.Engine) {

	// Admin data apis
	e.GET("/webapi/online/sports", controllers.GetSports())
	e.GET("/webapi/online/series", controllers.GetSportSeries())
	e.GET("/webapi/online/matches", controllers.GetSportSeriesMatches())
	e.GET("/webapi/online/markets", controllers.GetSportSeriesMatchMarkets())
	e.GET("/webapi/online/sessions", controllers.GetSportSeriesMatchSessions())
	e.GET("/webapi/online/bookmakers", controllers.GetSportSeriesMatchBookmakerMarkets())

	// e.GET("/webapi/online/odds", controllers.GetMarketsOdds())

	// User
	e.POST("/webapi/login", controllers.Login())
	e.POST("/webapi/logout", controllers.LogOut())
	e.POST("/webapi/signup", controllers.SignUp())
	e.POST("/webapi/adduser", controllers.AddUser())
	e.POST("/webapi/get-child-delails", controllers.GetChildDetailById())
	e.POST("/webapi/get-user-delails", controllers.GetUserDetailById())
	e.GET("/webapi/getUserBalance", controllers.GetUserBalance())
	e.POST("/webapi/updatePassword", controllers.UpdatePassword())
	e.POST("/webapi/changePassword", controllers.ChangePassword())
	e.POST("/webapi/updateUserAndBetStatus", controllers.UpdateUserAndBetStatus())
	e.POST("/webapi/updateUserDetails", controllers.UpdateUserDetails())
	e.POST("/webapi/getUserLogs", controllers.GetUserLogs())
	e.GET("/webapi/adminDashboard", controllers.AdminDashboard())

	// Sport
	e.POST("/webapi/sport/getSports", controllers.GetSportsDB())
	e.POST("/webapi/sport/updateSportStatus", controllers.UpdateSportStatus())

	// Series
	e.POST("/webapi/series/createSeries", controllers.CreateSeries())
	e.POST("/webapi/series/getSeries", controllers.GetSeries())
	e.POST("/webapi/series/updateSeriesStatus", controllers.UpdateSeriesStatus())

	// Match
	e.POST("/webapi/match/createMatch", controllers.CreateMatch())
	e.POST("/webapi/match/createMatchesAuto", controllers.CreateMatchesAuto())
	e.POST("/webapi/match/getMatches", controllers.GetMatches())
	e.POST("/webapi/match/updateMatchStatus", controllers.UpdateMatchStatus())
	e.GET("/webapi/match/getSportsListForMenu", controllers.GetSportsListForMenu())
	e.POST("/webapi/match/getMatchDetails", controllers.GetMatchDetails())
	e.POST("/webapi/match/getHomeMatches", controllers.GetHomeMatches())
	e.POST("/webapi/match/updateMatchDetails", controllers.UpdateMatchDetails())
	e.GET("/webapi/match/getUpcomingFixtures", controllers.GetUpcomingFixtures())
	e.GET("/webapi/match/getScoreMatches", controllers.GetScoreMatches())
	e.GET("/webapi/match/score", controllers.MatchScore())
	e.GET("/webapi/match/scoreboardMatchesKeyUpdate", controllers.ScoreboardMatchesKeyUpdate())

	// Market
	e.POST("/webapi/market/createMarket", controllers.CreateMarket())
	e.POST("/webapi/market/updateMarketStatus", controllers.UpdateMarketStatus())
	e.POST("/webapi/market/getMarkets", controllers.GetMarkets())
	e.POST("/webapi/market/getMarketSelections", controllers.GetMarketSelections())
	e.POST("/webapi/market/getFeeds", controllers.GetSessionAndMarketOdds())

	// Fancy
	e.POST("/webapi/fancy/createFancy", controllers.CreateFancy())
	e.POST("/webapi/fancy/updateFancyStatus", controllers.UpdateFancyStatus())
	e.POST("/webapi/fancy/getFancies", controllers.GetFancies())

	// Bets
	e.POST("/webapi/bet/placeMarketBet", controllers.PlaceMarketBet())
	e.POST("/webapi/bet/placeSessionBet", controllers.PlaceSessionBet())
	e.POST("/webapi/bet/deleteMarketBet", controllers.DeleteMarketBet())
	e.POST("/webapi/bet/betHistory", controllers.BetHistory())
	e.POST("/webapi/bet/currentBets", controllers.GetCurrentBet())

	e.POST("/webapi/betResult/declareMarketResult", controllers.DeclareMarket())
	e.POST("/webapi/betResult/declareSessionResult", controllers.DeclareSession())
	e.POST("/webapi/betResult/abandonedMarketSession", controllers.AbandonedMarketFancy())
	e.POST("/webapi/betResult/rollbackMarketSession", controllers.RollbackMarketFancy())

	// Account
	e.POST("/webapi/account/depositWithdrawl", controllers.DepositWithdrawl())
	e.POST("/webapi/account/depositWithdrawlHistory", controllers.DepositWithdrawlHistory())
	e.POST("/webapi/account/userAccountStatementHistory", controllers.UserAccountStatementHistory())

	// Main Settings
	e.GET("/webapi/mainSetting/getSetting", controllers.GetSetting())

}
