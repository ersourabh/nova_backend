package main

import (
	"fmt"
	"log"
	"novaexchbackend/api/config"
	"novaexchbackend/api/services"
	"novaexchbackend/handler"
	"os"
	"strings"

	"github.com/robfig/cron"

	"github.com/leemcloughlin/logfile"

	"github.com/gin-gonic/gin"
)

func main() {
	var port, argument string
	if len(os.Args) < 1 {
		log.Panic("Provide a valid argument [with dev/prod] currenlty selected >> none")
	} else {
		argument = os.Args[1]
		port = ":5001"
		if strings.HasPrefix(strings.ToLower(argument), "dev") {
			// Create LOG file for errors start ----------------------
			logFile, err := logfile.New(
				&logfile.LogFile{
					FileName: "error_log_api.txt",
					MaxSize:  5000 * 1024,
					Flags:    logfile.FileOnly | logfile.OverWriteOnStart})
			if err != nil {
				log.Println("Failed to create logFile : ", logFile.FileName, ", error : ", err)
			}
			defer logFile.Close()
			log.SetOutput(logFile)
			log.SetFlags(log.Ldate | log.Lmicroseconds | log.Llongfile)
			// Create LOG file for errors end ----------------------
		} else if strings.HasPrefix(strings.ToLower(argument), "prod") {
		} else {
			log.Panic("Provide a valid argument, currenlty selected >>", argument)
		}

		fmt.Println("environment selected :", argument)
		fmt.Println("listening on port", port)
	}

	// load config file from given path for credentials
	credFilePath, err := os.UserHomeDir()
	if err != nil {
		log.Panic("Credential file not avaiable : file not available - " + err.Error())
	} else {
		config.ReadCredentials(credFilePath + "/config.yaml")
	}
	// config.ReadCredentials("C:/novaexchbackend/config.yaml")

	// Database connections >> Connect the MYSQL database
	_, err1 := config.InitMySQLDB()
	if err1 != nil {
		log.Panic(err1)
	}

	// Connect to Redis DB
	config.InitRedisDB()

	// Cron Job to get market odds and session odds and save to redis from Third Party api
	job := cron.New()
	err2 := job.AddFunc("@every 0h1m0s", func() { services.FetchOnlineMarketsSessionsBookmakers() })
	if err2 == nil {
		fmt.Println("Cron Started.... FetchOnlineMarketsSessionsBookmakers")
	} else {
		fmt.Println("Error corn start >> FetchOnlineMarketsSessionsBookmakers", err2.Error())
	}
	job.Start()

	// job := cron.New()
	// err2 := job.AddFunc("@every 0h0m20s", func() { services.GetOnlineMarketOdds() })
	// if err2 == nil {
	// 	fmt.Println("Cron Started.... GetOnlineMarketOdds")
	// } else {
	// 	fmt.Println("Error corn start >> GetOnlineMarketOdds", err2.Error())
	// }
	// job.Start()

	// job2 := cron.New()
	// err3 := job2.AddFunc("@every 0h0m20s", func() { services.GetOnlineSessionOdds() })
	// if err3 == nil {
	// 	fmt.Println("Cron Started.... GetOnlineSessionOdds")
	// } else {
	// 	fmt.Println("Error corn start >> GetOnlineSessionOdds", err3.Error())
	// }
	// job2.Start()

	// Start Gin engine here
	engine := gin.New()
	engine.Use(CORSMiddleware())
	handler.RoutesHandler(engine)

	engine.Run(port)
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		if c.Request.Method == "OPTIONS" {
			c.Header("Access-Control-Allow-Origin", "http://localhost:5001")
			c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Accept, Origin, Cache-Control, X-Requested-With")
			c.Header("Access-Control-Allow-Methods", "POST, HEAD, PATCH, OPTIONS, GET, PUT")
			c.Header("Access-Control-Expose-Headers", "Content-Range,X-Total-Count")
			c.AbortWithStatus(204)
			return
		}

		c.Header("Access-Control-Allow-Origin", "http://localhost:5001")
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Accept, Origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Allow-Methods", "POST, HEAD, PATCH, OPTIONS, GET, PUT")
		c.Header("Access-Control-Expose-Headers", "Content-Range,X-Total-Count")

		c.Next()
	}
}
